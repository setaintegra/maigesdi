package it.integrasistemi.maigesdi.bean;

public class ParameterMappedStringValue {
	
	private String value;
	private boolean mandatory;
	
	public ParameterMappedStringValue() {
		
	}

	public ParameterMappedStringValue(String value, boolean mandatory) {
		
		this.value = value;
		this.mandatory = mandatory;
		
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Boolean getMandatory() {
		return mandatory;
	}
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	
	
	
}
