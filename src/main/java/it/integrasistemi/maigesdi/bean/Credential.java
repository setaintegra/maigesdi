package it.integrasistemi.maigesdi.bean;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Credential {
	
	@NotNull(message="Credential type is required")
	@Valid
	private CredentialType credentialType;
	@NotNull(message="Credential's value is required")
	@NotBlank(message="Credential's value is required")
	@Size(min =1, max = 500 , message="Credential's value is required, can't be bigger than 500 characters")
	private String value; //500

	
	public Credential() {
		
	}
	
	public Credential(CredentialType credentialType, String value) {
		this.credentialType = credentialType;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "Credential: { CredentialType : "+credentialType+
				", value: "+ value+
				"}"; 
	}

	public CredentialType getCredentialType() {
		return credentialType;
	}

	public void setCredentialType(CredentialType credentialType) {
		this.credentialType = credentialType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean result = false;

		if (o != null) {

			if (o == this) {
				result = true;
			} else {
				if ((o instanceof Credential)) {

					Credential credential = (Credential) o;

					result = credential.getCredentialType().getID().equals(this.credentialType.getID());

				} else {
					result = false;
				}
			}

		} else {
			result = false;
		}

		return result;
	}
	
	@Override
    public int hashCode() {
        return this.credentialType.getID().hashCode();
    }
	
	
}
