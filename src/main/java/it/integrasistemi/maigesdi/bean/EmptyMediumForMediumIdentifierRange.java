package it.integrasistemi.maigesdi.bean;

public class EmptyMediumForMediumIdentifierRange {
	
	//private Integer mediumID;
	private Integer mediumTypeID;
	private String mediumIdentifier;
	private String typographicNumber;
	private String lisTicketCode;
	
	public EmptyMediumForMediumIdentifierRange() {
		
	}
	
//	public Integer getMediumID() {
//		return mediumID;
//	}
//
//	public void setMediumID(Integer mediumID) {
//		this.mediumID = mediumID;
//	}

	public Integer getMediumTypeID() {
		return mediumTypeID;
	}
	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}
	public String getMediumIdentifier() {
		return mediumIdentifier;
	}
	public void setMediumIdentifier(String mediumIdentifier) {
		this.mediumIdentifier = mediumIdentifier;
	}
	public String getTypographicNumber() {
		return typographicNumber;
	}
	public void setTypographicNumber(String typographicNumber) {
		this.typographicNumber = typographicNumber;
	}
	public String getLisTicketCode() {
		return lisTicketCode;
	}
	public void setLisTicketCode(String lisTicketCode) {
		this.lisTicketCode = lisTicketCode;
	}

	
	
}
