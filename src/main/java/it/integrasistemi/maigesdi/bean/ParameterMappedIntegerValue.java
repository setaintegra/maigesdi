package it.integrasistemi.maigesdi.bean;

public class ParameterMappedIntegerValue {
	private Integer value;
	private boolean mandatory;
	
	public ParameterMappedIntegerValue() {
		
	}

	public ParameterMappedIntegerValue(Integer value, boolean mandatory) {
		
		this.value = value;
		this.mandatory = mandatory;
		
	}

	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public Boolean getMandatory() {
		return mandatory;
	}
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	

}
