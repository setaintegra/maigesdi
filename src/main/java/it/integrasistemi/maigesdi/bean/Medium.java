package it.integrasistemi.maigesdi.bean;

import java.util.Date;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModelProperty;
import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateTimeDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateTimeSerializer;

public class Medium {

	private Integer ID; //8
	//@NotNull
	//@NotBlank
	@Size(min =1, max = 12, message="Medium identifier can't be bigger than 12 characters")
	private String MediumIdentifier; //12
	@NotNull
	@Valid
	@ApiModelProperty(required = true)
	private Identity mediumType;
	@Size(max = 6, message="Medium's PIN can't be bigger than 6 characters")
	private String PIN; //6
	@Valid
	private Place birthPlace;
	@Size(max = 30, message="Medium's givenName can't be bigger than 30 characters")
	private String givenName; //30
	@Size(max = 30, message="Medium's familyName can't be bigger than 30 characters")
	private String familyName; // 30
	@ApiModelProperty(value="\"yyyy-MM-dd\"")
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date birthDate;
	@Size(max = 16, message="Medium's fiscalCode can't be bigger than 16 characters")
	private String fiscalCode; // 16
	@Size(max = 50, message="Medium's Address1 can't be bigger than 50 characters")
	private String Address1; //50
	@Size(max = 50, message="Medium's Address2 can't be bigger than 50 characters")
	private String Address2; //50 
	@Size(max = 30, message="Medium's Address3 can't be bigger than 30 characters")
	private String Address3; //30
	@Valid
	private Document document;
	@Size(max = 15, message="Medium's phoneNumber can't be bigger than 15 characters")
	private String phoneNumber; //15
	@Size(max = 15, message="Medium's mobileNumber can't be bigger than 15 characters")
	private String mobileNumber; //15
	@Size(max = 40, message="Medium's eMail can't be bigger than 40 characters")
	private String eMail; // 40
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date releaseDate;
	@ApiModelProperty(value="\"yyyy-MM-dd\"")
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date validFrom;
	@ApiModelProperty(value="\"yyyy-MM-dd\"")
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date validTo;
	@Valid
	private Tutor tutor;
	private Integer authorizationID; 
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date authorizationDateTime;	
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date restrictionDateTime;
	//@Size(max = 15, message="Medium's mobileNumber can't be bigger than 15 characters")
	private String restrictionMotivation; //CLOB
	private Integer temporaryAuthorizationID;
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date cancellationDateTime;
	@Size(max = 255, message="Medium's CENException can't be bigger than 255 characters")
	private String CENException;
	@JsonProperty("CENExceptionDateTime")
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date CENExceptionDateTime;
	@Size(max = 255, message="Medium's note can't be bigger than 255 characters")
	private String note;
	@Size(max = 255, message="Medium's denomination can't be bigger than 255 characters")
	private String denomination;
	@Valid
	private Set<Credential> credentials;
	
//	@NotNull
//	@NotBlank
	private String photo;
	
	private String sign;

	private Identity operativeFlow;
	private Identity state;
	
	@Size(max = 1, message="Medium's gender can't be bigger than 1 character")
	private String gender;
	
	public Medium() {

	}
	
	/**
	 * Costruttore utile per la verifica medium al sistema esterno
	 * @param mediumIdentifier
	 * @param givenName
	 * @param familyName
	 * @param fiscalCodeBirthPlace
	 * @param birthDate
	 * @param gender
	 * @param validTo
	 */
	public Medium(String mediumIdentifier,
					String givenName, 
					String familyName, 
					String fiscalCodeBirthPlace, 
					Date birthDate, 
					String gender,
					Date validTo) {
		this.MediumIdentifier = mediumIdentifier;
		this.givenName = givenName;
		this.familyName = familyName;
		this.gender = gender;
		this.birthDate = birthDate;
		this.validTo = validTo;
		this.birthPlace = new Place();
		birthPlace.setFiscalCode(fiscalCodeBirthPlace);
		

	}
	
	@Override
	public String toString() {
		
		
		return "Medium: { ID: "+ ID +
						", MediumIdentifier: " + 			 MediumIdentifier		   +
				 	 	", mediumType: " +                   mediumType		           +
						", PIN: " +                          PIN			           +
						", birthPlace: " +                   birthPlace		           +
						", givenName: " +                    givenName		           +
						", familyName: " +                   familyName		           +
						", birthDate: " +                    birthDate		           +
						", fiscalCode: " +                   fiscalCode		           +
						", gender: " +                  	 gender             	   +
						", Address1: " +                     Address1		           +
						", Address2: " +                     Address2		           +
						", Address3: " +                     Address3		           +
						", document: " +                     document		           +
						", phoneNumber: " +                  phoneNumber	           +
						", mobileNumber: " +                 mobileNumber	           +
						", eMail: " +                        eMail			           +
						", releaseDate: " +                  releaseDate	           +
						", validFrom: " +                    validFrom		           +
						", validTo: " +                      validTo		           +
						", tutor: " +                        tutor			           +
						", authorizationID: " +              authorizationID           +
						", authorizationDateTime: " + 	     authorizationDateTime	   +
						", restrictionDateTime: " +          restrictionDateTime       +
						", restrictionMotivation: " +        restrictionMotivation     +
						", temporaryAuthorizationID: " +     temporaryAuthorizationID  +
						", cancellationDateTime: " +         cancellationDateTime      +
						", CENException: " +                 CENException              +
						", CENExceptionDateTime: " +         CENExceptionDateTime      +
						", note: " +                         note                      +
						", denomination: " +                 denomination              +
						", credentials: " +                  credentials               +
						", operativeFlow: " +                operativeFlow             +
						", state: " +                 		 state        			   +
						", photo: " +         				 photo      			   +
						", sign: " +         				 sign	      			   +
						
						"}"; 
		
	}


//	public byte[] getPhoto() {
//		return photo;
//	}
//
//	public void setPhoto(byte[] photo) {
//		this.photo = photo;
//	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Identity getOperativeFlow() {
		return operativeFlow;
	}



	public void setOperativeFlow(Identity operativeFlow) {
		this.operativeFlow = operativeFlow;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Identity getState() {
		return state;
	}


	public void setState(Identity state) {
		this.state = state;
	}


	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Identity getMediumType() {
		return mediumType;
	}

	public void setMediumType(Identity mediumType) {
		this.mediumType = mediumType;
	}

	public String getMediumIdentifier() {
		return MediumIdentifier;
	}

	public void setMediumIdentifier(String mediumIdentifier) {
		MediumIdentifier = mediumIdentifier;
	}

	public String getPIN() {
		return PIN;
	}

	public void setPIN(String pIN) {
		PIN = pIN;
	}

	public Place getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(Place birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public void setAddress3(String address3) {
		Address3 = address3;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Tutor getTutor() {
		return tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

	public Integer getAuthorizationID() {
		return authorizationID;
	}

	public void setAuthorizationID(Integer authorizationID) {
		this.authorizationID = authorizationID;
	}

	public Date getAuthorizationDateTime() {
		return authorizationDateTime;
	}

	public void setAuthorizationDateTime(Date authorizationDateTime) {
		this.authorizationDateTime = authorizationDateTime;
	}

	public Date getRestrictionDateTime() {
		return restrictionDateTime;
	}

	public void setRestrictionDateTime(Date restrictionDateTime) {
		this.restrictionDateTime = restrictionDateTime;
	}

	public String getRestrictionMotivation() {
		return restrictionMotivation;
	}

	public void setRestrictionMotivation(String restrictionMotivation) {
		this.restrictionMotivation = restrictionMotivation;
	}

	public Integer getTemporaryAuthorizationID() {
		return temporaryAuthorizationID;
	}

	public void setTemporaryAuthorizationID(Integer temporaryAuthorizationID) {
		this.temporaryAuthorizationID = temporaryAuthorizationID;
	}

	public Date getCancellationDateTime() {
		return cancellationDateTime;
	}

	public void setCancellationDateTime(Date cancellationDateTime) {
		this.cancellationDateTime = cancellationDateTime;
	}

	public String getCENException() {
		return CENException;
	}

	public void setCENException(String cENException) {
		CENException = cENException;
	}

	@JsonProperty("CENExceptionDateTime")
	public Date getCENExceptionDateTime() {
		return this.CENExceptionDateTime;
	}

	@JsonProperty("CENExceptionDateTime")
	public void setCENExceptionDateTime(Date cENExceptionDateTime) {
		this.CENExceptionDateTime = cENExceptionDateTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	

	public String getDenomination() {
		return denomination;
	}


	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}


	public Set<Credential> getCredentials() {
		return credentials;
	}

	public void setCredentials(Set<Credential> credentials) {
		this.credentials = credentials;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
