package it.integrasistemi.maigesdi.bean;

import java.util.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModelProperty;
import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;

public class Tutor {
	
	@Size(max = 20, message="Tutor's givenName can't be bigger than 20 characters")
	private String givenName;  //20
	@Size(max = 20, message="Tutor's familyName can't be bigger than 20 characters")
	private String familyName; //20
	@Size(max = 30, message="Tutor's birthPlace can't be bigger than 30 characters")
	private String birthPlace; //30
	@ApiModelProperty(value="\"yyyy-MM-dd\"")
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date birthDate;
	@Size(max = 16, message="Tutor's fiscalCode can't be bigger than 16 characters")
	private String fiscalCode; //16
	@Size(max = 50, message="Tutor's Address1 can't be bigger than 50 characters")
	private String Address1;  //50
	@Size(max = 50, message="Tutor's Address2 can't be bigger than 50 characters")
	private String Address2;  //50
	@Size(max = 30, message="Tutor's Address3 can't be bigger than 30 characters")
	private String Address3;  //30
	//private byte[] signature;
	private Document document; 
	
	private String sign;

	public Tutor() {

	}
	
	@Override
	public String toString() {
		
		return "Tutor: { givenName: "+ 			givenName		   +
						", familyName: " + 		familyName		   +
				 	 	", birthPlace: " +      birthPlace         +
						", birthDate: " +       birthDate          +
						", fiscalCode: " + 		fiscalCode		   +
				 	 	", Address1: " +        Address1           +
						", Address2: " +        Address2           +
						", Address3: " +        Address3           +
						", document: " +        document           +
						", sign: " +            sign           +
						"}"; 
		
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public void setAddress3(String address3) {
		Address3 = address3;
	}

//	public byte[] getSignature() {
//		return signature;
//	}
//
//	public void setSignature(byte[] signature) {
//		this.signature = signature;
//	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
