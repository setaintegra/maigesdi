package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CredentialType {

	@NotNull(message="Credential type ID is required")
	@Min(1)//  message="Credential type ID is required")
	private Integer ID;
	@NotNull(message="Credential type name is required")
	@NotBlank(message="Credential type name is required")
	@Size(min =1, max= 50, message="Credential type ID is required,can't be bigger than 50 characters")
	private String name;    //50
	@Size(max = 50, message="CredentialType's description can't be bigger than 50 characters")
	private String description; //50
	
	
	public CredentialType() {
		
	}
	
	public CredentialType(Integer ID, String name, String description) {
		this.ID = ID;
		this.name = name;
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "CredentialType: { ID: "+this.ID+
				", name: "+ this.name+
				", description: "+this.description+
			    "}"; 
	}


	public Integer getID() {
		return ID;
	}


	public void setID(Integer iD) {
		ID = iD;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
