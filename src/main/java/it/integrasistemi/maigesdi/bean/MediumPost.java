package it.integrasistemi.maigesdi.bean;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MediumPost {
	
	@NotNull
	@NotBlank
	@Size(max = 30, message="MediumPost's activityCode can't be bigger than 30 characters")
	private String activityCode;  //30
	@Valid
	@NotNull
	private Medium medium;
	
	public MediumPost() {
		
	}
	
	@Override
	public String toString() {
		
		
		return "MediumPost: { activityCode: "+ activityCode +
							", medium: " + medium +  
							"}"; 
		
	}
	

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

	public Medium getMedium() {
		return medium;
	}

	public void setMedium(Medium medium) {
		this.medium = medium;
	}

	
}
