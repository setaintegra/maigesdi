package it.integrasistemi.maigesdi.bean;

import java.util.List;

public class MediumReportContainer {
	
	private List<Medium> items;
	private Paginator pagination;
	
	public MediumReportContainer() {
		
	}
	
	public MediumReportContainer(List<Medium> items, Paginator pagination) {
		this.items =items;
		this.pagination = pagination;
	}
	
	public MediumReportContainer(Paginator pagination) {
		this.items = null;
		this.pagination = pagination;
	}
	
	
	public List<Medium> getItems() {
		return items;
	}
	public void setItems(List<Medium> items) {
		this.items = items;
	}
	public Paginator getPagination() {
		return pagination;
	}
	public void setPagination(Paginator pagination) {
		this.pagination = pagination;
	}

	
	
}
