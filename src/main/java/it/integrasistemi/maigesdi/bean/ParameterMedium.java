package it.integrasistemi.maigesdi.bean;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.PreconditionException;

public class ParameterMedium {

	@NotNull
	@Valid
	private Integer mediumTypeID;
	@Valid
	private Set<Credential> credentials;

	private ParameterMappedIntegerValue ID;
	private ParameterMappedStringValue mediumIdentifier;
	private ParameterMappedStringValue PIN;
	private ParameterMappedStringValue birthPlaceFiscalCode;
	private ParameterMappedStringValue givenName;
	private ParameterMappedStringValue familyName;
	private ParameterMappedDateValue birthDate;
	private ParameterMappedStringValue gender;
	private ParameterMappedStringValue fiscalCode;
	private ParameterMappedStringValue address1;
	private ParameterMappedStringValue address2;
	private ParameterMappedStringValue address3;
	private ParameterMappedStringValue phoneNumber;
	private ParameterMappedStringValue mobileNumber;
	private ParameterMappedStringValue eMail;
	private ParameterMappedDateValue releaseDate;
	private ParameterMappedDateValue validFrom;
	private ParameterMappedDateValue validTo;
	private ParameterMappedStringValue denomination;
	private ParameterMappedStringValue photo;
	private ParameterMappedStringValue sign;
	private ParameterMappedIntegerValue stateID;

	// document
	private ParameterMappedStringValue documentType;
	private ParameterMappedStringValue documentCode;
	private ParameterMappedDateValue documentReleaseDate;
	private ParameterMappedStringValue documentAuthority;
	private ParameterMappedDateValue documentExpirationDate;

	// tutor
	private ParameterMappedStringValue tutorGivenName;
	private ParameterMappedStringValue tutorFamilyName;
	private ParameterMappedStringValue tutorBirthPlace;
	private ParameterMappedDateValue   tutorBirthDate;
	private ParameterMappedStringValue tutorFiscalCode;
	private ParameterMappedStringValue tutorAddress1;
	private ParameterMappedStringValue tutorAddress2;
	private ParameterMappedStringValue tutorAddress3;
	private ParameterMappedStringValue tutorSign;

	// tutor document
	private ParameterMappedStringValue tutorDocumentType;
	private ParameterMappedStringValue tutorDocumentCode;
	private ParameterMappedDateValue tutorDocumentReleaseDate;
	private ParameterMappedStringValue tutorDocumentAuthority;
	private ParameterMappedDateValue tutorDocumentExpirationDate;
	
//	// operative flow - flusso operativo
//	private ParameterMappedIntegerValue operativeFlowID;
//	private ParameterMappedStringValue operativeFlowName;
//	
//	// operative flow - flusso operativo
//	private ParameterMappedIntegerValue stateID;
//	private ParameterMappedStringValue stateName;
		

	public ParameterMedium() {

	}

	//la classe sa trasfrmarsi in un medium
	public Medium toMedium() {
		Medium result = new Medium();
		Document doc = null;
		Tutor tutor = null;
		Place place = null;
		Identity identity = null;
		
		result.setMediumType(new Identity(this.mediumTypeID, null));
		result.setCredentials(this.credentials);

		result.setID(this.ID != null ? this.ID.getValue() : null);
		result.setMediumIdentifier(this.mediumIdentifier != null ? this.mediumIdentifier.getValue() : null);
		result.setPIN(this.PIN !=null ? this.PIN.getValue() : null);
		result.setGivenName(this.givenName !=null ? this.givenName.getValue() : null);
		result.setFamilyName(this.familyName !=null? this.familyName.getValue(): null);
		result.setBirthDate(this.birthDate != null ? this.birthDate.getValue() : null ) ;
		result.setGender(this.gender != null ? this.gender.getValue() : null);
		result.setFiscalCode(this.fiscalCode!=null? this.fiscalCode.getValue() : null);
		result.setAddress1(this.address1!=null ? this.address1.getValue(): null);
		result.setAddress2(this.address2!=null ? this.address2.getValue(): null);
		result.setAddress3(this.address3!=null ? this.address3.getValue(): null);
		result.setPhoneNumber(this.phoneNumber !=null ? this.phoneNumber.getValue() : null);
		result.setMobileNumber(this.mobileNumber != null ? this.mobileNumber.getValue() : null);
		result.seteMail(this.eMail != null ? this.eMail.getValue(): null);
		result.setReleaseDate(this.releaseDate != null ? this.releaseDate.getValue() : null);
		result.setValidFrom(this.validFrom != null ? this.validFrom.getValue(): null);
		result.setValidTo(this.validTo != null ? this.validTo.getValue(): null);
		result.setDenomination(this.denomination != null ? this.denomination.getValue() : null);
		result.setPhoto(this.photo != null ? this.photo.getValue() : null);
		result.setSign(this.sign != null ? this.sign.getValue() : null);
		
		//Place
		if(birthPlaceFiscalCode!= null) {
			place = new Place();
			place.setFiscalCode(this.birthPlaceFiscalCode.getValue());
			
			result.setBirthPlace(place);
		}
		
		
		//Document
		if(documentType != null ||  documentCode !=null ||	documentReleaseDate != null || documentAuthority !=null || documentExpirationDate != null)
		{
			doc = new Document();
			doc.setDocumentType(this.documentType !=null ? this.documentType.getValue() : null);
			doc.setCode(this.documentCode != null ? this.documentCode.getValue() : null);
			doc.setReleaseDate(this.documentReleaseDate != null ? this.documentReleaseDate.getValue() : null);
			doc.setAuthority(this.documentAuthority != null ? this.documentAuthority.getValue() : null);
			doc.setExpirationDate(this.documentExpirationDate != null ? this.documentExpirationDate.getValue() : null);
			
			result.setDocument(doc);
		}
		
		//Tutor
		if( tutorGivenName != null ||  tutorFamilyName != null ||  tutorBirthPlace != null ||  
				tutorBirthDate != null ||  tutorFiscalCode != null ||  tutorAddress1 != null ||  
				tutorAddress2 != null || 	tutorAddress3 != null || tutorSign != null) {
			tutor = new Tutor();
			
			tutor.setGivenName(	this.tutorGivenName !=null ? this.tutorGivenName.getValue() : null);
			tutor.setFamilyName(this.tutorFamilyName !=null? this.tutorFamilyName.getValue(): null);
			tutor.setBirthDate(	this.tutorBirthDate != null ? this.tutorBirthDate.getValue() : null ) ;
			tutor.setFiscalCode(this.tutorFiscalCode!=null? this.tutorFiscalCode.getValue() : null);
			tutor.setAddress1(this.tutorAddress1!=null ? this.tutorAddress1.getValue(): null);
			tutor.setAddress2(this.tutorAddress2!=null ? this.tutorAddress2.getValue(): null);
			tutor.setAddress3(this.tutorAddress3!=null ? this.tutorAddress3.getValue(): null);
			tutor.setBirthPlace(this.tutorBirthPlace != null ? this.tutorBirthPlace.getValue() : null);
			tutor.setSign(this.tutorSign != null ? this.tutorSign.getValue() : null);
			
			result.setTutor(tutor);
		}
		
		
		//Tutor document
		if(tutorDocumentType != null ||  tutorDocumentCode !=null ||	tutorDocumentReleaseDate != null || tutorDocumentAuthority !=null || tutorDocumentExpirationDate != null)
		{
			doc = new Document();
			doc.setDocumentType(this.tutorDocumentType !=null ? this.tutorDocumentType.getValue() : null);
			doc.setCode(this.tutorDocumentCode != null ? this.tutorDocumentCode.getValue() : null);
			doc.setReleaseDate(this.tutorDocumentReleaseDate != null ? this.tutorDocumentReleaseDate.getValue() : null);
			doc.setAuthority(this.tutorDocumentAuthority != null ? this.tutorDocumentAuthority.getValue() : null);
			doc.setExpirationDate(this.tutorDocumentExpirationDate != null ? this.tutorDocumentExpirationDate.getValue() : null);
			
			tutor = result.getTutor()!= null ? result.getTutor(): new Tutor();
			tutor.setDocument(doc);
			result.setTutor(tutor);
		}
		
//--------- GESTIONE STATO E FLUSSO da capire se serve----------------------		
//		//operative Flow - flusso operativo
//		if(operativeFlowID != null || operativeFlowName != null) {
//			identity = new Identity();
//			identity.setID(operativeFlowID != null ? operativeFlowID.getValue(): null);
//			identity.setName(operativeFlowName!= null ? operativeFlowName.getValue(): null);
//			
//			result.setOperativeFlow(identity);
//			
//		}
//		
		//state - stato
		if(stateID != null) {
			identity = new Identity();
			identity.setID(stateID.getValue());
//			identity.setName(stateName!= null ? stateName.getValue(): null);
			result.setState(identity);
		}	
//------------------------------------------------------------------------
		return result;
	}
	
	//la classe sa aggiornarsi e copiarsi in un altro parameterMedium con i dati di un'altro medium
	public ParameterMedium cloneAndUpdateParameterMedium(Medium medium) {
		ParameterMedium result = new ParameterMedium();
			
		//variabili del medium che vanno riportate sempre
		//result.setMediumTypeID(medium.getMediumType().getID());
		result.setMediumTypeID(this.getMediumTypeID());
		result.setCredentials(medium.getCredentials());
		
		// per ogni parametro obbligatorio verifico se è stato effettivamente
		// valorizzato nel medium passato
		result.setID(this.getID() != null ? new ParameterMappedIntegerValue (this.getID().getValue(), this.getID().getMandatory()) : null );
		result.setMediumIdentifier(this.mediumIdentifier != null ? new ParameterMappedStringValue(this.mediumIdentifier.getValue(),this.mediumIdentifier.getMandatory()): null);
		result.setPIN(this.PIN != null ? new ParameterMappedStringValue(medium.getPIN() ,this.PIN.getMandatory()): null);
		result.setGivenName(this.givenName != null ? new ParameterMappedStringValue(medium.getGivenName() ,this.givenName.getMandatory()): null);
		result.setFamilyName(this.familyName != null ? new ParameterMappedStringValue(medium.getFamilyName(),this.familyName.getMandatory()): null);
		result.setBirthPlaceFiscalCode(this.birthPlaceFiscalCode != null ? new ParameterMappedStringValue( (medium.getBirthPlace()!=null ? medium.getBirthPlace().getFiscalCode(): null) 
																											,this.birthPlaceFiscalCode.getMandatory()): null);
		result.setBirthDate(this.birthDate != null ? new ParameterMappedDateValue(medium.getBirthDate(),birthDate.getMandatory()): null);
		result.setGender(this.gender != null ? new ParameterMappedStringValue(medium.getGender() ,this.gender.getMandatory()): null);
		result.setFiscalCode(this.fiscalCode != null ? new ParameterMappedStringValue(medium.getFiscalCode() ,this.fiscalCode.getMandatory()): null);
		result.setAddress1(this.address1 != null ? new ParameterMappedStringValue(medium.getAddress1(),this.address1.getMandatory()): null);
		result.setAddress2(this.address2 != null ? new ParameterMappedStringValue(medium.getAddress2(),this.address2.getMandatory()): null);
		result.setAddress3(this.address3 != null ? new ParameterMappedStringValue(medium.getAddress3(),this.address3.getMandatory()): null);
		
		result.setDocumentType(this.documentType != null ? new ParameterMappedStringValue(medium.getDocument() != null ? medium.getDocument().getDocumentType() : null,
																						  	this.documentType.getMandatory()): null);
		result.setDocumentCode(this.documentCode != null ? new ParameterMappedStringValue(medium.getDocument() != null ? medium.getDocument().getCode() : null,
																						  	this.documentCode.getMandatory()): null);
		result.setDocumentReleaseDate(this.documentReleaseDate != null ? new ParameterMappedDateValue(medium.getDocument() != null ? medium.getDocument().getReleaseDate() : null,
			  																							this.documentReleaseDate.getMandatory()): null);
		result.setDocumentAuthority(this.documentAuthority != null ? new ParameterMappedStringValue(medium.getDocument() != null ? medium.getDocument().getAuthority() : null,
																									this.documentAuthority.getMandatory()): null);
		result.setDocumentExpirationDate(this.documentExpirationDate != null ? new ParameterMappedDateValue(medium.getDocument() != null ? medium.getDocument().getExpirationDate() : null,
																											this.documentExpirationDate.getMandatory()): null);
		result.setPhoneNumber(this.phoneNumber != null ? new ParameterMappedStringValue(medium.getPhoneNumber() ,this.phoneNumber.getMandatory()): null);
		result.setMobileNumber(this.mobileNumber != null ? new ParameterMappedStringValue(medium.getMobileNumber() ,this.mobileNumber.getMandatory()): null);
		result.seteMail(this.eMail != null ? new ParameterMappedStringValue(medium.geteMail() ,this.eMail.getMandatory()): null);
		
		result.setReleaseDate(this.releaseDate != null ? new ParameterMappedDateValue(medium.getReleaseDate() ,this.releaseDate.getMandatory()): null);
		result.setValidFrom(this.validFrom != null ? new ParameterMappedDateValue(medium.getValidFrom() ,this.validFrom.getMandatory()): null);
		result.setValidTo(this.validTo != null ? new ParameterMappedDateValue(medium.getValidTo() ,this.validTo.getMandatory()): null);
		result.setTutorGivenName(this.tutorGivenName != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getGivenName(): null
																								,this.tutorGivenName.getMandatory()): null);
		result.setTutorFamilyName(this.tutorFamilyName != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getFamilyName() : null
																								,this.tutorFamilyName.getMandatory()): null);
		result.setTutorBirthPlace(this.tutorBirthPlace != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getBirthPlace() : null ,
																								this.tutorBirthPlace.getMandatory()): null);
		result.setTutorBirthDate(this.tutorBirthDate != null ? new ParameterMappedDateValue(medium.getTutor() != null ? medium.getTutor().getBirthDate() : null ,
																							this.tutorBirthDate.getMandatory()): null);
		result.setTutorFiscalCode(this.tutorFiscalCode != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getFiscalCode() : null,
																								this.tutorFiscalCode.getMandatory()): null);
		result.setTutorAddress1(this.tutorAddress1 != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getAddress1() : null ,
																							this.tutorAddress1.getMandatory()): null);
		result.setTutorAddress2(this.tutorAddress2 != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getAddress2() : null ,
																							this.tutorAddress2.getMandatory()): null);
		result.setTutorAddress3(this.tutorAddress3 != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getAddress3() : null ,
																							this.tutorAddress3.getMandatory()): null);
		
		result.setTutorDocumentType(this.tutorDocumentType != null ? new ParameterMappedStringValue(medium.getTutor() != null && medium.getTutor().getDocument() != null ? 
																									medium.getTutor().getDocument().getDocumentType() : null ,
																									this.tutorDocumentType.getMandatory()): null);
		result.setTutorDocumentCode(this.tutorDocumentCode != null ? new ParameterMappedStringValue(medium.getTutor() != null && medium.getTutor().getDocument() != null ? 
																									medium.getTutor().getDocument().getCode() : null  ,
																									this.tutorDocumentCode.getMandatory()): null);
		result.setTutorDocumentReleaseDate(this.tutorDocumentReleaseDate != null ? new ParameterMappedDateValue(medium.getTutor() != null && medium.getTutor().getDocument() != null ? 
																												medium.getTutor().getDocument().getReleaseDate() : null ,
																												this.tutorDocumentReleaseDate.getMandatory()): null);
		result.setTutorDocumentAuthority(this.tutorDocumentAuthority != null ? new ParameterMappedStringValue(medium.getTutor() != null && medium.getTutor().getDocument() != null ? 
																												medium.getTutor().getDocument().getAuthority() : null,
																												this.tutorDocumentAuthority.getMandatory()): null);
		result.setTutorDocumentExpirationDate(this.tutorDocumentExpirationDate != null ? new ParameterMappedDateValue(medium.getTutor() != null && medium.getTutor().getDocument() != null ? 
																														medium.getTutor().getDocument().getExpirationDate() : null,
																														this.tutorDocumentExpirationDate.getMandatory()): null);
		result.setTutorSign(this.tutorSign != null ? new ParameterMappedStringValue(medium.getTutor() != null ? medium.getTutor().getSign() : null,
																					this.tutorSign.getMandatory()) : null);
		result.setDenomination(this.denomination != null ? new ParameterMappedStringValue(medium.getDenomination() ,this.denomination.getMandatory()): null);
		result.setPhoto(this.photo != null ? new ParameterMappedStringValue(medium.getPhoto() ,this.photo.getMandatory()): null);
		result.setSign(this.sign != null ? new ParameterMappedStringValue(medium.getSign() ,this.sign.getMandatory()): null);
		
		result.setStateID(this.getStateID() != null ? new ParameterMappedIntegerValue(medium.getState().getID(), this.getStateID().getMandatory()) : null);
				
		return result;
	}
	
	public ParameterMedium clone() {
		ParameterMedium result = new ParameterMedium();
			
		//variabili del medium che vanno riportate sempre
		//result.setMediumTypeID(medium.getMediumType().getID());
		result.setMediumTypeID(new Integer(this.getMediumTypeID()));
		result.setCredentials(new HashSet<Credential>(this.getCredentials()));
		
		// per ogni parametro obbligatorio verifico se è stato effettivamente
		// valorizzato nel medium passato
		result.setID(this.getID() != null ? new ParameterMappedIntegerValue (this.getID().getValue(), this.getID().getMandatory()) : null );
		result.setMediumIdentifier(this.mediumIdentifier != null ? new ParameterMappedStringValue(this.mediumIdentifier.getValue(),this.mediumIdentifier.getMandatory()): null);
		result.setPIN(this.PIN != null ? new ParameterMappedStringValue(this.PIN.getValue(),this.PIN.getMandatory()): null);
		result.setGivenName(this.givenName != null ? new ParameterMappedStringValue(this.givenName.getValue(),this.givenName.getMandatory()): null);

		result.setFamilyName(this.familyName != null ? new ParameterMappedStringValue(this.familyName.getValue(),this.familyName.getMandatory()): null);
		result.setBirthPlaceFiscalCode(this.birthPlaceFiscalCode != null ? new ParameterMappedStringValue(this.getBirthPlaceFiscalCode().getValue(),this.birthPlaceFiscalCode.getMandatory()): null);
		result.setBirthDate(this.birthDate != null ? new ParameterMappedDateValue(this.birthDate.getValue(),birthDate.getMandatory()): null);
		result.setGender(this.gender != null ? new ParameterMappedStringValue(this.gender.getValue() ,this.gender.getMandatory()): null);
		result.setFiscalCode(this.fiscalCode != null ? new ParameterMappedStringValue(this.fiscalCode.getValue() ,this.fiscalCode.getMandatory()): null);
		result.setAddress1(this.address1 != null ? new ParameterMappedStringValue(this.address1.getValue() ,this.address1.getMandatory()): null);
		result.setAddress2(this.address2 != null ? new ParameterMappedStringValue(this.address2.getValue() ,this.address2.getMandatory()): null);
		result.setAddress3(this.address3 != null ? new ParameterMappedStringValue(this.address3.getValue() ,this.address3.getMandatory()): null);
		
		result.setDocumentType(this.documentType != null ? new ParameterMappedStringValue(this.documentType.getValue() ,this.documentType.getMandatory()): null);
		result.setDocumentCode(this.documentCode != null ? new ParameterMappedStringValue(this.documentCode.getValue() ,this.documentCode.getMandatory()): null);
		result.setDocumentReleaseDate(this.documentReleaseDate != null ? new ParameterMappedDateValue(this.documentReleaseDate.getValue() ,this.documentReleaseDate.getMandatory()): null);
		result.setDocumentAuthority(this.documentAuthority != null ? new ParameterMappedStringValue(this.documentAuthority.getValue() ,this.documentAuthority.getMandatory()): null);
		result.setDocumentExpirationDate(this.documentExpirationDate != null ? new ParameterMappedDateValue(this.documentExpirationDate.getValue() ,this.documentExpirationDate.getMandatory()): null);
		result.setPhoneNumber(this.phoneNumber != null ? new ParameterMappedStringValue(this.phoneNumber.getValue() ,this.phoneNumber.getMandatory()): null);
		result.setMobileNumber(this.mobileNumber != null ? new ParameterMappedStringValue(this.mobileNumber.getValue() ,this.mobileNumber.getMandatory()): null);
		result.seteMail(this.eMail != null ? new ParameterMappedStringValue(this.eMail.getValue() ,this.eMail.getMandatory()): null);
		
		result.setReleaseDate(this.releaseDate != null ? new ParameterMappedDateValue(this.releaseDate.getValue() ,this.releaseDate.getMandatory()): null);
		result.setValidFrom(this.validFrom != null ? new ParameterMappedDateValue(this.validFrom.getValue() ,this.validFrom.getMandatory()): null);
		result.setValidTo(this.validTo != null ? new ParameterMappedDateValue(this.validTo.getValue() ,this.validTo.getMandatory()): null);
		result.setTutorGivenName(this.tutorGivenName != null ? new ParameterMappedStringValue(this.tutorGivenName.getValue() ,this.tutorGivenName.getMandatory()): null);
		result.setTutorFamilyName(this.tutorFamilyName != null ? new ParameterMappedStringValue(this.tutorFamilyName.getValue() ,this.tutorFamilyName.getMandatory()): null);
		result.setTutorBirthPlace(this.tutorBirthPlace != null ? new ParameterMappedStringValue(this.tutorBirthPlace.getValue() ,this.tutorBirthPlace.getMandatory()): null);
		result.setTutorBirthDate(this.tutorBirthDate != null ? new ParameterMappedDateValue(this.tutorBirthDate.getValue() ,this.tutorBirthDate.getMandatory()): null);
		result.setTutorFiscalCode(this.tutorFiscalCode != null ? new ParameterMappedStringValue(this.tutorFiscalCode.getValue() ,this.tutorFiscalCode.getMandatory()): null);
		result.setTutorAddress1(this.tutorAddress1 != null ? new ParameterMappedStringValue(this.tutorAddress1.getValue() ,this.tutorAddress1.getMandatory()): null);
		result.setTutorAddress2(this.tutorAddress2 != null ? new ParameterMappedStringValue(this.tutorAddress2.getValue() ,this.tutorAddress2.getMandatory()): null);
		result.setTutorAddress3(this.tutorAddress3 != null ? new ParameterMappedStringValue(this.tutorAddress3.getValue() ,this.tutorAddress3.getMandatory()): null);
		result.setTutorDocumentType(this.tutorDocumentType != null ? new ParameterMappedStringValue(this.tutorDocumentType.getValue() ,this.tutorDocumentType.getMandatory()): null);
		result.setTutorDocumentCode(this.tutorDocumentCode != null ? new ParameterMappedStringValue(this.tutorDocumentCode.getValue() ,this.tutorDocumentCode.getMandatory()): null);
		result.setTutorDocumentReleaseDate(this.tutorDocumentReleaseDate != null ? new ParameterMappedDateValue(this.tutorDocumentReleaseDate.getValue() ,this.tutorDocumentReleaseDate.getMandatory()): null);
		result.setTutorDocumentAuthority(this.tutorDocumentAuthority != null ? new ParameterMappedStringValue(this.tutorDocumentAuthority.getValue() ,this.tutorDocumentAuthority.getMandatory()): null);
		result.setTutorDocumentExpirationDate(this.tutorDocumentExpirationDate != null ? new ParameterMappedDateValue(this.tutorDocumentExpirationDate.getValue() ,this.tutorDocumentExpirationDate.getMandatory()): null);
		result.setTutorSign(this.tutorSign != null ? new ParameterMappedStringValue(this.tutorSign.getValue() ,this.tutorSign.getMandatory()): null);
		result.setDenomination(this.denomination != null ? new ParameterMappedStringValue(this.denomination.getValue() ,this.denomination.getMandatory()): null);
		result.setPhoto(this.photo != null ? new ParameterMappedStringValue(this.photo.getValue() ,this.photo.getMandatory()): null);
		result.setSign(this.sign != null ? new ParameterMappedStringValue(this.sign.getValue() ,this.sign.getMandatory()): null);
		
		result.setStateID(this.stateID != null ? new ParameterMappedIntegerValue(this.stateID.getValue(), this.stateID.getMandatory()): null);
				
		return result;
	}
	
	

	public ParameterMappedIntegerValue getStateID() {
		return stateID;
	}

	public void setStateID(ParameterMappedIntegerValue stateID) {
		this.stateID = stateID;
	}

	public ParameterMappedStringValue getGender() {
		return gender;
	}

	public void setGender(ParameterMappedStringValue gender) {
		this.gender = gender;
	}

	public Integer getMediumTypeID() {
		return mediumTypeID;
	}

	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}

	public Set<Credential> getCredentials() {
		return credentials;
	}

	public void setCredentials(Set<Credential> credentials) {
		this.credentials = credentials;
	}

	public ParameterMappedIntegerValue getID() {
		return ID;
	}

	public void setID(ParameterMappedIntegerValue iD) {
		ID = iD;
	}

	public ParameterMappedStringValue getMediumIdentifier() {
		return mediumIdentifier;
	}

	public void setMediumIdentifier(ParameterMappedStringValue mediumIdentifier) {
		this.mediumIdentifier = mediumIdentifier;
	}

	public ParameterMappedStringValue getPIN() {
		return PIN;
	}

	public void setPIN(ParameterMappedStringValue pIN) {
		PIN = pIN;
	}

	public ParameterMappedStringValue getBirthPlaceFiscalCode() {
		return birthPlaceFiscalCode;
	}

	public void setBirthPlaceFiscalCode(ParameterMappedStringValue birthPlacefiscalCode) {
		this.birthPlaceFiscalCode = birthPlacefiscalCode;
	}

	public ParameterMappedStringValue getGivenName() {
		return givenName;
	}

	public void setGivenName(ParameterMappedStringValue givenName) {
		this.givenName = givenName;
	}

	public ParameterMappedStringValue getFamilyName() {
		return familyName;
	}

	public void setFamilyName(ParameterMappedStringValue familyName) {
		this.familyName = familyName;
	}

	public ParameterMappedDateValue getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(ParameterMappedDateValue birthDate) {
		this.birthDate = birthDate;
	}

	public ParameterMappedStringValue getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(ParameterMappedStringValue fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public ParameterMappedStringValue getAddress1() {
		return address1;
	}

	public void setAddress1(ParameterMappedStringValue address1) {
		this.address1 = address1;
	}

	public ParameterMappedStringValue getAddress2() {
		return address2;
	}

	public void setAddress2(ParameterMappedStringValue address2) {
		this.address2 = address2;
	}

	public ParameterMappedStringValue getAddress3() {
		return address3;
	}

	public void setAddress3(ParameterMappedStringValue address3) {
		this.address3 = address3;
	}

	public ParameterMappedStringValue getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(ParameterMappedStringValue phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ParameterMappedStringValue getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(ParameterMappedStringValue mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public ParameterMappedStringValue geteMail() {
		return eMail;
	}

	public void seteMail(ParameterMappedStringValue eMail) {
		this.eMail = eMail;
	}

	public ParameterMappedDateValue getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(ParameterMappedDateValue releaseDate) {
		this.releaseDate = releaseDate;
	}

	public ParameterMappedDateValue getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(ParameterMappedDateValue validFrom) {
		this.validFrom = validFrom;
	}

	public ParameterMappedDateValue getValidTo() {
		return validTo;
	}

	public void setValidTo(ParameterMappedDateValue validTo) {
		this.validTo = validTo;
	}

	public ParameterMappedStringValue getDenomitaion() {
		return denomination;
	}

	public void setDenomitaion(ParameterMappedStringValue denomitaion) {
		this.denomination = denomitaion;
	}

	public ParameterMappedStringValue getDocumentType() {
		return documentType;
	}

	public void setDocumentType(ParameterMappedStringValue documentType) {
		this.documentType = documentType;
	}

	public ParameterMappedStringValue getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(ParameterMappedStringValue documentCode) {
		this.documentCode = documentCode;
	}

	public ParameterMappedDateValue getDocumentReleaseDate() {
		return documentReleaseDate;
	}

	public void setDocumentReleaseDate(ParameterMappedDateValue documentReleaseDate) {
		this.documentReleaseDate = documentReleaseDate;
	}

	public ParameterMappedStringValue getDocumentAuthority() {
		return documentAuthority;
	}

	public void setDocumentAuthority(ParameterMappedStringValue documentAuthority) {
		this.documentAuthority = documentAuthority;
	}

	public ParameterMappedDateValue getDocumentExpirationDate() {
		return documentExpirationDate;
	}

	public void setDocumentExpirationDate(ParameterMappedDateValue documentExpirationDate) {
		this.documentExpirationDate = documentExpirationDate;
	}

	public ParameterMappedStringValue getTutorGivenName() {
		return tutorGivenName;
	}

	public void setTutorGivenName(ParameterMappedStringValue tutorGivenName) {
		this.tutorGivenName = tutorGivenName;
	}

	public ParameterMappedStringValue getTutorFamilyName() {
		return tutorFamilyName;
	}

	public void setTutorFamilyName(ParameterMappedStringValue tutorFamilyName) {
		this.tutorFamilyName = tutorFamilyName;
	}

	public ParameterMappedStringValue getTutorBirthPlace() {
		return tutorBirthPlace;
	}

	public void setTutorBirthPlace(ParameterMappedStringValue tutorBirthPlace) {
		this.tutorBirthPlace = tutorBirthPlace;
	}

	public ParameterMappedDateValue getTutorBirthDate() {
		return tutorBirthDate;
	}

	public void setTutorBirthDate(ParameterMappedDateValue tutorBirthDate) {
		this.tutorBirthDate = tutorBirthDate;
	}

	public ParameterMappedStringValue getTutorFiscalCode() {
		return tutorFiscalCode;
	}

	public void setTutorFiscalCode(ParameterMappedStringValue tutorFiscalCode) {
		this.tutorFiscalCode = tutorFiscalCode;
	}

	public ParameterMappedStringValue getTutorAddress1() {
		return tutorAddress1;
	}

	public void setTutorAddress1(ParameterMappedStringValue tutorAddress1) {
		this.tutorAddress1 = tutorAddress1;
	}

	public ParameterMappedStringValue getTutorAddress2() {
		return tutorAddress2;
	}

	public void setTutorAddress2(ParameterMappedStringValue tutorAddress2) {
		this.tutorAddress2 = tutorAddress2;
	}

	public ParameterMappedStringValue getTutorAddress3() {
		return tutorAddress3;
	}

	public void setTutorAddress3(ParameterMappedStringValue tutorAddress3) {
		this.tutorAddress3 = tutorAddress3;
	}

	public ParameterMappedStringValue getTutorSign() {
		return tutorSign;
	}

	public void setTutorSign(ParameterMappedStringValue tutorSign) {
		this.tutorSign = tutorSign;
	}

	public ParameterMappedStringValue getTutorDocumentType() {
		return tutorDocumentType;
	}

	public void setTutorDocumentType(ParameterMappedStringValue tutorDocumentType) {
		this.tutorDocumentType = tutorDocumentType;
	}

	public ParameterMappedStringValue getTutorDocumentCode() {
		return tutorDocumentCode;
	}

	public void setTutorDocumentCode(ParameterMappedStringValue tutorDocumentCode) {
		this.tutorDocumentCode = tutorDocumentCode;
	}

	public ParameterMappedDateValue getTutorDocumentReleaseDate() {
		return tutorDocumentReleaseDate;
	}

	public void setTutorDocumentReleaseDate(ParameterMappedDateValue tutorDocumentReleaseDate) {
		this.tutorDocumentReleaseDate = tutorDocumentReleaseDate;
	}

	public ParameterMappedStringValue getTutorDocumentAuthority() {
		return tutorDocumentAuthority;
	}

	public void setTutorDocumentAuthority(ParameterMappedStringValue tutorDocumentAuthority) {
		this.tutorDocumentAuthority = tutorDocumentAuthority;
	}

	public ParameterMappedDateValue getTutorDocumentExpirationDate() {
		return tutorDocumentExpirationDate;
	}

	public void setTutorDocumentExpirationDate(ParameterMappedDateValue tutorDocumentExpirationDate) {
		this.tutorDocumentExpirationDate = tutorDocumentExpirationDate;
	}

	public ParameterMappedStringValue getDenomination() {
		return denomination;
	}

	public void setDenomination(ParameterMappedStringValue denomination) {
		this.denomination = denomination;
	}

	public ParameterMappedStringValue getPhoto() {
		return photo;
	}

	public void setPhoto(ParameterMappedStringValue photo) {
		this.photo = photo;
	}

	public ParameterMappedStringValue getSign() {
		return sign;
	}

	public void setSign(ParameterMappedStringValue sign) {
		this.sign = sign;
	}

//	public ParameterMappedIntegerValue getOperativeFlowID() {
//		return operativeFlowID;
//	}
//
//	public void setOperativeFlowID(ParameterMappedIntegerValue operativeFlowID) {
//		this.operativeFlowID = operativeFlowID;
//	}
//
//	public ParameterMappedStringValue getOperativeFlowName() {
//		return operativeFlowName;
//	}
//
//	public void setOperativeFlowName(ParameterMappedStringValue operativeFlowName) {
//		this.operativeFlowName = operativeFlowName;
//	}
//
//	public ParameterMappedStringValue getStateName() {
//		return stateName;
//	}
//
//	public void setStateName(ParameterMappedStringValue stateName) {
//		this.stateName = stateName;
//	}
//
//	public ParameterMappedIntegerValue getStateID() {
//		return stateID;
//	}
//
//	public void setStateID(ParameterMappedIntegerValue stateID) {
//		this.stateID = stateID;
//	}

}
