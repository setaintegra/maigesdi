package it.integrasistemi.maigesdi.bean;

/**
 * Oggetto creato per gestire il body della richesta POST medium/persons/validity poichè non si è creata uan risorsa ad-hoc per la funzionalità
 * @author Limiti
 *
 */
public class PersonValidityBody {
	
	private Integer mediumTypeID;
	private Person personData;
	
	public PersonValidityBody() {
		
	}
	
	public PersonValidityBody(Integer mediumTypeID, Person personData) {
		super();
		this.mediumTypeID = mediumTypeID;
		this.personData = personData;
	}
	public Integer getMediumTypeID() {
		return mediumTypeID;
	}
	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}
	public Person getPersonData() {
		return personData;
	}
	public void setPersonData(Person personData) {
		this.personData = personData;
	}

	
}
