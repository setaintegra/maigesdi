package it.integrasistemi.maigesdi.bean;

import java.util.Date;

public class ErrorDetails {
	private Date timestamp;
	private String message;
	private Integer code;
	
	public ErrorDetails(String message, Integer code) {
		this.message = message;
		this.code = code;
	}
	
	public ErrorDetails(Date timestamp, String message, Integer code) {
		this.timestamp = timestamp;
		this.message = message;
		this.code = code;
	}
	
	public ErrorDetails() {
		super();
	}
	
	@Override
	public String toString() {

		return "ErrorDetails {" + " timestamp: " + timestamp + 
								", message: " + message + 
								", code: "+ code + "}";
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
