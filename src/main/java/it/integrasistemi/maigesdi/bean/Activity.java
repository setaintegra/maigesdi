package it.integrasistemi.maigesdi.bean;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Activity {

	private Integer ID; //10
	@NotNull(message ="Activity's code is required")
	@Size(min=1,max =30, message ="Activity's code is required, can't be bigger than 30 characters")
	@NotBlank(message ="Activity's code is required")
	private String code; //30
	@Size(max = 255, message="Activity's description can't be bigger than 255 characters")
	private String description;	//255
	private Identity activityType; 
	@Valid
	@NotNull(message ="Activity's parameters are required")
	private Set<Parameter> parameters;

	@Override
	public String toString() {

		return "Activity {" + " ID: " + ID + 
								", code: " + code + 
								", activityType: "+ activityType + // (activityType !=null ? activityType.toString() :"NULL") + 
								", description: " + description +
								", Parameters: "+ parameters + "}";
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(Set<Parameter> parameters) {
		this.parameters = parameters;
	}
	
	

	public Identity getActivityType() {
		return activityType;
	}

	public void setActivityType(Identity activityType) {
		this.activityType = activityType;
	}

	@Override
	public boolean equals(Object o) {
		boolean result = false;

		if (o != null) {

			if (o == this) {
				result = true;
			} else {
				if ((o instanceof Activity)) {

					Activity activity = (Activity) o;

					result = activity.getCode().toUpperCase().equals(this.code.toUpperCase());

				} else {
					result = false;
				}
			}

		} else {
			result = false;
		}

		return result;
	}
	
	@Override
    public int hashCode() {
        return this.code.toUpperCase().hashCode();
    }
	

}
