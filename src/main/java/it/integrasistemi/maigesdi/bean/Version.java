package it.integrasistemi.maigesdi.bean;

public class Version {
	private String version;
	private String versionCode;
	private String releaseDate;
	private String note;

	public Version(String version,
				   String versionCode,
				   String releaseDate,
				   String note) {
		
		this.version = version;
		this.versionCode = versionCode;
		this.releaseDate = releaseDate;
		this.note = note;
		
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
