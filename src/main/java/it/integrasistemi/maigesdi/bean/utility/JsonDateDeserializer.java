package it.integrasistemi.maigesdi.bean.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.springframework.stereotype.Component;

@Component
public class JsonDateDeserializer extends JsonDeserializer<Date> 
{
    @Override
    public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException 
    {
        Date date = null;
        try 
        {
        	SimpleDateFormatThreadSafe df = new SimpleDateFormatThreadSafe("yyyy-MM-dd");
        	Calendar cal = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("CET");
            
            df.setTimeZone(tz);
            df.setLenient(false); // verifica la data

            date = df.parse(jp.getText());
            
            cal.setTime(date);
            cal.add(Calendar.HOUR, 12);
            
            //cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            
            date = cal.getTime();
           
        } 
        catch (ParseException ex) 
        {
            throw new RuntimeException(ex);
        }
        return date;
    }
    
}
