package it.integrasistemi.maigesdi.bean.utility;

import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class Utility {
	
	public static enum Profile{
		AMMINISTRATOR(1),
		CONFIGURATOR(5);
		
		private Integer value;

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}
		
        private Profile(Integer value) {
			 this.value = value;
		 }
	}
	
	//IMPORT DELLA LOGICA CONTENUTA NELLA CLASSE COSTANTI IN APIRESTGESDI CREATA DA OLIVARI DANIELE
	// successivamente le informazioni riportate qua sotto verranno raffinate e/o eliminate
	
	// Tabella GESDI.TIPOSTATOFLUSSOOPERATIVO
    public static final int TSFO_RICHIESTA_ACQUISITA__DATI_CON_ANOMALIE = 1; // I dati forniti non sono corretti
    public static final int TSFO_RICHIESTA_ACQUISITA__DATI_CORRETTI = 2;     // I dati forniti sono corretti
    public static final int TSFO_VERIFICA_PRESSO_AUTHORITY = 3;              // Dati inviati presso Authority per la verifica
    public static final int TSFO_RICHIESTA_PRONTA_PER_LA_PRODUZIONE = 4;     // Dati verificati e pronti per la produzione
    public static final int TSFO_RICHIESTA_NON_ACCETTATA_DALL_AUTHORITY = 5; // Diniego da parte della Authority
    public static final int TSFO_SUPPORTO_IN_PRODUZIONE = 6;                 // Dati inviati al produttore
    public static final int TSFO_SUPPORTO_PRODOTTO = 7;                      // Ritorno dati dal produttore dopo la produzione e spedizione del supporto
    public static final int TSFO_MANCATA_CONSEGNA = 8;                       // Non è stato possibile consegnare il supporto al richiedente
    public static final int TSFO_SUPPORTO_RISPEDITO = 9;                     // Nuovo tentativo di spedizione del supporto al richiedente
    public static final int TSFO_ATTESA_CONFERMA_BO = 10;                    // In attesa di conferma del supporto da parte del Back Office
    
    // Tabella GESDI.TIPOSTATOSUPPORTODIGITALE
    public static final int TSSD_IN_LAVORAZIONE = 1;  // SONO STATI ACQUISITI I DATI ED È STATO AVVIATO IL PROCESSO DI PRODUZIONE DEL SUPPORTO
    public static final int TSSD_ATTIVO = 2;          // SUPPORTO ATTIVO
    public static final int TSSD_RINUNCIATO = 3;      // SUPPORTO RINUNCIATO DAL CLIENTE TRAMITE CLIENT OPERATIVO GESDI_GUI
    public static final int TSSD_SMARRITO = 4;        // SUPPORTO SMARRITO
    public static final int TSSD_RUBATO = 5;          // SUPPORTO RUBATO
    public static final int TSSD_DANNEGGIATO = 6;     // SUPPORTO DANNEGGIATO O DISTRUTTO
    public static final int TSSD_ERRATO = 7;          // SUPPORTO ERRATO
    public static final int TSSD_RETTIFICATO_CEN = 8; // SUPPORTO RETTIFICATO DALL`AUTHORITY
    
    // Tabella GESDI.STATOPRODUTTORE
    public static final int SP_ISSD = 1; // Emessa
    public static final int SP_PRDG = 2; // Data alla produzione
    public static final int SP_PRDT = 3; // Presa in carico dalla produzione
    public static final int SP_DELV = 4; // Consegnata al cliente (emissione immediata) o al corriere (spedizione)
    public static final int SP_DEAC = 5; // Deattivata (per cancellazione, smarrimento, furto, Daspo, etc.)
    public static final int SP_REAC = 6; // Riattivata

    // SETA
    public static final int ID_NAZIONE_ITALIA_SETA = 108;
    
    // Eccezione
    public static final String CODICE_ECCEZIONE_MAI_POLICE_CEN_TIMEOUT = "MP_CEN_TIMEOUT";
    public static final String CODICE_ECCEZIONE_MAI_POLICE_MOTIVI_OSTATIVI = "MP_MOTIVI_OSTATIVI";
        
    
    // tipi operazioni per il salvataggio del risultato tornato da mai police ( in teoria da tutti, da vedere poi)
    public static final String ADDING_MEDIUM_EXTERNAL_SYSTEM = "ADDING_MEDIUM";
    public static final String REMOVING_MEDIUM_EXTERNAL_SYSTEM = "REMOVING_MEDIUM";
    
    public static final String GENDER_MALE = "M";
    public static final String GENDER_FEMALE = "F";
    
    //TIPO ATTIVITA
    public static final String ACTIVITY_TYPE_NEW				          = "NEW";
    public static final String ACTIVITY_TYPE_SEARCH                       = "SEARCH";
    public static final String ACTIVITY_TYPE_MODIFY                       = "MODIFY";
    public static final String ACTIVITY_TYPE_NEW_WEB                      = "NEW_WEB";
    public static final String ACTIVITY_TYPE_DISABLE                      = "DISABLE";
    public static final String ACTIVITY_TYPE_VERIFY                       = "VERIFY";
    public static final String ACTIVITY_TYPE_RESTORE                      = "RESTORE";
    public static final String ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM   = "ACTIVATE_ON_EXTERNALSYSTEM";
        
    
    //PARAMETRI
    public static final String PARAM_ID							  = "ID";
    public static final String PARAM_IDENTIFIER                   = "IDENTIFIER";
    public static final String PARAM_PIN                          = "PIN";
    public static final String PARAM_GIVENNAME                    = "GIVENNAME";
    public static final String PARAM_FAMILYNAME                   = "FAMILYNAME";
    public static final String PARAM_BIRTHPLACE                   = "BIRTHPLACE";
    public static final String PARAM_BIRTHDATE                    = "BIRTHDATE";
    public static final String PARAM_FISCALCODE                   = "FISCALCODE";
    public static final String PARAM_ADDRESS1                     = "ADDRESS1";
    public static final String PARAM_ADDRESS2                     = "ADDRESS2";
    public static final String PARAM_ADDRESS3                     = "ADDRESS3";
    public static final String PARAM_DOCUMENT_TYPE                = "DOCUMENT_TYPE";
    public static final String PARAM_DOCUMENT_CODE                = "DOCUMENT_CODE";
    public static final String PARAM_DOCUMENT_RELEASEDATE         = "DOCUMENT_RELEASEDATE";
    public static final String PARAM_DOCUMENT_AUTHORITY           = "DOCUMENT_AUTHORITY";
    public static final String PARAM_DOCUMENT_EXPIRATIONDATE      = "DOCUMENT_EXPIRATIONDATE";
    public static final String PARAM_PHONENUMBER                  = "PHONENUMBER";
    public static final String PARAM_MOBILENUMBER                 = "MOBILENUMBER";
    public static final String PARAM_EMAIL                        = "EMAIL";
    public static final String PARAM_RELEASEDATE                  = "RELEASEDATE";
    public static final String PARAM_VALIDFROM                    = "VALIDFROM";
    public static final String PARAM_VALIDTO                      = "VALIDTO";
    public static final String PARAM_TUTOR_GIVENNAME              = "TUTOR_GIVENNAME";
    public static final String PARAM_TUTOR_FAMILYNAME             = "TUTOR_FAMILYNAME";
    public static final String PARAM_TUTOR_BIRTHPLACE             = "TUTOR_BIRTHPLACE";
    public static final String PARAM_TUTOR_BIRTHDATE              = "TUTOR_BIRTHDATE";
    public static final String PARAM_TUTOR_FISCALCODE             = "TUTOR_FISCALCODE";
    public static final String PARAM_TUTOR_ADDRESS1               = "TUTOR_ADDRESS1";
    public static final String PARAM_TUTOR_ADDRESS2               = "TUTOR_ADDRESS2";
    public static final String PARAM_TUTOR_ADDRESS3               = "TUTOR_ADDRESS3";
    public static final String PARAM_TUTOR_DOCUMENT_TYPE          = "TUTOR_DOCUMENT_TYPE";
    public static final String PARAM_TUTOR_DOCUMENT_CODE          = "TUTOR_DOCUMENT_CODE";
    public static final String PARAM_TUTOR_DOCUMENT_RELEASEDATE   = "TUTOR_DOCUMENT_RELEASEDATE";
    public static final String PARAM_TUTOR_DOCUMENT_AUTHORITY     = "TUTOR_DOCUMENT_AUTHORITY";
    public static final String PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE= "TUTOR_DOCUMENT_EXPIRATIONDATE";
    public static final String PARAM_TUTOR_SIGN                   = "TUTOR_SIGN";
    public static final String PARAM_DENOMINATION                 = "DENOMINATION";
    public static final String PARAM_GENDER                       = "GENDER";
    public static final String PARAM_PHOTO                        = "PHOTO";
    public static final String PARAM_SIGN	                      = "SIGN";
    public static final String PARAM_STATE_TYPE_ID				  = "STATE_TYPE_ID";
    
    //sistemaesterno
    public static final int EXTERNAL_SYSTEM_MAIGESDI_CEN    =2;
    public static final int EXTERNAL_SYSTEM_MAIGESDI        =1;
    public static final int EXTERNAL_SYSTEM_EXTERNAL_CEN    =3;
    
    
    public static final Map<String, Integer> mappaDecodificaFormulaLuhn;
    static {
        Map<String, Integer> LuhnStaticMap = new HashMap<String, Integer>();
        LuhnStaticMap.put("A", 0);
        LuhnStaticMap.put("B", 1);
        LuhnStaticMap.put("C", 2);
        LuhnStaticMap.put("D", 3);
        LuhnStaticMap.put("E", 4);
        LuhnStaticMap.put("F", 5);
        LuhnStaticMap.put("G", 6);
        LuhnStaticMap.put("H", 7);
        LuhnStaticMap.put("I", 8);
        LuhnStaticMap.put("J", 9);
        LuhnStaticMap.put("K", 10);
        LuhnStaticMap.put("L", 11);
        LuhnStaticMap.put("M", 12);
        LuhnStaticMap.put("N", 13);
        LuhnStaticMap.put("O", 14);
        LuhnStaticMap.put("P", 15);
        LuhnStaticMap.put("Q", 16);
        LuhnStaticMap.put("R", 17);
        LuhnStaticMap.put("S", 18);
        LuhnStaticMap.put("T", 19);
        LuhnStaticMap.put("U", 20);
        LuhnStaticMap.put("V", 21);
        LuhnStaticMap.put("W", 22);
        LuhnStaticMap.put("X", 23);
        LuhnStaticMap.put("Y", 24);
        LuhnStaticMap.put("Z", 25);
        LuhnStaticMap.put("0", 0);
        LuhnStaticMap.put("1", 1);
        LuhnStaticMap.put("2", 2);
        LuhnStaticMap.put("3", 3);
        LuhnStaticMap.put("4", 4);
        LuhnStaticMap.put("5", 5);
        LuhnStaticMap.put("6", 6);
        LuhnStaticMap.put("7", 7);
        LuhnStaticMap.put("8", 8);
        LuhnStaticMap.put("9", 9);
        mappaDecodificaFormulaLuhn = Collections.unmodifiableMap(LuhnStaticMap);
    }
    
    public final static String STRINGA__DOMINIO_CARATTERI = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
    //costanti per i nomi dei DB negli ambienti di test, validazione e sviluppo
    public static final String TEST_NOME_DB = "TEST";
    public static final String STAGE_NOME_DB = "STAGE";
    public static final String VALIDAZIONE_NOME_DB = "VALIDA";
    public static final String AMBIENTE_INTEGRA_NOME_DB = "DBSETA";
    
    //valore dello iua fittizio per la modalità debug del cen
	public static final String IUA_FITTIZIO = "666";
    
    //enumerato per la gestione dei codici di errore
    public static enum ErrorType{
		
		
		//PRECONDITION BAD REQUEST 
		ET_PRECONDITION_ERROR(100),
		ET_ORGANIZATION_NOT_FOUND(201),
		ET_OPERATOR_NOT_FOUND(202), 
		ET_MEDIUM_NOT_FOUND(203), 
		ET_MEDIUM_TYPE_NOT_FOUND(204),
		ET_EXTERNAL_SYSTEM_NOT_FOUND(205), 
		ET_PERSON_DATA_NOT_FOUND(206),
		ET_MEDIUM_IDENTIFIER_RANGE_NOT_FOUND(207),
		ET_BIRTHDATE_AND_BIRTHPLACE_NOT_VALID(208),
		
		//NOT_AUTHORIZED
		ET_OPERATOR_NOT_AUTHORIZED(212), 
		//FORBIDDEN
		ET_FORBIDDEN_ERROR(300),
		ET_PERSON_ALREDY_OWN_MEDIUM_FOR_SAME_ORGANIZATION(301), 
		ET_PERSON_ALREDY_OWN_TOO_MANY_MEDIUM_FOR_SAME_ORGANIZATION(302), 
		ET_MEDIUM_UPDATING_NOT_ALLOWED_EXPIRATION_DATE_NOT_EDITABLE(311), 
		ET_MEDIUM_UPDATING_NOT_ALLOWED_PERSON_DATA_NOT_EDITABLE(312),
		ET_MEDIUM_OWNER_IS_MINOR_TUTOR_MISSING(320),
		ET_MEDIUM_IDENTIFIER_RANGE_NOT_CONFIGURATED(330),
		ET_MEDIUM_IDENTIFIER_RANGE_TERMINATED(331),
		
		//CONFLICT 409
		ET_CONFLICT_ERROR(350),
		ET_MEDIUM_ALREDY_EXIST(351),
		ET_MEDIUM_MISMATCH(352),
		ET_MEDIUM_STATE_NOT_VALID(353),
		ET_MEDIUM_IDENTIFIER_RANGE_ALREDY_USED(354),
		ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE(355),
		ET_MEDIUM_IDENTIFIER_RANGE_IDENTIFIER_NOT_RETURNED(356),
		ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE_ALREDY_USED_IN_A_RANGE(357),
		ET_MEDIUM_OPERATIVE_FLOV_NOT_VALID(358),
		ET_MEDIUM_TYPE_DAYS_VALIDITY_NOT_VALORIZED(370),
		ET_TOO_MANY_RESULT(380),
				
		//INTERNAL SERCVER ERROR
		ET_DBMS_ERROR(400), 
		
		ET_OPERATOR_PASSWORD_NOT_CHANGED(410),
		ET_UNSUPPORTED_ACTIVITY_TYPE(420), 
		ET_UNSUPPORTED_ACTIVITY(421), 
		ET_UNSUPPORTED_OPERATION(422),
		ET_MEDIUM_NOT_CREATED(430), 
		ET_MEDIUM_NOT_CREATED_BUT_VALID_ON_CEN(431), 
		ET_MEDIUM_NOT_CREATED_UKNOWN_CEN_STATE_INCONSISTENT_SITUATION(432),
		ET_MEDIUM_NOT_DISABLED(440), 
		ET_MEDIUM_NOT_DISABLED_BUT_DISABLED_ON_CEN(441),
		ET_MEDIUM_NOT_DISABLED_UKNOWN_CEN_STATE_INCONSISTENT_SITUATION(442),
		ET_MEDIUM_NOT_RESTORED(450),
		ET_MEDIUM_NOT_RESTORED_BUT_VALID_ON_CEN(451),
		ET_MEDIUM_NOT_RESTORED_UKNOWN_CEN_STATE_INCONSISTENT_SITUATION(452),
		ET_MEDIUM_IDENTIFIER_RANGE_NOT_CREATED(460),
		ET_MEDIUM_IDENTIFIER_RANGE_RESTORING_PROBLEM(471), 
		ET_MEDIUM_IDENTIFIER_RANGE_COMPLETING_MEDIUM_PROBLEM(470),
		
		ET_GENERIC_ERROR(500), 
		ET_CEN_EXCEPTION(501), 
		ET_MEDIUM_OPERATIVE_FLOV_MISSING(510),
		ET_MEDIUM_STATE_MISSING(511)
		;
		
		
		private Integer value;

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}
		
        private ErrorType(Integer value) {
			 this.value = value;
		 }
        
        
	}
    
   
  
    
    
    
    

}
