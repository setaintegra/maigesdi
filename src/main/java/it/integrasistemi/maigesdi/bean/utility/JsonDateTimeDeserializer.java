package it.integrasistemi.maigesdi.bean.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.springframework.stereotype.Component;

@Component
public class JsonDateTimeDeserializer extends JsonDeserializer<Date> 
{
    @Override
    public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException 
    {
        Date date = null;
        try 
        {
        	SimpleDateFormatThreadSafe df = new SimpleDateFormatThreadSafe("yyyy-MM-dd'T'HH:mm:ss'Z'");

            TimeZone tz = TimeZone.getTimeZone("CET");
            df.setTimeZone(tz);
            df.setLenient(false); // verifica la data

            date = df.parse(jp.getText());
        	
        } 
        catch (ParseException ex) 
        {
            throw new RuntimeException(ex);
        }
        return date;
    }
    
}