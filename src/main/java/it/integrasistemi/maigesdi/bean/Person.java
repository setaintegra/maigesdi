package it.integrasistemi.maigesdi.bean;

import java.util.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;

public class Person {
	
	@Size(max = 4, message="Person's fiscalCodeBirthPlace can't be bigger than 4 characters")
	private String fiscalCodeBirthPlace; //4
	@Size(max = 30, message="Person's givenName can't be bigger than 30 characters")
	private String givenName; //30
	@Size(max = 30, message="Person's familyName can't be bigger than 30 characters")
	private String familyName; //30
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date birthDate;
	@Size(max = 1, message="Person's gender can't be bigger than 1 characters")
	private String gender; //1
	
	public Person(String givenName, String familyName, String fiscalCodeBirthPlace, Date birthDate, String gender ) {
		 this.fiscalCodeBirthPlace = fiscalCodeBirthPlace;
		 this.givenName = givenName;
		 this.familyName = familyName;
		 this.birthDate = birthDate;
		 this.gender = gender;
		 
	}

	public Person() {
		
	}
	
	public String getFiscalCodeBirthPlace() {
		return fiscalCodeBirthPlace;
	}

	public void setFiscalCodeBirthPlace(String fiscalCodeBirthPlace) {
		this.fiscalCodeBirthPlace = fiscalCodeBirthPlace;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
		
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public boolean equals(Object o) {
		boolean result = true;

		if (o != null) {

			if (o == this) {
				result = true;
			} else {
				if ((o instanceof Person)) {
					
					Person person = (Person) o;

					result = result && person.getFamilyName().toUpperCase().equals(this.familyName.toUpperCase());
					result = result && person.getGivenName().toUpperCase().equals(this.givenName.toUpperCase() );
					result = result && person.getBirthDate().equals(this.birthDate);
					result = result && person.getFiscalCodeBirthPlace().toUpperCase().equals(this.fiscalCodeBirthPlace.toUpperCase());
					result = result && person.getGender().toUpperCase().equals(this.gender.toUpperCase());

				} else {
					result = false;
				}
			}

		} else {
			result = false;
		}

		return result;
	}
	
	@Override
	public String toString() {

		return "Person {" + " givenName: " + givenName + 
								", familyName: " + familyName + 
								", birthDate: "+ birthDate +
								", fiscalCodeBirthPlace: " + fiscalCodeBirthPlace +
								", gender: "+ gender + "}";
		}
	
	

}
