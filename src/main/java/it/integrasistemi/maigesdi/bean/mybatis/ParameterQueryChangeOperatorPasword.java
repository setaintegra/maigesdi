package it.integrasistemi.maigesdi.bean.mybatis;

public class ParameterQueryChangeOperatorPasword {

	private Integer operatorID;
	private String oldPassword;
	private String newPassword;

	public ParameterQueryChangeOperatorPasword(Integer id, String oldPassword, String newPassword) {
		this.operatorID = id;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}

	public Integer getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(Integer operatorID) {
		this.operatorID = operatorID;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	

}
