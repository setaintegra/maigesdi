package it.integrasistemi.maigesdi.bean.mybatis;

public class GetMediumSignResult {
	
	private Integer mediumID;
	private byte[] sign;
	
	public GetMediumSignResult(){
		
	}

	public Integer getMediumID() {
		return mediumID;
	}

	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}

	public byte[] getSign() {
		return sign;
	}

	public void setSign(byte[] sign) {
		this.sign = sign;
	}

}