package it.integrasistemi.maigesdi.bean.mybatis;

public class ParameterQueryGetOrganizzationByID {
	
	private Integer organizationID;
	private Integer operatorID;
	
	public ParameterQueryGetOrganizzationByID(Integer organizationID, Integer operatorID) {
		this.operatorID = operatorID;
		this.organizationID = organizationID;
	}

	public Integer getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(Integer organizationID) {
		this.organizationID = organizationID;
	}

	public Integer getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(Integer operatorID) {
		this.operatorID = operatorID;
	}
	
	
}
