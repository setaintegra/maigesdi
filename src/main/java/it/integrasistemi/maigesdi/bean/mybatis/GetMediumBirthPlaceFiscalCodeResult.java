package it.integrasistemi.maigesdi.bean.mybatis;

public class GetMediumBirthPlaceFiscalCodeResult {

	private Integer mediumID;
	private String birthPlaceFiscalCode;
	
	public GetMediumBirthPlaceFiscalCodeResult(){
		
	}

	public Integer getMediumID() {
		return mediumID;
	}

	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}

	public String getBirthPlaceFiscalCode() {
		return birthPlaceFiscalCode;
	}

	public void setBirthPlaceFiscalCode(String birthPlaceFiscalCode) {
		this.birthPlaceFiscalCode = birthPlaceFiscalCode;
	}

	
	
	
	

}
