package it.integrasistemi.maigesdi.bean.mybatis;

public class ParameterQueryGetMediumByMediumTypeIDMediumIdentifier{
	
	private Integer mediumTypeID;
	private String mediumIdentifier;
	private Integer operatorID;
	
	public ParameterQueryGetMediumByMediumTypeIDMediumIdentifier() {
		
	}

	public ParameterQueryGetMediumByMediumTypeIDMediumIdentifier(Integer mediumTypeID, String mediumIdentifier, Integer operatorID) {
		this.mediumIdentifier = mediumIdentifier;
		this.mediumTypeID = mediumTypeID;
		this.operatorID = operatorID;
	}

	public Integer getMediumTypeID() {
		return mediumTypeID;
	}

	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}

	
	public String getMediumIdentifier() {
		return mediumIdentifier;
	}

	public void setMediumIdentifier(String mediumIdentifier) {
		this.mediumIdentifier = mediumIdentifier;
	}

	public Integer getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(Integer operatorID) {
		this.operatorID = operatorID;
	}
	
	

}
