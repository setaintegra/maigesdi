package it.integrasistemi.maigesdi.bean.mybatis;

public class ParameterQueryGetMediumTypeByID {

	private Integer operatorID;
	private Integer mediumTypeID;

	public ParameterQueryGetMediumTypeByID(Integer mediumTypeID, Integer operatorID) {
		this.operatorID = operatorID;
		this.mediumTypeID = mediumTypeID;
	}

	public Integer getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(Integer operatorID) {
		this.operatorID = operatorID;
	}

	public Integer getMediumTypeID() {
		return mediumTypeID;
	}

	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}
	
	

}
