package it.integrasistemi.maigesdi.bean.mybatis;



public class GetNextMediumIdentifierRangeResult {
	
	private Integer mediumTypeID;
	private Integer operatorID;
	private Integer mediumID;
	private Integer operativeFlowID;
	private Integer stateID;
	private String mediumIdentifier;
	
	public GetNextMediumIdentifierRangeResult(){
		
	}
	
	
	public GetNextMediumIdentifierRangeResult(Integer mediumTypeID, Integer operatorID, Integer operativeFlowID, Integer stateID) {
		this.mediumTypeID = mediumTypeID;
		this.operatorID = operatorID;
		this.operativeFlowID = operativeFlowID;
		this.stateID = stateID;
		this.mediumID = 0;
		this.mediumIdentifier = null;
	}


	public Integer getMediumTypeID() {
		return mediumTypeID;
	}
	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}
	public Integer getOperatorID() {
		return operatorID;
	}
	public void setOperatorID(Integer operatorID) {
		this.operatorID = operatorID;
	}
	public Integer getMediumID() {
		return mediumID;
	}
	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}
	public Integer getOperativeFlowID() {
		return operativeFlowID;
	}
	public void setOperativeFlowID(Integer operativeFlowID) {
		this.operativeFlowID = operativeFlowID;
	}
	public Integer getStateID() {
		return stateID;
	}
	public void setStateID(Integer stateID) {
		this.stateID = stateID;
	}
	public String getMediumIdentifier() {
		return mediumIdentifier;
	}
	public void setMediumIdentifier(String mediumIdentifier) {
		this.mediumIdentifier = mediumIdentifier;
	}
	
	
}
