package it.integrasistemi.maigesdi.bean.mybatis;

import java.util.List;
import java.util.Set;

import it.integrasistemi.maigesdi.bean.Identity;

public class ParameterQueryInserMediumTypeForOperator {

	private Integer operatorID;
	private Set<Identity> mediumTypes;
	

	public ParameterQueryInserMediumTypeForOperator(Integer operatorID, Set<Identity> mediumTypes) {
		this.operatorID = operatorID;
		this.mediumTypes = mediumTypes;
	}
	
	

}
