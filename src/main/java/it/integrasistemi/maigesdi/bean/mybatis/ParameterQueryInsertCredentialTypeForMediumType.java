package it.integrasistemi.maigesdi.bean.mybatis;

import java.util.Set;

import it.integrasistemi.maigesdi.bean.CredentialType;

public class ParameterQueryInsertCredentialTypeForMediumType {
	
	private Integer mediumTypeID;
	private Set<CredentialType> credentialTypes;

	
	public ParameterQueryInsertCredentialTypeForMediumType() {
		
	}


	public ParameterQueryInsertCredentialTypeForMediumType(Integer mediumTypeID, Set<CredentialType> credentialTypes) {
		this.credentialTypes = credentialTypes;
		this.mediumTypeID = mediumTypeID;
	}


	public Integer getMediumTypeID() {
		return mediumTypeID;
	}


	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}


	public Set<CredentialType> getCredentialTypes() {
		return credentialTypes;
	}


	public void setCredentialType(Set<CredentialType> credentialTypes) {
		this.credentialTypes = credentialTypes;
	}
	
	
}
