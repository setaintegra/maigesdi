package it.integrasistemi.maigesdi.bean.mybatis;

public class GetMediumPhotoResult {
	
	private Integer mediumID;
	private byte[] photo;
	
	public GetMediumPhotoResult(){
		
	}

	public Integer getMediumID() {
		return mediumID;
	}

	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
 
	
	
	

}
