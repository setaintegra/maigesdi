package it.integrasistemi.maigesdi.bean.mybatis;

public class ParameterQueryInsertCredentialForMedium {
	
	private Integer mediumID;
	private Integer credentialTypeID;
	private String credentialValue;

	public ParameterQueryInsertCredentialForMedium(Integer mediumID, Integer credentialTypeID, String credentialValue) {
		this.mediumID = mediumID;
		this.credentialTypeID = credentialTypeID;
		this.credentialValue = credentialValue;
	}

	public Integer getMediumID() {
		return mediumID;
	}

	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}

	public Integer getCredentialTypeID() {
		return credentialTypeID;
	}

	public void setCredentialTypeID(Integer credentialTypeID) {
		this.credentialTypeID = credentialTypeID;
	}

	public String getCredentialValue() {
		return credentialValue;
	}

	public void setCredentialValue(String credentialValue) {
		this.credentialValue = credentialValue;
	}
	
	

}
