package it.integrasistemi.maigesdi.bean.mybatis;

public class ParameterQueryCheckOperatorMediumTypeRights {
	
	private Integer operatorID;
	private Integer mediumTypeID;
	

	public ParameterQueryCheckOperatorMediumTypeRights(Integer operatorID, Integer mediumTypeID) {
		this.operatorID = operatorID;
		this.mediumTypeID = mediumTypeID;
	}

	public Integer getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(Integer operatorID) {
		this.operatorID = operatorID;
	}

	public Integer getMediumTypeID() {
		return mediumTypeID;
	}

	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}
	
	

}
