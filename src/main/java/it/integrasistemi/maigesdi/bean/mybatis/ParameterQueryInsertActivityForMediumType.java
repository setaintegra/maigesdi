package it.integrasistemi.maigesdi.bean.mybatis;

import java.util.List;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Parameter;

public class ParameterQueryInsertActivityForMediumType {
	
	private Integer mediumTypeID;
	private String activityCode;
	private String paramCode;
	private Boolean paramMandatory;

	public ParameterQueryInsertActivityForMediumType(MediumType mediumType, String activityCode, Parameter parameter) {
		this.mediumTypeID = mediumType.getID();
		this.activityCode = activityCode;
		this.paramCode = parameter.getCode();
		this.paramMandatory = parameter.getMandatory();
		
	}

	public Integer getMediumTypeID() {
		return mediumTypeID;
	}

	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityTypeCode) {
		this.activityCode = activityTypeCode;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public Boolean getParamMandatory() {
		return paramMandatory;
	}

	public void setParamMandatory(Boolean paramMandatory) {
		this.paramMandatory = paramMandatory;
	}

		
	

}
