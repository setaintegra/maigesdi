package it.integrasistemi.maigesdi.bean.mybatis;

public class Sequence {

	private String name;

	public Sequence(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
