package it.integrasistemi.maigesdi.bean.mybatis;

public class GetMediumTutorSignResult {
	
	private Integer mediumID;
	private byte[] sign;
	
	public GetMediumTutorSignResult() {
		
	}

	public Integer getMediumID() {
		return mediumID;
	}

	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}

	public byte[] getSign() {
		return sign;
	}

	public void setSign(byte[] sign) {
		this.sign = sign;
	}

}