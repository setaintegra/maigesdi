package it.integrasistemi.maigesdi.bean;

import java.util.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModelProperty;
import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;

public class ShortMediumData {
	
	@Size(min =1, max = 12, message="Medium identifier can't be bigger than 12 characters")
	private String mediumIdentifier; //12
	@Size(max = 4, message="Person's fiscalCodeBirthPlace can't be bigger than 4 characters")
	private String fiscalCodeBirthPlace; //4
	@Size(max = 30, message="Person's givenName can't be bigger than 30 characters")
	private String givenName; //30
	@Size(max = 30, message="Person's familyName can't be bigger than 30 characters")
	private String familyName; //30
	@ApiModelProperty(value="\"yyyy-MM-dd\"")
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date birthDate;
	@Size(max = 1, message="Person's gender can't be bigger than 1 characters")
	private String gender; //1
	@ApiModelProperty(value="\"yyyy-MM-dd\"")
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date validTo;
	
	public String getMediumIdentifier() {
		return mediumIdentifier;
	}
	public void setMediumIdentifier(String mediumIdentifier) {
		this.mediumIdentifier = mediumIdentifier;
	}
	public String getFiscalCodeBirthPlace() {
		return fiscalCodeBirthPlace;
	}
	public void setFiscalCodeBirthPlace(String fiscalCodeBirthPlace) {
		this.fiscalCodeBirthPlace = fiscalCodeBirthPlace;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	
	@Override
	public String toString() {
		
		
		return "ShorMediumData: {  MediumIdentifier: " + 			 mediumIdentifier		   +
				 	 			", givenName: " +                    givenName		           +
								", familyName: " +                   familyName		           +
								", birthDate: " +                    birthDate		           +
								", fiscalCodeBirthPlace: " +         fiscalCodeBirthPlace	   +
								", gender: " +                  	 gender             	   +
								", validTo: " +                      validTo		           +
						
						
								"}"; 
			
	}
}
