package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Place {

	@NotNull(message="Place's fiscalCode is required")
	@NotBlank(message="Place's fiscalCode is required")
	@Size(min = 4, max = 4, message="Place's fiscalCode must have four characters")
	private String 	fiscalCode;     //4
	@Size(max = 3, message="Place's ISOCountryCode can't be bigger than 3 characters")
	private String	ISOCountryCode; //3
	@Size(max = 100, message="Place's name can't be bigger than 100 characters")
	private String	name; //100
	@Size(max = 2, message="Place's province can't be bigger than 2 characters")
	private String 	province; //2
	
	public Place() {
		
	}
	
	@Override
	public String toString() {
		
		return "Place: { fiscalCode: "+ 		fiscalCode 			   +
						", ISOCountryCode: " + 	ISOCountryCode		   +
				 	 	", name: " +            name		           +
						", province: " +        province	           +
						"}"; 
		
	}


	
	
	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getISOCountryCode() {
		return ISOCountryCode;
	}

	public void setISOCountryCode(String iSOCountryCode) {
		ISOCountryCode = iSOCountryCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	

	
}
