package it.integrasistemi.maigesdi.bean;

import java.util.Date;

/**
 * Classe creata per gestire gli intervalli di date.
 * si è scelto di non usare Period di Jodatime poichè in precedenza si è deciso di usare sempre java.util.date
 * per gestire le date
 * @author Limiti
 *
 */
public class DateRange {

	private Date startRange;
	private Date endRange;
	
	public DateRange(Date startRange, Date endRange) {
		this.startRange = startRange;
		this.endRange = endRange;
	}

	public Date getStartRange() {
		return startRange;
	}

	public void setStartRange(Date startRange) {
		this.startRange = startRange;
	}

	public Date getEndRange() {
		return endRange;
	}

	public void setEndRange(Date endRange) {
		this.endRange = endRange;
	}
	
	
	
}
