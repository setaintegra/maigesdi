package it.integrasistemi.maigesdi.bean;

//dati di un medium riservato da una spezzata
public class ReservedMedium {
	private Integer mediumID;
	private String mediumIdentifier;
	private Integer mediumTypeID;
	
	public ReservedMedium() {
		
	}
	
	public ReservedMedium( Integer mediumID, String mediumIdentifier, Integer mediumTypeID) {
		this.mediumID = mediumID;
		this.mediumIdentifier = mediumIdentifier;
		this.mediumTypeID = mediumTypeID;
		
	}
	
    @Override
	public String toString() {

		return "ReservedMedium {" + " mediumID: " + mediumID + 
								", mediumIdentifier: " + mediumIdentifier + 
								", mediumTypeID: "+ mediumTypeID  + "}";
	}
	
	
	public Integer getMediumID() {
		return mediumID;
	}
	public void setMediumID(Integer mediumID) {
		this.mediumID = mediumID;
	}
	public String getMediumIdentifier() {
		return mediumIdentifier;
	}
	public void setMediumIdentifier(String mediumIdentifier) {
		this.mediumIdentifier = mediumIdentifier;
	}
	public Integer getMediumTypeID() {
		return mediumTypeID;
	}
	public void setMediumTypeID(Integer mediumTypeID) {
		this.mediumTypeID = mediumTypeID;
	}
	
	

}
