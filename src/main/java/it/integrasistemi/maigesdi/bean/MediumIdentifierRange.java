package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MediumIdentifierRange {
	
	private Integer ID;
	@NotNull(message="Start range is required")
	@Pattern(regexp = "^[0-9]+$")
	@Size(min=1, max=8)
	private String  start;
	@NotNull(message="End range is required")
	@Pattern(regexp = "^[0-9]+$")
	@Size(min=1, max=8)
	private String  end;
    @NotNull(message="Organization is required")
    private Identity organization;
    //private Integer organizationID;
    @JsonIgnore
    private String  startMediumIdentifier;
    @JsonIgnore
    private String  endMediumIdentifier;
    @NotNull(message="Medium type is required")
    private Identity mediumType;
//    private Integer idTipoSupportoDigitale;
//    private String  nomeTipoSupportoDigitale;
	
    @Override
	public String toString() {

		return "MediumIdentifierRange {" + " ID: " + ID + 
								", start: " + start + 
								", end: "+ end + 
								", organization: " + organization +
								", mediumType: "+ mediumType + "}";
	}
    
    public String getStart() {
		return start;
	}
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public Identity getOrganization() {
		return organization;
	}
	public void setOrganization(Identity organization) {
		this.organization = organization;
	}
	public Identity getMediumType() {
		return mediumType;
	}
	public void setMediumType(Identity mediumType) {
		this.mediumType = mediumType;
	}
	public String getStartMediumIdentifier() {
		return startMediumIdentifier;
	}
	public void setStartMediumIdentifier(String startMediumIdentifier) {
		this.startMediumIdentifier = startMediumIdentifier;
	}
	public String getEndMediumIdentifier() {
		return endMediumIdentifier;
	}
	public void setEndMediumIdentifier(String endMediumIdentifier) {
		this.endMediumIdentifier = endMediumIdentifier;
	}

	
    
    
}
