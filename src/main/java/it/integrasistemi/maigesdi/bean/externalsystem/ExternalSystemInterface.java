package it.integrasistemi.maigesdi.bean.externalsystem;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;

public interface ExternalSystemInterface {
	
	public ExternalSystemResult insertMedium(ParameterMedium parameterMedium, Operator operator);
	
	public ExternalSystemResult insertWebMedium(ParameterMedium parameterMedium, Operator operator);
	
	public ExternalSystemResult updateMedium(ParameterMedium parameterMedium, Operator operator);
	
	public ExternalSystemResult disableMedium(ParameterMedium parameterMedium, Operator operator);
	
	public ExternalSystemResult getMediums(ParameterMedium parameterMedium, Operator operator);
	
	public ExternalSystemResult verifyMedium(ParameterMedium parameterMedium, Operator operator);

	public ExternalSystemResult restoreMedium(ParameterMedium parameterMedium, Operator operator);
	
	 /**
     * Verifica se la quintupletta è valida per la creazione di medium (per ora funziona solo al VRO)
     * @param givenName nome
     * @param familyName cognome
     * @param fiscalCodeBirthPlace codice erariale del luogo di nascita
     * @param birthDate data di nascita
     * @param gender genere 
     * @param operator che effettua l'operazione
     * @return 
     */
	public ExternalSystemResult checkValidPersonOnExternalSystem(String givenName,
																 String familyName,
																 String fiscalCodeBirthPlace,
																 Date birthDate, 
																 String gender,
																 Operator operator);

	/**
	 * Verifica che il medium esista persso il sistema esterno (per ora funziona solo al VRO)
	 * @param externalSystemID
	 * @param mediumIdentifier
	 * @param givenName
	 * @param familyName
	 * @param fiscalCodeBirthPlace
	 * @param birthDate
	 * @param gender
	 * @param validTo
	 * @param operator
	 * @return
	 */
	public ExternalSystemResult checkExistingMediumOnExternalSystem(Integer externalSystemID,
																	String mediumIdentifier,
																	String givenName,
																	String familyName,
																	String fiscalCodeBirthPlace,
																	Date birthDate, 
																	String gender,
																	Date validTo,
																	Operator operator);
	
	/**
	 * Attiva un medium esistente e attivo su MAIGeSDi presso il sistema esterno che non lo conosce (per ora funziona solo al VRO)
	 * @param parameterMedium il medium da attivare
	 * @param operator l'operatore che effettua 
	 * @return
	 */
	public ExternalSystemResult activeExistingMediumOnExternalSystem(ParameterMedium parameterMedium, Operator operator);

	
	/**
	 * Metodo che incapsula i controlli sull'anagrafica del potenziale titolare del supporto, controllo pensato da essere eseguito prima di una NEW
	 * @param personData dati della persona da controllare
	 * @return
	 */
	public ExternalSystemResult checkPersonDataIsValid(Integer mediumTypeID, Person personData, Operator operator);
	

}	
