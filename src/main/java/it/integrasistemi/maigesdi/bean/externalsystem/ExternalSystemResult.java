package it.integrasistemi.maigesdi.bean.externalsystem;

import java.util.List;

import it.integrasistemi.maigesdi.bean.Medium;

public class ExternalSystemResult {
	
	private boolean result;
	private String resultDescription;
	private Exception exceptionCatched;
	private List<Medium> mediumResult;
	
	public ExternalSystemResult() {
		
	}

	public ExternalSystemResult(boolean result, Exception exceptionCatched) {
		this.result = result;
		this.exceptionCatched = exceptionCatched;
		this.mediumResult = null;
	}
	
	public ExternalSystemResult(boolean result, List<Medium> mediumResult) {
		this.result = result;
		this.exceptionCatched = null;
		this.mediumResult = mediumResult;
	}
	
	public ExternalSystemResult(boolean result, Exception exceptionCatched, List<Medium> mediumResult) {
		this.result = result;
		this.exceptionCatched = exceptionCatched;
		this.mediumResult = mediumResult;
	}
	
	public ExternalSystemResult(boolean result, String resultDescription) {
		this.result = result;
		this.setResultDescription(resultDescription);
		this.exceptionCatched = null;
		this.mediumResult = null;
	}
	
	
	public Exception getExceptionCatched() {
		return exceptionCatched;
	}

	public void setExceptionCatched(Exception exceptionCatched) {
		this.exceptionCatched = exceptionCatched;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	
	public List<Medium> getMediumResult() {
		return mediumResult;
	}

	public void setMediumResult(List<Medium> mediumResult) {
		this.mediumResult = mediumResult;
	}

	public String getResultDescription() {
		return resultDescription;
	}

	public void setResultDescription(String resultDescription) {
		this.resultDescription = resultDescription;
	}
	
	

}
