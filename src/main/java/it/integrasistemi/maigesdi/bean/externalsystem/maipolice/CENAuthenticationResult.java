package it.integrasistemi.maigesdi.bean.externalsystem.maipolice;

import it.integrasistemi.maigesdi.bean.utility.Utility;

//classe di copia della classe AutrozzazioneVRO del progetto APIRESTGESDI 
//VA FATTO UN REFACTOR
public class CENAuthenticationResult {
		
	    private Boolean autorizzato;
	    private String IuA;
	    private String codiceEccezione;
	    private String descrizioneEccezione;
	    

	    public CENAuthenticationResult(Boolean richiestaDaMAIPoliceACEN, Boolean autorizzato, String IuA, String codiceEccezione, String descrizioneEccezione) 
	    {
	        if(richiestaDaMAIPoliceACEN == false && autorizzato == true && (IuA == null || IuA.isEmpty()))
	        {
	            autorizzato = false;
	            IuA = null;
	            codiceEccezione = Utility.CODICE_ECCEZIONE_MAI_POLICE_CEN_TIMEOUT;
	            descrizioneEccezione = "MAI-Police non ha ricevuto risposta dal CEN, verificare i log di MAI-Police";
	        }
	        
	        this.autorizzato = autorizzato;
	        this.IuA = IuA;
	        this.codiceEccezione = codiceEccezione;
	        this.descrizioneEccezione = descrizioneEccezione;
	    }

	    public CENAuthenticationResult() {
	    }

	    public Boolean getAutorizzato() {
	        return autorizzato;
	    }

	    public void setAutorizzato(Boolean autorizzato) {
	        this.autorizzato = autorizzato;
	    }

	    public String getIuA() {
	        return IuA;
	    }

	    public void setIuA(String IuA) {
	        this.IuA = IuA;
	    }

	    public String getCodiceEccezione() {
	        return codiceEccezione;
	    }

	    public void setCodiceEccezione(String codiceEccezione) {
	        this.codiceEccezione = codiceEccezione;
	    }

	    public String getDescrizioneEccezione() {
	        return descrizioneEccezione;
	    }

	    public void setDescrizioneEccezione(String descrizioneEccezione) {
	        this.descrizioneEccezione = descrizioneEccezione;
	    }
	   
		@Override
	    public String toString() {
	        return "AutorizzazioneVRO{" + "autorizzato=" + getAutorizzato() + ", IuA=" + getIuA() + ", codiceEccezione=" + getCodiceEccezione() + ", descrizioneEccezione=" + getDescrizioneEccezione() + '}';
	    }

}
