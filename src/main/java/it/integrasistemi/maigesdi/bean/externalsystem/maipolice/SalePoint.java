package it.integrasistemi.maigesdi.bean.externalsystem.maipolice;

//REFACTOR della classe puntoVendita del progetto APIRestGESDI
public class SalePoint {
    private Integer ID;
    private String name;
    private String address;
    private String ISTATCode;
    private String province;

    public SalePoint() {
    }

    public SalePoint(Integer id, String name, String address, String ISTATCode, String province) {
        this.ID = id;
        this.name = name;
        this.address = address;
        this.ISTATCode = ISTATCode;
        this.province = province;
    }

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getISTATCode() {
		return ISTATCode;
	}

	public void setISTATCode(String iSTATCode) {
		ISTATCode = iSTATCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

    
    
}