package it.integrasistemi.maigesdi.bean.externalsystem.maipolice;

//classe di copia della classe LuogoNascitaVRO del progetto APIRESTGESDI 
//VA FATTO UN REFACTOR
public class BirthPlaceVRO 
{
    private String nome;
    private String siglaProvincia;
    private Boolean estero;

    public BirthPlaceVRO(String nome, String siglaProvincia, Boolean estero) {
        this.nome = nome;
        this.siglaProvincia = siglaProvincia;
        this.estero = estero;
    }

    public BirthPlaceVRO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSiglaProvincia() {
        return siglaProvincia;
    }

    public void setSiglaProvincia(String siglaProvincia) {
        this.siglaProvincia = siglaProvincia;
    }

    public Boolean getEstero() {
        return estero;
    }

    public void setEstero(Boolean estero) {
        this.estero = estero;
    }
    
}
