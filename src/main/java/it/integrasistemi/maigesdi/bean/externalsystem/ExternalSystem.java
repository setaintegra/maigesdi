package it.integrasistemi.maigesdi.bean.externalsystem;

import it.integrasistemi.maigesdi.bean.Medium;

public class ExternalSystem{
	
	private Integer ID;
	private String name;
	private String description;
	private String URL;
	private String username;
	private String password;
	
	public ExternalSystem() {
		
	}
	
	@Override
	public String toString() {
		
		return "ExternalSystem: { ID: "+this.ID+
								", name: "+ this.name+
								", description: "+this.description+
								", URL: "+this.URL+
								", username: "+ this.username+
								", password: "+ this.password+
								" }";
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
	
}
