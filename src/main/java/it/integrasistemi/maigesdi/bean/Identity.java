package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Identity {

	@NotNull
	@Min(1)
	private Integer ID;
	private String name;
	
	public Identity() {
		
	}
	
	public Identity(Integer ID, String name) {
		this.ID = ID;
		this.name = name;
	}
	
	@Override
	public String toString() {

		return "Identity {" + " ID: " + ID + 
								", name: " + name 
						+"}";
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean result = false;

		if (o != null) {

			if (o == this) {
				result = true;
			} else {
				if ((o instanceof Identity)) {

					Identity identity = (Identity) o;

					result = identity.getID().equals(this.ID);

				} else {
					result = false;
				}
			}

		} else {
			result = false;
		}

		return result;
	}
	
	@Override
    public int hashCode() {
        return this.ID.hashCode();
    }
	
	
	
}
