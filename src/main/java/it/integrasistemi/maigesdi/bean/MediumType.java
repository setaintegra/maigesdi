package it.integrasistemi.maigesdi.bean;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MediumType {
	
	private Integer ID; //8
	@NotNull(message ="OrganizationID missing; is required")
	@Min(value = 1, message = "OrganizationID must be grather than zero")
	private Integer organizationID; //8
	@Size(max = 6, message="MediumType's organizationCode can't be bigger than 6 characters")
	private String organizationCode; 
	@Size(max = 60, message="MediumType's name can't be bigger than 60 characters")
	private String name; //60
	@Size(max = 3, message="MediumType's seriesCode can't be bigger than 3 characters")
	private String seriesCode; //3
	@NotNull(message="MediumType's numberDaysValidity is required")
	@Min(value= 1, message = "MediumType's numberDaysValidity must be grather than 0")
	@Max(value= 99999, message = "MediumType's numberDaysValidity must be lower than 99999")
	private Integer numberDaysValidity; //5
	private Boolean corporate;
	@Valid
	private Set<Activity> activity;
	@Valid
	@NotNull(message="externalSystem is required")
	private Identity externalSystem;
	private Set<CredentialType> credentialTypes;
		
	public MediumType() {
		
	}
	
	@Override
	public String toString() {
		
		
		return "MediumType: { ID: "+ ID +
							", name: " + name + 
							", organizationID:" + organizationID + 
							", organizationCode:" + organizationCode + 
							", seriesCode: "+seriesCode+ 
							", numberDaysValidity: "+numberDaysValidity+
							", corporate: "+corporate+
							", credentialTypes: "+ credentialTypes + //(credentialTypes != null ? credentialTypes.toString() : "NULL")+
							", externalSystem: "+ externalSystem +// (externalSystem != null ? externalSystem.toString() : "NULL")+
							", Activity: " + activity + //(activity != null ? activity.toString() : "NULL")+  
							"}"; 
		
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Integer getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(Integer organizationID) {
		this.organizationID = organizationID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public Integer getNumberDaysValidity() {
		return numberDaysValidity;
	}

	public void setNumberDaysValidity(Integer numberDaysValidity) {
		this.numberDaysValidity = numberDaysValidity;
	}

	public Boolean getCorporate() {
		return corporate;
	}

	public void setCorporate(Boolean corporate) {
		this.corporate = corporate;
	}

	public Set<Activity> getActivity() {
		return activity;
	}

	public void setActivity(Set<Activity> activity) {
		this.activity = activity;
	}

	public Identity getExternalSystem() {
		return externalSystem;
	}

	public void setExternalSystem(Identity externalSystem) {
		this.externalSystem = externalSystem;
	}

	public Set<CredentialType> getCredentialTypes() {
		return credentialTypes;
	}

	public void setCredentialTypes(Set<CredentialType> credentialTypes) {
		this.credentialTypes = credentialTypes;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}
	
	
	
	
	

}
