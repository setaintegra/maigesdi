package it.integrasistemi.maigesdi.bean;

/**
 * Classe che espone all'esterno del sistema il risultato delle richieste ai sistemi esterni, come la verifica di presenza di un supporto digitale o 
 * la verifica di validità di un'anagrafica per un'emissione di un supporto digitale
 * @author Limiti
 *
 */
public class ExternalSystemCheckingResult {
	
	private boolean result;
	private String resultDescription;
	
	public ExternalSystemCheckingResult(boolean result, String resultDescription) {
		this.result= result;
		this.resultDescription = resultDescription;
	}

	

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getResultDescription() {
		return resultDescription;
	}

	public void setResultDescription(String resultDescription) {
		this.resultDescription = resultDescription;
	}

	
}
