package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Organization {
	private Integer ID; //8
	@NotNull(message="Missing Organizzation name")
	@NotBlank(message="Missing Organizzation name")
	@Size(max = 100, message="Organization's name can't be bigger than 100 characters")
	private String name; //100
	@NotNull(message="Missing Organizzation code")
	@NotBlank(message="Missing Organizzation code")
	@Size(max = 6, message="Organization's code can't be bigger than 6 characters")
	private String code; //6
	@NotNull(message="Missing Organizzation fiscal code")
	@NotBlank(message="Missing Organizzation fiscal code")
	@Size(max = 16, message="Organization's fiscalCode can't be bigger than 16 characters")
	private String fiscalCode; //16
	@Size(max = 30, message="Organization's cenEventID can't be bigger than 30 characters")
	private String cenEventID; //30
	

	public Organization() {
		
	}
	
	public Organization(Integer id, 
						String name, 
						String coniCode, 
						String fiscalCode,
						String cenEventID) {
		this.ID = id;
		this.name = name;
		this.code = coniCode;
		this.fiscalCode = fiscalCode;
		this.cenEventID = cenEventID;
	}
	
	//costruttore per l'inserimento dell'oggetto (senza ID)
	public Organization(String name, 
						String code, 
						String fiscalCode,
						String cenEventID) {
		this.name = name;
		this.code = code;
		this.fiscalCode = fiscalCode;
		this.cenEventID = cenEventID;
	}


	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getCenEventID() {
		return cenEventID;
	}

	public void setCenEventID(String cenEventID) {
		this.cenEventID = cenEventID;
	}

	@Override
	public String toString() {
		
		return "Organization{ ID = " + this.getID()
						   +", name = " + this.getName()
						   +", code = " + this.getCode()
						   +", fiscalCode = " + this.getFiscalCode()
						   +", cenEventID = " + this.getCenEventID()
						   +"}";
	}

}
