package it.integrasistemi.maigesdi.bean;

import java.util.Date;

public class ParameterMappedDateValue {
	private Date value;
	private boolean mandatory;
	
	public ParameterMappedDateValue() {
		
	}

	public ParameterMappedDateValue(Date value, boolean mandatory) {
		
		this.value = value;
		this.mandatory = mandatory;
		
	}

	public Date getValue() {
		return value;
	}
	public void setValue(Date value) {
		this.value = value;
	}
	public Boolean getMandatory() {
		return mandatory;
	}
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}
}
