package it.integrasistemi.maigesdi.bean;

public class Paginator {

	private Integer offset;
	private Integer count;
	private Integer total;
	
	public Paginator() {
		
	}
	
	public Paginator(Integer offset, 
	                 Integer count, 
                    Integer total) {

		this.count = count;
		this.offset = offset;
		this.total = total;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	
	
}