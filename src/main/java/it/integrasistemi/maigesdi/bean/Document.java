package it.integrasistemi.maigesdi.bean;

import java.util.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;

public class Document {

	@Size(max = 26, message="Document's documentType can't be bigger than 26 characters")
	private String documentType; //26
	@Size(max = 15, message="Document's code can't be bigger than 50 characters")
	private String code; //15
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date releaseDate;
	@Size(max = 50, message="Document's authority can't be bigger than 50 characters")
	private String authority; //50
	@JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
	private Date expirationDate;

	public Document() {

	}
	
	@Override
	public String toString() {
		
		return "Document: { documentType: "+ 	documentType	   +
						", code: " + 			code		  	   +
				 	 	", releaseDate: " +     releaseDate        +
						", authority: " +       authority          +
						", expirationDate: " + 	expirationDate	   +
				 	 	"}"; 
		
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

}
