package it.integrasistemi.maigesdi.bean;

public class StateType {
	
	private Integer ID;
	private String name;
	private String description;
	
	public StateType() {
		
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
