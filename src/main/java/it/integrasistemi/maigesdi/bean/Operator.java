package it.integrasistemi.maigesdi.bean;

import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Operator {

	private Integer ID; //8
	@NotNull(message="Operator username is required")
	@NotBlank(message="Operator username must be not blank")
	@Size(min =1, max=100, message="Operator username must be not empty, can't be bigger than 100 characters")
	private String username;  //100
	@NotNull(message="Operator password is required")
	@NotBlank(message="Operator password must be not blank")
	@Size(min =1, max=100, message="Operator password must be not empty, can't be bigger than 100 characters")
	private String password;   //100
 	private Integer profileID; //8 
	private Set<Identity> MediumTypes;
	
	public static enum Profile{
		//AMMINISTRATOR(1),
		CONFIGURATOR(5),
		USER(6);
		
		private Integer value;

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}
		
        private Profile(Integer value) {
			 this.value = value;
		 }
	}
	
	@Override
	public String toString() {

		return "Operator {" + 
				" ID: " + ID +
				", username: " + username + 
				", profileID: " + profileID + 
				", MediumTypes: "+ MediumTypes + 
				"}";
	}

	
	
	public Operator() {

	}

	public Operator(String username, String password) {
		this.password = password;
		this.username = username;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer operatorID) {
		ID = operatorID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getProfileID() {
		return profileID;
	}

	public void setProfileID(Integer profileID) {
		this.profileID = profileID;
	}

	public Set<Identity> getMediumTypes() {
		return MediumTypes;
	}

	public void setMediumTypes(Set<Identity> mediumTypes) {
		MediumTypes = mediumTypes;
	}

	
	

}
