package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Parameter {
	private Integer ID;  //10
	@Size(max = 255, message="Parameter's description can't be bigger than 255 characters")
	private String  description;  //255
	@NotNull(message="Parameter's code is required")
	@NotBlank(message="Parameter's code is required")
	@Size(min=1, max=30 , message="Parameter's code is required, can't be bigger than 30characters")
	private String  code;  //30
	@NotNull(message="Parameter's mandatory is required")
	private Boolean mandatory;
	
	
	@Override
	public String toString() {
	
		return "Parameter {"+
							" ID: "+ID+
							", code: "+ code +
							", description: "+description +
							", mandatory: "+mandatory+
							"}";
	}
	
	public Parameter() {
		
	}
	
	public Parameter(Integer id, String code, String description, Boolean mandatory) {
		this.ID = id;
		this.description = description;
		this.code = code;
		this.mandatory = mandatory;
	}
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Boolean getMandatory() {
		return mandatory;
	}
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	
	@Override
	public boolean equals(Object o) {
		boolean result = false;

		if (o != null) {

			if (o == this) {
				result = true;
			} else {
				if ((o instanceof Parameter)) {

					Parameter param = (Parameter) o;

					result = param.getCode().toUpperCase().equals(this.code.toUpperCase());

				} else {
					result = false;
				}
			}

		} else {
			result = false;
		}

		return result;
	}

	@Override
    public int hashCode() {
        return this.code.toUpperCase().hashCode();
    }
	

}
