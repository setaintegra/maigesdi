package it.integrasistemi.maigesdi.bean;

import javax.validation.constraints.Size;

public class ActivityType {

	private Integer ID;  //10
	@Size(max = 30, message="ActivityType's code can't be bigger than 30 characters")
	private String code; //30
	@Size(max = 255, message="ActivityType's description can't be bigger than 255 characters")
	private String description; //255
	
	public ActivityType() {
		
	}
	
	@Override
	public String toString() {

		return "ActivityType {" + " ID: " + ID + ", code: " + code + ", description: " + description+"}";
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
