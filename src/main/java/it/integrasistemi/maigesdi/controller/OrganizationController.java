package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.OrganizationService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;


@RestController
@Validated
@RequestMapping("MAIGeSDi/V2/organizations")
public class OrganizationController{
	private static final Logger logger = LoggerFactory.getLogger(OrganizationController.class);
	
	@Autowired
	private OrganizationService service;

	@Autowired
	private UtilityService utilityService;
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves all the Operator's Organization",
        notes = "Username and password are required",
        response = Organization.class,
        responseContainer="List"
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 201, response = Organization.class,  responseContainer="List", message = "Created"),
				@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
				}
			)
	public ResponseEntity<?> getOrganizations(@RequestParam("username") String username, 
												@RequestParam("password") String password, 
												@RequestParam(name="traceLogID", required=false) String traceLogID) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		ErrorDetails errorDetails = null;
		List<Organization> organizations = null;
		
		try {
									
			organizations = this.service.getOrganizations(new Operator(username,password));
		    
			result = new ResponseEntity<List<Organization>>(organizations, HttpStatus.OK);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End getOrganizations");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;

	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Get a specific Organization by Organization ID",
        notes = "ID must be grather than zero; Username and password are required ",
        response = Organization.class
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 201, response = Organization.class, message = "Created"),
				@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
				}
			)
	public ResponseEntity<?> getOrganizationByID(@RequestParam("username") String username, 
												 @RequestParam("password") String password, 
												 @RequestParam(name="traceLogID", required=false) String traceLogID ,
												 @PathVariable("id") Integer id) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start getOrganizationByID; Request: ID: "+id );
		
		ResponseEntity<?> result;
		Organization organization = null;
		ErrorDetails errorDetails = null;
		try {
				
			organization= this.service.getOrganizationByID(new Operator(username, password), id);
			
			result = new ResponseEntity<Organization>(organization, HttpStatus.OK);
			
			logger.info("getOrganizationByID Repsonse: Organization: "+organization.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End getOrganizationByID");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				

	}
	
	
	@RequestMapping(value="", method = RequestMethod.PUT)
	@ApiOperation
    (
        value = "Update an existing Organization",
        notes = "The Organization ID, username and password are required; You cannot update coniCode and cenEventID",
        response = Organization.class
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 201, response = Organization.class, message = "Created"),
				@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
				}
			)
	public ResponseEntity<?> updateOrganization(@RequestParam("username") String username, 
												@RequestParam("password") String password, 
												@RequestParam(name="traceLogID", required=false) String traceLogID ,
												@RequestBody Organization organization) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start getOrganizationByID; Request: "+ organization.toString());
		ResponseEntity<?> result;
		ErrorDetails errorDetails = null;
		try {
			
			organization= this.service.updateOrganization(new Operator(username, password), organization);
			
			result = new ResponseEntity<Organization>(organization, HttpStatus.OK);
			
			logger.info("getOrganizationByID Response: "+result.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}
	
	
	@RequestMapping(value="", method = RequestMethod.POST)
	@ApiOperation
    (
        value = "Add a new Organization",
        notes = "All organization's variable are required except the ID and cenEventID; Username and password are required too",
        response = Organization.class
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 201, response = Organization.class, message = "Created"),
				@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
				}
			)
	public ResponseEntity<?> addOrganization(@RequestParam("username") String username, 
											 @RequestParam("password") String password, 
											@RequestParam(name="traceLogID", required=false) String traceLogID , 
											@RequestBody Organization organization) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start addOrganization; Request: "+ organization.toString());
		ResponseEntity<?> result;
		Organization returnedOrganization=null;		
		ErrorDetails errorDetails = null;
		try {
			
			returnedOrganization = this.service.addOrganization(new Operator(username, password), organization);
			
			result = new ResponseEntity<Organization>(returnedOrganization, HttpStatus.CREATED);
			
			logger.info("Response addOrganizationResponse: "+ returnedOrganization.toString());
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End addOrganization");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}
	

	
}
