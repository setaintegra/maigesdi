package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@RestController
@RequestMapping("MAIGeSDi/V2")
public class UtilityController {

	private static final Logger logger = LoggerFactory.getLogger(VersionController.class);

	@Autowired
	private UtilityService utilityService;

	@ApiOperation
    (
        value = "Check the system status",
        response = Integer.class
    )
	@ApiResponses( value = {
		@ApiResponse(code = 200, response = Integer.class, message = "OK"),
		@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
		}
	)
	@RequestMapping(value = "/keepAlive", method = RequestMethod.GET)
	public ResponseEntity<?> keepAlive() {
		logger.info("Start");
		ResponseEntity<?> result;
		Integer returnValue = 0;

		try {
			
			returnValue = this.utilityService.checkDbConnection();
			result = new ResponseEntity<Integer>(returnValue, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			result = new ResponseEntity<ErrorDetails>( new ErrorDetails(new Date(), e.getMessage() , Utility.ErrorType.ET_GENERIC_ERROR.getValue()), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info("End");
		return result;

	}
	
}
	

