package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ExternalSystemCheckingResult;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.ShortMediumData;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.ExternalSystemService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@RestController
@Validated
@RequestMapping("MAIGeSDi/V2/externalSystems")
public class ExternalSystemController {

	private static final Logger logger = LoggerFactory.getLogger(ExternalSystemController.class);

	@Autowired
	private ExternalSystemService service;
	
	@Autowired
	private UtilityService utilityService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves all the Operator's ExternalSystems",
        notes = "Username and password are required",
        response = ExternalSystem.class,
        responseContainer="List"
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 200, response = ExternalSystem.class, responseContainer ="List", message = "OK"),
				@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getExternalSystems(@RequestParam("username") String username, 
												@RequestParam("password") String password, 
												@RequestParam(name="traceLogID", required=false) String traceLogID)
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		List<ExternalSystem> externalSystem = null;
		ErrorDetails errorDetails = null;
		
		try {
						
			externalSystem = this.service.getExternalSystems(new Operator(username,password));
		    
			result = new ResponseEntity<List<ExternalSystem>>(externalSystem, HttpStatus.OK);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;

	}
	

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation
    (
    		value = "Retrieves a specific  Operator's ExternalSystem ",
            notes = "Username, password and ID are required",
            response = ExternalSystem.class
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 200, response = ExternalSystem.class,  message = "OK"),
				@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getExternalSystemByID(@RequestParam("username") String username, 
												 	@RequestParam("password") String password, 
													@RequestParam(name="traceLogID", required=false) String traceLogID, 
													@PathVariable("id") Integer id) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();	
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		ExternalSystem externalSystem = null;
		ErrorDetails errorDetails = null;
		
		try {
												
			externalSystem = this.service.getExternalSystemByID(new Operator(username,password), id);
		    
			result = new ResponseEntity<ExternalSystem>(externalSystem, HttpStatus.OK);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;

	}
	
	@RequestMapping(value="/{id}/authorizations", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Check if the Person is valid for the specific ExternalSystem",
        notes = "Check if the Person is valid for the specific ExternalSystem, NOW WORK ONLY ON VRO (ExternalSystem MAIGESDI_CEN and EXTERNAL_CEN)  "+
        		"<br> All parameters are required, except traceLogID"+
        		"<br> The format of birthDateis yyyy-MM-dd"+
        		"<br> The only two charachters accepted for the parameter gender are 'M' and 'F'  ",
        response = ExternalSystemCheckingResult.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = ExternalSystemCheckingResult.class,  message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> checkPersonOnExternalSystem(	@RequestParam("username") String username, 
															@RequestParam("password") String password, 
															@RequestParam(name="traceLogID", required=false) String traceLogID , 
															@PathVariable("id") Integer id,
															@RequestParam("fiscalCodeBirthPlace") String fiscalCodeBirthPlace,
															@RequestParam("givenName") String givenName,
															@RequestParam("familyName") String familyName,
															@DateTimeFormat(pattern = "yyyy-MM-dd")
															@RequestParam("birthDate") Date birthDate,
															@RequestParam("gender") String gender) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: ");
		ResponseEntity<?> result;
		ExternalSystemCheckingResult checkResult = null;		
		ErrorDetails errorDetails = null;
		
		try {
			
			checkResult = this.service.checkPersonOnExternalSystem(new Operator(username, password), id, fiscalCodeBirthPlace,givenName, familyName, birthDate, gender);
			
			result = new ResponseEntity<ExternalSystemCheckingResult>(checkResult, HttpStatus.OK);
			
			logger.info("Response : "+ result);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		return result;
	}
	
	@RequestMapping(value="/{id}/validity", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Check if exist a medium on ExternalSystem with provided data",
        notes = "Check if exist a medium on ExternalSystem with provided data, NOW WORK ONLY ON VRO (ExternalSystem MAIGESDI_CEN and EXTERNAL_CEN)  "+
        		"<br> All parameters are required, except traceLogID"+
        		"<br> The format of birthDate and validTo is yyyy-MM-dd"+
        		"<br> The only two charachters accepted for the parameter gender are 'M' and 'F'",
        response = ExternalSystemCheckingResult.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = ExternalSystemCheckingResult.class,  message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> checkExistingMediumOnExternalSystem(	@RequestParam("username") String username, 
																	@RequestParam("password") String password, 
																	@RequestParam(name="traceLogID", required=false) String traceLogID , 
																	@PathVariable("id") Integer id,
																	@RequestParam("mediumIdentifier") String mediumIdentifier,
																	@RequestParam("givenName") String givenName,
																	@RequestParam("familyName") String familyName,
																	@RequestParam("fiscalCodeBirthPlace") String fiscalCodeBirthPlace,
																	@DateTimeFormat(pattern = "yyyy-MM-dd")
																	@RequestParam("birthDate") Date birthDate,
																	@RequestParam("gender") String gender,
																	@DateTimeFormat(pattern = "yyyy-MM-dd")
																	@RequestParam("validTo") Date validTo) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		//logger.info("Start; Request: "+ mediumData);
		ResponseEntity<?> result;
		ExternalSystemCheckingResult checkResult = null;		
		ErrorDetails errorDetails = null;
		
		try {
			
			checkResult = this.service.checkExistingMediumOnExternalSystem(new Operator(username, password), 
																			id, 
																			mediumIdentifier,
																			givenName,
																			familyName,
																			fiscalCodeBirthPlace,
																			birthDate,
																			gender,
																			validTo);
			
			result = new ResponseEntity<ExternalSystemCheckingResult>(checkResult, HttpStatus.OK);
			
			logger.info("Response : "+ result);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}
	
	
}
