package it.integrasistemi.maigesdi.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Version;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;

@RestController
@RequestMapping("/version")
public class VersionController {
	private static final Logger logger = LoggerFactory.getLogger(VersionController.class);

	@Autowired
	UtilityService service;
	
	@ApiOperation
    (
        value = "Get the system version",
        response = Version.class
    )
	@ApiResponses( 
		value = {
				@ApiResponse(code = 200, response = Version.class, message = "OK"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
		}
	)
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getVersion() {
		logger.info("Start getVersion");
		ResponseEntity<?> result;
		Version version = null;

		try {

			//version = new Version("0.0.1", "V1", "01/06/2018", "MAIGESDI first release");
			//version = new Version("0.0.2", "V1", "15/06/2018", "MAIGESDI second release");
			//version = new Version("0.0.3", "V1", "10/08/2018", "MAIGESDI third release");
			//version = new Version("1.0.0", "V1", "17/09/2018", "MAIGESDI fourth release; Version redy for NEW STD");
			//version = new Version("2.0.0", "V2", "02/10/2018", "MAIGESDI fifth release; Version ready for NEW STD");
			//version = new Version("2.1.0", "V2", "15/01/2019", "MAIGESDI sixth release; Version ready for NEW STD");
			//version = new Version("2.2.0", "V2", "09/04/2019", "MAIGESDI seventh release; added route GET /MAIGeSDi/V2/mediumTypes/codes");
			//version = new Version("2.3.0", "V2", "29/04/2019", "MAIGESDI seventh release; added SIGN and TUTOR_SIGN");
			//version = new Version("2.3.1", "V2", "16/05/2019", "MAIGESDI eighth release; DISABLE logic changed for IN_LAVORAZIONE - ATTESA_CONFERAMA_BO medium ");
			//version = new Version("2.3.2", "V2", "21/05/2019", "MAIGESDI ninth release; Medium validTo always valorized by MediumType numberDaysOfValidity");
			//version = new Version("2.3.3", "V2", "24/05/2019", "MAIGESDI tenth release; MediumType's IDCODICEPRIMARIO handled");
//			version = new Version("2.3.4", "V2", "27/05/2019", "MAIGESDI eleventh release; Bugfix medium LUOGO and DATANASCITA");
//			version = new Version("2.3.5", "V2", "20/06/2019", "MAIGESDI twelfth release; Bugfix create MediumIdentifierRange");
//			version = new Version("2.3.6", "V2", "25/07/2019", "MAIGESDI thirteenth release; Bugfix duplicate CENExceptionDateTime");
			//version = new Version("2.3.7", "V2", "27/08/2019", "MAIGESDI fourteenth release; Bugfix restore expired merdium ");
//			version = new Version("2.3.8", "V2", "11/10/2019", "Bugfix medium parameter conversion (DEV-1306) ");
//			version = new Version("3.0.0", "V2", "28/08/2019", "MAIGESDI DEV-979 ready");
//			version = new Version("3.1.0", "V2", "25/02/2020", "MAIGESDI DEV-1622 ready");
			version = new Version(service.readMavenVersion(), "V2", "25/02/2020", "MAIGESDI DEV-1622 ready");
			
			result = new ResponseEntity<Version>(version,HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			result = new ResponseEntity<ErrorDetails>( new ErrorDetails(new Date(), e.getMessage() , Utility.ErrorType.ET_GENERIC_ERROR.getValue()), HttpStatus.INTERNAL_SERVER_ERROR);
		
        }
		
		logger.info("End getVersion");
		return result;

	}

}
