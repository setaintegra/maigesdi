package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.MediumTypeService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@RestController
@Validated
@RequestMapping("MAIGeSDi/V2/mediumTypes")
public class MediumTypeController {
	
private static final Logger logger = LoggerFactory.getLogger(MediumTypeController.class);
	
	@Autowired
	private MediumTypeService service;
	
	@Autowired
	private UtilityService utilityService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves all the Operator's MediumType",
        notes = "Username and password are required",
        response = MediumType.class,
        responseContainer="List"
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumType.class, responseContainer ="List", message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumTypes(@RequestParam("username") String username, 
											@RequestParam("password") String password, 
											@RequestParam(name="traceLogID", required=false) String traceLogID) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
				
		logger.info("Start");
		ResponseEntity<?> result;
		List<MediumType> mediumTypes = null;
		ErrorDetails errorDetails = null;
		
		try {
			
			mediumTypes = this.service.getMediumTypes(new Operator(username,password));
		    
			result = new ResponseEntity<List<MediumType>>(mediumTypes, HttpStatus.OK);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (DBMSFaultException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;

	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieve a specific MediumType by id",
        notes = "Username and password are required",
        response = MediumType.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumType.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumTypesByID(@RequestParam("username") String username, 
												@RequestParam("password") String password, 
												@RequestParam(name="traceLogID", required=false) String traceLogID, 
												@PathVariable("id") Integer id) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
				
		logger.info("Start");
		ResponseEntity<?> result;
		MediumType mediumType = null;
		ErrorDetails errorDetails = null;
		
		try {
			
			mediumType = this.service.getMediumTypesByID(new Operator(username,password), id);
		    
			result = new ResponseEntity<MediumType>(mediumType, HttpStatus.OK);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		return result;

	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	@ApiOperation
    (
        value = "Add a new Medium Type",
        notes = "All medium type's variable are required except the ID;"
        		+ "<br> ExternalSystem is required, in particular the ExternalSystem's ID is required  "     
        		+ "<br> ActivityTypes are not required, in case they are defined ActivityType's code is required  "        
        		+ "<br> ActivityTypes must be unique, you cannot define the same ActivityType many times for the MediumType; duplicate will be ignorated"
        		+ "<br> Parameter's code and mandatory variables are required"
        		+ "<br> Parameters must be unique, you cannot define the same Parameter many times for the ActivityTipe; duplicate will be ignorated"
        		+ "<br> Username and password are required too, Only an Operator whit Configurator's right can add a new MediumType",
        response = MediumType.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 201, response = MediumType.class, message = "Created"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> addMediumType(	@NotNull @NotBlank @RequestParam("username") String username, 
											@NotNull @NotBlank @RequestParam("password") String password, 
											@RequestParam(name="traceLogID", required=false) String traceLogID ,
											@Valid @RequestBody MediumType mediumType) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: "+ mediumType);
		ResponseEntity<?> result;
		MediumType returnedMediumType = null;		
		ErrorDetails errorDetails = null;
		
		try {
			
			returnedMediumType = this.service.addMediumType(new Operator(username, password), mediumType);
			
			result = new ResponseEntity<MediumType>(returnedMediumType, HttpStatus.CREATED);
			
			logger.info("Response addOrganizationResponse: "+ returnedMediumType.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}
	
	@RequestMapping(value="", method = RequestMethod.PUT)
	@ApiOperation
    (
        value = "Update an existing Medium Type",
	    notes = "Only the Medium Type's ID is required; It's possible update only Name, SeriesCode, NumberDaysValidity and ExternalSystemID Medium Type's variable."
        		+ "<br> Activitys are not required, in case they are defined Activity's code is required  "        
        		+ "<br> Activitys must be unique, you cannot define the same Activity many times for the MediumType; duplicate will be ignorated"
        		+ "<br> Parameter's code and mandatory variables are required"
        		+ "<br> Parameters must be unique, you cannot define the same Parameter many times for the Activity; duplicate will be ignorated."
        		+ "<br> All Activitys and Parameter will be replaced ;"
        		+ "<br> Username and password are required too, Only an Operator whit Configurator's right can upadate an existing MediumType",


        response = MediumType.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumType.class, message = "OK"),
			@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> updateMediumType(	@RequestParam("username") String username, 
												@RequestParam("password") String password, 
												@RequestParam(name="traceLogID", required=false) String traceLogID,
												@RequestBody MediumType mediumType) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: "+ mediumType);
		ResponseEntity<?> result;
		MediumType returnedMediumType = null;		
		ErrorDetails errorDetails = null;
		
		try {
			
			returnedMediumType = this.service.updateMediumType(new Operator(username, password), mediumType);
			
			result = new ResponseEntity<MediumType>(returnedMediumType, HttpStatus.OK);
			
			logger.info("Response updateMediumType: "+ returnedMediumType.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}
	
	@RequestMapping(value = "/mediumIdentifier", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves all the Operator's MediumType which have a medium with a specific medium identifier",
        notes = "Username and password are required, medium identifier is required too",
        response = MediumType.class,
        responseContainer="List"
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 200, response = MediumType.class, responseContainer ="List", message = "OK"),
				@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumTypesByMediumIdentifier(@RequestParam("username") String username, 
															  @RequestParam("password") String password, 
															  @RequestParam(name ="traceLogID", required=false) String traceLogID, 
															  @RequestParam("mediumIdentifier") String mediumIdentifier) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
				
		logger.info("Start; INPUT: MediumIdentifier: "+mediumIdentifier);
		ResponseEntity<?> result;
		List<MediumType> mediumTypes = null;
		ErrorDetails errorDetails = null;
		
		try {
									
			mediumTypes = this.service.getMediumTypesByMediumIdentifier(new Operator(username,password), mediumIdentifier);
		    
			result = new ResponseEntity<List<MediumType>>(mediumTypes, HttpStatus.OK);
			logger.info("OUTPUT: List mediumType: "+ mediumTypes);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		//logger.info("End");
		return result;

	}
	
	@RequestMapping(value = "/codes", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves the Operator's MediumType which a specific organizationCode and seriesCode pair",
        notes = "Username and password are required, organizationCode and seriesCpde are required too",
        response = MediumType.class
    )
	@ApiResponses( 
			value = {
				@ApiResponse(code = 200, response = MediumType.class, message = "OK"),
				@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
				@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
				@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
				@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
				@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
				@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	
	public ResponseEntity<?> getMediumTypesByCodes(@RequestParam("username") String username, 
															  @RequestParam("password") String password, 
															  @RequestParam(name ="traceLogID", required=false) String traceLogID, 
															  @RequestParam("organizationCode") String organizationCode,
															  @RequestParam("seriesCode") String seriesCode) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
				
		logger.info("Start; INPUT: organizationCode: "+ organizationCode + " seriesCode: " + seriesCode);
		ResponseEntity<?> result;
		MediumType mediumType = null;
		ErrorDetails errorDetails = null;
		
		try {
									
			mediumType = this.service.getMediumTypesByCodes(new Operator(username,password), organizationCode, seriesCode);
		    
			result = new ResponseEntity<MediumType>(mediumType, HttpStatus.OK);
			logger.info("OUTPUT: mediumType: "+ mediumType);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		//logger.info("End");
		return result;

	}
	

}
