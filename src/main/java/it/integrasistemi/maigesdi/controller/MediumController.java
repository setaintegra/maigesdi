package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.Document;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumReportContainer;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.PersonValidityBody;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.MediumService;
import it.integrasistemi.maigesdi.businesslogic.MediumTypeService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@RestController
@Validated
@RequestMapping("MAIGeSDi/V2/medium")
public class MediumController {

	private static final Logger logger = LoggerFactory.getLogger(MediumController.class);
	
	
	@Autowired
	private MediumService service;
	
	@Autowired
	private UtilityService utilityService;
	
	@RequestMapping(value = "/{mediumID}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves a specific Medium",
        notes = "Username and password are required",
        response = Medium.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Medium.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMedium(@RequestParam("username") String username, 
									 	@RequestParam("password") String password,
										@RequestParam(name="traceLogID", required=false) String traceLogID,
										@PathVariable("mediumID") Integer mediumID
										) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		Medium returnedMedium =null;
		ErrorDetails errorDetails = null;
		
		try {
			returnedMedium = this.service.getMedium(new Operator(username, password), mediumID);
			
			result = new ResponseEntity<Medium>(returnedMedium, HttpStatus.OK);
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/photo/{mediumID}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves the photo of the specific Medium",
        notes = "This service can retrieves a photo of the medium, encoded in Base64;  "+
        		"<br>Username, password and MediumID are required; ",
        response = String.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = String.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumPhoto(@RequestParam("username") String username, 
											@RequestParam("password") String password,
											@RequestParam(name="traceLogID", required=false) String traceLogID,
											@PathVariable("mediumID") @Valid @NotNull @Min(1) Integer mediumID
										) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		String photoResult =null;
		ErrorDetails errorDetails = null;
		
		try {
			
			photoResult = this.service.getMediumPhoto(new Operator(username, password), mediumID);
			
			result = new ResponseEntity<String>(photoResult, HttpStatus.OK);
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/sign/{mediumID}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves the sign of the specific Medium",
        notes = "This service can retrieves a sign of the medium, encoded in Base64;  "+
        		"<br>Username, password and MediumID are required; ",
        response = String.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = String.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumSign(@RequestParam("username") String username, 
											@RequestParam("password") String password,
											@RequestParam(name="traceLogID", required=false) String traceLogID,
											@PathVariable("mediumID") @Valid @NotNull @Min(1) Integer mediumID
										) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		String signResult =null;
		ErrorDetails errorDetails = null;
		
		try {
			signResult = this.service.getMediumSign(new Operator(username, password), mediumID);
			
			result = new ResponseEntity<String>(signResult, HttpStatus.OK);
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/tutorsign/{mediumID}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves the tutor sign of the specific Medium",
        notes = "This service can retrieves a tutor sign of the medium, encoded in Base64;  "+
        		"<br>Username, password and MediumID are required; ",
        response = String.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = String.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumTutorSign(@RequestParam("username") String username, 
												@RequestParam("password") String password,
												@RequestParam(name="traceLogID", required=false) String traceLogID,
												@PathVariable("mediumID") @Valid @NotNull @Min(1) Integer mediumID
										) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		String tutorSignResult =null;
		ErrorDetails errorDetails = null;
		
		try {
			tutorSignResult = this.service.getMediumTutorSign(new Operator(username, password), mediumID);
			
			result = new ResponseEntity<String>(tutorSignResult, HttpStatus.OK);
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/{mediumTypeID}/{mediumIdentifier}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieves a specific Medium",
        notes = "Medium type id and medium identifier are requred; Username and password are required too",
        response = Medium.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Medium.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumByMediumTypeIDMediumIdentifier(	@RequestParam("username") String username, 
																		@RequestParam("password") String password, 
																		@RequestParam(name="traceLogID", required=false) String traceLogID,
																		@PathVariable("mediumTypeID") Integer mediumTypeID,
																		@PathVariable("mediumIdentifier") String mediumIdentifier
																	) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start getMedium; INPUT: MediumTypeID: "+mediumTypeID+" MediumIdentifier: "+mediumIdentifier);
		ResponseEntity<?> result;
		Medium returnedMedium =null;
		ErrorDetails errorDetails = null;
		
		try {
				
			returnedMedium = this.service.getMedium(new Operator(username, password), mediumTypeID, mediumIdentifier);
			
			result = new ResponseEntity<Medium>(returnedMedium, HttpStatus.OK);
			logger.info("OUTPUT: Medium result: "+returnedMedium);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	

	@RequestMapping(value="", method = RequestMethod.POST)
	@ApiOperation
    (
        value = "Handle all the activities of the medium ",
        notes = "This specific service can handle all the operations of the medium;  "+
        		"<br> For each macro operation there is an ActivityType which describes its behavior; in details, ActvityType have a Code and a Description;" +
        		"<br> All the ActivityType are: "+
        		"<br> &emsp;   - NEW : Create a new medium "+	
        		"<br> &emsp;   - NEW_WEB : Create a new medium from WEB, the created medium will be checked from a BO operator cause its state is not VALIDO but IN_LAVORAZIONE"+
        		"<br> &emsp;   - SEARCH :  Search (CUSTOM) mediums; research for state and operativeFlow are not allowed "+
        		"<br> &emsp;   - MODIFY : Edit a medium"+
        		"<br> &emsp;   - DISABLE : Disable a medium in MAIGeSDi and on external system (like VRO), the reason of the disabling must be chosen (they are Rinunciato, Smarrito, Rubato, Danneggiato, Errato, RETTIFICATO_CEN)"+
        		"<br> &emsp;   - RESTORE : Restore a medium disabled, in MAIGeSDi and on external system (like VRO), at the same time it's possible update the medium to be restored"+
        		"<br> &emsp;   - VERIFY : Verify a medium, it not means verify the state but verify if exist in MAIGeSDi and on external system (like VRO)"+
        		"<br> &emsp;   - ACTIVATE_ON_EXTERNALSYSTEM : Activate an existing active medium (only on MAIGeSDi) on external system VRO; VRO NOT KNOW this MAIGeSDi's active medium"+
        		"<br> "+
        		"<br> All these ActivityType have, one or more, Activity that allow to define different types of operation of the same ActivityType;"+
        		"<br> for example, for the ActivtyType NEW you can have more Activity, one for creating a medium for a physical person and one for creating mediums for a non-physical person"+
        		"<br> The Activity an their code are: "+
        		"<br> &emsp;   - NEW_PHISICAL_PERSON	: Create a new medium for a phisical person "+ 
        		"<br> &emsp;   - NEW_NOT_PHISICAL_PERSON : Create a new medium not for a phisical person "+ 
        		"<br> &emsp;   - SEARCH : Search a subset of medium by one o more Parameter (like GivenName or Birthdate); research for state and operativeFlow are not allowed "+ 
        		"<br> &emsp;   - MODIFY : Edit an existing medium "+ 
        		"<br> &emsp;   - NEW_WEB_PHISICAL_PERSON : Create a new medium from web for a phisical person, the created medium will be checked from a BO operator cause its state is not VALIDO but IN_LAVORAZIONE "+ 
        		"<br> &emsp;   - DISABLE	: Disable a valid medium in MAIGeSDi and on external system (like VRO) "+ 
        		"<br> &emsp;   - VERIFY : Verify a existing medium, it not means verify the state but verify if exist in MAIGeSDi and on external system (like VRO) "+ 
        		"<br> &emsp;   - RESTORE : Restore a disabled medium in MAIGeSDi and on external system (like VRO), at the same time it's possible update the medium to be restored "+ 
        		"<br> &emsp;   - ACTIVATE_ON_EXTERNALSYSTEM : Activate an existing active medium (only on MAIGeSDi) on external system VRO; VRO NOT KNOW this MAIGeSDi's active medium"+
        		"<br> "+
        		"<br> In every call must be provided an Activity code and a Medium, wrapped in an object called MediumPost"+
        		"<br> The list of mandatary parameters of medium for a specific Activity is defined in the MediumType's configuration;  "+
        		"<br> The format of the parameters 'birthDate', 'releaseDate', 'validFrom' and 'validTo' is yyyy-MM-dd"+
        		"<br> The only two charachters accepted for the parameter 'Gender' are 'M' and 'F'  "+
        		"<br> The parameter 'photo' is encoded in Base64  "+
        		"<br> The parameter 'sign' is encoded in Base64  "+
        		"<br> The mediumType is always required "
        		,
        		
        response = Medium.class,
        responseContainer="List"
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Medium.class, responseContainer ="List", message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> handleMediumActivity(	@RequestParam("username") String username, 
													@RequestParam("password") String password, 
													@RequestParam(name="traceLogID", required=false) String traceLogID , 
													@RequestBody MediumPost mediumPost) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: "+ mediumPost.toString());
		ResponseEntity<?> result;
		List<Medium> returnedMediums = null;		
		ErrorDetails errorDetails = null;
		
		try {
			
			returnedMediums = this.service.handleMediumActivity(new Operator(username, password), mediumPost);
			
			result = new ResponseEntity<List<Medium>>(returnedMediums, HttpStatus.OK);
			
			logger.info("Response : "+ returnedMediums.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}
	
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieve a collection of Medium that have properties provided  ",
        notes = "This service allows to search medium using the supplied parameters as filters"
        		+ "<br> An operator can only search for mediums on which he has rights"
        		+ "<br> Here all parameter with a short description: "
        		+ "<br> &emsp;   - mediumTypeID: discretionary parameter for mediumType ID, the primary key of mediumType"
	 			+ "<br> &emsp;   - mediumIdentifier: discretionary parameter that rapresent medium identifier like 016110003887"
				+ "<br> &emsp;   - releaseDateFrom & releaseDateTo: discretionary date range for release date, their format are yyyy-MM-dd; "
				+ "<br> &emsp; &emsp; if only one parameter of these will be set, the missing date will be managed like these examples:"
				+ "<br> &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; - releaseDateFrom provided and releaseDateTo null: range will be releaseDateFrom - 2100-01-01 "
				+ "<br> &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; - releaseDateFrom null and releaseDateTo provided: range will be 1900-01-01 - releaseDateTo "
				+ "<br> &emsp;   - expiryDateFrom &  expiryDateTo: discretionary date range for validTo date, their format are yyyy-MM-dd; " 
				+ "<br> &emsp; &emsp; if only one parameter of these will be set, the missing date will be managed like releaseDate examples"
				+ "<br> &emsp;   - stateTypeID: discretionary parameter for stateType"
				+ "<br> &emsp;   - operativeFlowTypeID: discretionary parameter for operativeFlowType"
				+ "<br> &emsp;   - lastTransitionOperatorID: discretionary parameter for operator who made the last change of stateType or operativeflowType "
				+ "<br> &emsp;   - givenName: discretionary parameter for given name of the medium holder"
				+ "<br> &emsp;   - familyName: discretionary parameter for family name of the medium holder"
				+ "<br> &emsp;   - fiscalCodeBirthPlace: discretionary parameter for fiscal code birthplace of the medium holder"
				+ "<br> &emsp;   - birthDate: discretionary parameter for birtdate of the medium holder, its format is yyyy-MM-dd"
				+ "<br> &emsp;   - emitterOperatorID: discretionary parameter for the emitter operator that is the one who created the first state "
				+ "<br> &emsp;   - offset: discretionary parameter for pagination, it indicate the number of the item to search from "
				+ "<br> &emsp;   - maxResults: mandatory parameter for pagination, it indicate the maximum number of results to show"
        		+ "<br> "
        		+ "Username, password and maxResults are required",
        response = MediumReportContainer.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumReportContainer.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> searchMediumCustom(@RequestParam("username") String username, 
									 			@RequestParam("password") String password,
									 			@RequestParam(name="traceLogID", required=false) String traceLogID,
									 			@RequestParam(name="mediumTypeID", required=false) Integer mediumTypeID,
									 			@RequestParam(name="mediumIdentifier", required=false) String mediumIdentifier,
									 			@DateTimeFormat(pattern = "yyyy-MM-dd")
												@RequestParam(name="releaseDateFrom", required=false) Date releaseDateFrom,
												@DateTimeFormat(pattern = "yyyy-MM-dd")
												@RequestParam(name="releaseDateTo", required=false) Date releaseDateTo,
												@DateTimeFormat(pattern = "yyyy-MM-dd")
												@RequestParam(name="expiryDateFrom", required=false) Date expiryDateFrom,
												@DateTimeFormat(pattern = "yyyy-MM-dd")
												@RequestParam(name="expiryDateTo", required=false) Date expiryDateTo,
												@RequestParam(name="stateTypeID", required=false) Integer stateTypeID,
												@RequestParam(name="operativeFlowTypeID", required=false) Integer operativeFlowTypeID,
												@RequestParam(name="lastTransitionOperatorID", required=false) Integer lastTransitionOperatorID,
												@RequestParam(name="givenName", required=false) String givenName,
												@RequestParam(name="familyName", required=false) String familyName,
												@RequestParam(name="fiscalCodeBirthPlace", required=false) String fiscalCodeBirthPlace,
												@DateTimeFormat(pattern = "yyyy-MM-dd")
												@RequestParam(name="birthDate", required=false) Date birthDate,
												@RequestParam(name="emitterOperatorID", required=false) Integer emitterOperatorID,
												@RequestParam(name="offset", required=false) Integer offset,
												@RequestParam(name="maxResults") Integer maxResults
										) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		MediumReportContainer returnedMedium =null;
		ErrorDetails errorDetails = null;
		
		try {
			returnedMedium = this.service.searchMediumCustom(new Operator(username, password), 
																mediumTypeID,
													 			mediumIdentifier,
													 			releaseDateFrom,
																releaseDateTo,
																expiryDateFrom,
																expiryDateTo,
																stateTypeID,
																operativeFlowTypeID,
																lastTransitionOperatorID,
																givenName,
																familyName,
																fiscalCodeBirthPlace,
																birthDate,
																emitterOperatorID,
																offset,
																maxResults
															);
			
			result = new ResponseEntity<MediumReportContainer>(returnedMedium, HttpStatus.OK);
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}

	
	@RequestMapping(value="/persons/validity", method = RequestMethod.POST)
	@ApiOperation
    (
        value = "Check if person's data (future medium owner) is valid for creation of new medium by ActivityType NEW",
        notes = "This method return null value if all check is ok; return a medium if person alredy own a valid or IN_LAVORAZIONE medium, otherwise return an exception with appropriate error message",
        response = Medium.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Medium.class,  message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> checkPersonDataIsValid(@RequestParam("username") String username, 
													@RequestParam("password") String password, 
													@RequestParam(name="traceLogID", required=false) String traceLogID , 
													@RequestBody PersonValidityBody request) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: "+ request.toString() + "; Username: "+ username);
		ResponseEntity<?> result;
		Medium returnedMedium = null;		
		ErrorDetails errorDetails = null;
		
		try {
			
			returnedMedium = this.service.checkPersonDataIsValid(new Operator(username, password), request.getMediumTypeID(), request.getPersonData());
			
			result = new ResponseEntity<Medium>(returnedMedium, HttpStatus.OK);
			
			logger.info("Response : "+ returnedMedium);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
				
	}	
	

}
