package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.MediumIdentifierRangeService;
import it.integrasistemi.maigesdi.businesslogic.MediumService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@RestController
@Validated
@RequestMapping("MAIGeSDi/V2/mediumIdentifierRanges")
public class MediumIdentifierRangeController {
		
	@Autowired
	UtilityService utilityService;
	
	@Autowired
	private MediumIdentifierRangeService service;
	
	private static final Logger logger = LoggerFactory.getLogger(MediumController.class);
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieve all MediumIdentifierRange",
        notes = "Username and password are required",
        response = MediumIdentifierRange.class,
        responseContainer="List"
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumIdentifierRange.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumIdentifierRanges(	@RequestParam("username") String username, 
														@RequestParam("password") String password, 
														@RequestParam(name="traceLogID", required=false) String traceLogID
														) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		List<MediumIdentifierRange> returnedMediumIdentifierRange =null;
		ErrorDetails errorDetails = null;
		
		try {
				
			returnedMediumIdentifierRange = this.service.getMediumIdentifierRanges(new Operator(username, password));
			
			result = new ResponseEntity<List<MediumIdentifierRange>>(returnedMediumIdentifierRange, HttpStatus.OK);
			//logger.info("OUTPUT: Medium result: "+returnedMediumIdentifierRange);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/mediumType", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieve all MediumIdentifierRange for a specific MediumType",
        notes = "mediumTypeID is requred; Username and password are required too",
        response = MediumIdentifierRange.class,
        responseContainer="List"
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumIdentifierRange.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumIdentifierRangeByMediumTypeID(	@RequestParam("username") String username, 
																		@RequestParam("password") String password, 
																		@RequestParam(name="traceLogID", required=false) String traceLogID,
																		@RequestParam("mediumTypeID") Integer mediumTypeID
																		) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		List<MediumIdentifierRange> returnedMediumIdentifierRange =null;
		ErrorDetails errorDetails = null;
		
		try {
				
			returnedMediumIdentifierRange = this.service.getMediumIdentifierRangesByMediumTypeID(new Operator(username, password), mediumTypeID);
			
			result = new ResponseEntity<List<MediumIdentifierRange>>(returnedMediumIdentifierRange, HttpStatus.OK);
			//logger.info("OUTPUT: Medium result: "+returnedMediumIdentifierRange);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/{mediumIdentifierRangeID}", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieve all MediumIdentifierRange",
        notes = "Username and password are required",
        response = MediumIdentifierRange.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumIdentifierRange.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumIdentifierRange(	@RequestParam("username") String username, 
														@RequestParam("password") String password, 
														@RequestParam(name="traceLogID", required=false) String traceLogID,
														@PathVariable("mediumIdentifierRangeID") Integer mediumIdentifierRangeID
														) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		MediumIdentifierRange returnedMediumIdentifierRange =null;
		ErrorDetails errorDetails = null;
		
		try {
				
			returnedMediumIdentifierRange = this.service.getMediumIdentifierRangeByID(new Operator(username, password), mediumIdentifierRangeID);
			
			result = new ResponseEntity<MediumIdentifierRange>(returnedMediumIdentifierRange, HttpStatus.OK);
			//logger.info("OUTPUT: Medium result: "+returnedMediumIdentifierRange);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/organization", method = RequestMethod.GET)
	@ApiOperation
    (
        value = "Retrieve all MediumIdentifierRange for a specific Organization",
        notes = "OrganizationID is requred; Username and password are required too",
        response = MediumIdentifierRange.class,
        responseContainer="List"
    )
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = MediumIdentifierRange.class, message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getMediumIdentifierRangeByOrganizationID(	@RequestParam("username") String username, 
																		@RequestParam("password") String password, 
																		@RequestParam(name="traceLogID", required=false) String traceLogID,
																		@RequestParam("organizationID") Integer organizationID
																		) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		List<MediumIdentifierRange> returnedMediumIdentifierRange =null;
		ErrorDetails errorDetails = null;
		
		try {
				
			returnedMediumIdentifierRange = this.service.getMediumIdentifierRangesByOrganizationID(new Operator(username, password), organizationID);
			
			result = new ResponseEntity<List<MediumIdentifierRange>>(returnedMediumIdentifierRange, HttpStatus.OK);
			//logger.info("OUTPUT: Medium result: "+returnedMediumIdentifierRange);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation
    (
        value = "Create a new MediumIdentifierRange for a specific MediumType",
        notes = "mediumTypeID is requred; Username and password are required too",
        response = MediumIdentifierRange.class
    )
	@ApiResponses( value = {
			@ApiResponse(code = 201, response = MediumIdentifierRange.class, message = "Created"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> addMediumIdentifierRange(	@RequestParam("username") String username, 
														@RequestParam("password") String password, 
														@RequestParam(name="traceLogID", required=false) String traceLogID,
														@RequestBody MediumIdentifierRange mediumIdentifierRange
														) 	
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start");
		ResponseEntity<?> result;
		MediumIdentifierRange returnedMediumIdentifierRange =null;
		ErrorDetails errorDetails = null;
		
		try {
				
			returnedMediumIdentifierRange = this.service.addMediumIdentifierRange(new Operator(username, password), mediumIdentifierRange);
			
			result = new ResponseEntity<MediumIdentifierRange>(returnedMediumIdentifierRange, HttpStatus.CREATED);
			//logger.info("OUTPUT: Medium result: "+returnedMediumIdentifierRange);
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}
	
	
//	@RequestMapping(value = "/TESTSelect", method = RequestMethod.GET)
//	@ApiOperation
//    (
//        value = "Retrieve all MediumIdentifierRange",
//        notes = "Username and password are required",
//        response = String.class
//        )
//	@ApiResponses( value = {
//			@ApiResponse(code = 200, response = MediumIdentifierRange.class, message = "OK"),
//			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
//			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
//			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
//			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
//			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Conflict"),
//			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
//			}
//		)
//	public ResponseEntity<?> TestSelectForUpdate(	@RequestParam("username") String username, 
//														@RequestParam("password") String password, 
//														@RequestParam(name="traceLogID", required=false) String traceLogID
//														) 
//	{
//		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
//		//imposto il traceLogID nell'MDC
//		this.utilityService.generateTraceLogID(traceLogID, methodName);
//		
//		logger.info("Start");
//		ResponseEntity<?> result;
//		String returnedMediumIdentifierRange =null;
//		ErrorDetails errorDetails = null;
//		
//		try {
//				
//			returnedMediumIdentifierRange = this.service.testSelectForUpdate(new Operator(username, password));
//			
//			result = new ResponseEntity<String>("SONO USCITO : "+returnedMediumIdentifierRange, HttpStatus.OK);
//			//logger.info("OUTPUT: Medium result: "+returnedMediumIdentifierRange);
//		}catch (ValidationException e) {
//			logger.error(e.getMessage(), e);
//			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
//	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
////		}catch (PreconditionException e) {
////			logger.error(e.getMessage(), e);
////			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
////	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
////		}catch (UnauthorizedException e) {
////			logger.error(e.getMessage(), e);
////			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
////	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
////		}catch (ResourceNotFoundException e) {
////			logger.error(e.getMessage(), e);
////			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
////			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
////		}catch (ForbiddenException e) {
////			logger.error(e.getMessage(), e);
////			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
////			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
////		}catch (ConflictException e) {
////			logger.error(e.getMessage(), e);
////			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
////			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
//		}catch (MAIGeSDiException e) {
//			logger.error(e.getMessage(), e);
//			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
//			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
//		}catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
//			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
//		}finally {
//			logger.info("End");
//			//rimuovo la session id dal MDC
//			this.utilityService.removeTraceLogID();
//		}
//		
//		return result;
//	}

}
