package it.integrasistemi.maigesdi.controller;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.OperatorService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@Validated
@RestController
@RequestMapping("MAIGeSDi/V2/operators")
public class OperatorController {

	private static final Logger logger = LoggerFactory.getLogger(OperatorController.class);

	@Autowired
	private OperatorService service;
	
	@Autowired
	private UtilityService utilityService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "Retrieves all the Operators.", 
				notes = "Username and password are required, only an operator with configurator's rights can use this service", 
				response = Operator.class, 
				responseContainer = "List")
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Operator.class, responseContainer ="List", message = "OK"),
			@ApiResponse(code = 400  , response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401  , response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403  , response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getOperators(@RequestParam("username") String username,
											@RequestParam("password") String password, 
											@RequestParam(name="traceLogID", required=false) String traceLogID) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start getOperators");
		ResponseEntity<?> result;
		List<Operator> operatorList = null;
		ErrorDetails errorDetails = null;

		try {
			operatorList = this.service.getOperators(new Operator(username, password));

			result = new ResponseEntity<List<Operator>>(operatorList, HttpStatus.OK);

		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End getMediumTypes");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;

	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(	
					value = "Retrieves a specific operator", 
					notes = "Username and password are required, only an operator with configurator's rights can use this service\",", 
					response = Operator.class
				)
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Operator.class, message = "OK"),
			@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> getOperatorByID(@RequestParam("username") String username,  
											@RequestParam("password") String password, 
											@RequestParam(name="traceLogID", required=false) String traceLogID, 
											@PathVariable("id") Integer id) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		logger.info("Start; ID: "+id);
		ResponseEntity<?> result;
		Operator operatorRequested = null;
		ErrorDetails errorDetails = null;
		
		try {
			operatorRequested = this.service.getOperatorByID(new Operator(username, password),id);

			result = new ResponseEntity<Operator>(operatorRequested, HttpStatus.OK);

			logger.info("Response getOperatorByID: "+ operatorRequested.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End getMediumTypes");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		return result;
	}


	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation(	value = "Add a new Operator"
					, notes = "Operator's Name, password and profileID are required"
							+ "<br> Username and password are required too, Only an Operator whit Configurator's right can add a new Operator"
					, response = Operator.class)
	@ApiResponses( 
		value = {
			@ApiResponse(code = 201, response = Operator.class, message = "Created"),
			@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> addOperator(@RequestParam("username") String username,	
										@RequestParam("password") String password, 
										@RequestParam(name="traceLogID", required=false) String traceLogID, 
										@RequestBody Operator operator) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: " + operator);
		ResponseEntity<?> result;
		Operator returnedOperator = null;		
		ErrorDetails errorDetails = null;
		
		try {
			returnedOperator = this.service.addOperator(new Operator(username, password), operator);
			
			result = new ResponseEntity<Operator>(returnedOperator, HttpStatus.CREATED);
			
			logger.info("Response addOperator: "+ returnedOperator.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End addOperator");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@ApiOperation(	value = "Update an existin Operator", 
					notes = "It's possible change Operator's profileID and MediumTypes"
							+ "<br> MediumTypes are not required, in case they are defined MediumTypes's ID is required  "
							+ "<br> MediumTypes must be unique, you cannot define the same MediumTypes many times for an Operator; duplicate will be ignorated"
							+ "<br> Username and password are required, Only an Operator whit Configurator's right can add a new MediumType", 
					response = Operator.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = Operator.class, responseContainer ="List", message = "OK"),
			@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> updateOperator(@RequestParam("username") String username,
											@RequestParam("password") String password, 
											@RequestParam(name="traceLogID", required=false) String traceLogID, 
											@RequestBody Operator operator)
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: " + operator.toString());
		ResponseEntity<?> result;
		Operator returnedOperator = null;		
		ErrorDetails errorDetails = null;
		
		try {
			returnedOperator = this.service.updateOperator(new Operator(username, password), operator);
			
			result = new ResponseEntity<Operator>(returnedOperator, HttpStatus.OK);
			
			logger.info("Response updateOperator: "+ returnedOperator.toString());
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End updateOperator");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;

	}
	
	@RequestMapping(value = "/password", method = RequestMethod.PUT)
	@ApiOperation(value = "Change the current Operator's password", 
			notes = "New password must be different than older one; An Operator can change only his own password",
			response = String.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, response = String.class, message = "OK"),
			@ApiResponse(code = 400, response = ErrorDetails.class,  message = "Bad request"),
			@ApiResponse(code = 401, response = ErrorDetails.class,  message = "Unauthorized"),
			@ApiResponse(code = 403, response = ErrorDetails.class,  message = "Forbidden"),
			@ApiResponse(code = 404, response = ErrorDetails.class,  message = "Not found"),
			@ApiResponse(code = 409, response = ErrorDetails.class,  message = "Confict"),
			@ApiResponse(code = 500, response = ErrorDetails.class,  message = "Internal server error")
			}
		)
	public ResponseEntity<?> changeOperatorPassword(@RequestParam("username") String username,
													@RequestParam("password") String password, 
													@RequestParam(name="traceLogID", required=false) String traceLogID, 
													@RequestParam("oldPassword") String oldPassword,
													@RequestParam("newPassword") String newPassword) 
	{
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
		//imposto il traceLogID nell'MDC
		this.utilityService.generateTraceLogID(traceLogID, methodName);
		
		logger.info("Start; Request: User: "+username + " password: "+ password+
					" oldPassword: "+oldPassword+" newPassword: "+newPassword);
		ResponseEntity<?> result;
		ErrorDetails errorDetails = null;
		
		try {
			this.service.changeOperatorPassword(new Operator(username,password), oldPassword, newPassword);
			
			result = new ResponseEntity<String>("Password correctlty changed", HttpStatus.OK);
			
		}catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (PreconditionException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.BAD_REQUEST);
		}catch (UnauthorizedException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
	        result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.UNAUTHORIZED);
		}catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
		}catch (ForbiddenException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.FORBIDDEN);
		}catch (ConflictException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);
		}catch (MAIGeSDiException e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), e.getErrorCode());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorDetails = new ErrorDetails(new Date(),  e.getMessage(), Utility.ErrorType.ET_GENERIC_ERROR.getValue());
			result = new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("End updateOperator");
			//rimuovo la session id dal MDC
			this.utilityService.removeTraceLogID();
		}
		
		
		return result;
		
	}



}
