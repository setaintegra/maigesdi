package it.integrasistemi.maigesdi.mapper;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.ActivityType;
import it.integrasistemi.maigesdi.bean.EmptyMediumForMediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.OperativeFlowType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.StateType;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.BirthPlaceVRO;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.SalePoint;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumBirthPlaceFiscalCodeResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumPhotoResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumSignResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumTutorSignResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetNextMediumIdentifierRangeResult;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryChangeOperatorPasword;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryCheckOperatorMediumTypeRights;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetMediumByMediumTypeIDMediumIdentifier;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetMediumTypeByID;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetOrganizzationByID;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInserMediumTypeForOperator;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInsertActivityForMediumType;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInsertCredentialForMedium;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInsertCredentialTypeForMediumType;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;

@Mapper
public interface MAIGESDIMapper {
	
	//UTILITY
	public String checkDbConnection();
	public int reserveIdentifier(Sequence sequence);
	
	//ORGANIZATION
	public List<Organization> getOrganizations();
	public List<Organization> getOrganizationsByOperator(Integer operatorID);
	public Organization getOrganizationByID(Integer organizationID);
	public Organization getOrganizationByID(ParameterQueryGetOrganizzationByID request);
	public Integer insertOrganization(Organization organization);
	public Integer updateOrganization(Organization organization);
	public String getOrganizationFiscalCodeByMediumTypeID(@Param("mediumTypeID") Integer mediumTypeID);
	public String getOrganizationFiscalCodeByConiCode(@Param("coniCode") String coniCode);
	
		
	//OPERATOR
	public Operator getOperatorLiteForLogin(Operator operator);
	public List<Operator> getOperators();
	public Operator getOperatorByID(Integer id);
	public Integer insertOperator(Operator operator);
	public Integer updateOperator(Operator operator);
	public Integer insertMediumTypeForOperator(ParameterQueryInserMediumTypeForOperator parameterRequest);
	public Integer deleteMediumTypeForOperatorByIDOperator(Integer operatorID);
	public Integer updateOperatorPassword(ParameterQueryChangeOperatorPasword request);
	public Integer getOperatorMediumType(ParameterQueryCheckOperatorMediumTypeRights request);
	
	//MEDIUMTYPE
	public List<MediumType> getMediumTypeForConfigurator();
	public List<MediumType> getMediumType(Integer operatorID);
	public MediumType getMediumTypeByIDForConfigurator(Integer ID);
	public MediumType getMediumTypeByID(ParameterQueryGetMediumTypeByID request);
	public Integer insertMediumType(MediumType mediumType);
	public Integer updateMediumType(MediumType mediumType);
	public Integer insertActivityForMediumType(ParameterQueryInsertActivityForMediumType parameterRequest);
	public Integer deleteActivityTypesForMediumTypeByIDMediumType(Integer id);
	public Activity getParameterListByMediumTypeIDActivityID(MediumPost mediumPost);
	public Integer insertCredentialTypeForMediumType(ParameterQueryInsertCredentialTypeForMediumType parameterCredentialRequest);
	public List<MediumType> getMediumTypeByMediumIdentifier(@Param("operatorID") Integer operatorID, @Param("mediumIdentifier") String mediumIdentifier);
	public List<MediumType> getMediumTypeByCodes(@Param("operatorID") Integer operatorID, @Param("organizationCode") String organizationCode, @Param("seriesCode") String seriesCode);
	public Integer getMediumTypeDayOfValidity(Integer mediumTypeID);
	
	//public Integer updateMediumTypeAddingExternalSystem(@Param("mediumTypeID") Integer mediumTypeID, @Param("ExternalSystemID") Integer ExternalSystemID);
	
	public Integer deleteMediumTypeSpezzataSupporti(Integer mediumTypeID);
	public Integer deleteMediumTypeOperators(Integer mediumTypeID);
	public Integer deleteMediumTypeMediums(Integer mediumTypeID);
	public Integer deleteMediumTypeCredentialTypes(Integer mediumTypeID);
	public Integer deleteMediumTypePrezzoTipoSupportoDigitale(Integer mediumTypeID);
	public Integer deleteMediumTypeParameters(Integer mediumTypeID);
	public Integer deleteMediumType(Integer mediumTypeID);
		
	//EXTERNAL SYSTEM
	public List<ExternalSystem> getExternalSystems(Integer operatorID);
	public ExternalSystem getExternalSystemByID(@Param("operatorID") Integer operatorID, @Param("externalSystemID") Integer externalSystemID);
	public ExternalSystem getExternalSystemByMediumTypeID(Integer mediumTypeID);
	
	//MAIPOLICE
	public BirthPlaceVRO getBirthPlaceVRO(Person persona);
	public SalePoint getDummySalePointVRO();
	
	//MEDIUM
	public Medium getMediumByMediumTypeIDMediumIdentifier(ParameterQueryGetMediumByMediumTypeIDMediumIdentifier parameterQueryGetMediumByID);
	public Medium getMediumByID(@Param("mediumID") Integer mediumID,@Param("operatorID") Integer operatorID);
	public Integer insertMedium(Medium medium);
	public Integer insertMediumCredential(ParameterQueryInsertCredentialForMedium request);
	public List<Medium> getMediumCustom(@Param("medium") Medium medium, @Param("operatorID") Integer operatorID);
	public Integer updateMedium(ParameterMedium mediumForActivity);
	public Integer insertMediumOperativeFlowRelation(@Param("idSupportoDigitale") Integer mediumID, 
													 @Param("idFlussoOperativo") Integer operativeFlowID, 
													 @Param("idTipoStatoFlussoOperativo") Integer operativeFlowTypeID,
													 @Param("idOperatore") Integer operatorID);
	public Integer insertMediumStateRelation(@Param("idSupportoDigitale") Integer mediumID, 
											 @Param("idStatoSupportoDigitale") Integer stateID, 
											 @Param("idTipoStatoSupportoDigitale") Integer stateTypeID,
											 @Param("idOperatore") Integer operatorID);
	public Integer updateMediumOperativeFlow(@Param("mediumID") Integer mediumID, @Param("operativeFlowID") Integer operativeFlowID);
	public Integer updateMediumState(@Param("mediumID") Integer mediumID, @Param("stateID") Integer stateID);
	
	public Integer updateMediumExternalSystemResult(@Param("mediumID") Integer mediumID, 
													@Param("operationType") String operationType,
													@Param("authorizationID") String authorizationID,
													@Param("CENException") String CENException,
													@Param("operativeFlowID") Integer operativeFlowID, 
													@Param("stateID") Integer stateID);
	
	public Integer updateMediumOperativeFlowState(@Param("mediumID") Integer mediumID,@Param("operativeFlowID") Integer operativeFlowID, @Param("stateID") Integer stateID);
	
	public Integer checkPersonOwnMediumSameOrganziationAndSpecificExternalSystem(@Param("mediumTypeID") Integer mediumTypeID, @Param("person") Person person, @Param("externalSystemID") Integer externalSystemID);
	public Integer addMediumPhoto(@Param("mediumID") Integer mediumID, @Param("photo") byte[] photo);
	public Integer addMediumSign(@Param("mediumID") Integer mediumID, @Param("sign") byte[] sign);
	public Integer addMediumTutorSign(@Param("mediumID") Integer mediumID, @Param("sign") byte[] sign);
	public GetMediumPhotoResult getMediumPhotoByID(@Param("mediumID") Integer mediumID, @Param("operatorID") Integer operatorID);
	public GetMediumSignResult getMediumSignByID(@Param("mediumID") Integer mediumID, @Param("operatorID") Integer operatorID);
	public GetMediumTutorSignResult getMediumTutorSignByID(@Param("mediumID") Integer mediumID, @Param("operatorID") Integer operatorID);
	public Medium getValidMediumNoSecurityByIdentifier(@Param("mediumIdentifier") String mediumIdentifier, @Param("mediumTypeID") Integer mediumTypeID);
	public Medium getValidMediumNoSecurityByID(Integer mediumID);
	public Medium getMediumNoSecurityByMediumID(@Param("mediumID") Integer mediumID);
	public Medium getMediumNoSecurityByMediumIndentifierMediumTypeID(@Param("mediumTypeID") Integer mediumTypeID, @Param("mediumIdentifier") String mediumIdentifier);
	public Integer checkAndUpdateBirthPlaceFiscalCode(@Param("mediumID") Integer mediumID, @Param("mediumTypeID") Integer mediumTypeID, @Param("mediumIdentifier") String mediumIdentifier);
	public GetMediumBirthPlaceFiscalCodeResult getMediumBirthPlaceFiscalCode(@Param("mediumID") Integer mediumID, 
																			 @Param("mediumTypeID") Integer mediumTypeID, 
																			 @Param("mediumIdentifier") String mediumIdentifier);
	public Integer deleteMediumOperativeFlow(Integer mediumID);
	public Integer deleteMediumState(Integer mediumID);
	public Integer deleteMediumCredential(Integer mediumID);
	public Integer deleteMedium(Integer mediumID);
	public Medium getMediumForDisableNoSecurityByID(Integer mediumID);
	
	public List<Medium> searchMediumCustom(@Param("operatorID") Integer operatorID, 
											@Param("mediumTypeID") Integer mediumTypeID,
											@Param("mediumIdentifier") String mediumIdentifier,
											@Param("releaseDateFrom") Date releaseDateFrom, 
											@Param("releaseDateTo") Date releaseDateTo, 
											@Param("expiryDateFrom") Date expiryDateFrom, 
											@Param("expiryDateTo") Date expiryDateTo, 
											@Param("stateTypeID") Integer stateTypeID,
											@Param("operativeFlowTypeID") Integer operativeFlowTypeID, 
											@Param("lastTransitionOperatorID") Integer lastTransitionOperatorID, 
											@Param("givenName") String givenName, 
											@Param("familyName") String familyName,
											@Param("fiscalCodeBirthPlace") String fiscalCodeBirthPlace, 
											@Param("birthDate") Date birthDate, 
											@Param("emitterOperatorID") Integer emitterOperatorID,
											@Param("offset") Integer offset,
											@Param("maxResults") Integer maxResults);
	
	public Integer countSearchMediumCustom(@Param("operatorID") Integer operatorID, 
											@Param("mediumTypeID") Integer mediumTypeID,
											@Param("mediumIdentifier") String mediumIdentifier,
											@Param("releaseDateFrom") Date releaseDateFrom, 
											@Param("releaseDateTo") Date releaseDateTo, 
											@Param("expiryDateFrom") Date expiryDateFrom, 
											@Param("expiryDateTo") Date expiryDateTo, 
											@Param("stateTypeID") Integer stateTypeID,
											@Param("operativeFlowTypeID") Integer operativeFlowTypeID, 
											@Param("lastTransitionOperatorID") Integer lastTransitionOperatorID, 
											@Param("givenName") String givenName, 
											@Param("familyName") String familyName,
											@Param("fiscalCodeBirthPlace") String fiscalCodeBirthPlace, 
											@Param("birthDate") Date birthDate, 
											@Param("emitterOperatorID") Integer emitterOperatorID);
	
	public Integer updateMediumAuthorizationID(@Param("mediumID") Integer mediumID, 
											  @Param("authorizationID") String authorizationID, 
											  @Param("CENException") String CENException);
	
	//MEDIUMIDENTIFIERRANGE (SPEZZATE)
	public List<MediumIdentifierRange> getAllMediumIdentifierRange();
	public List<MediumIdentifierRange> getMediumIdentifierRangesByOrganizationID(@Param("organizationID") Integer organizationID); 
	public List<MediumIdentifierRange> getMediumIdentifierRangesByMediumTypeID(@Param("mediumTypeID") Integer mediumTypeID);
	public List<MediumIdentifierRange> checkOverlayMediumIdentifierRange(MediumIdentifierRange mediumIdentifierRange);
	public Integer insertMediumIdentifierRange(MediumIdentifierRange mediumIdentifierRange);
	public Integer insertMediumOfMediumIdentifierRange(@Param("mediumID") Integer mediumID, 
														@Param("mediumTypeID") Integer mediumTypeID, 
														@Param("typographicNumber") String typographicNumber,
														@Param("lisTicketCode") String lisTicketCode, 
														@Param("mediumIdentifier") String mediumIdentifier);
	public Integer insertAllMediumOfMediumIdentifierRange(@Param("listEmptyMedium") List<EmptyMediumForMediumIdentifierRange> listEmptyMedium);
	public MediumIdentifierRange getMediumIdentifierRangeByID(Integer mediumIdentifierRangeID);
	public Integer countExistingMediumInMediumIdentifierRange(@Param("mediumTypeID") Integer mediumTypeID, 
															  @Param("startMediumIdentifier") String startMediumIdentifier,
															  @Param("endMediumIdentifier") String endMediumIdentifier);
	public void getNextMediumIdentifierRange(GetNextMediumIdentifierRangeResult parameter);
	public Integer countMediumIdentifierRangeForMediumType(Integer mediumTypeID);
	public Integer countMediumInMediumIdentifierRange(Integer mediumTypeID);
	public Integer completeMediumFromMediumIndetifierRange(Medium medium);
	public Integer updateMediumAddComuneAndNazione(@Param("mediumID") Integer mediumID, 
													@Param("birthPlacefiscalCode") String birthPlacefiscalCode, 
													@Param("birthdate") Date birthdate);
	public Integer clearReservedMedium(@Param("mediumID") Integer mediumID, @Param("pin") String pin);
	public MediumIdentifierRange getMediumIdentifierRangeByMediumIdentifier(@Param("mediumTypeID") Integer mediumTypeID,@Param("mediumIdentifier") String mediumIdentifier);
	public List<MediumIdentifierRange> selectAndLockMediumIdentifierRangeByMediumTypeID(Integer mediumTypeID);
	public List<MediumIdentifierRange> checkParallelOverlayMediumIdentifierRange(@Param("mediumIdentifierRangeResultID") Integer mediumIdentifierRangeResultID,
																				@Param("mediumIdentifierRange") MediumIdentifierRange mediumIdentifierRange);
	
	// delete delle spezzate per gli unit test
	public Integer deleteMediumIdentifierRange(Integer mediumIdentifierRangeID);
	public Integer deleteMediumFromMediumIdentifierRange(Integer mediumTypeID);
	// fine delete per gli unit test
	
	//Utility
	public List<ActivityType> getAllActivityType();
	public List<Activity> getAllActivity();
	public List<StateType> getAllStateType();
	public List<OperativeFlowType> getAllOperativeFlowType();
	
	public Integer validateBirthDateAndFiscalCodeBirthPlace(@Param("birthdate") Date birthdate, @Param("birthPlaceFiscalCode") String birthPlaceFiscalCode);
	public List<Medium> getListValidMediumOfOrganizationByPerson(@Param("mediumTypeID") Integer mediumTypeID, @Param("person") Person person, @Param("externalSystemID") Integer externalSystemID);
	public String getDBName();
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	

}
