package it.integrasistemi.maigesdi;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import it.integrasistemi.maigesdi.businesslogic.UtilityService;

@SpringBootApplication(scanBasePackages = "it.integrasistemi.maigesdi")
@MapperScan("it.integrasistemi.maigesdi.mapper")
public class MaigesdiApplication implements ApplicationRunner{

	private static final Logger logger = LoggerFactory.getLogger(MaigesdiApplication.class);

	@Autowired
	private UtilityService utility;
	
	public static void main(String[] args) {
		SpringApplication.run(MaigesdiApplication.class, args);
	}


	@Override
	public void run(ApplicationArguments args) throws Exception {
		//se il flag è impostato a true, ossia se le chiamate al CEN 'mock-ate'
		if(utility.isDebugMode()) {
			//l'ambiente lo permette?
			if(!utility.isEnviromentTest())
				throw new Exception("Enviroment not valid for CEN debug mode, change value of parameter cen.debug_mode");
		}
				
		logger.info("MAIGeSDi started; CEN DEBUG MODE: {} ", utility.isDebugMode());
		
	}
	
}
