package it.integrasistemi.maigesdi.dao;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.integrasistemi.maigesdi.bean.Credential;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.ReservedMedium;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.BirthPlaceVRO;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.CENAuthenticationResult;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.SalePoint;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumBirthPlaceFiscalCodeResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumPhotoResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumSignResult;
import it.integrasistemi.maigesdi.bean.mybatis.GetMediumTutorSignResult;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetMediumByMediumTypeIDMediumIdentifier;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInsertCredentialForMedium;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Component
public class MediumDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(MediumDAO.class);
	@Autowired
	private MAIGESDIMapper mapper;
	
	@Autowired
	private MediumIdentifierRangeDAO mediumIdenfierRangeDAO;
	

	public Medium getMedium(Integer mediumTypeID, String mediumIdentifier, Integer operatorID) throws MAIGeSDiException {
		Medium result = null;
		try 
		{
			result = this.mapper.getMediumByMediumTypeIDMediumIdentifier(new ParameterQueryGetMediumByMediumTypeIDMediumIdentifier(mediumTypeID, mediumIdentifier, operatorID));
			//logger.info("DB RESULT: Medium: "+result.toString());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
			 return result;
	}
	
	public Medium getMedium(Integer mediumID, Integer operatorID) throws MAIGeSDiException {
		Medium result = null;
		try 
		{
			result = this.mapper.getMediumByID(mediumID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
	}
	

	//metodo per la ricerca dei medium custom; i filtri di ricerca sono in base ai parametri dell'attività 
	/**
	 * Metodo per la ricerca dei medium custom; i filtri di ricerca sono in base ai parametri dell'attività 
	 * @param medium
	 * @param operatorID
	 * @return
	 * @throws MAIGeSDiException
	 */
	public List<Medium> getMediumCustom(Medium medium, Integer operatorID) throws MAIGeSDiException {
		List<Medium> result =null;
		try 
		{
			result = this.mapper.getMediumCustom(medium, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		return result;
		
	}

	/**
	 * Ritorna un medium valido, sia con stato = 2 sia con date di validità valide a partire dalla coppia identificativo - l'id del mediumType
	 * senza verificare i diritti di un operatore
	 * @param mediumIdentifier identificativo del supporto
	 * @param mediumTypeID id del mediumtype
	 * @return ritorna il medium con mediumIdentifier e mediumTypeID e stato valido oppure null
	 * @throws MAIGeSDiException
	 */
	public Medium getValidMediumNoSecurity(String mediumIdentifier, Integer mediumTypeID) throws MAIGeSDiException {
		Medium result = null;
		try 
		{
			result = this.mapper.getValidMediumNoSecurityByIdentifier(mediumIdentifier, mediumTypeID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}
	
	/**
	 * Ritorna un medium valido, sia con stato = 2 sia con date di validità valide a partire dall'id del medium
	 * senza verificare i diritti di un operatore
	 * @param mediumID id del medium
	 * @return ritorna il medium con mediumID e stato valido oppure null
	 * @throws DBMSFaultException
	 */
	public Medium getValidMediumNoSecurity(Integer mediumID) throws DBMSFaultException {
		Medium result = null;
		try 
		{
			result = this.mapper.getValidMediumNoSecurityByID(mediumID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}
	
	/**
	 * Reperimento di un medium da disabilitare, ossia o un medium valido  o un medium con stato = IN_LAVORAZIONE  e flusso = ATTESA_CONFERMA_BO
	 * @param mediumID id del medium
	 * @return un medium in uno dei due casi oppure null
	 * @throws DBMSFaultException
	 */
	public Medium getMediumForDisableNoSecurity(Integer mediumID) throws DBMSFaultException {
		Medium result = null;
		try 
		{
			result = this.mapper.getMediumForDisableNoSecurityByID(mediumID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	
		
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer addMedium(Operator operator, Medium medium) throws Exception {
		Integer mediumID = 0;
		Integer insertMediumResult = 0;
		Integer insertCredential = 0;
		ParameterQueryInsertCredentialForMedium request = null;
		
		Integer stateID         = 0;
		Integer operativeFlowID = 0;
		Integer queryResult     = 0;
		
		Integer updatePhoto     = 0;
		Integer updateSign      = 0;
		Integer updateTutorSign = 0;
		
		//controlli preliminari su stato e flusso operativo; se mancano non faccio nulla, vanno configurati prima di chiamare il metodo
		if(medium.getOperativeFlow() == null || medium.getOperativeFlow().getID() == null || medium.getOperativeFlow().getID() <= 0)
			throw new MAIGeSDiException("Medium's operative flow missing",Utility.ErrorType.ET_MEDIUM_OPERATIVE_FLOV_MISSING.getValue());
		
		if(medium.getState() == null || medium.getState().getID() == null || medium.getState().getID() <= 0)
			throw new MAIGeSDiException("Medium's state missing",Utility.ErrorType.ET_MEDIUM_STATE_MISSING.getValue());
				
		//Gestione foto
		byte[] decodedPhoto = null;
		if(medium.getPhoto()!= null) {
			decodedPhoto = (Base64.getDecoder().decode(medium.getPhoto()));
			medium.setPhoto(null);
		}
		
		//Gestione firma
		byte[] decodedSign = null;
		if(medium.getSign()!= null) {
			decodedSign = (Base64.getDecoder().decode(medium.getSign()));
			medium.setSign(null);
		}
		
		//Gestione firma tutore
		byte[] decodedTutorSign = null;
		if(medium.getTutor() != null && medium.getTutor().getSign() != null) {
			decodedTutorSign = (Base64.getDecoder().decode(medium.getTutor().getSign()));
			medium.getTutor().setSign(null);
		}
				
		//reperisco un id per il medium
		try 
		{
			mediumID = this.mapper.reserveIdentifier(new Sequence("SEQ_SUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (mediumID.equals(0))
			throw new DBMSFaultException("Medium not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());

		//aggiorno il medium aggiungendo l'id
		medium.setID(mediumID);
				
		//inserisco il medium
		try 
		{
			insertMediumResult = this.mapper.insertMedium(medium);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(!insertMediumResult.equals(1))
			throw new DBMSFaultException("Adding medium problem; Medium: "+ medium.toString(), 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
	    
		//inserimento della foto
		if(decodedPhoto != null) {
			try 
			{
				updatePhoto = this.mapper.addMediumPhoto(medium.getID(), decodedPhoto);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if(!updatePhoto.equals(1))
				throw new DBMSFaultException("Medium not added; Adding medium's photo problem; Medium: "+ medium.toString(), 
												Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
	    
		//inserimento della firma
		if(decodedSign != null) {
			try 
			{
				updateSign = this.mapper.addMediumSign(medium.getID(), decodedSign);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if(!updateSign.equals(1))
				throw new DBMSFaultException("Medium not added; Adding medium's sign problem; Medium: "+ medium.toString(), 
												Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
	    
		//inserimento della firma del tutore
		if(decodedTutorSign != null) {
			try 
			{
				updateTutorSign = this.mapper.addMediumTutorSign(medium.getID(), decodedTutorSign);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if(!updateTutorSign.equals(1))
				throw new DBMSFaultException("Medium not added; Adding medium's tutor sign problem; Medium: "+ medium.toString(), 
												Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		//se sono presenti le credenziali di accesso allora le inserisco
		if(medium.getCredentials()!=null && medium.getCredentials().size()>0) 
		{
			for(Credential cred: medium.getCredentials()) {
				request = new ParameterQueryInsertCredentialForMedium(mediumID, cred.getCredentialType().getID(), cred.getValue());
				insertCredential = 0;
				try 
				{
					insertCredential = this.mapper.insertMediumCredential(request);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				if(!insertCredential.equals(1))
					throw new DBMSFaultException("Medium not added, adding medium's credential problem", 
							Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
		}
		
		//reperisco un id per il flusso operativo
		try 
		{
			operativeFlowID = this.mapper.reserveIdentifier(new Sequence("SEQ_FLUSSOOPERATIVO"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (operativeFlowID.equals(0))
			throw new DBMSFaultException("Medium not added; Medium's operative flow not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		//inserisco la relazione
		try 
		{
			queryResult = this.mapper.insertMediumOperativeFlowRelation(mediumID, operativeFlowID, medium.getOperativeFlow().getID(), operator.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(queryResult == null || !queryResult.equals(1))
			throw new DBMSFaultException("Medium not added; Medium's operative flow not added; adding relation problem", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
			
		
		//reperisco un id per lo stato
		try 
		{
			stateID = this.mapper.reserveIdentifier(new Sequence("SEQ_STATOSUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (stateID.equals(0))
			throw new DBMSFaultException("Medium not added; Medium's state not added", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		//inserisco la relazione
		try 
		{
			queryResult = this.mapper.insertMediumStateRelation(mediumID, stateID, medium.getState().getID(), operator.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(queryResult == null || !queryResult.equals(1))
			throw new DBMSFaultException("Medium not added; Medium's state not added; adding state relation problem", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			queryResult = this.mapper.updateMediumOperativeFlowState(mediumID, operativeFlowID, stateID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(queryResult == null || !queryResult.equals(1))
			throw new DBMSFaultException("Medium not added; updating state and operative flow problem", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
				
		return mediumID;
		
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void updateMedium(ParameterMedium mediumForActivity) throws MAIGeSDiException {
		Integer updateMediumResult = 0;
		
		//Gestione foto
		byte[]  decodedPhoto      = null;
		boolean updatePhoto       = false;
		Integer updatePhotoResult = 0;
		
		//Gestione firma
		byte[]  decodedSign      = null;
		boolean updateSign       = false;
		Integer updateSignResult = 0;
		
		//Gestione firma tutore
		byte[]  decodedTutorSign      = null;
		boolean updateTutorSign       = false;
		Integer updateTutorSignResult = 0;
		
		if(mediumForActivity.getPhoto()!= null) {
			updatePhoto = true;
			if(mediumForActivity.getPhoto().getValue()!= null) {
				decodedPhoto = (Base64.getDecoder().decode(mediumForActivity.getPhoto().getValue()));
			}
			
			mediumForActivity.setPhoto(null);
		}
		
		if(mediumForActivity.getSign()!= null) {
			updateSign = true;
			if(mediumForActivity.getSign().getValue()!= null) {
				decodedSign = (Base64.getDecoder().decode(mediumForActivity.getSign().getValue()));
			}
			
			mediumForActivity.setSign(null);
		}
		
		if(mediumForActivity.getTutorSign() != null) {
			updateTutorSign = true;
			if(mediumForActivity.getTutorSign().getValue()!= null) {
				decodedTutorSign = (Base64.getDecoder().decode(mediumForActivity.getTutorSign().getValue()));
			}
			
			mediumForActivity.setTutorSign(null);
		}
		
		try 
		{
			updateMediumResult = this.mapper.updateMedium(mediumForActivity);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		
		if(!updateMediumResult.equals(1))
			throw new DBMSFaultException("Medium not updated", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		//la foto è da aggiornare?
		if(updatePhoto) {
			try 
			{
				updatePhotoResult = this.mapper.addMediumPhoto(mediumForActivity.getID().getValue(), decodedPhoto);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			if(!updatePhotoResult.equals(1))
				throw new DBMSFaultException("Medium not updated. Updating photo problem.", 
						Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		//la firma è da aggiornare?
		if(updateSign) {
			try 
			{
				updateSignResult = this.mapper.addMediumSign(mediumForActivity.getID().getValue(), decodedSign);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			if(!updateSignResult.equals(1))
				throw new DBMSFaultException("Medium not updated. Updating sign problem.", 
						Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		//la firma del tutore è da aggiornare?
		if(updateTutorSign) {
			try 
			{
				updateTutorSignResult = this.mapper.addMediumTutorSign(mediumForActivity.getID().getValue(), decodedTutorSign);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			if(!updateTutorSignResult.equals(1))
				throw new DBMSFaultException("Medium not updated. Updating tutor sign problem.", 
						Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		// le credenziali non possono essere cambiate
	}

	//verifica se una persona è proprietaria di un medium dell'organizzazione con uno specifico sistema esterno (esempio se possiede una tdt o una fidelity)
	/**
	 * Verifica se una persona è proprietaria di un medium dell'organizzazione con uno specifico sistema esterno (esempio se possiede una tdt o una fidelity)
	 * @param mediumTypeID
	 * @param person
	 * @param externalSystemID
	 * @return
	 * @throws MAIGeSDiException
	 */
	public boolean checkPersonOwnMediumSameOrganizationAndSpecificExternalSystem(Integer mediumTypeID, Person person, Integer externalSystemID) throws MAIGeSDiException {
		Integer result = 0;
		try 
		{
			result = this.mapper.checkPersonOwnMediumSameOrganziationAndSpecificExternalSystem(mediumTypeID, person, externalSystemID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result != 0;
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer updateMediumOperativeFlow(Integer operatorID, Integer mediumID, Integer operativeFlowTypeID) throws MAIGeSDiException {
		Integer result = 0;
		Integer operativeFlowID = 0;
		Integer updateMediumResult = 0;
		
		// 1 reperisco un id per il flusso operativo
		try 
		{
			operativeFlowID = this.mapper.reserveIdentifier(new Sequence("SEQ_FLUSSOOPERATIVO"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (operativeFlowID.equals(0))
			throw new DBMSFaultException("Medium's operative flow not added", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		// 2 inserisco la relazione
		try 
		{
			result = this.mapper.insertMediumOperativeFlowRelation(mediumID, operativeFlowID, operativeFlowTypeID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Medium's operative flow not added; adding problem", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		// 3 aggiorno il medium
		try 
		{
			updateMediumResult = this.mapper.updateMediumOperativeFlow(mediumID, operativeFlowID);	
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(updateMediumResult  == null || !updateMediumResult.equals(1))
			throw new DBMSFaultException("Medium's operative flow not updated; updating problem", 
					Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		return result;

		
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer updateMediumState(Integer operatorID, Integer mediumID, Integer stateTypeID) throws MAIGeSDiException {
		Integer result = 0;
		Integer stateID = 0;
		Integer updateMediumResult = 0;
		
		try 
		{
			// 1 reperisco un id per lo stato
			stateID = this.mapper.reserveIdentifier(new Sequence("SEQ_STATOSUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (stateID.equals(0))
			throw new DBMSFaultException("Medium's state not added", 
											Utility.ErrorType.ET_DBMS_ERROR.getValue());
		try 
		{
			// 2 inserisco la relazione
			result = this.mapper.insertMediumStateRelation(mediumID, stateID, stateTypeID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Medium's state not added; adding problem", 
											Utility.ErrorType.ET_DBMS_ERROR.getValue());
		try 
		{
			// 3 aggiorno il medium
			updateMediumResult = this.mapper.updateMediumState(mediumID, stateID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if(updateMediumResult  == null || !updateMediumResult.equals(1))
			throw new DBMSFaultException("Medium's state not updated; updating problem", 
											Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		
		return result;
	
	}


//	public String getOrganizationFiscalCode(Integer mediumTypeID) throws MAIGeSDiException {
//		String result = null;
//		try 
//		{
//			result = this.mapper.getOrganizationFiscalCodeByMediumIdentifier(mediumTypeID);	
//		}catch(Exception e) {
//			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
//		}
//				
//		if(result == null) 
//			throw new DBMSFaultException("Organization fiscal code not found; Medium identifier: " + mediumIdentifier, 
//										Utility.ErrorType.ET_DBMS_ERROR.getValue());
//		
//		return result;
//	}

	public BirthPlaceVRO getBirthPlaceVRO(Person persona) throws Exception {
		BirthPlaceVRO lnVRO = null;
		try 
		{
			lnVRO = this.mapper.getBirthPlaceVRO(persona);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(lnVRO == null) 
			throw new DBMSFaultException("Birth place VRO not found: " + persona.getFiscalCodeBirthPlace() + " dataNascita: " + persona.getBirthDate(), 
										Utility.ErrorType.ET_DBMS_ERROR.getValue());
        
		return lnVRO;
	}

	public SalePoint getDummySalePoint() throws Exception {
		SalePoint salePoint =null;
		try 
		{
			salePoint = mapper.getDummySalePointVRO();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
	    if(salePoint == null) 
	    	throw new DBMSFaultException("Dummy sale point not foun in MAIGESDI", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		return salePoint;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void updateMediumExternalSystemResult(Integer operatorID, 
    											 Integer mediumID, 
    											 String operationType, 
    											 CENAuthenticationResult cenAuthResult, 
    											 Integer operativeFlowTypeID, 
    											 Integer stateTypeID) throws MAIGeSDiException {
		Integer result = 0;
		Integer operativeFlowID = 0;
		Integer stateID = 0;
		Integer updateMediumResult = 0;
				
		// 1 gestione flusso operativo
		try 
		{
			//reperisco un id per il flusso operativo
			operativeFlowID = this.mapper.reserveIdentifier(new Sequence("SEQ_FLUSSOOPERATIVO"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (operativeFlowID.equals(0))
			throw new DBMSFaultException("Medium's operative flow not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		try 
		{
			result = this.mapper.insertMediumOperativeFlowRelation(mediumID, operativeFlowID, operativeFlowTypeID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Medium's operative flow not added; adding relation problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		// fine gestione flusso operativo	
		
		// 2 gestione  dello stato
		try 
		{
			//reperisco un id per lo stato
			stateID = this.mapper.reserveIdentifier(new Sequence("SEQ_STATOSUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (stateID.equals(0))
			throw new DBMSFaultException("Medium's state not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			//inserisco la relazione
			result = this.mapper.insertMediumStateRelation(mediumID, stateID, stateTypeID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Medium's state not added; adding relation problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		// fine gestione  dello stato
		
		// 3 Update del medium
		try 
		{
			String CENExcepiton = cenAuthResult.getCodiceEccezione() != null && cenAuthResult.getDescrizioneEccezione() != null ?
									cenAuthResult.getCodiceEccezione()+" - "+ cenAuthResult.getDescrizioneEccezione() :
									null;
			//aggiorno il medium
			updateMediumResult = this.mapper.updateMediumExternalSystemResult(	mediumID, 
																				operationType,
																				cenAuthResult.getIuA(), 
																				CENExcepiton,
																				operativeFlowID,
																				stateID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(updateMediumResult  == null || !updateMediumResult.equals(1))
			throw new DBMSFaultException("Medium's external system result and state not updated; updating problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteMedium(Medium inputMedium) throws MAIGeSDiException {
		
		//throw new ForbiddenException("Activity not allowed for this ExternalSystem", Utility.ErrorType.ET_FORBIDDEN_ERROR.getValue());
		Integer deleteResult = 0;
		
		
		//cancellare stati e flussi
		try 
		{
			//elimino il flusso operativo
			deleteResult = this.mapper.deleteMediumOperativeFlow(inputMedium.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (deleteResult.equals(0))
			throw new DBMSFaultException("Medium's operative flow not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			//elimino lo stato
			deleteResult = this.mapper.deleteMediumState(inputMedium.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (deleteResult.equals(0))
			throw new DBMSFaultException("Medium's State not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		//elimino  CREDENZIALE	
		try 
		{
			//elimino le credenziali
			deleteResult = this.mapper.deleteMediumCredential(inputMedium.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (deleteResult.equals(0))
			throw new DBMSFaultException("Medium's Credential not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
				
		//eliminare la riga
		try 
		{
			//elimino il flusso operativo
			deleteResult = this.mapper.deleteMedium(inputMedium.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (deleteResult.equals(0) || deleteResult > 1 )
			throw new DBMSFaultException("Medium not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		
	}

	public String getMediumPhoto(Integer mediumID, Integer operatorID) throws ResourceNotFoundException, UnsupportedEncodingException, MAIGeSDiException {
		String result = null;
		GetMediumPhotoResult mediumPhotoResult = null;		
		try 
		{
			mediumPhotoResult = this.mapper.getMediumPhotoByID(mediumID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(mediumPhotoResult == null)
			throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
		
		if(mediumPhotoResult.getPhoto()!=null) {
			result = new String(Base64.getEncoder().encode(mediumPhotoResult.getPhoto()), "UTF8");
		}
		
		return result;
	}

	public String getMediumSign(Integer mediumID, Integer operatorID) throws ResourceNotFoundException, UnsupportedEncodingException, MAIGeSDiException {
		String result = null;
		GetMediumSignResult mediumSignResult = null;	
		
		try 
		{
			mediumSignResult = this.mapper.getMediumSignByID(mediumID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(mediumSignResult == null)
			throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
		
		if(mediumSignResult.getSign()!=null) {
			result = new String(Base64.getEncoder().encode(mediumSignResult.getSign()), "UTF8");
		}
		
		return result;
	}

	public String getMediumTutorSign(Integer mediumID, Integer operatorID) throws ResourceNotFoundException, UnsupportedEncodingException, MAIGeSDiException {
		String result = null;
		GetMediumTutorSignResult mediumTutorSignResult = null;	
		
		try 
		{
			mediumTutorSignResult = this.mapper.getMediumTutorSignByID(mediumID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(mediumTutorSignResult == null)
			throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
		
		if(mediumTutorSignResult.getSign() != null) {
			result = new String(Base64.getEncoder().encode(mediumTutorSignResult.getSign()), "UTF8");
		}
		
		return result;
	}

	/**
	 * Gestione del codice erariale per il luogo di nascita al posto del comune e della nazione
	 * metodo per risolvere tutte le righe che hanno il valore del codice erariale del luogo di nascita a null
	 * ossia tutti i casi di inserimenti effettuati al di fuori di MAIGESDI
	 * in teoria dovrà sparire
	 * @param mediumID
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void checkAndUpdateBirthPlaceFiscalCode(Integer mediumID) throws Exception {
		GetMediumBirthPlaceFiscalCodeResult result = null;
		Integer rowAffectedUpdate = 0;
		if(mediumID != null && !mediumID.equals(0)) {
			try 
			{
				result = this.mapper.getMediumBirthPlaceFiscalCode(mediumID, null, null);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if(result != null && result.getMediumID() != null)
			{
				if(result.getBirthPlaceFiscalCode() == null || result.getBirthPlaceFiscalCode().isEmpty() ) 
				{
					try 
					{
						rowAffectedUpdate = this.mapper.checkAndUpdateBirthPlaceFiscalCode(mediumID, null, null);
					}catch(Exception e) {
						throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}
					
					
					if(rowAffectedUpdate > 1) {
						throw new DBMSFaultException("Error updating birth place fiscal code of the specific Medium with mediumID ="+mediumID, Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}
				}
			}else {
				throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
			}
		}else
		{
			throw new PreconditionException("MediumID is required", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
		}
	}
	
	
	/**
	 * gestione del codice erariale per il luogo di nascita al posto del comune e della nazione                
	 * metodo per risolvere tutte le righe che hanno il valore del codice erariale del luogo di nascita a null 
	 * ossia tutti i casi di inserimenti effettuati al di fuori di MAIGESDI                                    
	 * in teoria dovrà sparire                                                                                 
	 * @param mediumTypeID
	 * @param mediumIdentifier
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void checkAndUpdateBirthPlaceFiscalCode(Integer mediumTypeID, String mediumIdentifier) throws Exception {
		Integer rowAffectedUpdate = 0;
		GetMediumBirthPlaceFiscalCodeResult result = null;
		if(mediumTypeID != null && mediumTypeID>0 && mediumIdentifier!= null && !mediumIdentifier.isEmpty()) {
			try 
			{
				result = this.mapper.getMediumBirthPlaceFiscalCode(null, mediumTypeID, mediumIdentifier);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
						
			if(result != null && result.getMediumID() != null)
			{
				if(result.getBirthPlaceFiscalCode() == null || result.getBirthPlaceFiscalCode().isEmpty() ) 
				{
					try 
					{
						rowAffectedUpdate = this.mapper.checkAndUpdateBirthPlaceFiscalCode(null, mediumTypeID, mediumIdentifier);
					}catch(Exception e) {
						throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}
					if(rowAffectedUpdate > 1) {
						throw new DBMSFaultException("Error updating birth place fiscal code of the specific Medium with mediumTypeID ="+mediumTypeID+" and mediumIdentifier ="+ mediumIdentifier,
													Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}
				}
			}else {
				throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
			}
		}else{
			throw new PreconditionException("MediumTypeID and mediumIdentifier are required", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
		}
	}

	/**
	 * Reperisce un medium a partire dal suo id senza verificare i diritti di un operatore
	 * @param mediumID id del medium
	 * @return il medium o null
	 * @throws DBMSFaultException
	 */
	public Medium getMediumNoSecurity(Integer mediumID) throws DBMSFaultException {
		Medium result = null;
		try 
		{
			result = this.mapper.getMediumNoSecurityByMediumID(mediumID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	
	/**
	 * Reperisce un medium a partire dalla coppia mediumTypeID e mediumIdentifier senza verificare i diritti di un operatore
	 * @param mediumIdentifier identificativo del supporto digitale
	 * @param mediumTypeID id del tipo supporto digitale
	 * @return il medium o null
	 * @throws DBMSFaultException
	 */
	public Medium getMediumNoSecurity(String mediumIdentifier, Integer mediumTypeID) throws DBMSFaultException {
		Medium result = null;
		try 
		{
			result = this.mapper.getMediumNoSecurityByMediumIndentifierMediumTypeID(mediumTypeID, mediumIdentifier);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}
	
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void updateMedium(Operator operator, ParameterMedium parameterMedium, Integer stateTypeID, Integer operativeFlowTypeID) throws MAIGeSDiException {
		//Gestione foto
		byte[] decodedPhoto = null;
		boolean updatePhoto = false;
		Integer updatePhotoResult = 0;
		//Gestione firma
		byte[] decodedSign = null;
		boolean updateSign = false;
		Integer updateSignResult = 0;
		//Gestione firma tutore
		byte[] decodedTutorSign = null;
		boolean updateTutorSign = false;
		Integer updateTutorSignResult = 0;
		//gestione update medium
		Integer updateMediumResult = 0;
		Integer operativeFlowID;
		Integer stateID;
		//gestione aggiornamento stato e fluso
		Integer result;
		
		if(parameterMedium.getPhoto()!= null) {
			updatePhoto = true;
			if(parameterMedium.getPhoto().getValue() != null) {
				decodedPhoto = (Base64.getDecoder().decode(parameterMedium.getPhoto().getValue()));
			}
			
			parameterMedium.setPhoto(null);
		}
		
		if(parameterMedium.getSign()!= null) {
			updateSign = true;
			if(parameterMedium.getSign().getValue() != null) {
				decodedSign = (Base64.getDecoder().decode(parameterMedium.getSign().getValue()));
			}
			
			parameterMedium.setSign(null);
		}
		
		if(parameterMedium.getTutorSign() != null) {
			updateTutorSign = true;
			if(parameterMedium.getTutorSign().getValue() != null) {
				decodedTutorSign = (Base64.getDecoder().decode(parameterMedium.getTutorSign().getValue()));
			}
			
			parameterMedium.setTutorSign(null);
		}
		
		try 
		{
			updateMediumResult = this.mapper.updateMedium(parameterMedium);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		
		if(!updateMediumResult.equals(1))
			throw new DBMSFaultException("Medium not updated", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		//la foto è da aggiornare?
		if(updatePhoto) {
			try 
			{
				updatePhotoResult = this.mapper.addMediumPhoto(parameterMedium.getID().getValue(), decodedPhoto);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			if(!updatePhotoResult.equals(1))
				throw new DBMSFaultException("Medium not updated. Updating photo problem.", 
						Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		//la firma è da aggiornare?
		if(updateSign) {
			try 
			{
				updateSignResult = this.mapper.addMediumSign(parameterMedium.getID().getValue(), decodedSign);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			if(!updateSignResult.equals(1))
				throw new DBMSFaultException("Medium not updated. Updating sign problem.", 
						Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		//la firma del tutore è da aggiornare?
		if(updateTutorSign) {
			try 
			{
				updateTutorSignResult = this.mapper.addMediumTutorSign(parameterMedium.getID().getValue(), decodedTutorSign);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			if(!updateSignResult.equals(1))
				throw new DBMSFaultException("Medium not updated. Updating tutor sign problem.", 
						Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		//gestisco lo stato e il flusso
		// 1 gestione flusso operativo
		try 
		{
			//reperisco un id per il flusso operativo
			operativeFlowID = this.mapper.reserveIdentifier(new Sequence("SEQ_FLUSSOOPERATIVO"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (operativeFlowID.equals(0))
			throw new DBMSFaultException("Updating medium problem: Medium's operative flow not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		try 
		{
			result = this.mapper.insertMediumOperativeFlowRelation(parameterMedium.getID().getValue(), operativeFlowID, operativeFlowTypeID, operator.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Updating medium problem: Medium's operative flow not added; adding relation problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		// fine gestione flusso operativo	
		
		// 2 gestione  dello stato
		try 
		{
			//reperisco un id per lo stato
			stateID = this.mapper.reserveIdentifier(new Sequence("SEQ_STATOSUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (stateID.equals(0))
			throw new DBMSFaultException("Updating medium problem: Medium's state not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			//inserisco la relazione
			result = this.mapper.insertMediumStateRelation(parameterMedium.getID().getValue(), stateID, stateTypeID, operator.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Updating medium problem: Medium's state not added; adding relation problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		// fine gestione  dello stato
		
		
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void updateMediumStateAndOperativeFlow(Integer operatorID, Integer mediumID, Integer stateTypeID, Integer operativeFlowTypeID) throws MAIGeSDiException {
		
		Integer operativeFlowID;
		Integer stateID;
		//gestione aggiornamento stato e fluso
		Integer result;
		
		//gestisco lo stato e il flusso
		// 1 gestione flusso operativo
		try 
		{
			//reperisco un id per il flusso operativo
			operativeFlowID = this.mapper.reserveIdentifier(new Sequence("SEQ_FLUSSOOPERATIVO"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (operativeFlowID.equals(0))
			throw new DBMSFaultException("Updating medium operative flow problem: Medium's operative flow not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		try 
		{
			result = this.mapper.insertMediumOperativeFlowRelation(mediumID, operativeFlowID, operativeFlowTypeID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Updating medium operative flow problem:  Medium's operative flow not added; adding relation problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		// fine gestione flusso operativo	
		
		// 2 gestione  dello stato
		try 
		{
			//reperisco un id per lo stato
			stateID = this.mapper.reserveIdentifier(new Sequence("SEQ_STATOSUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (stateID.equals(0))
			throw new DBMSFaultException("Updating medium state problem: Medium's state not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			//inserisco la relazione
			result = this.mapper.insertMediumStateRelation(mediumID, stateID, stateTypeID, operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || !result.equals(1))
			throw new DBMSFaultException("Updating medium state problem: Medium's state not added; adding relation problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		// fine gestione  dello stato
				
				
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void completeMediumFromMediumIdentfierRange(Operator operator, Medium medium, ReservedMedium reservedMedium) throws MAIGeSDiException {
		Integer result = 0;
		Integer updatePhoto = 0;
		Integer updateSign = 0;
		Integer updateTutorSign = 0;
		Integer updateComuneNazione = 0;
		ParameterQueryInsertCredentialForMedium request = null;
		Integer insertCredential = 0;
		
		//Medium incompleteMedium = this.mediumIdenfierRangeDAO.getNextMediumIdentifierRange(operator.getID(), medium.getMediumType().getID());
		if(reservedMedium != null && reservedMedium.getMediumID() != null && reservedMedium.getMediumID() > 0 && reservedMedium.getMediumIdentifier() != null && !reservedMedium.getMediumIdentifier().isEmpty() ) {
						
			//Gestione foto
			byte[] decodedPhoto = null;
			if(medium.getPhoto()!= null) {
				decodedPhoto = (Base64.getDecoder().decode(medium.getPhoto()));
				medium.setPhoto(null);
			}
			
			//Gestione firma
			byte[] decodedSign = null;
			if(medium.getSign()!= null) {
				decodedSign = (Base64.getDecoder().decode(medium.getSign()));
				medium.setSign(null);
			}
			
			//Gestione firma tutore
			byte[] decodedTutorSign = null;
			if(medium.getTutor() != null && medium.getTutor().getSign() != null) {
				decodedTutorSign = (Base64.getDecoder().decode(medium.getTutor().getSign()));
				medium.getTutor().setSign(null);
			}
			
			//GESTIONE COMPLETAMENTO SUPPORTO RISERVATO 
			medium.setID(reservedMedium.getMediumID());
			medium.setMediumIdentifier(reservedMedium.getMediumIdentifier());
			try 
			{
				//completo il supporto preso dalla spezzata
				result = this.mapper.completeMediumFromMediumIndetifierRange(medium);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
//			
			if(result == null || !result.equals(1))
				throw new DBMSFaultException("Completing medium problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			
			//inserimento della foto
			if(decodedPhoto != null) {
				try 
				{
					updatePhoto = this.mapper.addMediumPhoto(medium.getID(), decodedPhoto);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				if(!updatePhoto.equals(1))
					throw new DBMSFaultException("Medium not added; Adding medium's photo problem; Medium: "+ medium.toString(), 
													Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			//inserimento della firma
			if(decodedSign != null) {
				try 
				{
					updateSign = this.mapper.addMediumSign(medium.getID(), decodedSign);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				if(!updateSign.equals(1))
					throw new DBMSFaultException("Medium not added; Adding medium's sign problem; Medium: "+ medium.toString(), 
													Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			//inserimento della firma del tutore
			if(decodedTutorSign != null) {
				try 
				{
					updateTutorSign = this.mapper.addMediumTutorSign(medium.getID(), decodedTutorSign);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				if(!updateTutorSign.equals(1))
					throw new DBMSFaultException("Medium not added; Adding medium's tutor sign problem; Medium: "+ medium.toString(), 
													Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
					
			//se sono presenti le credenziali di accesso allora le inserisco
			if(medium.getCredentials()!=null && medium.getCredentials().size()>0) 
			{
				for(Credential cred: medium.getCredentials()) {
					request = new ParameterQueryInsertCredentialForMedium(reservedMedium.getMediumID(), cred.getCredentialType().getID(), cred.getValue());
					insertCredential = 0;
					try 
					{
						insertCredential = this.mapper.insertMediumCredential(request);
					}catch(Exception e) {
						throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}
					if(!insertCredential.equals(1))
						throw new DBMSFaultException("Medium not added, adding medium's credential problem", 
								Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
			}
			
			//GESTIONE  idComune e idNazione sulla tupla di SUPPORTODIGITALE per il pregresso
			if(medium.getBirthPlace() != null && medium.getBirthPlace().getFiscalCode() != null && !medium.getBirthPlace().getFiscalCode().isEmpty() ) {
				try 
				{
					updateComuneNazione = this.mapper.updateMediumAddComuneAndNazione(medium.getID(), medium.getBirthPlace().getFiscalCode(), medium.getBirthDate());
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				if(!updateComuneNazione.equals(1))
					throw new DBMSFaultException("Medium not added; Adding medium's Comune and Nazione problem; Medium: "+ medium.toString(), 
													Utility.ErrorType.ET_DBMS_ERROR.getValue());
				
			}
			
			
		}else {
			throw new ConflictException("", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_IDENTIFIER_NOT_RETURNED.getValue());
		}
				
		//return medium;
	}

	public List<Medium> searchMediumCustom(Operator operator, 
											Integer mediumTypeID,
											String mediumIdentifier,
											Date releaseDateFrom, 
											Date releaseDateTo, 
											Date expiryDateFrom,
											Date expiryDateTo,
											Integer stateTypeID,
											Integer operativeFlowTypeID,
											Integer lastTransitionOperatorID,
											String givenName,
											String familyName,
											String fiscalCodeBirthPlace,
											Date birthDate,
											Integer emitterOperatorID,
											Integer offset,
											Integer maxResults
											) throws DBMSFaultException {
		
		
		List<Medium> result =null;
		try 
		{
			result = this.mapper.searchMediumCustom(operator.getID(),
													mediumTypeID,
										 			mediumIdentifier,
										 			releaseDateFrom,
													releaseDateTo,
													expiryDateFrom,
													expiryDateTo,
													stateTypeID,
													operativeFlowTypeID,
													lastTransitionOperatorID,
													givenName,
													familyName,
													fiscalCodeBirthPlace,
													birthDate,
													emitterOperatorID,
													offset,
													maxResults);
												
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		return result;
	}

	
	public Integer countSearchMediumCustom(Operator operator, 
											Integer mediumTypeID,
											String mediumIdentifier,
											Date releaseDateFrom, 
											Date releaseDateTo, 
											Date expiryDateFrom,
											Date expiryDateTo,
											Integer stateTypeID,
											Integer operativeFlowTypeID,
											Integer lastTransitionOperatorID,
											String givenName,
											String familyName,
											String fiscalCodeBirthPlace,
											Date birthDate,
											Integer emitterOperatorID) throws DBMSFaultException {
						
			Integer result =null;
			try 
			{
				result = this.mapper.countSearchMediumCustom(operator.getID(),
											mediumTypeID,
								 			mediumIdentifier,
								 			releaseDateFrom,
											releaseDateTo,
											expiryDateFrom,
											expiryDateTo,
											stateTypeID,
											operativeFlowTypeID,
											lastTransitionOperatorID,
											givenName,
											familyName,
											fiscalCodeBirthPlace,
											birthDate,
											emitterOperatorID);
										
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
						
			return result;
		}

	/**
	 * Metodo per l'aggiornamento dello IuA nelle verifiche del sistema, differisce dall'altro metodo poichè questo non modifica lo stato del medium
	 * @param mediumID del medium
	 * @param cenAuthResult risultato dell'operazione
	 * @throws DBMSFaultException W
	 */
	public void updateMediumExternalSystemResult(Integer mediumID, CENAuthenticationResult cenAuthResult) throws DBMSFaultException {
		
			Integer updateMediumResult = 0;
		
				try 
				{
					String CENExcepiton = cenAuthResult.getCodiceEccezione() != null && cenAuthResult.getDescrizioneEccezione() != null ?
											cenAuthResult.getCodiceEccezione()+" - "+ cenAuthResult.getDescrizioneEccezione() :
											null;
					//aggiorno il medium
					updateMediumResult = this.mapper.updateMediumAuthorizationID(mediumID, 
																					  cenAuthResult.getIuA(), 
																					  CENExcepiton);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				if(updateMediumResult  == null || !updateMediumResult.equals(1))
					throw new DBMSFaultException("Medium's external system result not updated; updating problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
				
		
	}
	
	/**
	 * Servizio che controlla se il codice erariale del luogo di nascita è valido con la data di nascita
	 * @param birthDate rapprenseta la data di nascita
	 * @param fiscalCodeBirthPlace rappresenta il codice erariale del luogo di nascita (es H501 per Roma)
	 */
	public boolean validateBirthDateAndFiscalCodeBirthPlace(Date birthDate, String fiscalCodeBirthPlace) {
		boolean result = false;
		
		Integer validationResult = this.mapper.validateBirthDateAndFiscalCodeBirthPlace(birthDate, fiscalCodeBirthPlace);
		result = validationResult != null &&  validationResult == 1;
		
		if(!result) {
			logger.error("FiscalCodeBirthPlace not valid for specific birthdate: FiscalCodeBirthPlace: {0}, birthdate: {1}", fiscalCodeBirthPlace, birthDate);
		}
		
		return result;
		
	}

	public List<Medium> getListValidMediumOfOrganizationByPerson(Integer mediumTypeID, Person person, Integer externalSystemID) throws DBMSFaultException {
		List<Medium> result = null;
		
		try 
		{
			result = this.mapper.getListValidMediumOfOrganizationByPerson(mediumTypeID, person, externalSystemID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
	}

}
