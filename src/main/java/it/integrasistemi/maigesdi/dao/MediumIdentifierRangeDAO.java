package it.integrasistemi.maigesdi.dao;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.integrasistemi.maigesdi.bean.EmptyMediumForMediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.ReservedMedium;
import it.integrasistemi.maigesdi.bean.mybatis.GetNextMediumIdentifierRangeResult;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetMediumByMediumTypeIDMediumIdentifier;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Component
public class MediumIdentifierRangeDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(MediumIdentifierRangeDAO.class);
	@Autowired
	private MAIGESDIMapper mapper;

	public List<MediumIdentifierRange> getAllMediumIdentifierRange() throws DBMSFaultException {
		List<MediumIdentifierRange> result = null;
		try 
		{
			result = this.mapper.getAllMediumIdentifierRange();
			//logger.info("DB RESULT: Medium: "+result.toString());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
			 return result;
		
	}
	
	public List<MediumIdentifierRange> getMediumIdentifierRangesByMediumTypeID( Integer mediumTypeID) throws DBMSFaultException {
		List<MediumIdentifierRange> result = null;
		try 
		{
			result = this.mapper.getMediumIdentifierRangesByMediumTypeID(mediumTypeID);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}
	
	
	public List<MediumIdentifierRange> getMediumIdentifierRangesByOrganizationID(Integer organizationID) throws DBMSFaultException {
		List<MediumIdentifierRange> result = null;
		try 
		{
			result = this.mapper.getMediumIdentifierRangesByOrganizationID(organizationID);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public List<MediumIdentifierRange>  checkOverlayMediumIdentifierRange(MediumIdentifierRange mediumIdentifierRange) throws DBMSFaultException {
		List<MediumIdentifierRange> result = null;
		try 
		{
			result = this.mapper.checkOverlayMediumIdentifierRange(mediumIdentifierRange);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}
	
	public List<MediumIdentifierRange>  selectAndLockMediumIdentifierRangeByMediumTypeID(Integer mediumTypeID) throws DBMSFaultException {
		List<MediumIdentifierRange> result = null;
		try 
		{
			result = this.mapper.selectAndLockMediumIdentifierRangeByMediumTypeID(mediumTypeID);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer insertMediumIdentifierRange(MediumIdentifierRange mediumIdentifierRange) throws DBMSFaultException {
		Integer resultInsert = null;
		Integer mediumIdentifierRangeID = null;
		//reperisco un id per il medium
		try 
		{
			mediumIdentifierRangeID = this.mapper.reserveIdentifier(new Sequence("SEQ_SPEZZATASUPPORTI"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (mediumIdentifierRangeID.equals(0))
			throw new DBMSFaultException("Medium identifier range not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			mediumIdentifierRange.setID(mediumIdentifierRangeID);
			resultInsert = this.mapper.insertMediumIdentifierRange(mediumIdentifierRange);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(!resultInsert.equals(1))
			throw new DBMSFaultException("Adding medium identifier range problem;", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		return mediumIdentifierRangeID;
		
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer insertMediumOfMediumIdentifierRange(Integer mediumTypeID, String typographicNumber, String lisTicketCode, String mediumIdentifier) throws DBMSFaultException {
		Integer result = null;
		Integer mediumID = null;
		try 
		{
			mediumID = this.mapper.reserveIdentifier(new Sequence("SEQ_SUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (mediumID.equals(0))
			throw new DBMSFaultException("Medium not added for medium identifer range; Reserve oracle sequence identifier problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());

		try 
		{
			result = this.mapper.insertMediumOfMediumIdentifierRange(mediumID, mediumTypeID, typographicNumber, lisTicketCode, mediumIdentifier);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(!result.equals(1))
			throw new DBMSFaultException("Adding medium, for medium identifier range, problem;", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		return result;
	}
		
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer insertAllMediumOfMediumIdentifierRange(List<EmptyMediumForMediumIdentifierRange> listEmptyMedium) throws DBMSFaultException {
		Integer result = null;
		try 
		{
			result = this.mapper.insertAllMediumOfMediumIdentifierRange(listEmptyMedium);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(!result.equals(listEmptyMedium.size()))
			throw new DBMSFaultException("Adding medium, for medium identifier range, problem;", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		return result;
		
	}

	public MediumIdentifierRange getMediumIdentifierRangeByID(Integer mediumIdentifierRangeID) throws DBMSFaultException {
		MediumIdentifierRange result = null;
		try 
		{
			result = this.mapper.getMediumIdentifierRangeByID(mediumIdentifierRangeID);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
	}
	
	public Integer checkExistingMediumInMediumIdentifierRange(Integer mediumTypeID, String startMediumIdentifier, String endMediumIdentifier) throws DBMSFaultException {
		Integer result = null;
		try 
		{
			result = this.mapper.countExistingMediumInMediumIdentifierRange(mediumTypeID, startMediumIdentifier, endMediumIdentifier);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ReservedMedium getNextMediumIdentifierRange(Integer operatorID, Integer mediumTypeID, Integer operativeFlowID, Integer stateID) throws DBMSFaultException {
		ReservedMedium result =null;
		GetNextMediumIdentifierRangeResult parameter;
		try 
		{	
			parameter = new GetNextMediumIdentifierRangeResult(mediumTypeID, operatorID, operativeFlowID, stateID);
			this.mapper.getNextMediumIdentifierRange(parameter);
			
			result = new ReservedMedium(parameter.getMediumID(), parameter.getMediumIdentifier(), mediumTypeID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	
	}

	//esistem una spezzata per questo tipo supporto?
	public boolean checkExistingMediumIdentifierRange(Integer mediumTypeID) throws DBMSFaultException {
		boolean result = false;
		try 
		{	
			result = this.mapper.countMediumIdentifierRangeForMediumType(mediumTypeID) > 0;
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public boolean checkAvaibilityMediumIdentifierRange(Integer mediumTypeID) throws DBMSFaultException {
		boolean result = false;
		try 
		{	
			result = this.mapper.countMediumInMediumIdentifierRange(mediumTypeID) > 0;
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void clearReservedMedium(Integer mediumID, String pin) throws DBMSFaultException {
		Integer result;
		try 
		{	
			result = this.mapper.clearReservedMedium(mediumID, pin);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(!result.equals(1))
			throw new DBMSFaultException("Clear reserved medium identifier problem;", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{	
			result = this.mapper.deleteMediumCredential(mediumID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		 
	}

	
	public MediumIdentifierRange getMediumIdentifierRangeByMediumIdentifier(Integer mediumTypeID, String mediumIdentifier) throws DBMSFaultException {
		MediumIdentifierRange result = null;
		
		try 
		{	
			result = this.mapper.getMediumIdentifierRangeByMediumIdentifier(mediumTypeID, mediumIdentifier);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}		
		
		return result;
	}
	
	public void deleteMediumIdentifierRangeAndMediumForUnitTest(MediumIdentifierRange mediumIdentifierRange) throws MAIGeSDiException {
		Integer result;
		try 
		{	
			result = this.mapper.deleteMediumIdentifierRange(mediumIdentifierRange.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(!result.equals(1))
			throw new DBMSFaultException("Delete medium identifier problem;", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		Integer aspectedMedium = new Integer(mediumIdentifierRange.getEnd()) - new Integer(mediumIdentifierRange.getStart()) + 1;
		
		try 
		{	
			result = this.mapper.deleteMediumFromMediumIdentifierRange(mediumIdentifierRange.getMediumType().getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(!result.equals(aspectedMedium))
			throw new DBMSFaultException("Delete medium created by medium identifier range problem;", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		
	}

	public List<MediumIdentifierRange> checkParallelOverlayMediumIdentifierRange(Integer mediumIdentifierRangeResultID,
																				@Valid MediumIdentifierRange mediumIdentifierRange) throws DBMSFaultException {
		List<MediumIdentifierRange> result = null;
		try 
		{
			result = this.mapper.checkParallelOverlayMediumIdentifierRange(mediumIdentifierRangeResultID, mediumIdentifierRange);
			
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	
	
	
	
}
