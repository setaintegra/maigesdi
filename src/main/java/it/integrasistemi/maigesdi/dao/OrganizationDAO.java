package it.integrasistemi.maigesdi.dao;

import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Component
public class OrganizationDAO {

	@Autowired
	private MAIGESDIMapper mapper;

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer insertOrganization(Organization organization) throws Exception {
		Integer insertResult = 0;
		Integer organizationID = 0;

		try 
		{
			organizationID = mapper.reserveIdentifier(new Sequence("SEQ_ORGANIZZAZIONE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (organizationID > 0) {

			organization.setID(organizationID);
			try 
			{
				insertResult = this.mapper.insertOrganization(organization);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if (!insertResult.equals(1))
				throw new DBMSFaultException("Organization not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());

		} else {
			throw new DBMSFaultException("Organization not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}

		return organizationID;
	}

	public Organization getOrganizationByID(Integer organizationID) throws Exception {

		Organization organization = null;
		try 
		{
			organization = this.mapper.getOrganizationByID(organizationID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(organization == null)
			throw new ResourceNotFoundException("Organization not found", Utility.ErrorType.ET_ORGANIZATION_NOT_FOUND.getValue());
		
		return organization;
		
				
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Organization updateOrganization(Organization organization) throws Exception {
		Organization storedOrganization = null;
		Integer updateResult = 0;
		Organization result = null;

			try 
			{
				storedOrganization = this.mapper.getOrganizationByID(organization.getID());
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if (storedOrganization != null) {

			storedOrganization.setName(organization.getName());
			storedOrganization.setFiscalCode(organization.getFiscalCode());
			
			try 
			{
				updateResult = this.mapper.updateOrganization(storedOrganization);
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if (updateResult == 1) {
				result = storedOrganization;
			} else {
				throw new DBMSFaultException("Organization not updated", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}

		} else {
			throw new ResourceNotFoundException("Organization not found", Utility.ErrorType.ET_ORGANIZATION_NOT_FOUND.getValue());
		}
		return result;
	}

	public List<Organization> getOrganizations() throws DBMSFaultException {
		
		List<Organization> organizations = null;
		try 
		{
			organizations = this.mapper.getOrganizations();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return organizations;
	}

	public String getOrganizationFiscalCodeByMediumTypeID(Integer mediumTypeID) throws MAIGeSDiException {
		String result = null;
		try 
		{
			result = this.mapper.getOrganizationFiscalCodeByMediumTypeID(mediumTypeID);	
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(result == null || result.isEmpty()) 
			throw new DBMSFaultException("Organization fiscal code not found; Medium type ID: " + mediumTypeID, 
										Utility.ErrorType.ET_DBMS_ERROR.getValue());
		return result ;
	}

	public String getOrganizationFiscalCodeByConiCode(String coniCode) throws MAIGeSDiException {
		String result = null;
		try 
		{
			result = this.mapper.getOrganizationFiscalCodeByConiCode(coniCode);	
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(result == null || result.isEmpty()) 
			throw new DBMSFaultException("Organization fiscal code not found; ConiCode: " + coniCode, 
										Utility.ErrorType.ET_DBMS_ERROR.getValue());
		return result ;
	}
}
