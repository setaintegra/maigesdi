package it.integrasistemi.maigesdi.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.ActivityType;
import it.integrasistemi.maigesdi.bean.OperativeFlowType;
import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.StateType;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;


/**
 * 
 * @author Limiti
 *
 */
@Component
public class UtilityDAO {
	
	@Autowired
	private MAIGESDIMapper mapper;

	
	public String checkDbConnection() throws DBMSFaultException {
		String result;
		
		try 
		{
			result = this.mapper.checkDbConnection();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
	}
	
	public List<ActivityType> getAllActivityType() throws DBMSFaultException{
		
		List<ActivityType> result;
		try 
		{
			result = this.mapper.getAllActivityType();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
		
	}
	
	public List<Activity> getAllActivity() throws DBMSFaultException{
		
		List<Activity> result;
		try 
		{
			result = this.mapper.getAllActivity();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
		
	}
	
	public List<StateType> getAllStateType() throws DBMSFaultException{
		
		List<StateType> result;
		try 
		{
			result = this.mapper.getAllStateType();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
		
	}
	
	public List<OperativeFlowType> getAllOperativeFlowType() throws DBMSFaultException{
		
		List<OperativeFlowType> result;
		try 
		{
			result = this.mapper.getAllOperativeFlowType();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
		
	}

	/**
	 * Metodo che ritorna il nome del db al quale il sistema si è connesso.
	 * @return
	 * @throws DBMSFaultException
	 */
	public String getDBName() throws DBMSFaultException{
		String result;
		try 
		{
			result = this.mapper.getDBName();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		return result;
	}
	
	

}
