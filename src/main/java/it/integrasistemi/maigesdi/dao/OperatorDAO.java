package it.integrasistemi.maigesdi.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryChangeOperatorPasword;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryCheckOperatorMediumTypeRights;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInserMediumTypeForOperator;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Component
public class OperatorDAO {

	@Autowired
	private MAIGESDIMapper mapper;

	public Operator getOperator(Operator operator) throws MAIGeSDiException {
		Operator result =null;
		try 
		{
			result = this.mapper.getOperatorLiteForLogin(operator); 	
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public List<Operator> getOperators() throws MAIGeSDiException {
		List<Operator> result =null;
		try 
		{
			result = this.mapper.getOperators();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public Operator getOperatorByID(Integer id) throws MAIGeSDiException {
		Operator result =null;
		try 
		{
			result = this.mapper.getOperatorByID(id);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public Integer insertOpearator(Operator operator) throws MAIGeSDiException {
		Integer operatorID = 0;
		Integer insertResult = 0;
		
		Integer insertMediumType = 0;
		
		ParameterQueryInserMediumTypeForOperator request =null;
		try 
		{
			operatorID =  this.mapper.reserveIdentifier(new Sequence("SEQ_OPERATORE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (operatorID.equals(0))
			throw new DBMSFaultException("Operator not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		operator.setID(operatorID);
		try 
		{
			insertResult = this.mapper.insertOperator(operator);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (!insertResult.equals(1))
			throw new DBMSFaultException("Operator not added", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		if(operator.getMediumTypes()!=null && operator.getMediumTypes().size()>0) {
			
			//for(MediumTypeLite mediumType : operator.getMediumTypes()) {
				request = new ParameterQueryInserMediumTypeForOperator(operatorID, operator.getMediumTypes());
				
				insertMediumType = 0;
				try 
				{
					insertMediumType = this.mapper.insertMediumTypeForOperator(request);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				if(!insertMediumType.equals(operator.getMediumTypes().size()))
						throw new DBMSFaultException("Operator not added, adding problem of Operator's MediumType; Check the MediumType ID ", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			
		}
				
		return operatorID;
	}
	
	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public Integer updateOpearator(Operator operator) throws MAIGeSDiException {
		Integer updateResult = 0;
		Integer insertMediumTypeResult = 0;
		Integer deleteResult = 0;
		ParameterQueryInserMediumTypeForOperator request;
		Operator storedOperator =null;
		
		try 
		{
			storedOperator = this.mapper.getOperatorByID(operator.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		if(storedOperator !=null) {
			
			//storedOperator.setPassword(operator.getPassword());
			storedOperator.setProfileID(operator.getProfileID());
			try 
			{
				updateResult = this.mapper.updateOperator(storedOperator);	
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			if(!updateResult.equals(1))
				throw new DBMSFaultException("Operator not updated, update operator problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			
			// se nel DB ci sono relazioni tra operatore e tiposupporto
			if(storedOperator.getMediumTypes()!=null && storedOperator.getMediumTypes().size()>0) {
				//li elimino per inserire le nuove relazioni passate
				try 
				{
					deleteResult= this.mapper.deleteMediumTypeForOperatorByIDOperator(storedOperator.getID());
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				//controllo il risultato dell'eliminazione
				if(!deleteResult.equals(storedOperator.getMediumTypes().size()))
					throw new DBMSFaultException("Operator not updated, updating Operator's Medium Type problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			
			if(operator.getMediumTypes()!=null && operator.getMediumTypes().size()>0) {
				
				request = new ParameterQueryInserMediumTypeForOperator(operator.getID(), operator.getMediumTypes());
				
				insertMediumTypeResult = 0;
				try 
				{
					insertMediumTypeResult = this.mapper.insertMediumTypeForOperator(request);
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				if(!insertMediumTypeResult.equals(operator.getMediumTypes().size()))
						throw new DBMSFaultException("Operator not updated, adding problem of Operator's MediumType; Check the MediumType ID ", 
													Utility.ErrorType.ET_DBMS_ERROR.getValue());
			
			}
			
			
		}else {
			throw new ResourceNotFoundException("Operator not found", Utility.ErrorType.ET_OPERATOR_NOT_FOUND.getValue());
		}

		
		return storedOperator.getID();
	}

	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public void changeOperatorPassword(Operator operator, String oldPassword, String newPassword) throws Exception {
		
		Integer updateResult=0;
		ParameterQueryChangeOperatorPasword request = new ParameterQueryChangeOperatorPasword(operator.getID(), oldPassword, newPassword);
		try 
		{
			 updateResult = this.mapper.updateOperatorPassword(request);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
			 
		if(!updateResult.equals(1))
			 throw new DBMSFaultException("Password not changed", Utility.ErrorType.ET_DBMS_ERROR.getValue());

		
		
	}

	public boolean checkOperatorMediumTypeRights(Operator operator, Integer mediumTypeID) throws MAIGeSDiException {
		Boolean result= false;
		Integer queryResult = 0;
		ParameterQueryCheckOperatorMediumTypeRights request = new ParameterQueryCheckOperatorMediumTypeRights(operator.getID(), mediumTypeID);
		try 
		{
			queryResult = this.mapper.getOperatorMediumType(request);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
				
		result = queryResult > 0 ? true: false;
		
		return result;
	}

	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public void deleteOperator(Operator operatorCreated) {
		
	}
	
}
