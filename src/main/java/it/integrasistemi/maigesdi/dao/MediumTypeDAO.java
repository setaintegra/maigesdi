package it.integrasistemi.maigesdi.dao;

import java.util.List;

import org.apache.ibatis.exceptions.TooManyResultsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Parameter;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetMediumTypeByID;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInsertActivityForMediumType;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryInsertCredentialTypeForMediumType;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Component
public class MediumTypeDAO {
	private static final Logger logger = LoggerFactory.getLogger(MediumTypeDAO.class);

	@Autowired
	private MAIGESDIMapper mapper;

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer addMediumType(MediumType mediumType) throws Exception {
		Integer mediumTypeID = 0;
		Integer insertResult = 0;
		
		Integer insertParam = 0;
		Integer updateResult = 0;
		Integer insertCredential = 0;
		
		ParameterQueryInsertActivityForMediumType parameterActivityRequest = null;
		ParameterQueryInsertCredentialTypeForMediumType parameterCredentialRequest = null;
		
		try 
		{
			mediumTypeID = this.mapper.reserveIdentifier(new Sequence("SEQ_TIPOSUPPORTODIGITALE"));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (mediumTypeID.equals(0))
			throw new DBMSFaultException("MediumType not added, MediumType ID reservetion error", Utility.ErrorType.ET_DBMS_ERROR.getValue());

		mediumType.setID(mediumTypeID);
			
		try 
		{
			insertResult = this.mapper.insertMediumType(mediumType);
		}catch(Exception e) {
			throw new DBMSFaultException("MediumType not added "+ e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		if (!insertResult.equals(1))
			throw new DBMSFaultException("MediumType not added, insert MediumType error", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
//		//aggiunta del sistema esterno
//		try 
//		{
//			updateResult = this.mapper.updateMediumTypeAddingExternalSystem(mediumType.getID(), mediumType.getExternalSystem().getID());
//		}catch(Exception e) {
//			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
//		}
//		if (!updateResult.equals(1))
//			throw new DBMSFaultException("MediumType not added, insert External System error; Check validitt of external system used", Utility.ErrorType.ET_DBMS_ERROR.getValue());
//		
		
		//aggiunta delle attivita associate al tiposupporto
		if (mediumType.getActivity() != null && mediumType.getActivity().size() > 0) {

			for (Activity activity : mediumType.getActivity()) {

				for (Parameter param : activity.getParameters()) {
					parameterActivityRequest = new ParameterQueryInsertActivityForMediumType(mediumType, activity.getCode(), param);
					insertParam = 0;
					try 
					{
						insertParam = this.mapper.insertActivityForMediumType(parameterActivityRequest);
					}catch(Exception e) {
						throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}
					if (!insertParam.equals(1))
						throw new DBMSFaultException("MediumType not added, adding problem of Parameter", Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}

			}

		}
		
		//aggiunta dei tipi credenziali associati al tiposupporto
		if(mediumType.getCredentialTypes() != null && mediumType.getCredentialTypes().size() > 0) {
			
			parameterCredentialRequest = new ParameterQueryInsertCredentialTypeForMediumType(mediumType.getID(), mediumType.getCredentialTypes());
			try 
			{
				insertCredential = this.mapper.insertCredentialTypeForMediumType(parameterCredentialRequest);	
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
						
			if(!insertCredential.equals(mediumType.getCredentialTypes().size()))
				throw new DBMSFaultException("MediumType not added, credential type problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}

		return mediumTypeID;

	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Integer updateMediumType(MediumType mediumType) throws Exception {
		MediumType storedMediumType = null;
		Integer updateResult = 0;
		
		Integer insertParam = 0;
		ParameterQueryInsertActivityForMediumType parameterRequest = null;
		Integer deleteResult =0;
		try 
		{
			storedMediumType = this.mapper.getMediumTypeByIDForConfigurator(mediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (storedMediumType != null) {
			
			storedMediumType.setName(mediumType.getName());
			storedMediumType.setSeriesCode(mediumType.getSeriesCode());
			storedMediumType.setNumberDaysValidity(mediumType.getNumberDaysValidity());
			storedMediumType.setExternalSystem(mediumType.getExternalSystem());
			try 
			{
				updateResult= this.mapper.updateMediumType(storedMediumType);	
			}catch(Exception e) {
				throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
			if(!updateResult.equals(1))
				throw new DBMSFaultException("MediumType not updated, update problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			
			//controllo la presenza della lista di Action, se c'è la cancello altrimenti vado oltre
			if(storedMediumType.getActivity()!=null && storedMediumType.getActivity().size()>0) {
				try 
				{
					deleteResult = this.mapper.deleteActivityTypesForMediumTypeByIDMediumType(storedMediumType.getID());
				}catch(Exception e) {
					throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
				}
				
				//se le righe eliminate sono zero allora rilancio eccezione;
				//in prima battuta era stato pensato di controllare il numero effettivo di righe ma si è convenuto con ilf atto che
				//si da per scontato che la delete funzioni correttamente
				if(!(deleteResult>0))
					throw new DBMSFaultException("MediumType not updated, replacement Parameter problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
			}
										
			if (mediumType.getActivity() != null && mediumType.getActivity().size() > 0) {

				for (Activity activity : mediumType.getActivity()) {

					for (Parameter param : activity.getParameters()) {
						parameterRequest = new ParameterQueryInsertActivityForMediumType(mediumType, activity.getCode(), param);
						insertParam = 0;
						try 
						{
							insertParam = this.mapper.insertActivityForMediumType(parameterRequest);
						}catch(Exception e) {
							throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
						}
						if (!insertParam.equals(1))
							throw new DBMSFaultException("MediumType not added, insert Parameter problem", Utility.ErrorType.ET_DBMS_ERROR.getValue());
					}

				}

			}
		} else {
			throw new ResourceNotFoundException("MediumType not found", Utility.ErrorType.ET_MEDIUM_TYPE_NOT_FOUND.getValue());
		}

		return mediumType.getID();

	}

	public List<MediumType> getMediumTypeForConfigurator() throws MAIGeSDiException {
		List<MediumType> result = null;
		try 
		{
			result = this.mapper.getMediumTypeForConfigurator();
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public MediumType getMediumTypeByIDForConfigurator(Integer id) throws MAIGeSDiException {
		MediumType result = null;
		try 
		{
			result = this.mapper.getMediumTypeByIDForConfigurator(id);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public MediumType getMediumTypeByID(Integer id,  Integer operatorID) throws MAIGeSDiException {
		MediumType result = null;
		try 
		{
			result = this.mapper.getMediumTypeByID(new ParameterQueryGetMediumTypeByID(id, operatorID));
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public List<MediumType> getMediumType(Integer id) throws MAIGeSDiException {
		List<MediumType> result = null;
		try 
		{
			result = this.mapper.getMediumType(id);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
	}

	public Activity getParameterListByMediumTypeIDActivityID(MediumPost mediumPost) throws MAIGeSDiException {
		Activity result =null;
		try 
		{
			result = this.mapper.getParameterListByMediumTypeIDActivityID(mediumPost);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return result;
		
	}

	public List<MediumType> getMediumTypeByMediumIdentifier(Integer operatorID, String mediumIdentifier) throws MAIGeSDiException {
		List<MediumType> result = null;

		try 
		{
			result = this.mapper.getMediumTypeByMediumIdentifier(operatorID, mediumIdentifier);
			logger.info("DB RESULT: List medium type: "+result.toString());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}

		return result;
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteMediumType(MediumType inputMediumType) throws MAIGeSDiException {
		
		Integer deleteResult = 0;
		
		
		//cancellare Medium Type/tipo supportodigitale
		try 
		{
			//elimino il SPEZZATASUPPORTI
			deleteResult = this.mapper.deleteMediumTypeSpezzataSupporti(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
//		if (deleteResult.equals(0))
//			throw new DBMSFaultException("Medium's operative flow not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		try 
		{
			//elimino  OPERATORE_TIPOSUPPORTODIGITALE
			deleteResult = this.mapper.deleteMediumTypeOperators(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
//		if (deleteResult.equals(0))
//			throw new DBMSFaultException("MediumType's operator right not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		//elimino  SUPPORTODIGITALE	
		try 
		{
			//elimino le SUPPORTODIGITALE
			deleteResult = this.mapper.deleteMediumTypeMediums(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
//		if (deleteResult.equals(0))
//			throw new DBMSFaultException("Medium's Credential not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
				
		//elimino  TIPOSUPPORTO_TIPOCREDENZIALE	
		try 
		{
			//elimino le TIPOSUPPORTO_TIPOCREDENZIALE
			deleteResult = this.mapper.deleteMediumTypeCredentialTypes(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
//		if (deleteResult.equals(0))
//			throw new DBMSFaultException("Medium's Credential not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
						
		
		//elimino  PREZZOTIPOSUPPORTODIGITALE	
		try 
		{
			//elimino PREZZOTIPOSUPPORTODIGITALE
			deleteResult = this.mapper.deleteMediumTypePrezzoTipoSupportoDigitale(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
//		if (deleteResult.equals(0))
//			throw new DBMSFaultException("Medium's Credential not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		
		//elimino  UTILIZZOPARAMETRO	
		try 
		{
			//elimino le UTILIZZOPARAMETRO
			deleteResult = this.mapper.deleteMediumTypeParameters(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
//		if (deleteResult.equals(0))
//			throw new DBMSFaultException("Medium's Credential not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
								
		
		//eliminare la riga TIPOSUPPORTODIGITALE
		try 
		{
			//elimino TIPOSUPPORTODIGITALE
			deleteResult = this.mapper.deleteMediumType(inputMediumType.getID());
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if (deleteResult.equals(0) || deleteResult > 1 )
			throw new DBMSFaultException("Medium type not deleted", Utility.ErrorType.ET_DBMS_ERROR.getValue());
		
		
	}

	public MediumType getMediumTypeByCodes(Integer operatorID, String organizationCode, String seriesCode)  throws MAIGeSDiException {
		List<MediumType> result = null;

		try 
		{
			result = this.mapper.getMediumTypeByCodes(operatorID, organizationCode, seriesCode);
			logger.info("DB RESULT: List medium type by organizationCode {} and seriesCode {} : {}", organizationCode,seriesCode, result);

		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || result.size() == 0 ) {
			throw new ResourceNotFoundException("MediumType not found", Utility.ErrorType.ET_MEDIUM_TYPE_NOT_FOUND.getValue());
		}
		
		if(result.size() > 1 )
			throw new ConflictException("Too many MediunType for seriesCode and organziationCode pair", Utility.ErrorType.ET_TOO_MANY_RESULT.getValue());
	
		return result.get(0);
	}

	public Integer getMediumTypeDayOfValidity(Integer mediumTypeID) throws MAIGeSDiException {
		Integer result;
		try 
		{
			result = this.mapper.getMediumTypeDayOfValidity(mediumTypeID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		
		if(result == null || result == 0 ) {
			throw new ConflictException("MediumType's numberDayOfValidity is null, must be grather than 0", Utility.ErrorType.ET_MEDIUM_TYPE_DAYS_VALIDITY_NOT_VALORIZED.getValue());
		}
		
		
		return result;
	}

}
