package it.integrasistemi.maigesdi.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Component
public class ExternalSystemDAO {
	
	@Autowired
	private MAIGESDIMapper mapper;


	public List<ExternalSystem> getExternalSystems(Integer operatorID) throws MAIGeSDiException {
		
		List<ExternalSystem> externalSystems = null;
		
		try 
		{
			externalSystems = this.mapper.getExternalSystems(operatorID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return externalSystems;
	}

	public ExternalSystem getExternalSystemByID(Integer operatorID, Integer externalSystemID) throws MAIGeSDiException {
		ExternalSystem externalSystem = null;
		
		try 
		{
			externalSystem = this.mapper.getExternalSystemByID(operatorID, externalSystemID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return externalSystem;
	}

	public ExternalSystem getExternalSystemByMediumTypeID(Integer mediumTypeID) throws MAIGeSDiException {
		ExternalSystem externalSystem = null;
		
		try 
		{
			externalSystem = this.mapper.getExternalSystemByMediumTypeID(mediumTypeID);
		}catch(Exception e) {
			throw new DBMSFaultException(e.getMessage(), Utility.ErrorType.ET_DBMS_ERROR.getValue());
		}
		return externalSystem;
	}

}
