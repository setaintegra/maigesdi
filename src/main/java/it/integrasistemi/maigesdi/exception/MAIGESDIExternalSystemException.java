package it.integrasistemi.maigesdi.exception;

public class MAIGESDIExternalSystemException extends Exception {

	public MAIGESDIExternalSystemException(String message) {
		super(message);
	}

}
