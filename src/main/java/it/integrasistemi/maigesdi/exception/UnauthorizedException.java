package it.integrasistemi.maigesdi.exception;

public class UnauthorizedException extends MAIGeSDiException {

	public UnauthorizedException(String message, Integer errorCode) {
		super(message, errorCode);
		
	}
}
