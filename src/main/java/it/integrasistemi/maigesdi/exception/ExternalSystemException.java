package it.integrasistemi.maigesdi.exception;

public class ExternalSystemException extends MAIGeSDiException {

	public ExternalSystemException(String message, Integer errorCode) {
		super(message, errorCode);
	}

}
