package it.integrasistemi.maigesdi.exception;

public class ResourceNotFoundException extends MAIGeSDiException {

	public ResourceNotFoundException(String message, Integer errorCode) {
		super(message, errorCode);
		
	}

}
