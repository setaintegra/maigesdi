package it.integrasistemi.maigesdi.exception;

public class ConflictException extends MAIGeSDiException {
	
	public ConflictException(String message, Integer errorCode) {
		super(message, errorCode);
	}
}
