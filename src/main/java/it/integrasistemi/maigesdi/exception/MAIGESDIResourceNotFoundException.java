package it.integrasistemi.maigesdi.exception;

public class MAIGESDIResourceNotFoundException extends Exception {

	public MAIGESDIResourceNotFoundException(String message) {
		super(message);
	}
	
	public MAIGESDIResourceNotFoundException(String message, Throwable t) {
		super(message, t);
	}
}
