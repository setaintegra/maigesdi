package it.integrasistemi.maigesdi.exception;

public class DBMSFaultException extends MAIGeSDiException {
	
	public DBMSFaultException(String message, Integer errorCode) {
		super(message, errorCode);
	}

}
