package it.integrasistemi.maigesdi.exception;

public class MAIGeSDiException extends Exception {

	private Integer errorCode;
	
	public MAIGeSDiException(String message, Integer errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}
