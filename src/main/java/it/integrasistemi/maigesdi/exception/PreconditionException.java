package it.integrasistemi.maigesdi.exception;

public class PreconditionException extends MAIGeSDiException {

	public PreconditionException(String message, Integer errorCode) {
		super(message, errorCode);
	}

}
