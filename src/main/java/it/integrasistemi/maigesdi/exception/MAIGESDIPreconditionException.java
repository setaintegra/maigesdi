package it.integrasistemi.maigesdi.exception;

public class MAIGESDIPreconditionException extends Exception {

	public MAIGESDIPreconditionException(String message) {
		super(message);
	}

}
