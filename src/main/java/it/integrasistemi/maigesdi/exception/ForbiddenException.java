package it.integrasistemi.maigesdi.exception;

public class ForbiddenException extends MAIGeSDiException {
	
	public ForbiddenException(String message, Integer errorCode) {
		super(message, errorCode);
	}
}
