package it.integrasistemi.maigesdi.exception;

public class MAIGeSDiUnsupportedActivity extends Exception {
	
	public MAIGeSDiUnsupportedActivity(String message) {
		super(message);
	}

}
