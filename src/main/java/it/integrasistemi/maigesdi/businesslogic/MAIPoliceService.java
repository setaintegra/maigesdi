package it.integrasistemi.maigesdi.businesslogic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.integrasistemi.libreriamaipolice.ServiceMAIPolice;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.BirthPlaceVRO;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.CENAuthenticationResult;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.SalePoint;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.ticketone.mai.police.service.types_v1.AskPoliceAuthorization;
import it.ticketone.mai.police.service.types_v1.LocateSoccerCardInPoliceArchive;
import it.ticketone.mai.police.service.types_v1.SubmitProvisionalCardAssignment;
import it.ticketone.mai.police.service.types_v1.WithdrawCardAssignment;
import it.ticketone.mai.police.service_v1.PoliceFault;
import it.ticketone.mai.police.service_v1.ValidationFault;
import it.ticketone.mai.police.types_v1.AuthorizationObjectType;
import it.ticketone.mai.police.types_v1.ForeignBirthPlace;
import it.ticketone.mai.police.types_v1.Gender;
import it.ticketone.mai.police.types_v1.ItalianBirthPlace;
import it.ticketone.mai.police.types_v1.Location;
import it.ticketone.mai.police.types_v1.Outlet;
import it.ticketone.mai.police.types_v1.PoliceAnswerToSubmission;
import it.ticketone.mai.police.types_v1.PoliceAuthorizationOutcome;
import it.ticketone.mai.police.types_v1.PoliceFaultDetail;
import it.ticketone.mai.police.types_v1.SoccerCardIdentification;
import it.ticketone.mai.police.types_v1.ValidationFaultDetail;

@Validated
@Service
public class MAIPoliceService {
	
	@Autowired
	MediumDAO mediumDao;
	
	@Autowired
	OrganizationService organizationService;
	
	// INTERFACCIAENTO A MAIPolice
	
		/**
		 * Metodo per la verifica della persona verso il CEN attraverso MAIPolice
		 * @param persona rappresenta la sestupletta che identifica una persona 
		 * @return CENAuthenticationResult
		 */
		public CENAuthenticationResult verificaMotiviOstativi(ExternalSystem externalSystem, Person persona, boolean cenDebugMode) throws Exception {
			CENAuthenticationResult result = null;
			AskPoliceAuthorization parametri = null;
			PoliceAuthorizationOutcome risposta = null;

			//se è configurata la debug mode per il CEN allora ritorno un risultato positivo senza interrogare MAIPolice
			if(cenDebugMode) {
				result = this.getFintoRisultatoPositivo();
			}else {
				
				parametri = new AskPoliceAuthorization();
				parametri.setObjectType(AuthorizationObjectType.C);
	
				// try (SqlSession session = creaSessioneSql())
				// {
				// //trovaOperatore(session, op);
				//
				parametri.setOutlet(getOutlet());
				parametri.setPerson(getMaiPolicePerson(persona));
				// }
	
				ServiceMAIPolice serviceMAIP = new ServiceMAIPolice(externalSystem.getURL(),
						externalSystem.getUsername(), externalSystem.getPassword());
	
				try {
					risposta = serviceMAIP.askPoliceAuthorization(parametri);
					result = risolviCENAuthenticationResult(risposta);
				} catch (PoliceFault pf) {
					pf.printStackTrace();
					result = risolviCENAuthenticationResult(pf.getFaultInfo());
				} catch (ValidationFault vf) {
					vf.printStackTrace();
					result = risolviCENAuthenticationResult(vf.getFaultInfo());
				} catch(Exception e) {
					e.printStackTrace();
					result = new CENAuthenticationResult(false,false,null, "MAIPolice error: verificaMotiviOstativi: "+ e.getMessage(),"MAIPolice error: submitCENSoccerCard: "+ e.getMessage());
				}
	
				serviceMAIP = null;
			}

			return result;
		}

		/**
		 * metodo che emula il risultato alle chiamate ai servizi di MAIPolice per la gestione del parametro debug mode
		 * @return
		 */
		private CENAuthenticationResult getFintoRisultatoPositivo() {
			return new CENAuthenticationResult(true, true, Utility.IUA_FITTIZIO, null, null);
		}
		
		public CENAuthenticationResult submitCENSoccerCard(ExternalSystem externalSystem, Medium medium, Person person, boolean cenDebugMode) throws Exception {
			CENAuthenticationResult cenAuthResult;
			SubmitProvisionalCardAssignment cenCardParameter;

			//se è configurata la debug mode per il CEN allora ritorno un risultato positivo senza interrogare MAIPolice
			if(cenDebugMode) {
				cenAuthResult = this.getFintoRisultatoPositivo();
			}else {
				cenCardParameter = new SubmitProvisionalCardAssignment();
				cenCardParameter.setOutlet(getOutlet());
				cenCardParameter.setPerson(getMaiPolicePerson(person));
				cenCardParameter.setSoccerCardIssued(createSoccerCard(medium.getMediumType().getID(), medium.getMediumIdentifier()));
				cenCardParameter.setDtCardExpires(UtilityService.dateToXMLGregorianCalendar(medium.getValidTo()));
				//
				ServiceMAIPolice serviceMAIP = new ServiceMAIPolice(externalSystem.getURL(),
						externalSystem.getUsername(), externalSystem.getPassword());
	
				try {
					PoliceAnswerToSubmission risposta = null;
	
					risposta = serviceMAIP.submitProvisionalCardAssignment(cenCardParameter);
					cenAuthResult = risolviCENAuthenticationResult(risposta);
	
				} catch (PoliceFault pf) {
					pf.printStackTrace();
					cenAuthResult = risolviCENAuthenticationResult(pf.getFaultInfo());
	
				} catch (ValidationFault vf) {
					vf.printStackTrace();
					cenAuthResult = risolviCENAuthenticationResult(vf.getFaultInfo());
				} catch(Exception e) {
					e.printStackTrace();
					cenAuthResult = new CENAuthenticationResult(false,false,null, "MAIPolice error: submitCENSoccerCard: "+ e.getMessage(),"MAIPolice error: submitCENSoccerCard: "+ e.getMessage());
				}
	
				serviceMAIP = null;
			}
			return cenAuthResult;
		}

		public CENAuthenticationResult deleteCENSoccerCard(ExternalSystem externalSystem, Medium medium, Person person, boolean cenDebugMode) throws Exception {
			CENAuthenticationResult autVRO = null;
			
			//se è configurata la debug mode per il CEN allora ritorno un risultato positivo senza interrogare MAIPolice
			if(cenDebugMode) {
				autVRO = new CENAuthenticationResult(Boolean.TRUE, true, null, null, null);
			}
			else {
					
				// QueryTesseraDelTifoso qtdt = null;
				boolean cancellataVRO = false;
				WithdrawCardAssignment paramentriCancellazioneVRO = null;
	
				// Formattazione parametri per richiesta VRO
				paramentriCancellazioneVRO = new WithdrawCardAssignment();
				paramentriCancellazioneVRO.setPerson(getMaiPolicePerson(person));
				paramentriCancellazioneVRO.setSoccerCardWithdrawn(createSoccerCard(medium.getMediumType().getID(), medium.getMediumIdentifier()));
				paramentriCancellazioneVRO.setDtCardExpires(UtilityService.dateToXMLGregorianCalendar(medium.getValidTo()));
	
				// Cancellazione al VRO
				ServiceMAIPolice serviceMAIP = new ServiceMAIPolice(externalSystem.getURL(),
						externalSystem.getUsername(), externalSystem.getPassword());
	
				try {
					String risposta = null;
	
					risposta = serviceMAIP.withdrawCardAssignment(paramentriCancellazioneVRO);
					autVRO = risolviCancellazioneAutorizzazioneVRO(risposta);
	
					cancellataVRO = true;
				} catch (PoliceFault pf) {
					pf.printStackTrace();
					autVRO = risolviCENAuthenticationResult(pf.getFaultInfo());
				} catch (ValidationFault vf) {
					vf.printStackTrace();
					autVRO = risolviCENAuthenticationResult(vf.getFaultInfo());
				} catch(Exception e) {
					e.printStackTrace();
					autVRO = new CENAuthenticationResult(false,false,null, "MAIPolice error: deleteCENSoccerCard: "+ e.getMessage(),"MAIPolice error: submitCENSoccerCard: "+ e.getMessage());
				}
	
				serviceMAIP = null;

		}
		return autVRO;

		}
		
		public CENAuthenticationResult verifyCENSoccerCard(ExternalSystem externalSystem, Medium medium, Person persona, boolean cenDebugMode) throws Exception 
	    {
			CENAuthenticationResult autVRO = null;
	        LocateSoccerCardInPoliceArchive parametri = null;
	        String risposta = null;
	        Integer mediumTypeID = null;
	        
	        if(cenDebugMode) {
	        	autVRO = getFintoRisultatoPositivo();
	        }else {
		        parametri = new LocateSoccerCardInPoliceArchive();
	
		        mediumTypeID = medium.getMediumType() != null ? medium.getMediumType().getID(): null;
		        
		        parametri.setSoccerCard(createSoccerCard(mediumTypeID, medium.getMediumIdentifier()));
		        parametri.setPerson(getMaiPolicePerson(persona));
	//	             
	
		        ServiceMAIPolice serviceMAIP = new ServiceMAIPolice(externalSystem.getURL(),
						externalSystem.getUsername(), externalSystem.getPassword());
		        
		        try
		        {
		            risposta = serviceMAIP.locateSoccerCardInPoliceArchive(parametri);
		            autVRO = risolviCENAuthenticationResult(risposta);
		        }
		        catch(PoliceFault pf)
		        {
		            pf.printStackTrace();
		            autVRO = risolviCENAuthenticationResult(pf.getFaultInfo());
		        }
		        catch(ValidationFault vf)
		        {
		            vf.printStackTrace();
		            autVRO = risolviCENAuthenticationResult(vf.getFaultInfo());
		        } catch(Exception e) {
					e.printStackTrace();
					autVRO = new CENAuthenticationResult(false,false,null, "MAIPolice error: verifyCENSoccerCard: "+ e.getMessage(),"MAIPolice error: submitCENSoccerCard: "+ e.getMessage());
				}
		        
		        serviceMAIP = null;
	        }
	        return autVRO;
	    }


		public static CENAuthenticationResult risolviCENAuthenticationResult(PoliceAuthorizationOutcome outcome) {
			String denialReason = null;
			String codiceEccezione = null;

			if (outcome.getTxDenialReason() != null) {
				codiceEccezione = Utility.CODICE_ECCEZIONE_MAI_POLICE_MOTIVI_OSTATIVI;
				denialReason = outcome.getTxDenialReason();
			}

			CENAuthenticationResult autVRO = new CENAuthenticationResult(outcome.isAnswered(), outcome.isAuthorized(),
					outcome.getIuA(), codiceEccezione, denialReason);

			return autVRO;
		}

		public static CENAuthenticationResult risolviCancellazioneAutorizzazioneVRO(String ack) {
			boolean isAuthorized = false;

			if (ack != null && !ack.isEmpty())
				isAuthorized = true;

			CENAuthenticationResult autVRO = new CENAuthenticationResult(Boolean.TRUE, isAuthorized, null, null, null);

			return autVRO;
		}

		public static CENAuthenticationResult risolviCENAuthenticationResult(PoliceFaultDetail fault) {
			CENAuthenticationResult autVRO = new CENAuthenticationResult(Boolean.TRUE, Boolean.FALSE, null,
					fault.getErrorCode(), fault.getErrorText());

			return autVRO;
		}

		public static CENAuthenticationResult risolviCENAuthenticationResult(ValidationFaultDetail fault) {
			CENAuthenticationResult autVRO = new CENAuthenticationResult(Boolean.TRUE, Boolean.FALSE, null,
					String.valueOf(fault.getCdError()), fault.getDsError());

			return autVRO;
		}

		public static CENAuthenticationResult risolviCENAuthenticationResult(String IuA) {
			boolean isAuthorized = false;

			if (IuA != null && !IuA.isEmpty())
				isAuthorized = true;

			CENAuthenticationResult autVRO = new CENAuthenticationResult(Boolean.TRUE, isAuthorized, IuA, null, null);

			return autVRO;
		}

		public static CENAuthenticationResult risolviCENAuthenticationResult(PoliceAnswerToSubmission toSubmission) {
			CENAuthenticationResult autVRO = new CENAuthenticationResult(Boolean.TRUE, toSubmission.isAuthorized(),
					toSubmission.getIuA(), toSubmission.getTxDenialReason(), toSubmission.getTxDenialReason());

			return autVRO;
		}

		public SoccerCardIdentification createSoccerCard(Integer mediumTypeID, String mediumIdentifier) throws Exception {
			SoccerCardIdentification soccerCard = new SoccerCardIdentification();
			String cf;
			String codiceConi;
			
			//c'è l'id del tipo supporto digitale?
			if(mediumTypeID != null && mediumTypeID > 0 ) {
				//si; allora reperisco il cf dell'organizzazione direttamente dall0id organizzazione del tipo supporto
				cf = this.organizationService.getOrganizationFiscalCodeByMediumTypeID(mediumTypeID);
			}else {
				//altrimenti utilizzo il codice coni
				if(mediumIdentifier != null && !mediumIdentifier.isEmpty()) {
					
					codiceConi = mediumIdentifier.substring(0,  3);
					
					if(codiceConi.equals("009")) 
						codiceConi = mediumIdentifier.substring(0,  6);
					
					cf = this.organizationService.getOrganizationFiscalCodeByConiCode(codiceConi);	
					
				}else {
					throw new Exception("Access to MAIPolice problem, impossible to create SoccerCardIdentification cause mediumTypeID and mediumIdentifier are empty or null");
				}
				
			}
			soccerCard.setCdPoliceSoccerClub(cf);
			soccerCard.setSoccerCardNumber(mediumIdentifier);

			return soccerCard;
		}

		public it.ticketone.mai.police.types_v1.Person getMaiPolicePerson(Person persona) throws Exception {
			it.ticketone.mai.police.types_v1.Person person = new it.ticketone.mai.police.types_v1.Person();
			BirthPlaceVRO lnVRO = null;
			Gender gender = null;

			person.setNmGiven(persona.getGivenName().toUpperCase());
			person.setNmFamily(persona.getFamilyName().toUpperCase());

			lnVRO = this.mediumDao.getBirthPlaceVRO(persona);

			if (lnVRO.getEstero()) {
				ForeignBirthPlace birthPlace = new ForeignBirthPlace();
				birthPlace.setNmCountry(lnVRO.getNome());
				person.setBirthplace(birthPlace);
			} else {
				ItalianBirthPlace birthPlace = new ItalianBirthPlace();
				birthPlace.setNmLocality(lnVRO.getNome());
				birthPlace.setProvince(lnVRO.getSiglaProvincia());
				person.setBirthplace(birthPlace);
			}

			person.setDtBirth(UtilityService.dateToXMLGregorianCalendar(persona.getBirthDate()));

			switch (persona.getGender()) {
			case "M":
				gender = Gender.M;
				break;
			case "F":
				gender = Gender.F;
				break;
			default:
				throw new Exception("Gender not found or not valid");
			}

			person.setGender(gender);

			return person;
		}

		public Outlet getOutlet() throws Exception {
			Outlet outlet = null;
			Location loc = null;
			SalePoint salePoint = null;

			salePoint = this.mediumDao.getDummySalePoint();

			outlet = new Outlet();

			loc = new Location();
			loc.setCdIstat(salePoint.getISTATCode());
			loc.setProvince(salePoint.getProvince());
			loc.setTxAddress(salePoint.getAddress());

			outlet.setLocation(loc);
			outlet.setCdOutlet(String.valueOf(salePoint.getID()));
			outlet.setNmOutlet(salePoint.getName());

			return outlet;
		}

}
