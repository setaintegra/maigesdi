package it.integrasistemi.maigesdi.businesslogic;

import java.io.FileReader;
import java.io.InputStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.integrasistemi.maigesdi.MaigesdiApplication;
import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.ActivityType;
import it.integrasistemi.maigesdi.bean.DateRange;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.OperativeFlowType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.StateType;
import it.integrasistemi.maigesdi.bean.Tutor;
import it.integrasistemi.maigesdi.bean.utility.SimpleDateFormatThreadSafe;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.UtilityDAO;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;;

@Validated
@Service
public class UtilityService {

	@Value("${cen.debug_mode}")
    private boolean is_debug_mode;
	
	@Autowired 
	private UtilityDAO dao;
	
	@Autowired
	private OperatorService operatorService;

	private static final Logger logger = LoggerFactory.getLogger(UtilityService.class);

	public static final String PATTERN_MAIPOLICE = "([A-Z](([A-Z]| |'|-){0,28}))([A-Z]|')";
	
	public Integer checkDbConnection() {

		Integer result = 0;
		String queryResult = null;
		try {
			queryResult = this.dao.checkDbConnection();
			result = queryResult != null && !queryResult.isEmpty() ? 1 : 0;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;

	}

	// crea uuid random per generare il TraceLogID e lo mette all'interno dell'MDC
	// un domani il TraceLogID dovrà essere passata da chi chiamerà MAIGESDI
	public void generateTraceLogID(String methodName) {

		String traceLogID = "MAIGeSDi-" + methodName + "-" + UUID.randomUUID().toString();
		MDC.put("traceLogID", traceLogID);
	}

	public void generateTraceLogID(String traceLogID, String methodName) {
		if (traceLogID == null || traceLogID.isEmpty()) {
			this.generateTraceLogID(methodName);
		} else {
			MDC.put("traceLogID", traceLogID);
		}
	}

	// rimuove dall'MDC il session id
	public void removeTraceLogID() {
		MDC.remove("traceLogID");
	}

	// verifica se il parametro passato di tipo stringa sia non nullo e non vuoto
	public boolean isIntegerParameterValid(Integer parameter) {

		return parameter != null && parameter > 0;
	}

	// verifica se il parametro passato di tipo stringa sia non nullo e non vuoto
	public boolean isStringParameterValid(String parameter) {

		return parameter != null && !parameter.isEmpty();
	}

	public boolean isDateParameterValid(Date parameter) {

		return parameter != null; // && !parameter.;
	}

	// controlla se il valore della string è valido,
	// se lo è ritorna il valore trimmato
	// altrimenti ritorna null
	public String getValidStringValue(String value) {
		String result = null;

		if (this.isStringParameterValid(value))
			result = value.trim();

		return result;
	}

	// controlla se il valore dell'integer è valido,
	// se lo è ritorna il valore
	// altrimenti ritorna null
	public Integer getValidIntegerValue(Integer value) {
		Integer result = null;

		if (this.isIntegerParameterValid(value))
			result = value;

		return result;
	}

	// controlla se il valore del date è valido,
	// se lo è ritorna il valore
	// altrimenti ritorna null
	public Date getValidDateValue(Date value) {
		Date result = null;

		if (this.isDateParameterValid(value))
			result = value;

		return result;
	}

	// verifica se la persona è maggiorenne
	public boolean isAdultPerson(Person person) {
		boolean result = false;
		Integer age = 0;
		if (person.getBirthDate() != null) {

			age = getAge(person.getBirthDate());

			result = age >= 18;
		}
		return result;
	}

	// verifica se il tutor è valorizzato
	public boolean isValidTutor(Tutor tutor) {
		boolean result = tutor != null && tutor.getFamilyName() != null && !tutor.getFamilyName().isEmpty()
				&& tutor.getBirthPlace() != null && !tutor.getBirthPlace().isEmpty() && tutor.getFiscalCode() != null
				&& !tutor.getFiscalCode().isEmpty() && tutor.getBirthDate() != null && tutor.getGivenName() != null
				&& !tutor.getGivenName().isEmpty();
		return result;
	}

	// verifica se la sestupletta è valorizzata
	public boolean isValidMediumForMAIPolice(Medium inputMedium) {
		return inputMedium != null && inputMedium.getGivenName() != null && !inputMedium.getGivenName().isEmpty()
				&& inputMedium.getFamilyName() != null && !inputMedium.getFamilyName().isEmpty()
				&& inputMedium.getBirthPlace() != null && inputMedium.getBirthPlace().getFiscalCode() != null
				&& !inputMedium.getBirthPlace().getFiscalCode().isEmpty() && inputMedium.getBirthDate() != null
				&& inputMedium.getGender() != null && !inputMedium.getGender().isEmpty() && inputMedium.getValidTo() != null;
	}
	
	
	//ritorna la data di una persona a partire da un Date
	public static int getAge(Date dateOfBirth) {
	    Calendar today = Calendar.getInstance();
	    Calendar birthDate = Calendar.getInstance();
	    birthDate.setTime(dateOfBirth);
	    if (birthDate.after(today)) {
	        throw new IllegalArgumentException("You don't exist yet");
	    }
	    int todayYear = today.get(Calendar.YEAR);
	    int birthDateYear = birthDate.get(Calendar.YEAR);
	    int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
	    int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
	    int todayMonth = today.get(Calendar.MONTH);
	    int birthDateMonth = birthDate.get(Calendar.MONTH);
	    int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
	    int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
	    int age = todayYear - birthDateYear;

	    // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
	    if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)){
	        age--;
	    
	    // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
	    } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)){
	        age--;
	    }
	    return age;
	}
	
	
	//INZIIO CODICE DA GESDTI CC PER LA GESTIONE DELLA FORMULA DI LUHN
	
	public static boolean verificaCheckDigit_LuhnFormula(String stringa){
        boolean verifica_OK = false;
        /**
         * il mod 10 della somma restituita dal metodo
         * 'getSommaFormulaDiLuhn' deve essere ==0
         */
        int somma = getSomma_LuhnFormula(stringa,stringa.substring(stringa.length()));

        if(somma%10==0) verifica_OK = true;

        return verifica_OK;
}
	
	public static String getCheckDigit_LuhnFormula(String identificativo) throws Exception{
		/**
		 * Il procedimento per ottenerlo Ã¨ molto semplice:
		 * aggiungiamo uno 0 a destra e applichiamo allâ€™id la formula di Luhn.
		 *  Se il risultato Ã¨ divisibile per 10 abbiamo giÃ  ottenuto ciÃ² che vogliamo.
		 *  Altrimenti occorre sostituire lo 0 con la cifra corretta.
		 *  Prendiamo la cifra piÃ¹ a destra del risultato ottenuto (per 64 Ã¨ 4);
		 *  la chiameremo d. Eseguiamo 10 - d.
		 *  Il risultato Ã¨ la cifra che dovrÃ  sostituire lo 0 che abbiamo inserito.
		 */
		String checkDigit             = "0";
		int    checkDigitInt          = 0;
		String identificativoCompleto = "";

		try {
		    int    somma    = getSomma_LuhnFormula(identificativo, checkDigit);
			String sommaStr = String.valueOf(somma);
			int    i        = Integer.parseInt(sommaStr.substring(sommaStr.length()-1, sommaStr.length()));

			if(i!=0){
				checkDigitInt = 10 - i;
			}

			checkDigit             = String.valueOf(checkDigitInt);
			identificativoCompleto = identificativo + checkDigit;

			if(!verificaCheckDigit_LuhnFormula(identificativoCompleto)){
				throw new Exception("Errore nell'algoritmo di generazione del checksum (Luhn Formula)");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

		return checkDigit ;
    }
	
	private static  int getSomma_LuhnFormula(String identificativo, String checkDigit){
        /**
         * Partendo da destra:
         * la cifra in posizione dispari rimane invariata
         * la cifra in posizione pari viene moltiplicata per 2
         * se il risultato della moltiplicazione Ã¨ >9
         * allora va fatta la somma delle due cifre .
         * Sommare tutte le cifre ottenute
         */
        int     somma                  = 0;
        int     digit                  = 0;
        String  digitStr               = "";
        String  subDigitStr_dx         = "";
        String  subDigitStr_sx         = "";
        int     sommaSubDigitStr       = 0;
        int     posizione              = 0;
        String  identificativoCompleto = identificativo + checkDigit;

        for(int i=identificativoCompleto.length()-1;i>=0;i--){
                digit = Integer.parseInt(identificativoCompleto.substring(i,i+1));
                posizione++;
                if (isDispari(posizione)){
                        somma = somma + digit;
                }
                if(isPari(posizione)){
                        digit = digit*2;
                        if(digit>9){
                                digitStr         = String.valueOf(digit);
                                subDigitStr_dx   = digitStr.substring(1, 2);
                                subDigitStr_sx   = digitStr.substring(0, 1);
                                sommaSubDigitStr = Integer.parseInt(subDigitStr_dx )+ Integer.parseInt(subDigitStr_sx);
                                somma            = somma +sommaSubDigitStr;
                        }
                        else{
                                somma = somma +digit;
                        }
                }
        }

        return somma;
	}

	public static boolean isPari(int i){
		return (i%2 == 0);
	}

	public static boolean isDispari(int i){
        return (i%2 != 0);
	}
	
	/**
	 * Genera una stringa alfanumerica di 6 caratteri
	 * 
	 */
	public static String generaCodice() {
         int          lunghezzaCodice  = 6;
         StringBuffer codice           = new StringBuffer(lunghezzaCodice);
         String       dominioCaratteri = new String(Utility.STRINGA__DOMINIO_CARATTERI);
         Random       rand             = new Random();

         int n = dominioCaratteri.length()-1;

         for (int i=0; i<lunghezzaCodice; i++) {
		     int r = rand.nextInt(n+1);
		     codice.append(dominioCaratteri.charAt(r));
         }

         return codice.toString();
	}
	
	 public static boolean isNumeric(String str){
	        boolean isNumeric = true;
	        try
	        {
	           int i = Integer.parseInt(str);
	        }
	        catch (NumberFormatException ex) {
	                isNumeric = false;
	        }
	        return isNumeric;

	    }
	    
	    public static String getDecodificaPerFormulaLuhn(String str){
	        String valore = "";
	        String subStr;
	        for(int i = 0;i<= str.length()-1; i++){
	        	subStr =str.substring(i,i+1).trim().toUpperCase();
	            valore = valore + String.valueOf(Utility.mappaDecodificaFormulaLuhn.get(str.substring(i,i+1).trim().toUpperCase()));
	        }
	        return valore;
	    }
	    
	    public static XMLGregorianCalendar dateToXMLGregorianCalendar (Date date) throws DatatypeConfigurationException
	    {
	        XMLGregorianCalendar xmlDate = null;
	        GregorianCalendar calendar = new GregorianCalendar();
	        calendar.setTime(date);
	        xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
	        return xmlDate;
	    }

	    /**
	     * Aggiunge giorni alla data di oggi e la ritorna in output
	     * @param numberDays giorni da aggiungere
	     * @return ritorna un date con la data modificata
	     */
		public Date addDaysToToday(Integer numberDays) {
			
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
	        today.set(Calendar.SECOND, 0);
	        today.set(Calendar.MILLISECOND, 0);
	        
	        today.add(Calendar.DATE, numberDays);
			
			return today.getTime();
		}
		
		/**
	     * Ritorna la data di oggi espresso in date, per farlo si è usato 
	     * lo stratagemma di utilizzare il metodo di addizione di gionri ad oggi passando 0 come numero di giorni da sommare
	     * @return ritorna un date odierna
	     */
		public Date getTodayDate() {
			return this.addDaysToToday(0);
		}

		public List<ActivityType> getAllActivityTypes(Operator operator) throws MAIGeSDiException  {
			List<ActivityType> result;
			
			Operator storedOperator = this.operatorService.getOperator(operator);
			
			result = this.dao.getAllActivityType();
			
			return result;
		}
		
		public List<Activity> getAllActivities(Operator operator) throws MAIGeSDiException  {
			List<Activity> result;
			
			Operator storedOperator = this.operatorService.getOperator(operator);
			
			result = this.dao.getAllActivity();
			
			return result;
		}
		
		public List<StateType> getAllStateTypes(Operator operator) throws MAIGeSDiException  {
			List<StateType> result;
			
			Operator storedOperator = this.operatorService.getOperator(operator);
			
			result = this.dao.getAllStateType();
			
			return result;
		}
		
		public List<OperativeFlowType> getAllOperativeFlowTypes(Operator operator) throws MAIGeSDiException  {
			List<OperativeFlowType> result;
			
			Operator storedOperator = this.operatorService.getOperator(operator);
			
			result = this.dao.getAllOperativeFlowType();
			
			return result;
		}

		public boolean isValidPersonForMAIPolice(Person person) {
			boolean result = person != null && person.getGivenName() != null && !person.getGivenName().isEmpty()
								&& person.getFamilyName() != null && !person.getFamilyName().isEmpty()
								&& person.getBirthDate() != null 
								&& person.getFiscalCodeBirthPlace() != null && !person.getFiscalCodeBirthPlace().isEmpty() 
								&& person.getGender() != null && !person.getGender().isEmpty();
										
			result &= Pattern.matches(PATTERN_MAIPOLICE,  person.getGivenName());
			result &= Pattern.matches(PATTERN_MAIPOLICE,  person.getFamilyName());	
			result &= person.getGender().equals("M") || person.getGender().equals("F");
			return result;
			
		}

		public boolean isValidParameterForSearchMediumCustom(Integer mediumTypeID,
				String mediumIdentifier,
				Date releaseDateFrom, 
				Date releaseDateTo, 
				Date expiryDateFrom, 
				Date expiryDateTo,
				Integer stateTypeID,
				Integer operativeFlowTypeID,
				Integer lastTransitionOperatorID,
				String givenName,
				String familyName,
				String fiscalCodeBirthPlace,
				Date birthDate,
				Integer emitterOperatorID) {
			
			return mediumTypeID             != null ||
		 			mediumIdentifier        != null ||
		 			releaseDateFrom         != null ||
					releaseDateTo           != null ||
					expiryDateFrom          != null ||
					expiryDateTo            != null ||
					stateTypeID             != null ||
					operativeFlowTypeID     != null ||
					lastTransitionOperatorID!= null ||
					givenName               != null ||
					familyName              != null ||
					fiscalCodeBirthPlace    != null ||
					birthDate               != null ||
					emitterOperatorID       != null ;
		}

		public DateRange checkAndFormatDateRange(Date startRange, Date endRange) throws PreconditionException {
			//Date[] result = new Date[] {startRange, endRange};
	    	DateRange result = new DateRange(startRange, endRange);
			SimpleDateFormatThreadSafe dateFormatter = new SimpleDateFormatThreadSafe("yyyy-MM-dd");
	    	
	    	//verifica se il range passato è incompleto 
			if(startRange != null && endRange == null)
				try {
					result.setEndRange(dateFormatter.parse("2100-01-01"));
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			
			
			if(startRange == null && endRange != null)
				try {
					result.setStartRange(dateFormatter.parse("1900-01-01"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
		
			//verifico se il range è valido ossia dateFrom <= dateTo
			if(startRange != null && endRange != null) {
				
				
				if(result.getStartRange().after(result.getEndRange()))
					throw new PreconditionException("Range date is not valid, startRange is after endRange", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
			}
			
			return result;
			
			
		}
	    
		public String readMavenVersion() throws Exception
		{
			String version = "";
	
			MavenXpp3Reader reader = null;
			Model           model  = null;
			   
			boolean isDebug = false;
	
			try
			{
			    isDebug =java.lang.management.ManagementFactory.getRuntimeMXBean().
	                  getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;

	            reader = new MavenXpp3Reader();

	            if(isDebug)
	            {
	                model = reader.read(new FileReader("pom.xml"));            
	            }
	            else
	            {
	            	InputStream is = MaigesdiApplication.class.getClassLoader().getResourceAsStream("META-INF/maven/it.integrasistemi.maigesdi/MAIGESDI/pom.xml");
	                model = reader.read(is);
	                //System.out.println(model.getId());
	                //System.out.println(model.getGroupId());
	                //System.out.println(model.getArtifactId());
	                //System.out.println(model.getVersion());
	                // If you want to get fancy:
	                // model.getDependencies().stream().forEach(System.out::println);
	            }
			           
	            version = model.getVersion();
			}catch (Exception e){
				throw e;	
			}
			finally
			{
			    reader = null;
			    model = null;
			}
	
			return version;
		}
	    
		public boolean isDebugMode() {
			return this.is_debug_mode;
		}
		
		/**
		 * metodo che verifica se il sistema è in esecuzione su un db di test, stage o validazione
		 * @return true se il nome del db è uguale a uno dei tre nomi, cablati nel codice, relativi ai tre ambienti
		 * @throws DBMSFaultException
		 */
		public boolean isEnviromentTest() throws DBMSFaultException {
			boolean result = false;
			String nomeDB = this.dao.getDBName();
			logger.debug("DataBase name: "+ nomeDB);
						
			result = nomeDB.equals(Utility.TEST_NOME_DB) || 
					 nomeDB.equals(Utility.STAGE_NOME_DB) || 
					 nomeDB.equals(Utility.VALIDAZIONE_NOME_DB) ||
					 nomeDB.equals(Utility.AMBIENTE_INTEGRA_NOME_DB);
			
			return result;
			
		}
	
}
