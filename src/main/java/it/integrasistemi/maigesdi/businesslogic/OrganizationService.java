package it.integrasistemi.maigesdi.businesslogic;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.Operator.Profile;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetOrganizzationByID;
import it.integrasistemi.maigesdi.bean.mybatis.Sequence;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.OrganizationDAO;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Service
@Validated
public class OrganizationService {

	@Autowired
	private OperatorService operatorService;

	@Autowired
	private OrganizationDAO organizationDAO;

	public List<Organization> getOrganizations(@Valid Operator operator) throws Exception {

		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		return organizationDAO.getOrganizations();
	}

	public Organization getOrganizationByID(@Valid Operator operator, 
											@NotNull(message = "Organization ID is required; must be grather than zero") 
											@Min(value = 1, message = "Organization ID must be grather than zero")
											Integer ID) throws Exception {
		Organization result = null;
		Operator storedOperator = null;
		
		storedOperator = this.operatorService.getOperator(operator);

		result = organizationDAO.getOrganizationByID(ID);
		if (result == null) {
			throw new ResourceNotFoundException("Organization not found", Utility.ErrorType.ET_ORGANIZATION_NOT_FOUND.getValue());
		}

		return result;
	}

	public Organization addOrganization(@Valid Operator operator,@Valid @NotNull(message = "Organization is required") Organization organization) throws Exception {
		Organization result = null;
		Integer organizationID = 0;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		organizationID = this.organizationDAO.insertOrganization(organization);

		result = this.organizationDAO.getOrganizationByID(organizationID);

		return result;
	}

	public Organization updateOrganization(	@Valid Operator operator,
											@Valid @NotNull(message = "Organization is required") Organization organization
											) throws Exception {
		Organization result = null;
		Organization storedOrganization = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		if(organization.getID() == null || organization.getID()==0)
			throw new PreconditionException("Organizzation ID is required; must be grather than zero", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
		
		result = this.organizationDAO.updateOrganization(organization);
		
		return result;
	}

	public String getOrganizationFiscalCodeByMediumTypeID(Integer mediumTypeID) throws MAIGeSDiException {
	
		String result = this.organizationDAO.getOrganizationFiscalCodeByMediumTypeID(mediumTypeID);
		
		return result;
	}

	public String getOrganizationFiscalCodeByConiCode(String coniCode) throws MAIGeSDiException  {

		String result = this.organizationDAO.getOrganizationFiscalCodeByConiCode(coniCode);
		
		return result;
	}
	

}
