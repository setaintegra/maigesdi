package it.integrasistemi.maigesdi.businesslogic.externalsystem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.ReservedMedium;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemInterface;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemResult;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.CENAuthenticationResult;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.MediumIdentifierRangeService;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.ticketone.mai.police.service.types_v1.SubmitProvisionalCardAssignment;

@Component
public class MAIGESDISystem implements ExternalSystemInterface {
	
	private static final Logger logger = LoggerFactory.getLogger(MAIGESDISystem.class);

	@Autowired
	private MediumDAO mediumDao;
	
	@Autowired
	private MediumIdentifierRangeService mediumIdentifierRangeService;

	private ExternalSystem externalSystem;

	public MAIGESDISystem() {

	}

	public ExternalSystem getExternalSystem() {
		return externalSystem;
	}

	public void setExternalSystem(ExternalSystem externalSystem) {
		this.externalSystem = externalSystem;
	}

	@Override
	public ExternalSystemResult insertMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;

		Integer mediumID = 0;
		List<Medium> mediumResultList = new ArrayList<Medium>();

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
		
		//dati del medium riservato dalla spezzata
		ReservedMedium reservedMedium = null;
				
		// è forntio l'identificativo?  è necessario prelevare il supporto da una spezzata?
		boolean isMediumIdentifierProvided;


		try {
			//a priori verifico se la creazione è tramite una spezzata o se è tramite un identificativo passato
			//verifico se è presente l'identificativo
			isMediumIdentifierProvided =  inputMedium.getMediumIdentifier() != null &&  !inputMedium.getMediumIdentifier().isEmpty();
			
			// nel caso in cui l'identificativo è fornito devo verificare nel DB l'esistenza
			if(isMediumIdentifierProvided)
				storedMedium = this.mediumDao.getValidMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			
			// il medium non è presente nel sistema?
			if (storedMedium == null) {

				// inserisco stato attivo e flusso prodotto
				inputMedium.setState(new Identity(Utility.TSSD_ATTIVO, "ATTIVO"));
				inputMedium.setOperativeFlow(new Identity(Utility.TSFO_SUPPORTO_PRODOTTO, "SUPPORTO_PRODOTTO"));
				
				//Verifico, nel caso in cui serve un identificativo dalla spezzata, se è effettiviamente configurata 
				if(!isMediumIdentifierProvided) {
					//CASO UTILIZZO SPEZZATA
				
					//verifico se è stata configurata una spezzata e se è valida (se esiste una da poter usare)
					this.mediumIdentifierRangeService.checkExisitingValidMediumIdentifierRange(inputMedium.getMediumType().getID());
					
					//riservo il prossimo identificativo dalla spezzata
					reservedMedium = this.mediumIdentifierRangeService.getNextMediumFromMediumIdentifierRange(operator, 
																											  inputMedium.getMediumType().getID(),
																											  Utility.TSFO_SUPPORTO_PRODOTTO, 
																											  Utility.TSSD_ATTIVO);
					try 
					{
						//completo i dati della spezzata con quelli forniti in input
						this.mediumDao.completeMediumFromMediumIdentfierRange(operator, inputMedium, reservedMedium );
						
						inputMedium.setMediumIdentifier(reservedMedium.getMediumIdentifier());
						inputMedium.setID(reservedMedium.getMediumID());
						
						mediumID = reservedMedium.getMediumID();
					}catch(Exception e) {
						//bonifico e libero la spezzata
						logger.error("Completing reserved medium, from range, problem; Clear medium data and release in progress Medium: "+ inputMedium.toString() + " Exception : " + e.getMessage());
						try {
							this.mediumIdentifierRangeService.clearReservedMedium(reservedMedium.getMediumID());
						}catch(Exception nestedE) {
							//caso in cui il medium resta bloccato, identificativo non utilizzabile
							logger.error("Releasing medium Identifier after Error problem, impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + e.getMessage());
							throw new MAIGeSDiException("Releasing medium Identifier after Error problem, impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + nestedE.getMessage()
														, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_RESTORING_PROBLEM.getValue());
						}
						throw new MAIGeSDiException("Completing reserved medium, from range, problem; Reserved medium restore and ready for a new reservation; "+ inputMedium.toString() + " Exception : " + e.getMessage()
													, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_COMPLETING_MEDIUM_PROBLEM.getValue());
					}
					
				}else {
					//CASO IDENTIFICATIVO FORNITO
					//verifico che l'identificativo non ricada in una spezzata gia configurata
					if(!this.mediumIdentifierRangeService.checkExistingMediumIdentifierRangeForMediumIdentifier(inputMedium.getMediumType().getID(), inputMedium.getMediumIdentifier()))
					{
						//inserisco il nuovo medium nel db
						// MAIGESDI INSERT
						mediumID = this.mediumDao.addMedium(operator, inputMedium);
					}else {
						throw new ConflictException("Medium identifier not valid, Identifier used in a configurated range", 
								Utility.ErrorType.ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE_ALREDY_USED_IN_A_RANGE.getValue());
					}
					
				}
				
				// leggo il medium inserito dal db e lo ritorno come risultato
				mediumResultList.add(this.mediumDao.getMedium(mediumID, operator.getID()));
				result = new ExternalSystemResult(true, mediumResultList);

			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium alredy exist", Utility.ErrorType.ET_MEDIUM_ALREDY_EXIST.getValue()));

			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
	}

	@Override
	public ExternalSystemResult updateMedium(ParameterMedium parameterMedium, Operator operator) {

		ExternalSystemResult result;
		Medium storedMedium = null;

		List<Medium> mediumResultList = new ArrayList<Medium>();

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		try {

			storedMedium = this.mediumDao.getValidMediumNoSecurity(inputMedium.getMediumIdentifier(),
																	inputMedium.getMediumType().getID());
			if (storedMedium != null) {

				// aggiorno su maigesdi
				this.mediumDao.updateMedium(parameterMedium);

				// leggo il medium inserito dal db e lo ritorno come risultato
				mediumResultList.add(this.mediumDao.getMedium(inputMedium.getMediumType().getID(),	inputMedium.getMediumIdentifier(), operator.getID()));

				result = new ExternalSystemResult(true,  mediumResultList);

			} else {
				result = new ExternalSystemResult(false,new ResourceNotFoundException("Medium doesn't exist", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()) );
			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;

	}

	@Override
	public ExternalSystemResult disableMedium(ParameterMedium parameterMedium, Operator operator) {
		List<Medium> mediumResultList = null;
		
		ExternalSystemResult result;
			
		//risultato del cen fittizio DA GESTIRE MEGLIO ORA HO LA DIPENZSENZA CON L'OGGETTO CENAutenthicationResult
		CENAuthenticationResult emptyCenAuthResult = new CENAuthenticationResult(); 
		
		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
		
		//gestione dello stato del medium dopo la disabilitazione
		Integer idStatoFinale;
				
		try {

			//repeerisco il medium a partire dall'id nello stato o valido o in lavorazione e attesa conferma bo (appena creato dal web)
			storedMedium = this.mediumDao.getMediumForDisableNoSecurity(inputMedium.getID());
			
			// il medium è gia presente nel sistema?
			if (storedMedium != null) {
				
				//verifico lo  stato finale passato in input, ossia la causa della diasbilitazione
				if(inputMedium.getState() != null &&  inputMedium.getState().getID() != null) {
					//gli stati accettati di partenza per la disabilitazione del medium
					switch(inputMedium.getState().getID()) {
						case Utility.TSSD_DANNEGGIATO:
						case Utility.TSSD_ERRATO:
						case Utility.TSSD_RETTIFICATO_CEN:
						case Utility.TSSD_RINUNCIATO:
						case Utility.TSSD_RUBATO:
						case Utility.TSSD_SMARRITO:
							
							
							idStatoFinale = inputMedium.getState().getID();
				
							//lo stato è in lavorazione e il flusso è attesa conferma bo?
							if(storedMedium.getOperativeFlow().getID().equals(Utility.TSFO_ATTESA_CONFERMA_BO) && storedMedium.getState().getID().equals(Utility.TSSD_IN_LAVORAZIONE)) {
								this.mediumDao.updateMediumState(operator.getID(),
																 storedMedium.getID(),
																 idStatoFinale);
							}else {
								// MAIGESDI State Update Utility.TSSD_RINUNCIATO
								this.mediumDao.updateMediumExternalSystemResult(operator.getID(),
																				inputMedium.getID(), 
																				Utility.REMOVING_MEDIUM_EXTERNAL_SYSTEM,
																				emptyCenAuthResult,
																				Utility.TSFO_SUPPORTO_PRODOTTO, 
																				idStatoFinale);
			
							}
							mediumResultList = new ArrayList<Medium>();
							// leggo il medium aggiornato dal db e lo ritorno come risultato
							mediumResultList.add(this.mediumDao.getMedium(inputMedium.getID(), operator.getID()));
			
							result = new ExternalSystemResult(true, mediumResultList);
						break;
						//tutti gli altri stati
						default:
							result = new ExternalSystemResult(false, new ConflictException("Medium not disabled, medium state not valid",
																	Utility.ErrorType.ET_MEDIUM_STATE_NOT_VALID.getValue()));
					}		
					
				}else {
					// non è presente lo stato finale del medium che è la causale di disabilitazione
					result = new ExternalSystemResult(false, new PreconditionException("Medium final state is required",
							Utility.ErrorType.ET_PRECONDITION_ERROR.getValue()));
				}
		
			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium doesn't exist or not valid", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()) );
			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
		
		
	}

	@Override
	public ExternalSystemResult getMediums(ParameterMedium parameterMedium, Operator operator) {

		ExternalSystemResult result;

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();
		try 
		{
			List<Medium> mediumResultList = this.mediumDao.getMediumCustom(inputMedium, operator.getID());

			result = new ExternalSystemResult(true,  mediumResultList);
		}catch(Exception e) {
			result = new ExternalSystemResult(false,  e);
		}
		return result;
	}

	//da allineare alla creazione: FATTO
	@Override
	public ExternalSystemResult insertWebMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;

		Integer mediumID = 0;
		List<Medium> mediumResultList = new ArrayList<Medium>();

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
		
		//dati del medium riservato dalla spezzata
		ReservedMedium reservedMedium = null;
				
		// è forntio l'identificativo?  è necessario prelevare il supporto da una spezzata?
		boolean isMediumIdentifierProvided;


		try {
			//a priori verifico se la creazione è tramite una spezzata o se è tramite un identificativo passato
			//verifico se è presente l'identificativo
			isMediumIdentifierProvided =  inputMedium.getMediumIdentifier() != null &&  !inputMedium.getMediumIdentifier().isEmpty();
			
			// nel caso in cui l'identificativo è fornito devo verificare nel DB l'esistenza
			if(isMediumIdentifierProvided)
				storedMedium = this.mediumDao.getValidMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			
			// il medium non è presente nel sistema?
			if (storedMedium == null) {

				// inserisco stato attivo e flusso prodotto
				inputMedium.setState(new Identity(Utility.TSSD_IN_LAVORAZIONE, "IN_LAVORAZIONE"));
				inputMedium.setOperativeFlow(new Identity(Utility.TSFO_ATTESA_CONFERMA_BO, "ATTESA_CONFERMA_BO"));
				
				//Verifico, nel caso in cui serve un identificativo dalla spezzata, se è effettiviamente configurata 
				if(!isMediumIdentifierProvided) {
					//CASO UTILIZZO SPEZZATA
				
					//verifico se è stata configurata una spezzata e se è valida (se esiste una da poter usare)
					this.mediumIdentifierRangeService.checkExisitingValidMediumIdentifierRange(inputMedium.getMediumType().getID());
					
					//riservo il prossimo identificativo dalla spezzata
					reservedMedium = this.mediumIdentifierRangeService.getNextMediumFromMediumIdentifierRange(operator, 
																											  inputMedium.getMediumType().getID(),
																											  Utility.TSFO_ATTESA_CONFERMA_BO, 
																											  Utility.TSSD_IN_LAVORAZIONE);
					try 
					{
						//completo i dati della spezzata con quelli forniti in input
						this.mediumDao.completeMediumFromMediumIdentfierRange(operator, inputMedium, reservedMedium );
						
						inputMedium.setMediumIdentifier(reservedMedium.getMediumIdentifier());
						inputMedium.setID(reservedMedium.getMediumID());
						
						mediumID = reservedMedium.getMediumID();
					}catch(Exception e) {
						//bonifico e libero la spezzata
						logger.error("Completing reserved medium, from range, problem; Clear medium data and release in progress Medium: "+ inputMedium.toString() + " Exception : " + e.getMessage());
						try {
							this.mediumIdentifierRangeService.clearReservedMedium(reservedMedium.getMediumID());
						}catch(Exception nestedE) {
							//caso in cui il medium resta bloccato, identificativo non utilizzabile
							logger.error("Releasing medium Identifier after Error problem, impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + e.getMessage());
							throw new MAIGeSDiException("Releasing medium Identifier after Error problem, impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + nestedE.getMessage()
														, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_RESTORING_PROBLEM.getValue());
						}
						throw new MAIGeSDiException("Completing reserved medium, from range, problem; Reserved medium restore and ready for a new reservation; "+ inputMedium.toString() + " Exception : " + e.getMessage()
													, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_COMPLETING_MEDIUM_PROBLEM.getValue());
					}
					
				}else {
					//CASO IDENTIFICATIVO FORNITO
					//verifico che l'identificativo non ricada in una spezzata gia configurata
					if(!this.mediumIdentifierRangeService.checkExistingMediumIdentifierRangeForMediumIdentifier(inputMedium.getMediumType().getID(), inputMedium.getMediumIdentifier()))
					{
						//inserisco il nuovo medium nel db
						// MAIGESDI INSERT
						mediumID = this.mediumDao.addMedium(operator, inputMedium);
					}else {
						throw new ConflictException("Medium identifier not valid, Identifier used in a configurated range", 
								Utility.ErrorType.ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE_ALREDY_USED_IN_A_RANGE.getValue());
					}
					
				}
				
				// leggo il medium inserito dal db e lo ritorno come risultato
				mediumResultList.add(this.mediumDao.getMedium(mediumID, operator.getID()));
				result = new ExternalSystemResult(true, mediumResultList);

			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium alredy exist", Utility.ErrorType.ET_MEDIUM_ALREDY_EXIST.getValue()));

			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
	}

	@Override
	public ExternalSystemResult verifyMedium(ParameterMedium parameterMedium, Operator operator) {
		
		return new ExternalSystemResult(false, new ForbiddenException("Activity not allowed for this medium type; ExternalSystem MAIGeSDi cannot verify a medium",
				Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY_TYPE.getValue()));
	}

	@Override
	public ExternalSystemResult restoreMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;
		
		List<Medium> mediumResultList = new ArrayList<Medium>();
		Medium inputMedium = parameterMedium.toMedium();
		
		// medium presente nel DB
		Medium storedMedium = null;
		try 
		{
			
			// verifico nel DB se il medium esiste, indipendentemente dal suo stato
			storedMedium = this.mediumDao.getMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			// il medium è gia presente nel sistema?
			if (storedMedium != null) {

				//verifico che lo stato sia disabilitato
				// per disabilitato si intende che non si trovi in uno dei seguenti stati
				// - IN_LAVORAZIONE
				// - ATTIVO e non scaduto, ossia con data di scadenza superiore alla data odierna
				switch(storedMedium.getState().getID()) {
					case Utility.TSSD_DANNEGGIATO:
					case Utility.TSSD_ERRATO:
					//case Utility.TSSD_RETTIFICATO_CEN:
					case Utility.TSSD_RINUNCIATO:
					case Utility.TSSD_RUBATO:
					case Utility.TSSD_SMARRITO:
					case Utility.TSSD_ATTIVO:
						
						// verifico che il flusso sia SUPPORTO_PRODOTTO, poichè non è possibile fare la RESTORE (riattivazione) di un supporto che non è stato mai prodotto fisicamente
						if(storedMedium.getOperativeFlow().getID().equals(Utility.TSFO_SUPPORTO_PRODOTTO)) {
						
							//se lo stato è attivo verifico che la data di scadenza sia passata, altrimenti il medium è ancora  valido e non può essere riattivato
							if(storedMedium.getState().getID() == Utility.TSSD_ATTIVO) {
								if(storedMedium.getValidTo().before(new Date()))
									result = new ExternalSystemResult(false, new ConflictException("Medium not restorable, medium state not valid",
											Utility.ErrorType.ET_MEDIUM_STATE_NOT_VALID.getValue()));
							}
							
							// MAIGESDI update
							this.mediumDao.updateMedium(operator, parameterMedium, Utility.TSSD_ATTIVO, Utility.TSFO_SUPPORTO_PRODOTTO);
			
							// leggo il medium inserito dal db e lo ritorno come risultato
							mediumResultList.add(this.mediumDao.getMedium(inputMedium.getMediumType().getID(),	inputMedium.getMediumIdentifier(), operator.getID()));
	
							result = new ExternalSystemResult(true,  mediumResultList);
						}else {
							result = new ExternalSystemResult(false,
									new PreconditionException("Cannot restore this medium, because it has not yet been produced ",
																Utility.ErrorType.ET_MEDIUM_OPERATIVE_FLOV_NOT_VALID.getValue()));
						}
						break;
					//tutti gli altri stati
					default:
						result = new ExternalSystemResult(false, new ConflictException("Medium not restorable, medium state not valid",
																Utility.ErrorType.ET_MEDIUM_STATE_NOT_VALID.getValue()));
				}

			} else {
				result = new ExternalSystemResult(false, new ResourceNotFoundException("Medium doesn't exist",
																				Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()));
			}
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);
		}

		return result;
	}

	@Override
	public ExternalSystemResult checkValidPersonOnExternalSystem(String givenName, 
																 String familyName,
																 String fiscalCodeBirthPlace, 
																 Date birthDate, 
																 String gender, 
																 Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Operation not allowed for this ExternalSystem (MAIGeSDi) ",
				Utility.ErrorType.ET_UNSUPPORTED_OPERATION.getValue()));
	}

	@Override
	public ExternalSystemResult checkExistingMediumOnExternalSystem(Integer externalSystemID, 
																	String mediumIdentifier,
																	String givenName, 
																	String familyName, 
																	String fiscalCodeBirthPlace, 
																	Date birthDate, 
																	String gender,
																	Date validTo, 
																	Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Operation not allowed for this ExternalSystem (MAIGeSDi) ",
				Utility.ErrorType.ET_UNSUPPORTED_OPERATION.getValue()));
	}

	@Override
	public ExternalSystemResult activeExistingMediumOnExternalSystem(ParameterMedium parameterMedium, Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Activity not allowed for this medium type; ExternalSystem MAIGeSDi cannot active, on external system, an existing medium",
				Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY_TYPE.getValue()));
	}

	@Override
	public ExternalSystemResult checkPersonDataIsValid(Integer mediumTypeID, Person personData, Operator operator) {
		//non vengono fatti controlli, ma non deve nemmeno andare in errore
		return new ExternalSystemResult(true, new ArrayList<Medium>());
	}

}
