package it.integrasistemi.maigesdi.businesslogic.externalsystem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemInterface;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemResult;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.CENAuthenticationResult;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.MAIPoliceService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;

@Service
@Component
public class ExternalCENSystem implements ExternalSystemInterface {

	@Autowired
	private MediumDAO mediumDao;
	
	@Autowired
	private MAIPoliceService maiPoliceService;
	
	@Autowired
	private UtilityService utilityService;

	private ExternalSystem externalSystem;

	private static final Logger logger = LoggerFactory.getLogger(MAIGESDICENSystem.class);

	public void setExternalSystem(ExternalSystem externalSystem) {
		this.externalSystem = externalSystem;
	}
	
	@Override
	//da modificare
	public ExternalSystemResult insertMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;

		Integer mediumID = 0;
		List<Medium> mediumResultList = new ArrayList<Medium>();
		Person person = null;
		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
		
		CENAuthenticationResult cenAuthResult = null;
		
		try {
			
			// verifico nel DB se il medium esiste, indipendentemente dallo stato, se già c'è non si può creare
			storedMedium = this.mediumDao.getMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			// il medium è gia presente nel sistema?
			if (storedMedium == null) {
				
				// è presente la sestupletta?
				if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {

					person = new Person(inputMedium.getGivenName(), 
										inputMedium.getFamilyName(),
										inputMedium.getBirthPlace().getFiscalCode(), 
										inputMedium.getBirthDate(),
										inputMedium.getGender());
								
					//verifico se al CEN ESISTE ED é VALIDA
					cenAuthResult = this.maiPoliceService.verifyCENSoccerCard(this.externalSystem, inputMedium, person, false);
					if (cenAuthResult.getAutorizzato()) {

						// inserisco stato attivo e flusso prodotto
						inputMedium.setState(new Identity(Utility.TSSD_ATTIVO, "ATTIVO"));
						inputMedium.setOperativeFlow(new Identity(Utility.TSFO_SUPPORTO_PRODOTTO, "SUPPORTO_PRODOTTO"));
						
						// MAIGESDI INSERT
						mediumID = this.mediumDao.addMedium(operator, inputMedium);
		
						// leggo il medium inserito dal db e lo ritorno come risultato
						mediumResultList.add(this.mediumDao.getMedium(mediumID, operator.getID()));
						result = new ExternalSystemResult(true, mediumResultList);
					}
					else {
						result = new ExternalSystemResult(false, new MAIGeSDiException("CEN medium check error: " + cenAuthResult.getCodiceEccezione() + " - "
														+ cenAuthResult.getDescrizioneEccezione(),
														Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
					}
				} else {
					result = new ExternalSystemResult(false, new PreconditionException("Person givenName, familyName, birthPlace, birthDate and gender are required",
							Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
	
				}
			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium alredy exist", Utility.ErrorType.ET_MEDIUM_ALREDY_EXIST.getValue()));
				
			}
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
	}

	// DA FARE COME LA INSERT
	@Override
	public ExternalSystemResult insertWebMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;

		Integer mediumID = 0;
		List<Medium> mediumResultList = new ArrayList<Medium>();
		Person person = null;
		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
		
		CENAuthenticationResult cenAuthResult = null;
		
		try {
			
			// verifico nel DB se il medium esiste, indipendentemente dallo stato, se già c'è non si può creare
			storedMedium = this.mediumDao.getMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			// il medium è gia presente nel sistema?
			if (storedMedium == null) {
				
				// è presente la sestupletta?
				if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {

					person = new Person(inputMedium.getGivenName(), 
										inputMedium.getFamilyName(),
										inputMedium.getBirthPlace().getFiscalCode(), 
										inputMedium.getBirthDate(),
										inputMedium.getGender());
								
					//verifico se al CEN ESISTE ED é VALIDA
					cenAuthResult = this.maiPoliceService.verifyCENSoccerCard(this.externalSystem, inputMedium, person, false);
					if (cenAuthResult.getAutorizzato()) {
						
						// inserisco stato attivo e flusso prodotto
						inputMedium.setState(new Identity(Utility.TSSD_ATTIVO, "ATTIVO"));
						inputMedium.setOperativeFlow(new Identity(Utility.TSFO_SUPPORTO_PRODOTTO, "SUPPORTO_PRODOTTO"));
						
						// MAIGESDI INSERT
						mediumID = this.mediumDao.addMedium(operator, inputMedium);
		
						// leggo il medium inserito dal db e lo ritorno come risultato
						mediumResultList.add(this.mediumDao.getMedium(mediumID, operator.getID()));
						result = new ExternalSystemResult(true, mediumResultList);
					}
					else {
						result = new ExternalSystemResult(false, new MAIGeSDiException("CEN medium check error: " + cenAuthResult.getCodiceEccezione() + " - "
														+ cenAuthResult.getDescrizioneEccezione(),
														Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
					}
				} else {
					result = new ExternalSystemResult(false, new PreconditionException("Person givenName, familyName, birthPlace, birthDate and gender are required",
							Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
	
				}
			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium alredy exist", Utility.ErrorType.ET_MEDIUM_ALREDY_EXIST.getValue()));
				
			}
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
	}

	@Override
	public ExternalSystemResult updateMedium(ParameterMedium parameterMedium, Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Activity not allowed for this medium type; ExternalSystem ExternalCEN cannot insert new medium",
				Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY_TYPE.getValue()));
	}

	//DA FARE SOLO DB
	@Override
	public ExternalSystemResult disableMedium(ParameterMedium parameterMedium, Operator operator) {
		List<Medium> mediumResultList = null;
		
		ExternalSystemResult result;
			
		//risultato del cen fittizio
		CENAuthenticationResult emptyCenAuthResult = new CENAuthenticationResult(); 
		
		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
				
		try {
			
			//repeerisco il medium a aprtire dall'id nello stato o valido o in lavorazione e attesa conferma bo (appena creato dal web)
			//in questo "sistema esterno" non avrò mai tessere create da web, ossia tessere con
			storedMedium = this.mediumDao.getMediumForDisableNoSecurity(inputMedium.getID());
			
			
			// il medium è gia presente nel sistema?
			if (storedMedium == null) {
				//lo stato è in lavorazione e il flusso è attesa conferma bo?
				if(storedMedium.getOperativeFlow().getID().equals(Utility.TSFO_ATTESA_CONFERMA_BO) && storedMedium.getState().getID().equals(Utility.TSSD_IN_LAVORAZIONE)) {
					//questo caso in teoria non si verifica mai per questo sistema esterno
					this.mediumDao.updateMediumState(operator.getID(),
							 storedMedium.getID(),
							 Utility.TSSD_RINUNCIATO);
				}else {
					
					// MAIGESDI State Update Utility.TSSD_RINUNCIATO
					this.mediumDao.updateMediumExternalSystemResult(operator.getID(),
																	inputMedium.getID(), 
																	Utility.REMOVING_MEDIUM_EXTERNAL_SYSTEM,
																	emptyCenAuthResult,
																	Utility.TSFO_SUPPORTO_PRODOTTO, 
																	Utility.TSSD_RINUNCIATO);
				}
				
				mediumResultList = new ArrayList<Medium>();
				// leggo il medium aggiornato dal db e lo ritorno come risultato
				mediumResultList.add(this.mediumDao.getMedium(inputMedium.getID(), operator.getID()));

				result = new ExternalSystemResult(true, mediumResultList);
				
		
			} else {
				result = new ExternalSystemResult(false, new ResourceNotFoundException("Medium doesn't exist", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()) );

			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
		
		
	}

	@Override
	public ExternalSystemResult getMediums(ParameterMedium parameterMedium, Operator operator) {

		ExternalSystemResult result;

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();
		try {
			List<Medium> mediumResultList = this.mediumDao.getMediumCustom(inputMedium, operator.getID());
	
			result = new ExternalSystemResult(true,  mediumResultList);
		}catch(Exception e) {
			result = new ExternalSystemResult(false, e);
		}
		return result;
	}

	//è possiible verificare solo se il medium è presente in MAIGeSDi
	// si verifica la presenza nel sistema esterno
	@Override
	public ExternalSystemResult verifyMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;
		List<Medium> mediumList;
		CENAuthenticationResult cenAuthResult = null;
		Person inputPerson = null;
		Person storedPerson = null;
		Medium storedMedium = null;
		Medium inputMedium = parameterMedium.toMedium();
		
		try {		
			storedMedium = this.mediumDao.getMedium(inputMedium.getMediumType().getID(), inputMedium.getMediumIdentifier(), operator.getID());
						
			// il medium è gia presente nel sistema?
			if (storedMedium != null) {
						
				
				// è presente la sestupletta?
				if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {

					inputPerson = new Person(inputMedium.getGivenName(), 
										inputMedium.getFamilyName(),
										inputMedium.getBirthPlace().getFiscalCode(), 
										inputMedium.getBirthDate(),
										inputMedium.getGender());
					
					//devo verificare se la sestupletta salvata e quella passata sono uguali?
					
					storedPerson = new Person(  storedMedium.getGivenName(), 
												storedMedium.getFamilyName(),
												storedMedium.getBirthPlace().getFiscalCode(), 
												storedMedium.getBirthDate(),
												storedMedium.getGender());
					
					if(inputPerson.equals(storedPerson) && inputMedium.getValidTo().equals(storedMedium.getValidTo())){
						
						//verifico se al CEN ESISTE ED é VALIDA
						cenAuthResult = this.maiPoliceService.verifyCENSoccerCard(this.externalSystem, inputMedium, inputPerson, false);
						if (cenAuthResult.getAutorizzato()) {
							mediumList  = new ArrayList<Medium>();
							mediumList.add(storedMedium);
							result = new ExternalSystemResult(true, mediumList);
						}else {
							// non presente al CEN 
							result = new ExternalSystemResult(false, new MAIGeSDiException("CEN medium check error: " + cenAuthResult.getCodiceEccezione() + " - "
																							+ cenAuthResult.getDescrizioneEccezione(),
																							Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
						}
					}else {
						//la sestupletta fornita non coincide con quella memorizzata in MAIGeSDi
						result  = new ExternalSystemResult(false, new ConflictException("Medium supplied mismatch with the saved one", Utility.ErrorType.ET_MEDIUM_MISMATCH.getValue()));
					}
					
				}else {
					result = new ExternalSystemResult(false, new MAIGeSDiException("CEN medium check error: " + cenAuthResult.getCodiceEccezione() + " - "
																					+ cenAuthResult.getDescrizioneEccezione(),
																					Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
				}
						
			}else {
				result = new ExternalSystemResult(false, new ResourceNotFoundException("VerifyMedium problem; Medium not found, errorCode", 
																						Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()));
			}
			
		}catch(Exception e) {
			result = new ExternalSystemResult(false, e);
		}
		return result;
	}

	@Override
	public ExternalSystemResult restoreMedium(ParameterMedium parameterMedium, Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Activity not allowed for this medium type; ExternalSystem ExternalCEN cannot restore an existing medium",
				Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY_TYPE.getValue()));
	}

	@Override
	public ExternalSystemResult checkValidPersonOnExternalSystem(String givenName, 
																 String familyName,
																 String fiscalCodeBirthPlace, 
																 Date birthDate, 
																 String gender, 
																 Operator operator) {
		ExternalSystemResult result = null;
		Person person = null;
		try {
			person = new Person(givenName, 
								familyName,
								fiscalCodeBirthPlace.toUpperCase(), 
								birthDate, 
								gender);
			
			if (this.utilityService.isValidPersonForMAIPolice(person)) {
				
				CENAuthenticationResult cenAuthResult = this.maiPoliceService.verificaMotiviOstativi(this.externalSystem, person, false);
				if (cenAuthResult.getAutorizzato()) {
					result = new ExternalSystemResult(true, cenAuthResult.getIuA());
				}else{
					result = new ExternalSystemResult(false,	"CEN error: " + cenAuthResult.getCodiceEccezione() + " - " + cenAuthResult.getDescrizioneEccezione());
				}
			} else {
				result = new ExternalSystemResult(false,
						new PreconditionException("GivenName, familyName, birthPlace, birthDate, gender are required",
													Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
			}
			
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
	}

	@Override
	public ExternalSystemResult checkExistingMediumOnExternalSystem(Integer externalSystemID, 
																	String mediumIdentifier,
																	String givenName, 
																	String familyName, 
																	String fiscalCodeBirthPlace, 
																	Date birthDate, 
																	String gender,
																	Date validTo, 
																	Operator operator) {
		ExternalSystemResult result = null;
		try {
			//Sono coscente del fatto che è un refuso usare medium e persona per gestire le stesse inforazioni due volte.
			//lo lascio per non avere ripercussioni sul codice già scritto e testato
			Medium medium = new Medium(mediumIdentifier,
										givenName, 
										familyName,
										fiscalCodeBirthPlace.toUpperCase(),
										birthDate,
										gender,
										validTo);
			
			Person person = new Person(givenName, 
										familyName,
										fiscalCodeBirthPlace.toUpperCase(), 
										birthDate, 
										gender);
			
			if (this.utilityService.isValidMediumForMAIPolice(medium)) {
				//verifico se al CEN ESISTE 
				CENAuthenticationResult cenAuthResult = this.maiPoliceService.verifyCENSoccerCard(this.externalSystem, medium, person, false);
				if (cenAuthResult.getAutorizzato()) {
					result = new ExternalSystemResult(true, cenAuthResult.getIuA());
				}else {
					// non presente al CEN 
					result = new ExternalSystemResult(false, "CEN error: " + cenAuthResult.getCodiceEccezione() + " - " + cenAuthResult.getDescrizioneEccezione());
				}
				
			}else {
				result = new ExternalSystemResult(false,
						new PreconditionException("GivenName, familyName, birthPlace, birthDate, gender and medium validityTo are required",
													Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
			
			}
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}
		
		return result;
	}

	@Override
	public ExternalSystemResult activeExistingMediumOnExternalSystem(ParameterMedium parameterMedium, Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Activity not allowed for this medium type; ExternalSystem ExternalCEN cannot active, on external system, an existing medium",
				Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY_TYPE.getValue()));
	}

	@Override
	public ExternalSystemResult checkPersonDataIsValid(Integer mediumTypeID, Person personData, Operator operator) {
		return new ExternalSystemResult(false, new ForbiddenException("Operation not allowed for this medium type; ExternalSystem ExternalCEN cannot check validity of person's data",
				Utility.ErrorType.ET_UNSUPPORTED_OPERATION.getValue()));
	}
	
}
