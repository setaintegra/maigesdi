package it.integrasistemi.maigesdi.businesslogic.externalsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemInterface;

@Component
public class ExternalSystemFactory {
	
	@Autowired
	private MAIGESDICENSystem maigesdiCenSystem;
	
	@Autowired
	private MAIGESDISystem maigesdiSystem;
	
	@Autowired
	private ExternalCENSystem externalCenSystem;
	
	public ExternalSystemInterface getConcreteExternalSystem(ExternalSystem externalSystemData) throws Exception {
	
		ExternalSystemInterface result = null;
		switch(externalSystemData.getName()) {
		case "MAIGESDI_CEN":
			
			//controlli per questo tipo
			result = this.maigesdiCenSystem;
			this.maigesdiCenSystem.setExternalSystem(externalSystemData);
			
			
			//result = new MAIGESDICENSystem(result.getID(), result.getName(), result.getDescription(), result.getURL(), result.getUsername(), result.getPassword()); ;
			break;
		case "MAIGESDI":
			
			result = this.maigesdiSystem;
			this.maigesdiSystem.setExternalSystem(externalSystemData);
			
			break;
		case "EXTERNAL_CEN":
			
			result = this.externalCenSystem;
			this.externalCenSystem.setExternalSystem(externalSystemData);
			
			break;	
			
		default:
			throw new Exception("External system not found or bad confugrated; check ");
		}
		
		return result;
	}
	
}
