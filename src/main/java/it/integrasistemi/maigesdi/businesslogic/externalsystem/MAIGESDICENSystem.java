package it.integrasistemi.maigesdi.businesslogic.externalsystem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.ReservedMedium;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemInterface;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemResult;
import it.integrasistemi.maigesdi.bean.externalsystem.maipolice.CENAuthenticationResult;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.MAIPoliceService;
import it.integrasistemi.maigesdi.businesslogic.MediumIdentifierRangeService;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;

@Component
public class MAIGESDICENSystem implements ExternalSystemInterface {

	@Autowired
	private MediumDAO mediumDao;

	@Autowired
	private MAIPoliceService maiPoliceService;

	// @Autowired
	// private OrganizationService organizationService;

	private ExternalSystem externalSystem;

	@Autowired
	private UtilityService utilityService;

	@Autowired
	private MediumIdentifierRangeService mediumIdentifierRangeService;
	
	private static final Logger logger = LoggerFactory.getLogger(MAIGESDICENSystem.class);

	public MAIGESDICENSystem() {

	}

	public ExternalSystem getExternalSystem() {
		return externalSystem;
	}

	public void setExternalSystem(ExternalSystem externalSystem) {
		this.externalSystem = externalSystem;
	}

	// la transazionalità non è necessaria
	@Override
	//@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ExternalSystemResult insertMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;
		Person person = null;
		boolean ownAnotherMedium = false;
		Integer mediumID = 0;
		List<Medium> mediumResultList = new ArrayList<Medium>();
		boolean isCENDebugMode = utilityService.isDebugMode();
		
		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;
		
		//dati del medium riservato dalla spezzata
		ReservedMedium reservedMedium = null;
		
		// è forntio l'identificativo?  è necessario prelevare il supporto da una spezzata?
		boolean isMediumIdentifierProvided;

		// riasultato della verifica preliminare del CEN
		CENAuthenticationResult cenAuthResult = null;

		// gestione eccezione dopo autorizzazione al cen
		Exception exceptionResult = null;

		try {

			//a priori verifico se la creazione è tramite una spezzata o se è tramite un identificativo passato
			//verifico se è presente l'identificativo
			isMediumIdentifierProvided =  inputMedium.getMediumIdentifier() != null &&  !inputMedium.getMediumIdentifier().isEmpty();
			
			// nel caso in cui l'identificativo è fornito devo verificare nel DB l'esistenza
			if(isMediumIdentifierProvided)
				storedMedium = this.mediumDao.getValidMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			
			// il medium è gia presente nel sistema?
			if (storedMedium == null) {
				// è presente la sestupletta e la data di scadenza?
				if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {

					person = new Person(inputMedium.getGivenName(), 
										inputMedium.getFamilyName(),
										inputMedium.getBirthPlace().getFiscalCode(), 
										inputMedium.getBirthDate(),
										inputMedium.getGender());
					// il titolare è minorenne?
					if (!this.utilityService.isAdultPerson(person)) {
						// controllo il tutore
						if (!this.utilityService.isValidTutor(inputMedium.getTutor())) {
							// se non è valido rilancio eccezione poichè un minorenne non può creare una
							// tessera senza il tutor
							throw new ForbiddenException("The future owner of the medium is a minor and requires a tutor",
														Utility.ErrorType.ET_MEDIUM_OWNER_IS_MINOR_TUTOR_MISSING.getValue());
						}
					}

					// la persona ha già medium della stessa organizzazione? (per questo sistema esterno, ossia possiede altre TDT?)
					ownAnotherMedium = this.mediumDao.checkPersonOwnMediumSameOrganizationAndSpecificExternalSystem(inputMedium.getMediumType().getID(), person, this.externalSystem.getID());
					if (!ownAnotherMedium) {
						
						// verifica motivi ostativi prima di adanre sul db GESDI (persona)
						// richiesta maipolice
						cenAuthResult = this.maiPoliceService.verificaMotiviOstativi(this.externalSystem, person, isCENDebugMode);
						if (cenAuthResult.getAutorizzato()) {

							// inserisco stato in lavorazione e flusso VERIFICA_PRESSO_AUTHORITY
							inputMedium.setState(new Identity(Utility.TSSD_IN_LAVORAZIONE, "IN_LAVORAZIONE"));
							inputMedium.setOperativeFlow(new Identity(Utility.TSFO_VERIFICA_PRESSO_AUTHORITY, "VERIFICA_PRESSO_AUTHORITY"));
							
							//Verifico, nel caso in cui serve un identificativo dalla spezzata, se è effettiviamente configurata 
							if(!isMediumIdentifierProvided) {
								//CASO UTILIZZO SPEZZATA
							
								//verifico se è stata configurata una spezzata e se è valida (se esiste una da poter usare)
								this.mediumIdentifierRangeService.checkExisitingValidMediumIdentifierRange(inputMedium.getMediumType().getID());
								
								//riservo il prossimo identificativo dalla spezzata
								reservedMedium = this.mediumIdentifierRangeService.getNextMediumFromMediumIdentifierRange(operator, inputMedium.getMediumType().getID(),Utility.TSFO_VERIFICA_PRESSO_AUTHORITY, Utility.TSSD_IN_LAVORAZIONE);
								try 
								{
									//completo i dati della spezzata con quelli forniti in input
									this.mediumDao.completeMediumFromMediumIdentfierRange(operator, inputMedium, reservedMedium );
									
									inputMedium.setMediumIdentifier(reservedMedium.getMediumIdentifier());
									inputMedium.setID(reservedMedium.getMediumID());
									
									mediumID = reservedMedium.getMediumID();
								}catch(Exception e) {
									//bonifico e libero la spezzata
									logger.error("Completing reserved medium, from range, problem; Clear medium data and release in progress Medium: "+ inputMedium.toString() + " Exception : " + e.getMessage());
									try {
										this.mediumIdentifierRangeService.clearReservedMedium(reservedMedium.getMediumID());
									}catch(Exception nestedE) {
										//caso in cui il medium resta bloccato, identificativo non utilizzabile
										logger.error("Releasing medium Identifier after Error problem, impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + e.getMessage());
										throw new MAIGeSDiException("Releasing medium Identifier after Error problem, impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + nestedE.getMessage()
																	, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_RESTORING_PROBLEM.getValue());
									}
									throw new MAIGeSDiException("Completing reserved medium, from range, problem; Reserved medium restore and ready for a new reservation; "+ inputMedium.toString() + " Exception : " + e.getMessage()
																, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_COMPLETING_MEDIUM_PROBLEM.getValue());
								}
								
							}else {
								//CASO IDENTIFICATIVO FORNITO
								//verifico che l'identificativo non ricada in una spezzata gia configurata
								if(!this.mediumIdentifierRangeService.checkExistingMediumIdentifierRangeForMediumIdentifier(inputMedium.getMediumType().getID(), inputMedium.getMediumIdentifier()))
								{
									//inserisco il nuovo medium nel db
									// MAIGESDI INSERT
									mediumID = this.mediumDao.addMedium(operator, inputMedium);
								}else {
									throw new ConflictException("Medium identifier not valid, Identifier used in a configurated range", 
											Utility.ErrorType.ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE_ALREDY_USED_IN_A_RANGE.getValue());
								}
								
							}
							
							// SISTEMA ESTERNO INSERT
							// PORZIONE DI CODICE RIPRESA DA APIRESTGESDI
							cenAuthResult = this.maiPoliceService.submitCENSoccerCard(this.externalSystem, inputMedium,	person, isCENDebugMode);

							// 3 - aggiornamento in GESDI stato ATTIVO flusso IN PRODUZIONE stato produttore

							if (cenAuthResult.getAutorizzato()) {

								try {

									this.mediumDao.updateMediumExternalSystemResult(operator.getID(),
																					mediumID,
																					Utility.ADDING_MEDIUM_EXTERNAL_SYSTEM, 
																					cenAuthResult,
																					Utility.TSFO_SUPPORTO_PRODOTTO, 
																					Utility.TSSD_ATTIVO);
								} catch (Exception exception) {
									try {

										logger.error("Updating medium state problem; CEN disabling needed; Medium: "+ inputMedium.toString() + " Exception : " + exception.getMessage());
										CENAuthenticationResult deleteResult = this.maiPoliceService.deleteCENSoccerCard(this.externalSystem, inputMedium, person, isCENDebugMode);
										if (deleteResult.getAutorizzato()) {
											//è un medium creato da zero o è stato preso da una spezzata?
											if(isMediumIdentifierProvided) {
												//tessera creata da zero con identificativo fornito
												logger.info("Medium disabled on CEN, deleting medium from DB; Medium: "+ inputMedium.toString());
												this.mediumDao.deleteMedium(inputMedium);

												exceptionResult = new MAIGeSDiException("Medium creation problem. Medium disabled on CEN and deleted on DB",
																						Utility.ErrorType.ET_MEDIUM_NOT_CREATED.getValue());
												
											}else {
												//è un medium preso da una spezzata
												logger.info("Medium disabled on CEN, releasing medium identifier from DB; Medium: "+ inputMedium.toString());
												this.mediumIdentifierRangeService.clearReservedMedium(mediumID);

												exceptionResult = new MAIGeSDiException("Medium creation problem. Medium disabled on CEN and medium identifier released on DB",
																						Utility.ErrorType.ET_MEDIUM_NOT_CREATED.getValue());
											}
											
										} else {
											exceptionResult = new MAIGeSDiException(
													"Medium creation problem. CEN disabling problem; Medium still valid on CEN, change medium state on DB or disable on CEN; Medium: "
															+ inputMedium.toString(),
													Utility.ErrorType.ET_MEDIUM_NOT_CREATED_BUT_VALID_ON_CEN
															.getValue());
										}
									} catch (Exception innerException) {
										exceptionResult = new MAIGeSDiException(
												"Medium creation problem. Medium state inconsistent; Check CEN and DB validity; Medium: "
														+ inputMedium.toString() + " Exception : "
														+ innerException.getMessage(),
												Utility.ErrorType.ET_MEDIUM_NOT_CREATED_UKNOWN_CEN_STATE_INCONSISTENT_SITUATION
														.getValue());
										logger.error("Medium creation problem. Medium state inconsistent; Check CEN and DB validity; Medium: "
												+ inputMedium.toString() + " Exception : "
												+ innerException.getMessage());
										
									}

									throw exceptionResult;

								}

								// FINE PORZIONE APIRESTGESDI

								// leggo il medium inserito dal db e lo ritorno come risultato
								mediumResultList.add(this.mediumDao.getMedium(mediumID, operator.getID()));

								result = new ExternalSystemResult(true, mediumResultList);
							} else {
								logger.info("CEN Submit medium error: " + cenAuthResult.getCodiceEccezione() + " - "
										+ cenAuthResult.getDescrizioneEccezione() 
										+"; Medium: "+ inputMedium.toString());
								
								result = new ExternalSystemResult(false,
										new MAIGeSDiException(
												"CEN Submit medium error: " + cenAuthResult.getCodiceEccezione() + " - "
														+ cenAuthResult.getDescrizioneEccezione(),
												Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
								try {
									logger.info("CEN not autorize medium; Clear medium data from DB in progress; Medium: "+ inputMedium.toString());
									
									if(!isMediumIdentifierProvided) {
										this.mediumIdentifierRangeService.clearReservedMedium(mediumID);
									}else {
										this.mediumDao.deleteMedium(inputMedium);
									}
								}catch(Exception e) {
									logger.error("Clearing medium data from DB ERROR; Medium: "+ inputMedium.toString());
									
								}
							}
						} else {
							result = new ExternalSystemResult(false,
									new MAIGeSDiException("CEN Preliminary check error: " + cenAuthResult.getCodiceEccezione() + " - "
															+ cenAuthResult.getDescrizioneEccezione(),
															Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
						}

					} else {
						result = new ExternalSystemResult(false,
								new ForbiddenException("Person own another medium of the same organization",
														Utility.ErrorType.ET_PERSON_ALREDY_OWN_MEDIUM_FOR_SAME_ORGANIZATION.getValue()));
					}
				} else {
					result = new ExternalSystemResult(false,
							new PreconditionException("Person givenName, familyName, birthPlace, birthDate, gender and medium validityTo are required",
														Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
				}

			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium alredy exist",
																				Utility.ErrorType.ET_MEDIUM_ALREDY_EXIST.getValue()));

			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;
	}

	@Override
	public ExternalSystemResult insertWebMedium(ParameterMedium parameterMedium, Operator operator) {
		ExternalSystemResult result;
		Person person = null;
		boolean ownAnotherMedium = false;
		Integer mediumID = 0;
		List<Medium> mediumResultList = new ArrayList<Medium>();
		Medium inputMedium = parameterMedium.toMedium();
		boolean isCENDebugMode = utilityService.isDebugMode();
		
		// medium presente nel DB
		Medium storedMedium = null;
		
		//dati del medium riservato dalla spezzata
		ReservedMedium reservedMedium = null;
		
		// identificativo fornito o meno
		boolean isMediumIdentifierProvided;

		// riasultato della verifica preliminare del CEN
		CENAuthenticationResult cenAuthResult = null;

		try {
			
			//a priori verifico se la creazione è tramite una spezzata o se è tramite un identificativo passato
			//verifico se è presente l'identificativo
			isMediumIdentifierProvided =  inputMedium.getMediumIdentifier() != null &&  !inputMedium.getMediumIdentifier().isEmpty();

			// nel caso in cui l'identificativo è fornito devo verificare nel DB l'esistenza
			if(isMediumIdentifierProvided)
				// verifico nel DB se il medium esiste, indipendentemente dal suo stato
				storedMedium = this.mediumDao.getMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
			
			// il medium è gia presente nel sistema?
			if (storedMedium == null) {

				// è presente la sestupletta?
				if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {

					person = new Person(inputMedium.getGivenName(), 
										inputMedium.getFamilyName(),
										inputMedium.getBirthPlace().getFiscalCode(), 
										inputMedium.getBirthDate(),
										inputMedium.getGender());
					// il titolare è minorenne?
					if (!this.utilityService.isAdultPerson(person)) {
						// controllo il tutore
						if (!this.utilityService.isValidTutor(inputMedium.getTutor())) {
							// se non è valido rilancio eccezione poichè un minorenne non può creare una
							// tessera senza il tutor
							throw new ForbiddenException("The future owner of the medium is a minor and requires a tutor",
														Utility.ErrorType.ET_MEDIUM_OWNER_IS_MINOR_TUTOR_MISSING.getValue());
						}
					}

					// la persona ha già medium della stessa organizzazione? e di quel tipo
					ownAnotherMedium = this.mediumDao.checkPersonOwnMediumSameOrganizationAndSpecificExternalSystem(inputMedium.getMediumType().getID(), person, this.externalSystem.getID());

					if (!ownAnotherMedium) {

						// verifica prima di insert (persona)
						// richiesta maipolice
						cenAuthResult = this.maiPoliceService.verificaMotiviOstativi(this.externalSystem, person, isCENDebugMode);
						if (cenAuthResult.getAutorizzato()) {

							// inserisco stato in lavorazione e flusso VERIFICA_PRESSO_AUTHORITY
							inputMedium.setState(new Identity(Utility.TSSD_IN_LAVORAZIONE, "IN_LAVORAZIONE"));
							inputMedium.setOperativeFlow(new Identity(Utility.TSFO_ATTESA_CONFERMA_BO, "ATTESA_CONFERMA_BO"));

							//Verifico, nel caso in cui serve un identificativo dalla spezzata, se è effettiviamente configurata 
							if(!isMediumIdentifierProvided) {
								//CASO UTILIZZO SPEZZATA
							
								//verifico se è stata configurata una spezzata e se è valida (se esiste una da poter usare)
								this.mediumIdentifierRangeService.checkExisitingValidMediumIdentifierRange(inputMedium.getMediumType().getID());
								
								//riservo il prossimo identificativo dalla spezzata
								reservedMedium = this.mediumIdentifierRangeService.getNextMediumFromMediumIdentifierRange(operator, 
																											  			  inputMedium.getMediumType().getID(),
																														  Utility.TSFO_ATTESA_CONFERMA_BO, 
																														  Utility.TSSD_IN_LAVORAZIONE);
								try 
								{
									//completo i dati della spezzata con quelli forniti in input
									this.mediumDao.completeMediumFromMediumIdentfierRange(operator, inputMedium, reservedMedium );
									
									inputMedium.setMediumIdentifier(reservedMedium.getMediumIdentifier());
									inputMedium.setID(reservedMedium.getMediumID());
									
									mediumID = reservedMedium.getMediumID();
								}catch(Exception e) {
									logger.error("Completing reserved medium, from range, problem; Clear medium data and release in progress; Medium: "+ inputMedium.toString() + " Exception : " + e.getMessage());
									try {
										this.mediumIdentifierRangeService.clearReservedMedium(reservedMedium.getMediumID());
									}catch(Exception nestedE) {
										//caso in cui il medium resta bloccato, identificativo non utilizzabile
										logger.error("Releasing medium Identifier, after Error, problem; impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + e.getMessage());
										throw new MAIGeSDiException("Releasing medium Identifier, after Error, problem; impossible release medium identifier : "+ inputMedium.toString() + " Exception : " + nestedE.getMessage()
																	, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_RESTORING_PROBLEM.getValue());
									}
									throw new MAIGeSDiException("Completing reserved medium, from range, problem; Reserved medium restored and ready for a new reservation; "+ inputMedium.toString() + " Exception : " + e.getMessage()
																, Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_COMPLETING_MEDIUM_PROBLEM.getValue());
										
								}
							}else {
								//CASO IDENTIFICATIVO FORNITO
								//verifico che l'identificativo non ricada in una spezzata gia configurata
								//verifico che l'identificativo non ricada in una spezzata gia configurata
								if(!this.mediumIdentifierRangeService.checkExistingMediumIdentifierRangeForMediumIdentifier(inputMedium.getMediumType().getID(), inputMedium.getMediumIdentifier()))
								{
								
									// MAIGESDI INSERT
									mediumID = this.mediumDao.addMedium(operator,inputMedium);
								}else {
									throw new ConflictException("Medium identifier not valid, Identifier used in a configurated range", 
											Utility.ErrorType.ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE_ALREDY_USED_IN_A_RANGE.getValue());
								}
							}
							
							// leggo il medium inserito dal db e lo ritorno come risultato
							mediumResultList.add(this.mediumDao.getMedium(mediumID, operator.getID()));

							// "Medium added correctly; medium must be authorized by the back office"
							result = new ExternalSystemResult(true, mediumResultList);
						} else {
							result = new ExternalSystemResult(false,
									new MAIGeSDiException("CEN Preliminary check error: " + cenAuthResult.getCodiceEccezione() + " - "
															+ cenAuthResult.getDescrizioneEccezione(),
															Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
						}
					} else {
						result = new ExternalSystemResult(false,
															new ForbiddenException("Person own another medium of the same organization",
																					Utility.ErrorType.ET_PERSON_ALREDY_OWN_MEDIUM_FOR_SAME_ORGANIZATION.getValue()));
					}		
				} else {
					result = new ExternalSystemResult(false,
							new PreconditionException("Person givenName, familyName, birthPlace, birthDate and gender are required",
														Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
				}
			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium alredy exist",
																				Utility.ErrorType.ET_MEDIUM_ALREDY_EXIST.getValue()));

			}

		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;

	}

	@Override
	public ExternalSystemResult updateMedium(ParameterMedium parameterMedium, Operator operator) {

		ExternalSystemResult result;
		Medium storedMedium = null;

		Person inputPerson = null;
		Person storedPerson = null;

		List<Medium> mediumResultList = new ArrayList<Medium>();

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		try {
			
			if(inputMedium.getID() != null && inputMedium.getID() > 0) {
			
				//inizialmente si verificava che il medium fosse valido oltre al fatto che fosse presente sul DB
//				storedMedium = this.mediumDao.getValidMediumNoSecurity(inputMedium.getMediumIdentifier(),
//						inputMedium.getMediumType().getID());
				//ora si verifica solo l'esistenza a partire dall'id del supportodigitale
				storedMedium = this.mediumDao.getMediumNoSecurity(inputMedium.getID());
								
				if (storedMedium != null) {
					
					//è una persona valida per MAIPOLICE?
					if(this.utilityService.isValidMediumForMAIPolice(inputMedium)) {
						
						inputPerson = new Person(inputMedium.getGivenName(), inputMedium.getFamilyName(),
								inputMedium.getBirthPlace().getFiscalCode(), inputMedium.getBirthDate(),
								inputMedium.getGender());
						
						storedPerson = new Person(storedMedium.getGivenName(), storedMedium.getFamilyName(),
								storedMedium.getBirthPlace().getFiscalCode(), storedMedium.getBirthDate(),
								storedMedium.getGender());
							
						// l'aggiornamento riguarda la persona?
		
						// SE è COSI ALLORA RILANCIA ECCEZIONE PERCHE IL CEN NON PERMETTE DI AGGIORNARE
						// UNA TESSERA
						if (inputPerson.equals(storedPerson)) {
							// STESSO DISCORSO PER LA DATA DI SCADENZA
							if (storedMedium.getValidTo().equals(inputMedium.getValidTo())) {
								// aggiorno su maigesdi
								// se va in errrore gesdi ripristino la tessera vecchia
								this.mediumDao.updateMedium(parameterMedium);
							} else {
								throw new ForbiddenException(
										"Updating Problem: Impossible to update expiration date; Disable medium from CEN an create a new one ",
										Utility.ErrorType.ET_MEDIUM_UPDATING_NOT_ALLOWED_EXPIRATION_DATE_NOT_EDITABLE
												.getValue());
							}
						} else {
							throw new ForbiddenException(
									"Updating Problem: Impossible to update data of the Person; Disable medium from CEN an create a new one ",
									Utility.ErrorType.ET_MEDIUM_UPDATING_NOT_ALLOWED_PERSON_DATA_NOT_EDITABLE.getValue());
		
						}
		
						// leggo il medium inserito dal db e lo ritorno come risultato
						mediumResultList.add(this.mediumDao.getMedium(inputMedium.getMediumType().getID(),
								inputMedium.getMediumIdentifier(), operator.getID()));
		
						result = new ExternalSystemResult(true, mediumResultList);
						
					}else {
						result = new ExternalSystemResult(false,
								new PreconditionException("Person givenName, familyName, birthPlace, birthDate, gender and are required",
															Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
					}
				} else {
					result = new ExternalSystemResult(false, new ResourceNotFoundException("Medium doesn't exist",
							Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()));
				}

			}else {
				result = new ExternalSystemResult(false, new PreconditionException("Medium ID is required",
						Utility.ErrorType.ET_PRECONDITION_ERROR.getValue()));
			}
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);

		}

		return result;

	}

	@Override
	public ExternalSystemResult getMediums(ParameterMedium parameterMedium, Operator operator) {

		ExternalSystemResult result;

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();
		try {
			List<Medium> mediumResultList = this.mediumDao.getMediumCustom(inputMedium, operator.getID());

			result = new ExternalSystemResult(true, mediumResultList);
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);
		}
		return result;
	}

	@Override
	public ExternalSystemResult disableMedium(ParameterMedium parameterMedium, Operator operator) {
		List<Medium> mediumResultList = new ArrayList<Medium>();
		boolean isCENDebugMode = utilityService.isDebugMode();
		ExternalSystemResult result;
		Person person = null;

		// medium da utilizzare per le operazioni
		Medium inputMedium = parameterMedium.toMedium();

		// medium presente nel DB
		Medium storedMedium = null;

		// riasultato della verifica preliminare del CEN
		CENAuthenticationResult cenDeleteResult = null;

		// gestione eccezione dopo autorizzazione al cen
		Exception exceptionResult = null;
		
		//gestione dello stato del medium dopo la disabilitazione
		Integer idStatoFinale;

		try {

			//repeerisco il medium a aprtire dall'id nello stato o valido o in lavorazione
			storedMedium = this.mediumDao.getMediumForDisableNoSecurity(inputMedium.getID());
			
			// il medium è gia presente nel sistema?
			if (storedMedium != null) {
				
				//verifico lo  stato finale passato in input, ossia la causa della diasbilitazione
				if(inputMedium.getState() != null &&  inputMedium.getState().getID() != null) {
					//gli stati accettati di partenza per la disabilitazione del medium
					switch(inputMedium.getState().getID()) {
						case Utility.TSSD_DANNEGGIATO:
						case Utility.TSSD_ERRATO:
						case Utility.TSSD_RETTIFICATO_CEN:
						case Utility.TSSD_RINUNCIATO:
						case Utility.TSSD_RUBATO:
						case Utility.TSSD_SMARRITO:
							
							idStatoFinale = inputMedium.getState().getID();
											
							//lo stato attuale è in lavorazione?
							if(storedMedium.getState().getID().equals(Utility.TSSD_IN_LAVORAZIONE)) {
								//allora cambio semplicemente lo stato in rinunciato senza passare dal CEN (maipolice)
								
								this.mediumDao.updateMediumState(operator.getID(),
																 storedMedium.getID(),
																 idStatoFinale);
								
								
							}else {
								//lo stato attuale è attivo quindi devo disabilitare anche al sistema esterno
								person = new Person(storedMedium.getGivenName(), 
										storedMedium.getFamilyName(),
										storedMedium.getBirthPlace().getFiscalCode(), 
										storedMedium.getBirthDate(),
										storedMedium.getGender());
					
								//Aggiornamento dello stato in Autorizzazione VRO poichè nel caso in cui ci siano problemi lato db dopo la 
								//disattivazione al CEN la tessera risultera non attiva ma in lavorazione
								this.mediumDao.updateMediumStateAndOperativeFlow(operator.getID(), storedMedium.getID(), Utility.TSSD_IN_LAVORAZIONE, Utility.TSFO_VERIFICA_PRESSO_AUTHORITY);
								
								// SISTEMA ESTERNO delete
								cenDeleteResult = this.maiPoliceService.deleteCENSoccerCard(this.externalSystem, storedMedium, person, isCENDebugMode);
						
								if (cenDeleteResult.getAutorizzato()) {
						
									try {
						
										// MAIGESDI State Update Utility.TSSD_RINUNCIATO
										this.mediumDao.updateMediumExternalSystemResult(operator.getID(),
																						storedMedium.getID(),
																						Utility.REMOVING_MEDIUM_EXTERNAL_SYSTEM, 
																						cenDeleteResult,
																						Utility.TSFO_SUPPORTO_PRODOTTO, 
																						idStatoFinale);
						
									} catch (Exception exception) {
										logger.error("Updating medium state problem; Medium disabled on CEN; Medium: "
													+ storedMedium + " Exception : " + exception.getMessage());
										
						
										exceptionResult = new MAIGeSDiException(
												"Medium disabling problem; Medium state is not valid on CEN but 'IN_LAVORAZIONE' on DB; Medium: "+ storedMedium,
												Utility.ErrorType.ET_MEDIUM_NOT_DISABLED_BUT_DISABLED_ON_CEN.getValue());
						
										throw exceptionResult;
									}
								
								} else {
									throw new MAIGeSDiException(
													"CEN Delete medium error: " + cenDeleteResult.getCodiceEccezione() + " - "
															+ cenDeleteResult.getDescrizioneEccezione(),
													Utility.ErrorType.ET_CEN_EXCEPTION.getValue());
						
								}
							}
							
							// leggo il medium inserito dal db e lo ritorno come risultato
							mediumResultList.add(this.mediumDao.getMedium(storedMedium.getID(), operator.getID()));
				
							result = new ExternalSystemResult(true, mediumResultList);
						
						break;
					//tutti gli altri stati
					default:
						result = new ExternalSystemResult(false, new ConflictException("Medium not disabled, medium state not valid",
																Utility.ErrorType.ET_MEDIUM_STATE_NOT_VALID.getValue()));
				}
				
				}else {
					// non è presente lo stato finale del medium che è la causale di disabilitazione
					result = new ExternalSystemResult(false, new PreconditionException("Medium final state is required",
							Utility.ErrorType.ET_PRECONDITION_ERROR.getValue()));
				}
				
			} else {
				result = new ExternalSystemResult(false, new ConflictException("Medium doesn't exist or not valid",
						Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()));
			}
		} catch (Exception e) {
			result = new ExternalSystemResult(false, e);
		}

		return result;

	}

	//è possiible verificare solo se il medium è presente in MAIGeSDi
		// si verifica la presenza nel sistema esterno
		@Override
		public ExternalSystemResult verifyMedium(ParameterMedium parameterMedium, Operator operator) {
			ExternalSystemResult result;
			List<Medium> mediumList;
			CENAuthenticationResult cenAuthResult = null;
			Person inputPerson = null;
			Person storedPerson = null;
			Medium storedMedium = null;
			Medium inputMedium = parameterMedium.toMedium();
			boolean isCENDebugMode = utilityService.isDebugMode();
			try {		
				//l'identificativo è valorizzato?
				if(inputMedium.getMediumIdentifier() != null && !inputMedium.getMediumIdentifier().isEmpty() ) {
					storedMedium = this.mediumDao.getMedium(inputMedium.getMediumType().getID(), inputMedium.getMediumIdentifier(), operator.getID());
					//storedMedium = this.mediumDao.getMedium(inputMedium.getID(), operator.getID());
								
					// il medium è gia presente nel sistema?
					if (storedMedium != null) {
								
						
						// è presente la sestupletta?
						if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {
	
							inputPerson = new Person(inputMedium.getGivenName(), 
												inputMedium.getFamilyName(),
												inputMedium.getBirthPlace().getFiscalCode(), 
												inputMedium.getBirthDate(),
												inputMedium.getGender());
							
							//devo verificare se la sestupletta salvata e quella passata sono uguali?
							
							storedPerson = new Person(  storedMedium.getGivenName(), 
														storedMedium.getFamilyName(),
														storedMedium.getBirthPlace().getFiscalCode(), 
														storedMedium.getBirthDate(),
														storedMedium.getGender());
							
							logger.info("inputMedium.getValidTo(): " + inputMedium.getValidTo());
							logger.info("storedMedium.getValidTo(): " + storedMedium.getValidTo());
							
							logger.info("inputPerson: " + inputPerson);
							logger.info("storedPerson: " + storedPerson);
							
							if(inputPerson.equals(storedPerson) && inputMedium.getValidTo().equals(storedMedium.getValidTo())){
								
								//verifico se al CEN ESISTE ED é VALIDA
								cenAuthResult = this.maiPoliceService.verifyCENSoccerCard(this.externalSystem, inputMedium, inputPerson, isCENDebugMode);
								if (cenAuthResult.getAutorizzato()) {
									
									//DEV 979 devo aggiornare lo IuA
									this.mediumDao.updateMediumExternalSystemResult(storedMedium.getID(), cenAuthResult);
									
									// leggo il medium aggiornato dal db e lo ritorno come risultato
									mediumList  = new ArrayList<Medium>();
									mediumList.add(this.mediumDao.getMedium(storedMedium.getID(), operator.getID()));
									result = new ExternalSystemResult(true, mediumList);
								}else {
									// non presente al CEN 
									result = new ExternalSystemResult(false, new MAIGeSDiException("CEN medium check error: " + cenAuthResult.getCodiceEccezione() + " - "
																									+ cenAuthResult.getDescrizioneEccezione(),
																									Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
								}
							}else {
								//la sestupletta fornita non coincide con quella memorizzata in MAIGeSDi
								result  = new ExternalSystemResult(false, new ConflictException("Medium supplied mismatch with the saved one", Utility.ErrorType.ET_MEDIUM_MISMATCH.getValue()));
							}
							
						}else {
							result = new ExternalSystemResult(false, new MAIGeSDiException("CEN medium check error: " + cenAuthResult.getCodiceEccezione() + " - "
																							+ cenAuthResult.getDescrizioneEccezione(),
																							Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
						}
								
					}else {
						result = new ExternalSystemResult(false, new ResourceNotFoundException("VerifyMedium problem; Medium not found, errorCode", 
																								Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()));
					}
				}else {
					result = new ExternalSystemResult(false, new PreconditionException("Medium Identifier is required for this activity", 
															Utility.ErrorType.ET_PRECONDITION_ERROR.getValue()));

				}
					
			}catch(Exception e) {
				result = new ExternalSystemResult(false, e);
			}
			return result;
		}

		@Override
		public ExternalSystemResult restoreMedium(ParameterMedium parameterMedium, Operator operator) {
			ExternalSystemResult result;
			Person person = null;
			boolean ownAnotherMedium = false;
			//Integer mediumID = 0;
			List<Medium> mediumResultList = new ArrayList<Medium>();
			Medium inputMedium = parameterMedium.toMedium();
			//parameter medium a partire dal medium presente nel db
			ParameterMedium storedParameterMedium = null;
			boolean isCENDebugMode = utilityService.isDebugMode();
			
			// medium presente nel DB
			Medium storedMedium = null;

			// riasultato della verifica preliminare del CEN
			CENAuthenticationResult cenAuthResult = null;
			
			// gestione eccezione dopo autorizzazione al cen
			Exception exceptionResult = null;

			try {
				//Per gli aggiornamenti L'id si è deciso di non calcolarlo, va fornito da fuori
				if(inputMedium.getID() != null && inputMedium.getID() > 0) {
					// verifico nel DB se il medium esiste, indipendentemente dal suo stato
					storedMedium = this.mediumDao.getMediumNoSecurity(inputMedium.getMediumIdentifier(), inputMedium.getMediumType().getID());
					// il medium è gia presente nel sistema?
					if (storedMedium != null) {
	
						//verifico che lo stato sia disabilitato
						// per disabilitato si intende che non si trovi in uno dei seguenti stati
						// - IN_LAVORAZIONE
						// - ATTIVO e non scaduto, ossia con data di scadenza superiore alla data odierna
						switch(storedMedium.getState().getID()) {
							case Utility.TSSD_DANNEGGIATO:
							case Utility.TSSD_ERRATO:
							case Utility.TSSD_RETTIFICATO_CEN:
							case Utility.TSSD_RINUNCIATO:
							case Utility.TSSD_RUBATO:
							case Utility.TSSD_SMARRITO:
							case Utility.TSSD_ATTIVO:
								
								// verifico che il flusso sia SUPPORTO_PRODOTTO, poichè non è possibile fare la RESTORE (riattivazione) di un supporto che non è stato mai prodotto fisicamente
								if(storedMedium.getOperativeFlow().getID().equals(Utility.TSFO_SUPPORTO_PRODOTTO)) {
									
									//se lo stato è attivo verifico che la data di scadenza sia passata, altrimenti il medium è ancora  valido e non può essere riattivato
									if(storedMedium.getState().getID() == Utility.TSSD_ATTIVO) {
										if(storedMedium.getValidTo().before(new Date()))
											result = new ExternalSystemResult(false, new ConflictException("Medium not restorable, medium state not valid",
													Utility.ErrorType.ET_MEDIUM_STATE_NOT_VALID.getValue()));
									}
								
									// è presente la sestupletta?
									if (this.utilityService.isValidMediumForMAIPolice(inputMedium)) {
				
										person = new Person(inputMedium.getGivenName(), 
															inputMedium.getFamilyName(),
															inputMedium.getBirthPlace().getFiscalCode(), 
															inputMedium.getBirthDate(),
															inputMedium.getGender());
										// il titolare è minorenne?
										if (!this.utilityService.isAdultPerson(person)) {
											// controllo il tutore
											if (!this.utilityService.isValidTutor(inputMedium.getTutor())) {
												// se non è valido rilancio eccezione poichè un minorenne non può creare una
												// tessera senza il tutor
												throw new ForbiddenException("The future owner of the medium is a minor and requires a tutor",
																			Utility.ErrorType.ET_MEDIUM_OWNER_IS_MINOR_TUTOR_MISSING.getValue());
											}
										}
				
										// la persona ha già medium della stessa organizzazione?
										ownAnotherMedium = this.mediumDao.checkPersonOwnMediumSameOrganizationAndSpecificExternalSystem(inputMedium.getMediumType().getID(), person, this.externalSystem.getID());
					
										if (!ownAnotherMedium) {
											
											// verifica prima della riattivazione (persona)
											// richiesta maipolice
											cenAuthResult = this.maiPoliceService.verificaMotiviOstativi(this.externalSystem, person, isCENDebugMode);
											if (cenAuthResult.getAutorizzato()) {
												
												//salvo i dati del medium allo stato attuale per l'ipotesi del roolback applicativo in caso di errore
												// reperisco e imposto la photo nel medium salvato sul db
												storedMedium.setPhoto(this.mediumDao.getMediumPhoto(storedMedium.getID(), operator.getID()));
												// reperisco e imposto la firma del medium salvato sul db
												storedMedium.setSign(this.mediumDao.getMediumSign(storedMedium.getID(), operator.getID()));
												// reperisco e imposto la firma del tutore del medium salvato sul db
												if (storedMedium.getTutor() != null)
													storedMedium.getTutor().setSign(this.mediumDao.getMediumTutorSign(storedMedium.getID(), operator.getID()));
												//creo un parameterMedium a partire dal medium salvato nel db cosi da agevolare il rollback
												storedParameterMedium = parameterMedium.cloneAndUpdateParameterMedium(storedMedium);
				
												// inserisco stato in lavorazione e flusso VERIFICA_PRESSO_AUTHORITY
				//								inputMedium.setState(new Identity(Utility.TSSD_IN_LAVORAZIONE, "IN_LAVORAZIONE"));
				//								inputMedium.setOperativeFlow(new Identity(Utility.TSFO_VERIFICA_PRESSO_AUTHORITY, "VERIFICA_PRESSO_AUTHORITY"));
				
												// MAIGESDI update
												this.mediumDao.updateMedium(operator, parameterMedium, Utility.TSSD_IN_LAVORAZIONE, Utility.TSFO_VERIFICA_PRESSO_AUTHORITY);
				
												// SISTEMA ESTERNO INSERT
												// PORZIONE DI CODICE RIPRESA DA APIRESTGESDI
												cenAuthResult = this.maiPoliceService.submitCENSoccerCard(this.externalSystem, inputMedium,	person, isCENDebugMode);
				
												// 3 - aggiornamento in GESDI stato ATTIVO flusso prodotto 
				
												if (cenAuthResult.getAutorizzato()) {
				
													try {
				
														this.mediumDao.updateMediumExternalSystemResult(operator.getID(),
																										inputMedium.getID(),
																										Utility.ADDING_MEDIUM_EXTERNAL_SYSTEM, 
																										cenAuthResult,
																										Utility.TSFO_SUPPORTO_PRODOTTO, 
																										Utility.TSSD_ATTIVO);
													} catch (Exception exception) {
														try {
				
															logger.error("Updating medium state problem; CEN disabling needed; Medium: "+ inputMedium.toString() + " Exception : " + exception.getMessage());
															CENAuthenticationResult deleteResult = this.maiPoliceService.deleteCENSoccerCard(this.externalSystem, inputMedium, person, isCENDebugMode);
															if (deleteResult.getAutorizzato()) {
																logger.info("Medium disabled on CEN, roolback medium from DB; Original medium: "
																		+ storedMedium +" Updated Medium: "+ inputMedium);
																//effettuo il rollback applicativo del medium (allineo la situazione al CEN)
																this.mediumDao.updateMedium(operator, storedParameterMedium, storedMedium.getState().getID(), storedMedium.getOperativeFlow().getID());
				
																exceptionResult = new MAIGeSDiException("Medium restore problem. Medium disabled on CEN and recovered on DB",
																										Utility.ErrorType.ET_MEDIUM_NOT_RESTORED.getValue());
															} else {
																exceptionResult = new MAIGeSDiException(
																		"Medium restore problem. CEN disabling problem; Medium still valid on CEN, change medium state on DB or disable on CEN; Original medium: "
																				+ storedMedium.toString() +" Updated Medium: "+ inputMedium,
																		Utility.ErrorType.ET_MEDIUM_NOT_RESTORED_BUT_VALID_ON_CEN
																				.getValue());
															}
														} catch (Exception innerException) {
															exceptionResult = new MAIGeSDiException(
																	"Medium restoring problem. Medium state inconsistent; Check CEN and DB validity; Original medium: "
																			+ storedMedium.toString() +" Updated Medium: "+ inputMedium + " Exception : "
																			+ innerException.getMessage(),
																	Utility.ErrorType.ET_MEDIUM_NOT_RESTORED_UKNOWN_CEN_STATE_INCONSISTENT_SITUATION
																			.getValue());
														}
				 
														throw exceptionResult;
				
													}
				
													// FINE PORZIONE APIRESTGESDI
				
													// leggo il medium riattivato dal db e lo ritorno come risultato
													mediumResultList.add(this.mediumDao.getMedium(inputMedium.getID(), operator.getID()));
				
													result = new ExternalSystemResult(true, mediumResultList);
												} else {
													result = new ExternalSystemResult(false,
															new MAIGeSDiException(
																	"Restore medium; CEN Submit medium error: " + cenAuthResult.getCodiceEccezione() + " - "
																			+ cenAuthResult.getDescrizioneEccezione(),
																	Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
				
													//effettuo il rollback applicativo del medium (allineo la situazione al CEN)
													this.mediumDao.updateMedium(operator, storedParameterMedium, storedMedium.getState().getID(), storedMedium.getOperativeFlow().getID());
				
												}
											} else {
												result = new ExternalSystemResult(false,
														new MAIGeSDiException("Restore medium; CEN Preliminary check error: " + cenAuthResult.getCodiceEccezione() + " - "
																				+ cenAuthResult.getDescrizioneEccezione(),
																				Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
											}
										} else {
											result = new ExternalSystemResult(false,
																				new ForbiddenException("Person own another medium of the same organization",
																										Utility.ErrorType.ET_PERSON_ALREDY_OWN_MEDIUM_FOR_SAME_ORGANIZATION.getValue()));
										}		
									} else {
										result = new ExternalSystemResult(false,
												new PreconditionException("Person givenName, familyName, birthPlace, birthDate and gender are required",
																			Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
									}
								}else {
									result = new ExternalSystemResult(false,
											new PreconditionException("Cannot restore this medium, because it has not yet been produced ",
																		Utility.ErrorType.ET_MEDIUM_OPERATIVE_FLOV_NOT_VALID.getValue()));
								}
								break;
							//tutti gli altri stati
							default:
								result = new ExternalSystemResult(false, new ConflictException("Medium not restorable, medium state not valid",
																		Utility.ErrorType.ET_MEDIUM_STATE_NOT_VALID.getValue()));
						}
	
					} else {
						result = new ExternalSystemResult(false, new ResourceNotFoundException("Medium doesn't exist",
																						Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue()));
	
					}
				}else {
					result = new ExternalSystemResult(false, new PreconditionException("Medium ID is required",
							Utility.ErrorType.ET_PRECONDITION_ERROR.getValue()));
				}
			} catch (Exception e) {
				result = new ExternalSystemResult(false, e);

			}

			return result;
		}


		@Override
		public ExternalSystemResult checkValidPersonOnExternalSystem(String givenName, 
																	 String familyName,
																	 String fiscalCodeBirthPlace, 
																	 Date birthDate, 
																	 String gender, 
																	 Operator operator) {
			ExternalSystemResult result = null;
			Person person = null;
			try {
				person = new Person(givenName, 
									familyName,
									fiscalCodeBirthPlace.toUpperCase(), 
									birthDate, 
									gender);
				
				if (this.utilityService.isValidPersonForMAIPolice(person)) {
					
					CENAuthenticationResult cenAuthResult = this.maiPoliceService.verificaMotiviOstativi(this.externalSystem, person, utilityService.isDebugMode());
					if (cenAuthResult.getAutorizzato()) {
						result = new ExternalSystemResult(true, cenAuthResult.getIuA());
					}else{
						result = new ExternalSystemResult(false,	"CEN error: " + cenAuthResult.getCodiceEccezione() + " - " + cenAuthResult.getDescrizioneEccezione());
					}
				} else {
					result = new ExternalSystemResult(false,
							new PreconditionException("GivenName, familyName, birthPlace, birthDate, gender are required",
														Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
				}
				
			} catch (Exception e) {
				result = new ExternalSystemResult(false, e);

			}

			return result;
		}

		@Override
		public ExternalSystemResult checkExistingMediumOnExternalSystem(Integer externalSystemID, 
																		String mediumIdentifier,
																		String givenName, 
																		String familyName, 
																		String fiscalCodeBirthPlace, 
																		Date birthDate, 
																		String gender,
																		Date validTo, 
																		Operator operator) {
			ExternalSystemResult result = null;
			try {
				//Sono coscente del fatto che è un refuso usare medium e persona per gestire le stesse inforazioni due volte.
				//lo lascio per non avere ripercussioni sul codice già scritto e testato
				Medium medium = new Medium(mediumIdentifier,
											givenName, 
											familyName,
											fiscalCodeBirthPlace.toUpperCase(),
											birthDate,
											gender,
											validTo);
				
				Person person = new Person(givenName, 
											familyName,
											fiscalCodeBirthPlace.toUpperCase(), 
											birthDate, 
											gender);
				
				if (this.utilityService.isValidMediumForMAIPolice(medium)) {
					//verifico se al CEN ESISTE 
					CENAuthenticationResult cenAuthResult = this.maiPoliceService.verifyCENSoccerCard(this.externalSystem, medium, person, utilityService.isDebugMode());
					if (cenAuthResult.getAutorizzato()) {
						result = new ExternalSystemResult(true, cenAuthResult.getIuA());
					}else {
						// non presente al CEN 
						result = new ExternalSystemResult(false, "CEN error: " + cenAuthResult.getCodiceEccezione() + " - " + cenAuthResult.getDescrizioneEccezione());
					}
					
				}else {
					result = new ExternalSystemResult(false,
							new PreconditionException("GivenName, familyName, birthPlace, birthDate, gender and medium validityTo are required",
														Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
				
				}
			} catch (Exception e) {
				result = new ExternalSystemResult(false, e);

			}
			
			return result;
		}

		@Override
		public ExternalSystemResult activeExistingMediumOnExternalSystem(ParameterMedium parameterMedium, Operator operator) {
	
			ExternalSystemResult result;
			Person person = null;
			boolean isCENDebugMode = utilityService.isDebugMode();
			
			List<Medium> mediumResultList = new ArrayList<Medium>();
			Medium inputMedium = parameterMedium.toMedium();
			// parameter medium a partire dal medium presente nel db
			ParameterMedium storedParameterMedium = null;
	
			// medium presente nel DB
			Medium storedMedium = null;
	
			// riasultato della verifica preliminare del CEN
			CENAuthenticationResult cenAuthResult = null;
	
			// gestione eccezione dopo autorizzazione al cen
			Exception exceptionResult = null;
	
			try {
				// Per l'attivazione si è deciso di non calcolarlo il medium id, va fornito da
				// fuori
				if (inputMedium.getID() != null && inputMedium.getID() > 0) {
	
					// verifico nel DB se il medium esiste ed è valido
					storedMedium = this.mediumDao.getValidMediumNoSecurity(inputMedium.getID());
	
					// il medium è gia presente nel sistema? il medium è valido?
					if (storedMedium != null) {
	
						// è presente la sestupletta?
						if (this.utilityService.isValidMediumForMAIPolice(storedMedium)) {
	
							person = new Person(storedMedium.getGivenName(), 
												storedMedium.getFamilyName(),
												storedMedium.getBirthPlace().getFiscalCode(), 
												storedMedium.getBirthDate(),
												storedMedium.getGender());
							// il titolare è minorenne?
							if (!this.utilityService.isAdultPerson(person)) {
								// controllo il tutore
								if (!this.utilityService.isValidTutor(storedMedium.getTutor())) {
									// se non è valido rilancio eccezione poichè un minorenne non può creare una
									// tessera senza il tutor
									throw new ForbiddenException(
											"The future owner of the medium is a minor and requires a tutor",
											Utility.ErrorType.ET_MEDIUM_OWNER_IS_MINOR_TUTOR_MISSING.getValue());
								}
							}
							// verifica prima della attivazione (persona)
							// richiesta maipolice
							cenAuthResult = this.maiPoliceService.verificaMotiviOstativi(this.externalSystem, person, isCENDebugMode);
							if (cenAuthResult.getAutorizzato()) {
	
								// SISTEMA ESTERNO INSERT
								cenAuthResult = this.maiPoliceService.submitCENSoccerCard(this.externalSystem, inputMedium, person, isCENDebugMode);
								if (cenAuthResult.getAutorizzato()) {
	
									try {
	
										this.mediumDao.updateMediumExternalSystemResult(storedMedium.getID(),
												cenAuthResult);
									} catch (Exception exception) {
										logger.error("Updating medium authorizationID (IuA) problem; Exception : "
												+ exception.getMessage());
										throw exceptionResult;
									}
	
									// leggo il medium riattivato dal db e lo ritorno come risultato
									mediumResultList.add(this.mediumDao.getMedium(inputMedium.getID(), operator.getID()));
	
									result = new ExternalSystemResult(true, mediumResultList);
								} else {
									result = new ExternalSystemResult(false,
											new MAIGeSDiException(
													"Activate medium; CEN Submit medium error: "
															+ cenAuthResult.getCodiceEccezione() + " - "
															+ cenAuthResult.getDescrizioneEccezione(),
													Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
								}
							} else {
								result = new ExternalSystemResult(false,
										new MAIGeSDiException(
												"Activate medium; CEN Preliminary check error: "
														+ cenAuthResult.getCodiceEccezione() + " - "
														+ cenAuthResult.getDescrizioneEccezione(),
												Utility.ErrorType.ET_CEN_EXCEPTION.getValue()));
							}
	
						} else {
							result = new ExternalSystemResult(false, new PreconditionException(
									"Activate medium; Stored medium incomplete: person  givenName, familyName, birthPlace, birthDate and gender are required",
									Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
						}
	
					} else {
						result = new ExternalSystemResult(false, new PreconditionException(
								"Cannot activate this medium on VRO, because it has not yet been produced or it's not valid ",
								Utility.ErrorType.ET_MEDIUM_OPERATIVE_FLOV_NOT_VALID.getValue()));
					}
	
				} else {
					result = new ExternalSystemResult(false, new PreconditionException("Medium ID is required",
							Utility.ErrorType.ET_PRECONDITION_ERROR.getValue()));
				}
			} catch (Exception e) {
				result = new ExternalSystemResult(false, e);
	
			}
	
			return result;
		}

		@Override
		public ExternalSystemResult checkPersonDataIsValid(Integer mediumTypeID, Person personData, Operator operator) {
			ExternalSystemResult result;
			boolean ownAnotherMedium = false;
			List<Medium> mediumResultList = new ArrayList<Medium>();
					
			try {
				// i dati della persona sono valorizzati?
				if (this.utilityService.isValidPersonForMAIPolice(personData)) {
										
					//controllo congruenza tra codiceerariale del luogo di nascita e data nascita
					if(this.mediumDao.validateBirthDateAndFiscalCodeBirthPlace(personData.getBirthDate(), personData.getFiscalCodeBirthPlace())) {
					
						// la persona ha già medium della stessa organizzazione? (per questo sistema esterno, ossia possiede altre TDT?)
						ownAnotherMedium = this.mediumDao.checkPersonOwnMediumSameOrganizationAndSpecificExternalSystem(mediumTypeID, personData, this.externalSystem.getID());
						if (!ownAnotherMedium) {
							//ANAGRAFICA VALIDA
							return new ExternalSystemResult(true, new ArrayList<Medium>()); 
													
						} else {
							
							//devo reperire il supporto o i supporti che già esistono per l'anagrafica
							mediumResultList = this.mediumDao.getListValidMediumOfOrganizationByPerson(mediumTypeID, personData, this.externalSystem.getID());
							
							//se sono più medium attivi o in lavorazione per la stessa anagrafica devo rilanciare errore, possibile disallineamento con CEN 
							if(mediumResultList.size() > 1) {
								result =  new ExternalSystemResult(false,
																	new ForbiddenException("Person own many medium of the same organization with state ATTIVO or IN_LAVORAZIONE, possible misalignment with CEN system. Search all medium for this person for more details ",
																	Utility.ErrorType.ET_PERSON_ALREDY_OWN_TOO_MANY_MEDIUM_FOR_SAME_ORGANIZATION.getValue()));
							}else {
								result = new ExternalSystemResult(false,
										new ForbiddenException("Person own another medium of the same organization",
													Utility.ErrorType.ET_PERSON_ALREDY_OWN_MEDIUM_FOR_SAME_ORGANIZATION.getValue()));
							
								result.setMediumResult(mediumResultList);
							}
						}
					} else {
						result = new ExternalSystemResult(false,
								new PreconditionException("FiscalCodeBirthPlace not valid for specific birthdate",
															Utility.ErrorType.ET_BIRTHDATE_AND_BIRTHPLACE_NOT_VALID.getValue()));
					}
				} else {
					result = new ExternalSystemResult(false,
							new PreconditionException("Person givenName, familyName, birthPlace, birthDate and gender missing or not valid; givenName and familyName must respect the regular expression "+UtilityService.PATTERN_MAIPOLICE+"; gender must be only 'M' or 'F' ",
														Utility.ErrorType.ET_PERSON_DATA_NOT_FOUND.getValue()));
				}

			
			} catch (Exception e) {
				result = new ExternalSystemResult(false, e);

			}

			return result;
		}

}
