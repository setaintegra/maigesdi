package it.integrasistemi.maigesdi.businesslogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import ch.qos.logback.classic.pattern.Util;
import io.swagger.models.properties.IntegerProperty;
import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.DateRange;
import it.integrasistemi.maigesdi.bean.Document;
import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumReportContainer;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Paginator;
import it.integrasistemi.maigesdi.bean.Parameter;
import it.integrasistemi.maigesdi.bean.ParameterMappedDateValue;
import it.integrasistemi.maigesdi.bean.ParameterMappedIntegerValue;
import it.integrasistemi.maigesdi.bean.ParameterMappedStringValue;
import it.integrasistemi.maigesdi.bean.ParameterMedium;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.Place;
import it.integrasistemi.maigesdi.bean.Tutor;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemInterface;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemResult;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@Service
@Validated
public class MediumService {

	@Autowired
	private OperatorService operatorService;

	@Autowired
	private MediumTypeService mediumTypeService;

	@Autowired
	private UtilityService utilityService;
	
	@Autowired
	private ExternalSystemService externalSistemService;
	
	@Autowired
	private MediumDAO mediumDao;

	public Medium getMedium(@Valid Operator operator, 
							@NotNull(message="MediumTypeID is required") 
							@Min(value = 1, message = "MediumTypeID must be grather than zero") 
							Integer mediumTypeID, 
							@NotNull(message="mediumIdentifier is required")
							@NotBlank(message="mediumIdentifier must be not blank") 
							@Size(min=1 , message= "mediumIdentifier is required")
							String mediumIdentifier
							) throws Exception 
	{
		Medium result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
		//gestione del codice erariale per il luogo di nascita al posto del comune e della nazione
		//metodo per risolvere tutte le righe che hanno il valore del codice erariale del luogo di nascita a null
		//ossia tutti i casi di inserimenti effettuati al di fuori di MAIGESDI
		//in teoria dovrà sparire
		this.mediumDao.checkAndUpdateBirthPlaceFiscalCode(mediumTypeID, mediumIdentifier);

		result = this.mediumDao.getMedium(mediumTypeID, mediumIdentifier, storedOperator.getID());

		if (result == null)
			throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
				
		return result;

	}
	
	public Medium getMedium(@Valid Operator operator, 
							@NotNull(message = "Medium ID is required") 
							@Min(value = 1, message = "Medium ID must be grather than zero")
							Integer mediumID) throws Exception {
		Medium result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		//gestione del codice erariale per il luogo di nascita al posto del comune e della nazione
		//metodo per risolvere tutte le righe che hanno il valore del codice erariale del luogo di nascita a null
		//ossia tutti i casi di inserimenti effettuati al di fuori di MAIGESDI
		//in teoria dovrà sparire
		this.mediumDao.checkAndUpdateBirthPlaceFiscalCode(mediumID);
		
		result = this.mediumDao.getMedium(mediumID, storedOperator.getID());

		if (result == null)
			throw new ResourceNotFoundException("Medium not found", Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
				
		return result;
	}

	public List<Medium> handleMediumActivity(@Valid Operator operator, @Valid MediumPost mediumPost) throws Exception {
		List<Medium> result = new ArrayList<Medium>();
		ParameterMedium mediumParameterForActivity = null;
		//Medium mediumForActivity = null;
		
		Operator storedOperator = null;
		boolean operatorIsAuthorized;
		Activity activity = null;
		Integer mediumID = 0;
		
		ExternalSystemInterface sistemaGestore;
		ExternalSystemResult externalSystemResult = null;
				
		// reperisco l'operatore dal DB
		storedOperator = this.operatorService.getOperator(operator);

		// verifico se l'operatore ha diritto sul tiposupportodigitale
		// (è la prima volta che viene fatto esplicitamente)
		operatorIsAuthorized = this.operatorService.checkOperatorMediumTypeRights(storedOperator,
				mediumPost.getMedium().getMediumType().getID());

		if (operatorIsAuthorized) {
						
			// reperisco le informazioni relative all' attività richiesta
			activity = this.mediumTypeService.getParameterListByMediumTypeIDActivityID(mediumPost);

			// se non ho un risultato dal DB potrebbe essere che l'attività non è permessa
			// per quel tipo supporto digitale
			if (activity == null || activity.getParameters() == null || activity.getParameters().size() == 0)
				throw new ForbiddenException("Activity non allowed for this specific Medium Type", Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY.getValue());
						  
					
			// verifico i parametri passati se sono coerenti con quelli richiesti per la
			// specifica attività , in caso contrario viene lanciata una MAIGESDIPreconditionException
			// il risultato della verifica è il ParameterMedium da utilizzare per l'attività
			mediumParameterForActivity = this.checkValidityMediumForActivity(mediumPost.getMedium(), activity.getParameters());
			
			//mi preparo il medium risultante della conversione del ParameterMedium creato sopra come risultato dei controlli dei parametri (variabile mediumParameterForActivity)
			//mediumForActivity = mediumParameterForActivity.toMedium();
			
			//reperisco il sistema gestore "esterno o interno" per il tipo supporto
			sistemaGestore = this.externalSistemService.getExternalSystem(mediumParameterForActivity.getMediumTypeID());
			
			
			// si sceglie l'operazione da effettuare in base al tipo attività
			switch (activity.getActivityType().getName()) {
					
			// attività di creazione 	
			case Utility.ACTIVITY_TYPE_NEW:
				
				//verifico se la data di validità (validTo) è valorizzato, in caso contrario verra valorizzato con la data corrente + numero giorni validità del tipo supporto
				mediumParameterForActivity = this.validateValidDateRangeOfParameterMedium(mediumParameterForActivity);
				
				externalSystemResult = sistemaGestore.insertMedium(mediumParameterForActivity, storedOperator);
				
				break;
			// attività di creazione DA WEB 	
			case Utility.ACTIVITY_TYPE_NEW_WEB:
				
				//verifico se la data di validità (validTo) è valorizzato, in caso contrario verra valorizzato con la data corrente + numero giorni validità del tipo supporto
				mediumParameterForActivity = this.validateValidDateRangeOfParameterMedium(mediumParameterForActivity);
				
				externalSystemResult = sistemaGestore.insertWebMedium(mediumParameterForActivity, storedOperator);
				
				break;
			//attività di ricerca	
			case Utility.ACTIVITY_TYPE_SEARCH:
				externalSystemResult = sistemaGestore.getMediums(mediumParameterForActivity, storedOperator);
				
				break;
			// modifica/aggiornamento	
			case Utility.ACTIVITY_TYPE_MODIFY:
				externalSystemResult = sistemaGestore.updateMedium(mediumParameterForActivity, storedOperator);

				break;
			// modifica/aggiornamento	
			case Utility.ACTIVITY_TYPE_DISABLE:
				externalSystemResult = sistemaGestore.disableMedium(mediumParameterForActivity, storedOperator);

				break;
						
			// Verifica	
			case Utility.ACTIVITY_TYPE_VERIFY:
				externalSystemResult = sistemaGestore.verifyMedium(mediumParameterForActivity, storedOperator);

				break;
			
			// riattivazione	
			case Utility.ACTIVITY_TYPE_RESTORE:
				externalSystemResult = sistemaGestore.restoreMedium(mediumParameterForActivity, storedOperator);

				break;
			
			// Attivazione di un medium già presente su maigesdi ma non presso il vro (sistema esterno)	
			case Utility.ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM:
				externalSystemResult = sistemaGestore.activeExistingMediumOnExternalSystem(mediumParameterForActivity, storedOperator);

				break;

			default:
				throw new  ForbiddenException("Activity Type not valid", Utility.ErrorType.ET_UNSUPPORTED_ACTIVITY_TYPE.getValue());
			
			}

			
			if(externalSystemResult.isResult()) {
			   result = externalSystemResult.getMediumResult();
			}else {
				if(externalSystemResult.getExceptionCatched()!=null) {
					throw externalSystemResult.getExceptionCatched();
				}else {
					throw new Exception("Generic error; External system result false and exception catched null");
				}
			}

			
		} else {
			
			throw new UnauthorizedException("Insufficient operator's rights, verify that mediumTypeID is valid", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		}

		return result;
	}

	/**
	 * Metodo per la gestione della data di inizio e fine validità del supporto digitale, se le date di validità sono state passate da fuori si usano quelle
	 * altrimenti si setta la data di scadenza come la data di oggi sommata al numero di giorni di validità impostata sul tipo supporto (mediumType.numberDaysOfValidity)
	 * e la data di inizio validità come sysdate
	 * nel caso il parametro sul tipo supporto non sia valorizzato ritorna eccezione
	 * @param mediumParameterForActivity
	 * @throws MAIGeSDiException nel caso in cui il numero di giorni di validità non siano valorizzati sul tipo supporto
	 */
	private ParameterMedium validateValidDateRangeOfParameterMedium(ParameterMedium mediumParameterForActivity) throws MAIGeSDiException {
		ParameterMappedDateValue validFrom;
		ParameterMappedDateValue validTo;
		Integer numberDaysOfValidity;
		
		//verifico se la data di inizio validità è valorizzata
		if(mediumParameterForActivity.getValidFrom()== null || mediumParameterForActivity.getValidFrom().getValue() == null) {
			numberDaysOfValidity = this.mediumTypeService.getMediumTypeDayOfValidity(mediumParameterForActivity.getMediumTypeID());
			Date dateValidFrom = this.utilityService.getTodayDate();
			validFrom = new ParameterMappedDateValue(dateValidFrom, true);
			
			mediumParameterForActivity.setValidFrom(validFrom);
			
		}
		//verifico se la data di fine validità è valorizzata
		if(mediumParameterForActivity.getValidTo()== null || mediumParameterForActivity.getValidTo().getValue() == null) {
			numberDaysOfValidity = this.mediumTypeService.getMediumTypeDayOfValidity(mediumParameterForActivity.getMediumTypeID());
			Date dateValidTo = this.utilityService.addDaysToToday(numberDaysOfValidity);
			validTo = new ParameterMappedDateValue(dateValidTo, true);
			
			mediumParameterForActivity.setValidTo(validTo);
			
		}
		return mediumParameterForActivity;
			
		
	}

	//TERZA VERSIONE, SI VALUTANO TUTTI I PARAMETRI DELL'ATTIVITA NON SOLO QUELLI MANDATORI, INOLTRE IL RISULTATO NON + PIù UN MEDIUM MA UN OGGETTO INTERMEDIO ParameterMedium
	//se c'è un problema viene rilancaita una MAIGESDIPreconditionException
	private ParameterMedium checkValidityMediumForActivity(Medium medium, Set<Parameter> parameters)	throws PreconditionException {
		ParameterMedium result = new ParameterMedium();
		
		//variabili utili per il controllo
		ParameterMappedDateValue dateParameter =null;
		ParameterMappedIntegerValue integerParameter = null;
		ParameterMappedStringValue stringParameter = null;
		
		//variabili del medium che vanno riportate sempre
		result.setMediumTypeID(medium.getMediumType().getID());
		result.setCredentials(medium.getCredentials());
		
		// per ogni parametro obbligatorio verifico se è stato effettivamente
		// valorizzato nel medium passato
		for (Parameter p : parameters) {

			switch (p.getCode()) {
			//descrizione della logica di controllo
			//case ParameterCode:
			// se il parametro è obbligatorio/mandatorio e non è valido rilancio eccezione
			// se passa il controllo valorizzo il valore del parmetro in questione nel ParameterMedium di risultato che verrà utilizzato realmente per l'attività
			
						
			// mapping ID -> medium.getID()
			case Utility.PARAM_ID:
				if (p.getMandatory() && !this.utilityService.isIntegerParameterValid(medium.getID()))
					throw new PreconditionException(" Medium's ID is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				integerParameter = new ParameterMappedIntegerValue(this.utilityService.getValidIntegerValue(medium.getID()),p.getMandatory());
				result.setID(integerParameter);
				
				break;
			// mapping IDENTIFIER -> .medium.getMediumIdentifier()
			case Utility.PARAM_IDENTIFIER:
				if (p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getMediumIdentifier()))
					throw new PreconditionException(" Medium's IDENTIFIER is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getMediumIdentifier()),p.getMandatory());
				result.setMediumIdentifier(stringParameter);
				
				break;
			// mapping  PIN -> 
			case Utility.PARAM_PIN:
				if (p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getPIN()))
					throw new PreconditionException(" Medium's PIN is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getPIN()),p.getMandatory());
				result.setPIN(stringParameter);
				
				break;
			// mapping GIVENNAME -> medium.getGivenName 
			case Utility.PARAM_GIVENNAME:
				if (p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getGivenName()))
					throw new PreconditionException(" Medium's GivenName is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getGivenName()),p.getMandatory());
				result.setGivenName(stringParameter);
				
				break;
			// mapping FAMILYNAME -> medium.getFamilyName()
			case Utility.PARAM_FAMILYNAME:
				
				if (p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getFamilyName()))
					throw new PreconditionException(" Medium's Family Name is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getFamilyName()),p.getMandatory());
				result.setFamilyName(stringParameter);
				
				
				break;
				// mapping BIRTHPLACE -> medium.getBirthPlace().getFiscalCode()
			case Utility.PARAM_BIRTHPLACE:
				if(p.getMandatory() && (medium.getBirthPlace() == null || !this.utilityService.isStringParameterValid(medium.getBirthPlace().getFiscalCode()))) 
					throw new PreconditionException(" Medium's birth place is required for this Activity. Place is null or Place.fiscalCode is null or empty", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getBirthPlace() != null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getBirthPlace().getFiscalCode()),p.getMandatory());
					result.setBirthPlaceFiscalCode(stringParameter);
				}
				break;
				// mapping BIRTHDATE -> medium.getBirthDate()
			case Utility.PARAM_BIRTHDATE:
				if(p.getMandatory() && !this.utilityService.isDateParameterValid(medium.getBirthDate()))
					throw new PreconditionException(" Medium's Birth Date  is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getBirthDate()),p.getMandatory());
				result.setBirthDate(dateParameter);
				break;
			// mapping GENDER -> medium.getBirthDate()
			case Utility.PARAM_GENDER:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getGender()))
					throw new PreconditionException(" Medium's Gender is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getGender()),p.getMandatory());
				result.setGender(stringParameter);
				break;
				// mapping FISCALCODE -> medium.getFiscalCode()
			case Utility.PARAM_FISCALCODE:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getFiscalCode()))
					throw new PreconditionException(" Medium's Fiscal Code is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getFiscalCode()) ,p.getMandatory());
				result.setFiscalCode(stringParameter);
				
				break;
				// mapping ADDRESS1 -> medium.getAddress1()
			case Utility.PARAM_ADDRESS1:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getAddress1()))
					throw new PreconditionException(" Medium's Address1 is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getAddress1()) ,p.getMandatory());
				result.setAddress1(stringParameter);
								
				break;
				// mapping ADDRESS2 -> medium.getAddress2()
			case Utility.PARAM_ADDRESS2:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getAddress2()))
					throw new PreconditionException(" Medium's Address2 is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getAddress2()) ,p.getMandatory());
				result.setAddress2(stringParameter);
				
				break;
				// mapping ADDRESS3 -> medium.getAddress3()
			case Utility.PARAM_ADDRESS3:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getAddress3()))
					throw new PreconditionException(" Medium's Address3 is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getAddress3()) ,p.getMandatory());
				result.setAddress3(stringParameter);
				
				break;
				// mapping DOCUMENT_TYPE -> medium.getDocument().getDocumentType() 
			case Utility.PARAM_DOCUMENT_TYPE:
				if(p.getMandatory() && (medium.getDocument() == null || !this.utilityService.isStringParameterValid(medium.getDocument().getDocumentType())))
					throw new PreconditionException(" Medium's Document Type is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getDocument() != null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getDocument().getDocumentType()) ,p.getMandatory());
					result.setDocumentType(stringParameter);
					
				}
				break;
				// mapping DOCUMENT_CODE -> medium.getDocument().getCode()
			case Utility.PARAM_DOCUMENT_CODE:
				if(p.getMandatory() && (medium.getDocument() == null || !this.utilityService.isStringParameterValid(medium.getDocument().getCode())))
					throw new PreconditionException(" Medium document code is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getDocument() != null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getDocument().getCode()) ,p.getMandatory());
					result.setDocumentCode(stringParameter);
					
				}
				break;
				// mapping  -> medium.getDocument().getReleaseDate()
			case Utility.PARAM_DOCUMENT_RELEASEDATE:
				if(p.getMandatory() &&(medium.getDocument() == null || !this.utilityService.isDateParameterValid(medium.getDocument().getReleaseDate())))
					throw new PreconditionException(" Medium document release date  is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
								
				if(medium.getDocument() != null) {
					dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getDocument().getReleaseDate()),p.getMandatory());
					result.setDocumentReleaseDate(dateParameter);
					
				}
				break;
				// mapping  -> medium.getDocument().getAuthority()
			case Utility.PARAM_DOCUMENT_AUTHORITY:
				if(p.getMandatory() &&(medium.getDocument() == null || !this.utilityService.isStringParameterValid(medium.getDocument().getAuthority())))
					throw new PreconditionException(" Medium document authority  is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getDocument() != null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getDocument().getAuthority()) ,p.getMandatory());
					result.setDocumentAuthority(stringParameter);
										
				}
				break;
				// mapping  -> getDocument().getExpirationDate()
			case Utility.PARAM_DOCUMENT_EXPIRATIONDATE:
				if(p.getMandatory() &&(medium.getDocument() == null || !this.utilityService.isDateParameterValid(medium.getDocument().getExpirationDate())))
					throw new PreconditionException(" Medium Document Expiration Date is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getDocument() != null) {
					
					dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getDocument().getExpirationDate()),p.getMandatory());
					result.setDocumentExpirationDate(dateParameter);
										
				}
				break;
				// mapping  -> medium.getPhoneNumber()
			case Utility.PARAM_PHONENUMBER:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getPhoneNumber()) )
					throw new PreconditionException(" Medium's Phone Number is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getPhoneNumber()) ,p.getMandatory());
				result.setPhoneNumber(stringParameter);
			
				break;
				// mapping MOBILENUMBER -> medium.getMobileNumber()
			case Utility.PARAM_MOBILENUMBER:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getMobileNumber()) )
					throw new PreconditionException(" Medium's Mobile Number is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getMobileNumber()) ,p.getMandatory());
				result.setMobileNumber(stringParameter);
				
				break;
				// mapping  -> medium.geteMail()
			case Utility.PARAM_EMAIL:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.geteMail()) )
					throw new PreconditionException(" Medium's email is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.geteMail()) ,p.getMandatory());
				result.seteMail(stringParameter);
				
				break;
				// mapping RELEASEDATE -> medium.getReleaseDate()
			case Utility.PARAM_RELEASEDATE:
				if(p.getMandatory() && !this.utilityService.isDateParameterValid(medium.getReleaseDate()) )
					throw new PreconditionException(" Medium's Release Date is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getReleaseDate()),p.getMandatory());
				result.setReleaseDate(dateParameter);
				
				break;
				// mapping  -> medium.getValidFrom()
			case Utility.PARAM_VALIDFROM:
				if(p.getMandatory() && !this.utilityService.isDateParameterValid(medium.getValidFrom()) )
					throw new PreconditionException(" Medium's Validity date (From) is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getValidFrom()),p.getMandatory());
				result.setValidFrom(dateParameter);
				
				break;
				// mapping  -> medium.getValidTo()
			case Utility.PARAM_VALIDTO:
				if(p.getMandatory() && !this.utilityService.isDateParameterValid(medium.getValidTo()) )
					throw new PreconditionException(" Medium's Validity date (To) is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getValidTo()),p.getMandatory());
				result.setValidTo(dateParameter);
				
				break;
				// mapping  -> (medium.getTutor().getGivenName()
			case Utility.PARAM_TUTOR_GIVENNAME:
				if(p.getMandatory() && (medium.getTutor() == null || !this.utilityService.isStringParameterValid(medium.getTutor().getGivenName())))
					throw new PreconditionException(" Medium's Tutor Given Name is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
								
				if(medium.getTutor()!=null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getGivenName()) ,p.getMandatory());
					result.setTutorGivenName(stringParameter);
				}
				break;
				// mapping  -> medium.getTutor().getFamilyName 
			case Utility.PARAM_TUTOR_FAMILYNAME:
				if(p.getMandatory() &&  (medium.getTutor() == null || !this.utilityService.isStringParameterValid(medium.getTutor().getFamilyName())))
					throw new PreconditionException(" Medium's Tutor Family Name is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
								
				if(medium.getTutor()!=null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getFamilyName()),p.getMandatory());
					result.setTutorFamilyName(stringParameter);
				}
				break;
			// mapping  -> medium.getTutor().getBirthPlace()
			case Utility.PARAM_TUTOR_BIRTHPLACE:
				if(p.getMandatory() && (medium.getTutor() == null ||  !this.utilityService.isStringParameterValid(medium.getTutor().getBirthPlace()) ))
					throw new PreconditionException(" Medium's Tutor Birth Place is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getBirthPlace()) ,p.getMandatory());
					result.setTutorBirthPlace(stringParameter);
				}
				break;
				// mapping TUTOR_BIRTHDATE -> medium.getTutor().getBirthDate()
			case Utility.PARAM_TUTOR_BIRTHDATE:
				if(p.getMandatory() && (medium.getTutor() == null ||  !this.utilityService.isDateParameterValid(medium.getTutor().getBirthDate())) )
					throw new PreconditionException(" Medium's Tutor Birth Date is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					
					dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getTutor().getBirthDate()),p.getMandatory());
					result.setTutorBirthDate(dateParameter);
				}
				break;
				// mapping TUTOR_FISCALCODE -> medium.getTutor().getFiscalCode()
			case Utility.PARAM_TUTOR_FISCALCODE:
				if(p.getMandatory() && (medium.getTutor() == null ||  !this.utilityService.isStringParameterValid(medium.getTutor().getFiscalCode())) )
					throw new PreconditionException(" Medium's  Tutor Fiscal Code is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getFiscalCode()),p.getMandatory());
					result.setTutorFiscalCode(stringParameter);
					
				}
				break;
				// mapping TUTOR_ADDRESS1 -> medium.getTutor().getAddress1())
			case Utility.PARAM_TUTOR_ADDRESS1:
				if(p.getMandatory() && (medium.getTutor() == null ||  !this.utilityService.isStringParameterValid(medium.getTutor().getAddress1())))
					throw new PreconditionException(" Medium's Tutor Address1 is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getAddress1()),p.getMandatory());
					result.setTutorAddress1(stringParameter);
				}
				break;
				// mapping TUTOR_ADDRESS2 -> medium.getTutor().getAddress2()
			case Utility.PARAM_TUTOR_ADDRESS2:
				if(p.getMandatory() && (medium.getTutor() == null ||  !this.utilityService.isStringParameterValid(medium.getTutor().getAddress2())) )
					throw new PreconditionException(" Medium's Tutor Address2 is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getAddress2()),p.getMandatory());
					result.setTutorAddress2(stringParameter);
				}
				break;
				// mapping TUTOR_ADDRESS3 -> medium.getTutor().getAddress3()
			case Utility.PARAM_TUTOR_ADDRESS3:
				if(p.getMandatory() && (medium.getTutor() == null ||  !this.utilityService.isStringParameterValid(medium.getTutor().getAddress3())))
					throw new PreconditionException(" Medium's Tutor Address3 is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getAddress3()),p.getMandatory());
					result.setTutorAddress3(stringParameter);
				}
				break;
				// mapping TUTOR_DOCUMENT_TYPE -> medium.getTutor().getDocument().getDocumentType()
			case Utility.PARAM_TUTOR_DOCUMENT_TYPE:
				if(p.getMandatory() && (medium.getTutor() == null || medium.getTutor().getDocument() == null ||  !this.utilityService.isStringParameterValid(medium.getTutor().getDocument().getDocumentType())))
					throw new PreconditionException(" Medium's Tutor Document Type is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor() != null && medium.getTutor().getDocument() != null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getDocument().getDocumentType()) ,p.getMandatory());
					result.setTutorDocumentType(stringParameter);
				}
				break;
				// mapping TUTOR_DOCUMENT_CODE -> medium.getTutor().getDocument().getCode()
			case Utility.PARAM_TUTOR_DOCUMENT_CODE:
				if(p.getMandatory() && (medium.getTutor() == null ||  medium.getTutor().getDocument() == null || !this.utilityService.isStringParameterValid(medium.getTutor().getDocument().getCode())))
					throw new PreconditionException(" Medium's Tutor Document Code is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
								
				if(medium.getTutor() != null && medium.getTutor().getDocument() != null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getDocument().getCode()) ,p.getMandatory());
					result.setTutorDocumentCode(stringParameter);
				}
								
				break;
				// mapping TUTOR_DOCUMENT_RELEASEDATE -> medium.getTutor().getDocument().getReleaseDate()
			case Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE:
				if(p.getMandatory() && (medium.getTutor() == null ||  medium.getTutor().getDocument() == null || !this.utilityService.isDateParameterValid(medium.getTutor().getDocument().getReleaseDate())))
					throw new PreconditionException(" Medium's Tutor Document Release Date is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
								
				if(medium.getTutor() != null && medium.getTutor().getDocument() != null) {
					
					dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getTutor().getDocument().getReleaseDate()) ,p.getMandatory());
					result.setTutorDocumentReleaseDate(dateParameter);
				}
				break;
				// mapping TUTOR_DOCUMENT_AUTHORITY -> medium.getTutor().getDocument().getAuthority()
			case Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY:
				if(p.getMandatory() && (medium.getTutor() == null ||  medium.getTutor().getDocument() == null || !this.utilityService.isStringParameterValid(medium.getTutor().getDocument().getAuthority())))
					throw new PreconditionException(" Medium's Tutor Document Authority is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor() != null && medium.getTutor().getDocument() != null) {
					
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getDocument().getAuthority()) ,p.getMandatory());
					result.setTutorDocumentAuthority(stringParameter);
					
				}
				break;
				// mapping TUTOR_DOCUMENT_EXPIRATIONDATE -> medium.getTutor().getDocument().getExpirationDate()
			case Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE:
				if(p.getMandatory() && (medium.getTutor() == null ||  medium.getTutor().getDocument() == null || !this.utilityService.isDateParameterValid(medium.getTutor().getDocument().getExpirationDate())))
					throw new PreconditionException(" Medium's tutor document expire date  is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor() != null && medium.getTutor().getDocument() != null) {
					
					dateParameter = new ParameterMappedDateValue(this.utilityService.getValidDateValue(medium.getTutor().getDocument().getExpirationDate()),p.getMandatory());
					result.setTutorDocumentExpirationDate(dateParameter);
					
				}
				break;
			case Utility.PARAM_TUTOR_SIGN:
				if(p.getMandatory() && (medium.getTutor() == null || !this.utilityService.isStringParameterValid(medium.getTutor().getSign())))
					throw new PreconditionException(" Medium's Tutor Sign is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getTutor()!=null) {
					stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getTutor().getSign()), p.getMandatory());
					result.setTutorSign(stringParameter);
				}
				break;
				// mapping DENOMINATION -> medium.getDenomitaion())
			case Utility.PARAM_DENOMINATION:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getDenomination()) )
					throw new PreconditionException(" Medium's denomination is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getDenomination()), p.getMandatory());
				result.setDenomitaion(stringParameter);
				break;
				
			case Utility.PARAM_PHOTO:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getPhoto()))
					throw new PreconditionException(" Medium's photo is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getPhoto()), p.getMandatory());
				result.setPhoto(stringParameter);
				break;
				
			case Utility.PARAM_SIGN:
				if(p.getMandatory() && !this.utilityService.isStringParameterValid(medium.getSign()))
					throw new PreconditionException(" Medium's sign is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				stringParameter = new ParameterMappedStringValue(this.utilityService.getValidStringValue(medium.getSign()), p.getMandatory());
				result.setSign(stringParameter);
				break;
				
			// mapping STATE_TYPE_ID -> medium.getDenomitaion())
			case Utility.PARAM_STATE_TYPE_ID:
				if(p.getMandatory() && (medium.getState() == null || !this.utilityService.isIntegerParameterValid(medium.getState().getID())))
					throw new PreconditionException(" Medium's StateTypeID is required for this Activity", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
				
				if(medium.getState() != null) {
					integerParameter = new ParameterMappedIntegerValue(this.utilityService.getValidIntegerValue(medium.getState().getID()), p.getMandatory());
					result.setStateID(integerParameter);
				}
				break;
				// mapping  -> not found Exception
			default:
				throw new PreconditionException("Activity not valid", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());

			}

		}

		return result;
	}

	public String getMediumPhoto(@Valid Operator operator, 
								@NotNull(message = "Medium ID is required") 
								@Min(value = 1, message = "Medium ID must be grather than zero")
								Integer mediumID) throws Exception {
		String result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		result = this.mediumDao.getMediumPhoto(mediumID, storedOperator.getID());
		
		return result;
	}

	public String getMediumSign(@Valid Operator operator, 
								@NotNull(message = "Medium ID is required") 
								@Min(value = 1, message = "Medium ID must be grather than zero")
								Integer mediumID) throws Exception {
		String result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		result = this.mediumDao.getMediumSign(mediumID, storedOperator.getID());
		
		return result;
	}

	public String getMediumTutorSign(@Valid Operator operator, 
									@NotNull(message = "Medium ID is required") 
									@Min(value = 1, message = "Medium ID must be grather than zero")
									Integer mediumID) throws Exception {
		String result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		result = this.mediumDao.getMediumTutorSign(mediumID, storedOperator.getID());
		
		return result;
	}

	public MediumReportContainer searchMediumCustom(@Valid Operator operator, 
											Integer mediumTypeID, 
											@Size(min =1, max = 12, message="Medium identifier can't be bigger than 12 characters")
											String mediumIdentifier,
											Date releaseDateFrom, 
											Date releaseDateTo, 
											Date expiryDateFrom, 
											Date expiryDateTo, 
											@Min(value = 1, message = "stateTypeID must be grather than zero")
											Integer stateTypeID,
											@Min(value = 1, message = "operativeFlowTypeID must be grather than zero")
											Integer operativeFlowTypeID, 
											@Min(value = 1, message = "lastTransitionOperatorID must be grather than zero")
											Integer lastTransitionOperatorID, 
											@Size(max = 30, message="GivenName can't be bigger than 30 characters")
											String givenName, 
											@Size(max = 30, message="FamilyName can't be bigger than 30 characters")
											String familyName,
											@Size(max = 4, message="FiscalCodeBirthPlace can't be bigger than 4 characters")
											String fiscalCodeBirthPlace,
											Date birthDate, 
											@Min(value = 1, message = "emitterOperatorID must be grather than zero")
											Integer emitterOperatorID,
											@Min(value = 0, message = "offset must be positive")
											Integer offset,
											@NotNull
											@Min(value = 1, message = "maxResults must be grather than zero")
											Integer maxResults) throws Exception  {
		MediumReportContainer result = null;
		List<Medium> mediumList;
		Operator storedOperator = null;
		DateRange dateRange;
		Integer countTotalMedium = 0;
		Paginator paginator ;
		
		storedOperator = this.operatorService.getOperator(operator);
		
		
		
		if(this.utilityService.isValidParameterForSearchMediumCustom(mediumTypeID,
															 			mediumIdentifier,
															 			releaseDateFrom,
																		releaseDateTo,
																		expiryDateFrom,
																		expiryDateTo,
																		stateTypeID,
																		operativeFlowTypeID,
																		lastTransitionOperatorID,
																		givenName,
																		familyName,
																		fiscalCodeBirthPlace,
																		birthDate,
																		emitterOperatorID))
		{
			
			//controllo dell'intervallo di date sulla data di emissione
			dateRange = this.utilityService.checkAndFormatDateRange(releaseDateFrom, releaseDateTo);
			
			releaseDateFrom = dateRange.getStartRange();
			releaseDateTo = dateRange.getEndRange();
			
			//controllo dell'intervallo di date sulla data di scadenza
			dateRange = this.utilityService.checkAndFormatDateRange(expiryDateFrom, expiryDateTo);
			
			expiryDateFrom = dateRange.getStartRange();
			expiryDateTo = dateRange.getEndRange();
			
			offset = offset != null ? offset : 0;
			maxResults += offset;
			
			//cerco il numero totale di elementi della query
			countTotalMedium = this.mediumDao.countSearchMediumCustom(storedOperator, 
														mediumTypeID,
											 			mediumIdentifier,
											 			releaseDateFrom,
														releaseDateTo,
														expiryDateFrom,
														expiryDateTo,
														stateTypeID,
														operativeFlowTypeID,
														lastTransitionOperatorID,
														givenName,
														familyName,
														fiscalCodeBirthPlace,
														birthDate,
														emitterOperatorID);
			
			result = new MediumReportContainer(new Paginator(offset, maxResults, countTotalMedium));
			
			
			//se il numero totale è inferiore all'offset
			if(offset < countTotalMedium) {
			
			
				mediumList = this.mediumDao.searchMediumCustom(storedOperator, 
															mediumTypeID,
												 			mediumIdentifier,
												 			releaseDateFrom,
															releaseDateTo,
															expiryDateFrom,
															expiryDateTo,
															stateTypeID,
															operativeFlowTypeID,
															lastTransitionOperatorID,
															givenName,
															familyName,
															fiscalCodeBirthPlace,
															birthDate,
															emitterOperatorID,
															offset,
															maxResults);
			
				result.setItems(mediumList);
			
			}
			
			

			
		}else {
			throw new PreconditionException("No filter provided for this search", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
		}
				
		return result;
	}

	public Medium checkPersonDataIsValid(Operator operator, 
										  @NotNull(message="MediumTypeID is required") 
										  @Min(value = 1, message = "MediumTypeID must be grather than zero") 
										  Integer mediumTypeID,
										  @Valid Person ownerData) throws Exception {
		
		Medium result = null;
		Operator storedOperator = null;
		ExternalSystemInterface sistemaGestore;
		ExternalSystemResult externalSystemResult = null;
		
		// reperisco l'operatore dal DB
		storedOperator = this.operatorService.getOperator(operator);

		sistemaGestore = this.externalSistemService.getExternalSystem(mediumTypeID);
		
		externalSystemResult = sistemaGestore.checkPersonDataIsValid(mediumTypeID, ownerData, storedOperator);
		
		if(!externalSystemResult.isResult()) { 
			if(externalSystemResult.getMediumResult() !=null &&externalSystemResult.getMediumResult().size() > 0) {
				result = externalSystemResult.getMediumResult().get(0);
			}else {
				if(externalSystemResult.getExceptionCatched() != null) {
					throw externalSystemResult.getExceptionCatched();
				}else {
					throw new Exception("Generic error; External system result false and exception catched null");
				}
			}
		}
		return result;
	}
		
	
}
