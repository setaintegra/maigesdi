package it.integrasistemi.maigesdi.businesslogic;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.integrasistemi.maigesdi.bean.ExternalSystemCheckingResult;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Operator.Profile;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.ShortMediumData;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemInterface;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystemResult;
import it.integrasistemi.maigesdi.bean.utility.JsonDateDeserializer;
import it.integrasistemi.maigesdi.bean.utility.JsonDateSerializer;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.externalsystem.ExternalSystemFactory;
import it.integrasistemi.maigesdi.dao.ExternalSystemDAO;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@Service
@Validated
public class ExternalSystemService {
	
	@Autowired
	private OperatorService operatorService;
	
	@Autowired
	private ExternalSystemDAO externalSystemDao;
	
	@Autowired
	private ExternalSystemFactory externalSystemFactory;
	

	public List<ExternalSystem> getExternalSystems(@Valid Operator operator) throws Exception {
		List<ExternalSystem> result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
//		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) 
//			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		
		result = this.externalSystemDao.getExternalSystems(storedOperator.getID());
		
		return result;
	}

	public ExternalSystem getExternalSystemByID(@Valid Operator operator,
												@NotNull(message = "ExternalSystem ID is required; must be grather than zero") 
												@Min(value = 1, message = "ExternalSystem ID must be grather than zero")
												Integer id) throws Exception {
		ExternalSystem result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
//		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) 
//			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		
		result = this.externalSystemDao.getExternalSystemByID(storedOperator.getID(), id);
		
		if(result == null)
			throw new ResourceNotFoundException("External system not found", Utility.ErrorType.ET_EXTERNAL_SYSTEM_NOT_FOUND.getValue());
		
		return result;
	}
	
	
	public ExternalSystemInterface getExternalSystem(Integer mediumTypeID) throws Exception {
		ExternalSystemInterface result;
		ExternalSystem externalSystemData = null;
		
		externalSystemData = this.externalSystemDao.getExternalSystemByMediumTypeID(mediumTypeID);
		
		if(externalSystemData != null && externalSystemData.getName() != null && !externalSystemData.getName().isEmpty()) {
			result = this.externalSystemFactory.getConcreteExternalSystem(externalSystemData);
		}else {
			throw new MAIGeSDiException("External system not found or bad configurated for specific Medium type; MediumTypeID: "+ mediumTypeID,
										Utility.ErrorType.ET_EXTERNAL_SYSTEM_NOT_FOUND.getValue());
		}
		return result;
		
	}

	public ExternalSystemCheckingResult checkPersonOnExternalSystem(@Valid Operator operator, 
										 	  @NotNull(message = "ExternalSystem ID is required; must be grather than zero") 
											  @Min(value = 1, message = "ExternalSystem ID must be grather than zero")
											  Integer externalSystemID,
											  @Size(max = 4, message="Person's fiscalCodeBirthPlace can't be bigger than 4 characters")
											  String fiscalCodeBirthPlace,
											  @Size(max = 30, message="Person's givenName can't be bigger than 30 characters")
											  String givenName,
											  @Size(max = 30, message="Person's familyName can't be bigger than 30 characters")
											  String familyName,
											  @JsonDeserialize(using = JsonDateDeserializer.class)
											  @JsonSerialize(using = JsonDateSerializer.class)
											  Date birthDate,
											  @Size(max = 1, message="Person's gender can't be bigger than 1 characters")
											  String gender) throws Exception {
		
		ExternalSystemCheckingResult result;
		ExternalSystem externalSystemData = null;
		ExternalSystemInterface concreteExternalSystem = null; 
		ExternalSystemResult externalSystemResult = null;
		
		externalSystemData = this.getExternalSystemByID(operator, externalSystemID);
		concreteExternalSystem = this.externalSystemFactory.getConcreteExternalSystem(externalSystemData);
		
		externalSystemResult = concreteExternalSystem.checkValidPersonOnExternalSystem(givenName,
																						familyName,
																						fiscalCodeBirthPlace,
																						birthDate,
																						gender,
																						operator);
		
		if(externalSystemResult.isResult()) {
			result = new ExternalSystemCheckingResult(true, externalSystemResult.getResultDescription());
		}else {
			if(externalSystemResult.getExceptionCatched() == null) {
				result = new ExternalSystemCheckingResult(false, externalSystemResult.getResultDescription());
			}else {
				throw externalSystemResult.getExceptionCatched();
			}
		}
		
		return result;
	}

	
	public ExternalSystemCheckingResult checkExistingMediumOnExternalSystem(@Valid Operator operator, 
														@NotNull(message = "ExternalSystem ID is required; must be grather than zero") 
														@Min(value = 1, message = "ExternalSystem ID must be grather than zero")
														Integer externalSystemID,
														@Size(min =1, max = 12, message="Medium identifier can't be bigger than 12 characters")
														String mediumIdentifier,
														@Size(max = 30, message="GivenName can't be bigger than 30 characters")
														String givenName,
														@Size(max = 30, message="FamilyName can't be bigger than 30 characters")
														String familyName,
														@Size(max = 4, message="FiscalCodeBirthPlace can't be bigger than 4 characters")
														String fiscalCodeBirthPlace,
														Date birthDate,
														@Size(max = 1, message="Person's gender can't be bigger than 1 characters")
														String gender,
														Date validTo) throws Exception  {
		ExternalSystemCheckingResult result;
		ExternalSystem externalSystemData = null;
		ExternalSystemInterface concreteExternalSystem = null; 
		ExternalSystemResult externalSystemResult = null;
		
		externalSystemData = this.getExternalSystemByID(operator, externalSystemID);
		concreteExternalSystem = this.externalSystemFactory.getConcreteExternalSystem(externalSystemData);
		
		externalSystemResult = concreteExternalSystem.checkExistingMediumOnExternalSystem(externalSystemID, 
																							mediumIdentifier,
																							givenName,
																							familyName,
																							fiscalCodeBirthPlace,
																							birthDate,
																							gender,
																							validTo,
																							operator
																							);
		if(externalSystemResult.isResult()) {
			result = new ExternalSystemCheckingResult(true, externalSystemResult.getResultDescription());
		}else {
			if(externalSystemResult.getExceptionCatched() == null) {
				result = new ExternalSystemCheckingResult(false, externalSystemResult.getResultDescription());
			}else {
				throw externalSystemResult.getExceptionCatched();
			}
		}
		
		return result;
	}

//	public Boolean checkExistingMediumOnExternalSystem(Operator operator, Integer externalSystemID, ShortMediumData mediumData) throws Exception {
//		Boolean result;
//		ExternalSystem externalSystemData = null;
//		ExternalSystemInterface concreteExternalSystem = null; 
//		ExternalSystemResult externalSystemResult = null;
//		
//		externalSystemData = this.getExternalSystemByID(operator, externalSystemID);
//		concreteExternalSystem = this.externalSystemFactory.getConcreteExternalSystem(externalSystemData);
//		
//		externalSystemResult = concreteExternalSystem.checkExistingMediumOnExternalSystem(mediumData, operator);
//		
//		if(externalSystemResult.isResult()) {
//			result = externalSystemResult.isResult();
//		}else {
//			if(externalSystemResult.getExceptionCatched() != null) {
//				throw externalSystemResult.getExceptionCatched();
//			}else {
//				throw new Exception("Generic error; External system result false and exception catched null");
//			}
//		}
//		
//		return result;
//	}
	
	
	

}
