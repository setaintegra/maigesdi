package it.integrasistemi.maigesdi.businesslogic;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.integrasistemi.maigesdi.bean.EmptyMediumForMediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.MediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Operator.Profile;
import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.ReservedMedium;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.MediumIdentifierRangeDAO;
import it.integrasistemi.maigesdi.exception.ConflictException;
import it.integrasistemi.maigesdi.exception.DBMSFaultException;
import it.integrasistemi.maigesdi.exception.ForbiddenException;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@Service
@Validated
public class MediumIdentifierRangeService {

	@Autowired
	private OperatorService operatorService;

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private MediumTypeService mediumTypeService;

	@Autowired
	private MediumIdentifierRangeDAO mediumIdentifierRangeDAO;

	// NUOVA VERSIONE: solo un configuratore può vedere le spezzate e le vede tutte
	// VECCHIA VERSIONE:Ritorna tutte le spezzate relative all'operatore, se esso è
	// configuratore le vede tutte
	public List<MediumIdentifierRange> getMediumIdentifierRanges(@Valid Operator operator) throws Exception {
		List<MediumIdentifierRange> result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights",
					Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		result = this.mediumIdentifierRangeDAO.getAllMediumIdentifierRange();

		return result;
	}

	// ritorna tutte le spezzate per uno specifico tipo supporto
	// Da gesdire meglio la doppia chiamata alla getOperator una direttamente nel
	// metodo una all'interno del metodo mediumTypeService.getMediumTypesByID
	public List<MediumIdentifierRange> getMediumIdentifierRangesByMediumTypeID(@Valid Operator operator,
			@NotNull @Min(1) Integer mediumTypeID) throws Exception {
		List<MediumIdentifierRange> result = null;
		Operator storedOperator = null;
		MediumType storedMediumType = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights",
					Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		storedMediumType = this.mediumTypeService.getMediumTypesByID(operator, mediumTypeID);

		result = this.mediumIdentifierRangeDAO.getMediumIdentifierRangesByMediumTypeID(mediumTypeID);

		return result;
	}

	// ritorna tutte le spezzate per una specifica organizzazione
	// Da gesdire meglio la doppia chiamata alla getOperator una direttamente nel
	// metodo una all'interno del metodo organizationService.getOrganizationByID
	public List<MediumIdentifierRange> getMediumIdentifierRangesByOrganizationID(@Valid Operator operator,
																				 @NotNull @Min(1) Integer organizationID) throws Exception {
		List<MediumIdentifierRange> result = null;
		Operator storedOperator = null;
		Organization storedOrganization = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights",
					Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		storedOrganization = this.organizationService.getOrganizationByID(storedOperator, organizationID);

		result = this.mediumIdentifierRangeDAO.getMediumIdentifierRangesByOrganizationID(organizationID);

		return result;
	}

	// verifica la presenza di spezzate per l'intervallo selezionato
	// se è false non ci sono
	// true altrimenti
	public boolean checkOverlayMediumIdentifierRange(@Valid @NotNull MediumIdentifierRange mediumIdentifierRange) throws MAIGeSDiException {
		boolean result = false;
		List<MediumIdentifierRange> listMediumIdentifierRange = null;

		listMediumIdentifierRange = this.mediumIdentifierRangeDAO.checkOverlayMediumIdentifierRange(mediumIdentifierRange);

		if (listMediumIdentifierRange != null && listMediumIdentifierRange.size() > 0)
			result = true;

		return result;
	}
	
	/**
	 * Verifica la presenza di spezzate per il tipo supporto passato e nel caso positivo mette un lock su quelle
	 * @param mediumIdentifierRange
	 * @return
	 * @throws MAIGeSDiException
	 */
	public boolean selectAndLockMediumIdentifierRange(@NotNull Integer mediumTypeID) throws MAIGeSDiException {
		boolean result = false;
		List<MediumIdentifierRange> listMediumIdentifierRange = null;

		listMediumIdentifierRange = this.mediumIdentifierRangeDAO.selectAndLockMediumIdentifierRangeByMediumTypeID(mediumTypeID);

		if (listMediumIdentifierRange != null && listMediumIdentifierRange.size() > 0)
			result = true;

		return result;
	}

	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public MediumIdentifierRange addMediumIdentifierRange(@Valid Operator operator,
														   @Valid MediumIdentifierRange mediumIdentifierRange) throws Exception {

		MediumIdentifierRange result = null;
		Operator storedOperator = null;
		MediumType storedMediumType = null;
		Integer insermediumResult = 0;
		String organizationCode;
//		List<MediumIdentifierRange> lockedRange;
		String luhnCodeFormula;
		
		String mediumIdentifier;
		String lisTicketCode;
		String typographicNumber;
				
		Integer insertMediumIdentifierRangeResult = 0;
		
		Integer startRange = new Integer(mediumIdentifierRange.getStart());
		Integer endRange = new Integer(mediumIdentifierRange.getEnd());
		
		Integer mediumNumber = endRange - startRange +1;
		
		storedOperator = this.operatorService.getOperator(operator);
		
		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		
		storedMediumType =  this.mediumTypeService.getMediumTypesByID(storedOperator, mediumIdentifierRange.getMediumType().getID());
		
		if(storedMediumType == null)
			throw new ForbiddenException("Medium type not exist", Utility.ErrorType.ET_MEDIUM_TYPE_NOT_FOUND.getValue());
		
		organizationCode = storedMediumType.getOrganizationCode();
		
		//verifico la presenza di medium all'interno del range della spezzata da creare
		if(!checkExistingMediumInMediumIdentifierRange(mediumIdentifierRange, organizationCode)) {
		
			//Gestione della concorrenza per le spezzate
			//verifico la presenza di spezzatate per il tipo supporto configurato
			if(this.mediumIdentifierRangeDAO.selectAndLockMediumIdentifierRangeByMediumTypeID(storedMediumType.getID()).size() == 0)
			{
				//se non esiste inserisco la SPEZZATASUPPORTI e poi blocco la spezzata creata
				insertMediumIdentifierRangeResult = this.mediumIdentifierRangeDAO.insertMediumIdentifierRange(mediumIdentifierRange);
				this.mediumIdentifierRangeDAO.selectAndLockMediumIdentifierRangeByMediumTypeID(storedMediumType.getID());
				
				//devo effettuare un controllo per capire se ci sono due transazioni parallele che ricadono in questo caso 
				//e che creano spezzate con intersezione maggiore di zero
				if(checkParallelOverlayMediumIdentifierRange(insertMediumIdentifierRangeResult, mediumIdentifierRange))
					throw new ConflictException("Medium Identifier Range alredy used, change range", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_ALREDY_USED.getValue());
				
			}else {
			
				//altrimenti il controllo blocca le spezzate per quel tipo supporto e posso effettuare i controlli
				//verifico la presenza di spezzate con intersezione diverssa da zero
				if(!checkOverlayMediumIdentifierRange(mediumIdentifierRange)){
	
					//inserisco la spezzata nella tabella SPEZZATASUPPORTI
					insertMediumIdentifierRangeResult = this.mediumIdentifierRangeDAO.insertMediumIdentifierRange(mediumIdentifierRange);
				}else {
					throw new ConflictException("Medium Identifier Range alredy used, change range", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_ALREDY_USED.getValue());
				}
			}
		}else {
			throw new ConflictException("There are medium in this Medium Identifier Range, change range", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_EXISTING_MEDIUM_IN_RANGE.getValue());
			
		}
			
		//SE la spezzata è stata creata nella tabella SPEZZATASUPPORTI procedo alla creazione dei medium della spezzata (questo controllo in teoria è inutile, mi sembra corretto farlo ugualemente)
		if(insertMediumIdentifierRangeResult>0) {
			
			luhnCodeFormula = UtilityService.isNumeric(organizationCode)? organizationCode : UtilityService.getDecodificaPerFormulaLuhn(organizationCode);
			//lista delle spezzate da inserire (spezzate intese come lista di medium vuoti)
			List<EmptyMediumForMediumIdentifierRange> listEmptyMedium = new ArrayList<EmptyMediumForMediumIdentifierRange>();
			EmptyMediumForMediumIdentifierRange emptyMedium;
			for(int i = 0; i < mediumNumber; i ++) {
				
				emptyMedium = new EmptyMediumForMediumIdentifierRange();
				emptyMedium.setMediumTypeID(storedMediumType.getID());
				
                typographicNumber = ("00000000" + String.valueOf(startRange + i));
                typographicNumber = typographicNumber.substring(typographicNumber.length()-8, typographicNumber.length());
                
                emptyMedium.setTypographicNumber(typographicNumber);
                emptyMedium.setMediumIdentifier(organizationCode + typographicNumber + UtilityService.getCheckDigit_LuhnFormula(luhnCodeFormula + typographicNumber));
                emptyMedium.setLisTicketCode(UtilityService.generaCodice());
               //emptyMedium.setMediumID(this.mediumIdentifierRangeDAO.reserveMediumIDFromSequence());
                
                listEmptyMedium.add(emptyMedium);
//                insermediumResult = this.mediumIdentifierRangeDAO.insertMediumOfMediumIdentifierRange(storedMediumType.getID(),
//                																						typographicNumber,
//                																						lisTicketCode,
//                																						mediumIdentifier);
       		
			}
			
			this.mediumIdentifierRangeDAO.insertAllMediumOfMediumIdentifierRange(listEmptyMedium);
			
			result = mediumIdentifierRangeDAO.getMediumIdentifierRangeByID(insertMediumIdentifierRangeResult);
		
		}else {
			throw new MAIGeSDiException("Insert medium identifier range problem, no SPEZZATASUPPORTI created", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_NOT_CREATED.getValue());
		}
		
		
		
		//result = this.mediumIdentifierRangeDAO.addMediumIdentifierRange();
		
		return result;
		
		
	}

	

	private boolean checkParallelOverlayMediumIdentifierRange(Integer mediumIdentifierRangeResultID,
															  @Valid MediumIdentifierRange mediumIdentifierRange) throws DBMSFaultException {
		boolean result = false;
		List<MediumIdentifierRange> listMediumIdentifierRange = null;

		listMediumIdentifierRange = this.mediumIdentifierRangeDAO.checkParallelOverlayMediumIdentifierRange(mediumIdentifierRangeResultID,mediumIdentifierRange);

		if (listMediumIdentifierRange != null && listMediumIdentifierRange.size() > 0)
			result = true;

		return result;
	}

	private boolean checkExistingMediumInMediumIdentifierRange(MediumIdentifierRange mediumIdentifierRange, String organizationCode) throws DBMSFaultException {
		boolean result = false;
		Integer mediumNumber = null;
		String startMediumIdentifier;
		String startTypographicNumber;
		String endMediumIdentifier;
		String endTypographicNumber;
		
		startTypographicNumber = "00000000" + mediumIdentifierRange.getStart();
		startTypographicNumber = startTypographicNumber.substring(startTypographicNumber.length()-8, startTypographicNumber.length());
		startMediumIdentifier = organizationCode + startTypographicNumber + "0";
		
		endTypographicNumber = "00000000" +  mediumIdentifierRange.getEnd();
		endTypographicNumber = endTypographicNumber.substring(endTypographicNumber.length()-8, endTypographicNumber.length());
		endMediumIdentifier = organizationCode + endTypographicNumber + "9";
		
		mediumIdentifierRange.setStartMediumIdentifier(startMediumIdentifier);
		mediumIdentifierRange.setEndMediumIdentifier(endMediumIdentifier);

		mediumNumber = this.mediumIdentifierRangeDAO.checkExistingMediumInMediumIdentifierRange(mediumIdentifierRange.getMediumType().getID(), startMediumIdentifier, endMediumIdentifier);

		if (mediumNumber != null && mediumNumber > 0)
			result = true;

		return result;
	}

	public MediumIdentifierRange getMediumIdentifierRangeByID(@Valid Operator operator, 
			 												  @NotNull @Min(1) Integer mediumIdentifierRangeID) throws MAIGeSDiException {
		MediumIdentifierRange result = null;
		Operator storedOperator = null;
		Organization storedOrganization = null;

		storedOperator = this.operatorService.getOperator(operator);

		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue()))
			throw new UnauthorizedException("Insufficient operator rights",	Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());

		result = this.mediumIdentifierRangeDAO.getMediumIdentifierRangeByID(mediumIdentifierRangeID);
		
		if (result == null)
			throw new ResourceNotFoundException("Medium Identifier Range not found", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_NOT_FOUND.getValue());

		return result;
	}

	public void checkExisitingValidMediumIdentifierRange(Integer mediumTypeID) throws MAIGeSDiException {
		
		//verifico se esiste una spezzata configurata
		if(this.mediumIdentifierRangeDAO.checkExistingMediumIdentifierRange(mediumTypeID)){
			//verifico se c'è una spezazta da riservare
			if(!this.mediumIdentifierRangeDAO.checkAvaibilityMediumIdentifierRange(mediumTypeID))
				throw new ForbiddenException("Medium identifier range terminated, configure another one for this specific medium type", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_TERMINATED.getValue());
			
		}else {
			throw new ForbiddenException("There is no medium identifier range configurated for specific medium type", Utility.ErrorType.ET_MEDIUM_IDENTIFIER_RANGE_NOT_CONFIGURATED.getValue());
		}
	}

	public ReservedMedium getNextMediumFromMediumIdentifierRange(Operator operator, Integer mediumTypeID, Integer operativeFlowID, Integer stateID) throws MAIGeSDiException {
		
		ReservedMedium result = this.mediumIdentifierRangeDAO.getNextMediumIdentifierRange(operator.getID(), mediumTypeID, operativeFlowID, stateID);
		return result;
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String testSelectForUpdate(Operator operator) throws DBMSFaultException {
		String i = this.mediumIdentifierRangeDAO.getNextMediumIdentifierRange(241, 1883,Utility.TSFO_VERIFICA_PRESSO_AUTHORITY, Utility.TSSD_IN_LAVORAZIONE).getMediumIdentifier();
		return i;
	}

	/**
	 * Pulisce il medium precedentemente riservato e lo reinserisce nella spezzata
	 * @param mediumID
	 * @throws DBMSFaultException
	 * 
	 */
	public void clearReservedMedium(Integer mediumID) throws DBMSFaultException {
		
		this.mediumIdentifierRangeDAO.clearReservedMedium(mediumID, UtilityService.generaCodice());
		
		
	}

	//esiste una spezzata che comprende l'identificativo fornito
	public boolean checkExistingMediumIdentifierRangeForMediumIdentifier(Integer mediumTypeID, String mediumIdentifier) throws MAIGeSDiException {
		
		//devo dare qua
		boolean result = false;
		MediumIdentifierRange storedMediumIdentifierRange;
				
		storedMediumIdentifierRange = this.mediumIdentifierRangeDAO.getMediumIdentifierRangeByMediumIdentifier(mediumTypeID, mediumIdentifier);
		
		if(storedMediumIdentifierRange != null && storedMediumIdentifierRange.getID() != null && storedMediumIdentifierRange.getID() > 0)
			result = true;
		
		return result;
	}



}
