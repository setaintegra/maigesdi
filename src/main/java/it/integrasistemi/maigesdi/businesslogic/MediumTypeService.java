package it.integrasistemi.maigesdi.businesslogic;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Organization;
import it.integrasistemi.maigesdi.bean.Operator.Profile;
import it.integrasistemi.maigesdi.bean.mybatis.ParameterQueryGetMediumTypeByID;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.MediumTypeDAO;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;
import it.integrasistemi.maigesdi.mapper.MAIGESDIMapper;

@Validated
@Service
public class MediumTypeService {

	@Autowired
	private OperatorService operatorService;
	
	@Autowired
	private MediumTypeDAO mediumTypeDao;

	public List<MediumType> getMediumTypes(@Valid Operator operator) throws Exception {
		List<MediumType> result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
		if (storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) {
			result = this.mediumTypeDao.getMediumTypeForConfigurator();
		} else {
			result = this.mediumTypeDao.getMediumType(storedOperator.getID());
		}

		return result;
	}

	public MediumType getMediumTypesByID(@Valid Operator operator, 
										@NotNull(message = "MediumType ID is required") 
										@Min(value = 1, message = "MediumType ID must be grather than zero")
										Integer id) throws Exception {
		MediumType result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
		if (storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) {
			result = this.mediumTypeDao.getMediumTypeByIDForConfigurator(id);
		} else {
			result = this.mediumTypeDao.getMediumTypeByID(id, storedOperator.getID());
		}
		
		if(result == null)
			throw new ResourceNotFoundException("Medium type not found", Utility.ErrorType.ET_MEDIUM_TYPE_NOT_FOUND.getValue());

		return result;
	}
	
	
	
	//@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public MediumType addMediumType(@Valid Operator operator, 
									@Valid MediumType mediumType) throws Exception {
		MediumType result = null;
		Integer mediumTypeID  = 0;
		
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) 
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
				
		mediumTypeID = this.mediumTypeDao.addMediumType(mediumType);
	
		result = this.mediumTypeDao.getMediumTypeByIDForConfigurator(mediumTypeID);
		
		return result;
	}

	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public MediumType updateMediumType(@Valid Operator operator, @Valid MediumType mediumType) throws Exception {
		MediumType result=null;
		Integer mediumTypeID  =0;
		
		if(mediumType.getID() == null || mediumType.getID() == 0 )
			throw new PreconditionException("MediumType ID is required; must be grather than zero", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
		
		Operator storedOperator = null;
		
		storedOperator = this.operatorService.getOperator(operator);

		
		if (!storedOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) 
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
					
		mediumTypeID = this.mediumTypeDao.updateMediumType(mediumType);
	
		result = this.mediumTypeDao.getMediumTypeByIDForConfigurator(mediumTypeID);
		
		return result;
	}

	public Activity getParameterListByMediumTypeIDActivityID(MediumPost mediumPost) throws MAIGeSDiException {
		return this.mediumTypeDao.getParameterListByMediumTypeIDActivityID(mediumPost);
	
	}

	public List<MediumType> getMediumTypesByMediumIdentifier(@Valid Operator operator,	
															 @NotNull(message="MediumIdentifier is required") 
															 @NotBlank(message="MediumIdentifier must be not blank")
															 String mediumIdentifier) throws Exception {
		List<MediumType> result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
		result = this.mediumTypeDao.getMediumTypeByMediumIdentifier(storedOperator.getID(), mediumIdentifier);
		
		return result;
	}

	public MediumType getMediumTypesByCodes(@Valid Operator operator,
												  @NotNull(message="organizationCode is required") 
                                                  @NotBlank(message="organizationCode must be not blank")
												  String organizationCode, 
												  @NotNull(message="seriesCode is required") 
                                                  @NotBlank(message="seriesCode must be not blank")
												  String seriesCode) throws MAIGeSDiException {
		MediumType result = null;
		Operator storedOperator = null;

		storedOperator = this.operatorService.getOperator(operator);
		
		result = this.mediumTypeDao.getMediumTypeByCodes(storedOperator.getID(), organizationCode, seriesCode);
		
		return result;
	}

	/**
	 * Reperisce il numero di giorni di validità di uno specifico tiposupportodigitale.
	 * Non è gestito l'operatore, ossia non si verificano i diritti dell'operatore sul tiposupporto,
	 * questo è un metodo di servizio che non viene esposto all'esterno, la sua chiamata dovrà avvenire 
	 * sempre a posteriori della verifica dei diritti dell'operatore sul tipo supporto 
	 * @param mediumTypeID id del tipo supporto dove leggere il numero giorni di validità
	 * @return il numero di giorni di validità
	 * @throws MAIGeSDiException 
	 */
	public Integer getMediumTypeDayOfValidity(Integer mediumTypeID) throws MAIGeSDiException {
		Integer result;
		result = this.mediumTypeDao.getMediumTypeDayOfValidity(mediumTypeID);
		
		return result;
	}
	

}
