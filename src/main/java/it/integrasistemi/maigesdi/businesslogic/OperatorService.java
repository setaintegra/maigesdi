package it.integrasistemi.maigesdi.businesslogic;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.ibatis.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Operator.Profile;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.dao.OperatorDAO;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.exception.PreconditionException;
import it.integrasistemi.maigesdi.exception.ResourceNotFoundException;
import it.integrasistemi.maigesdi.exception.UnauthorizedException;

@Validated
@Service
public class OperatorService {

	@Autowired
	private OperatorDAO operatorDAO;

	private static final Logger logger = LoggerFactory.getLogger(OperatorService.class);

	
	public Operator getOperator(@Valid Operator operator) throws MAIGeSDiException {
		Operator result = null;

		result = operatorDAO.getOperator(operator);
		
		if (result == null)
			throw new ResourceNotFoundException("Operator not found", Utility.ErrorType.ET_OPERATOR_NOT_FOUND.getValue());

		logger.info("Operator correctly logged in: "+ result.toString());
		return result;
	}

	public List<Operator> getOperators(@Valid Operator operator) throws Exception {
		List<Operator> result = null;
		// l'operatore con cui viene effettuata la funzionalità
		Operator currentStoredOperator = null;

		currentStoredOperator = this.getOperator(operator);

		if (currentStoredOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) {
			result = this.operatorDAO.getOperators();
		} else {
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		}

		return result;
	}

	public Operator getOperatorByID(@Valid Operator operator, 
									@NotNull(message = "Operator ID is required; must be grather than zero") 
									@Min(value = 1, message = "Operator ID must be grather than zero")
									Integer id) throws Exception {
		Operator result = null;
		
		// l'operatore con cui viene effettuata la funzionalità
		Operator currentStoredOperator = null;

		currentStoredOperator = this.getOperator(operator);

		if (currentStoredOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) {
			result = this.operatorDAO.getOperatorByID(id);
		} else {
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		}
		
		if(result == null)
			throw new ResourceNotFoundException("Operator not found", Utility.ErrorType.ET_OPERATOR_NOT_FOUND.getValue());

		return result;
	}

	//@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public Operator addOperator(@Valid Operator currentOperator, @Valid Operator operator) throws Exception {
		Integer operatorID =0;
		Operator result =null;
		
		// l'operatore con cui viene effettuata la funzionalità
		Operator currentStoredOperator = null;

		currentStoredOperator = this.getOperator(currentOperator);
		
		if (currentStoredOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) {
			operatorID = this.operatorDAO.insertOpearator(operator);
			
			result = this.operatorDAO.getOperatorByID(operatorID);
			
		} else {
			throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
		}
		
		return result;
	}
	
	//@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
	public Operator updateOperator(@Valid Operator currentOperator, @Valid Operator operator) throws Exception {
		Operator result =null;
		Integer operatorID = 0;
		
		if(operator.getID()!= null && operator.getID()>0) {
			// l'operatore con cui viene effettuata la funzionalità
			Operator currentStoredOperator = null;
	
			currentStoredOperator = this.getOperator(currentOperator);
			
			if (currentStoredOperator.getProfileID().equals(Profile.CONFIGURATOR.getValue())) {
				operatorID = this.operatorDAO.updateOpearator(operator);
				
				result = this.operatorDAO.getOperatorByID(operatorID);
				
			} else {
				throw new UnauthorizedException("Insufficient operator rights", Utility.ErrorType.ET_OPERATOR_NOT_AUTHORIZED.getValue());
			}
		}else
		{
			throw new PreconditionException("Operator ID is required", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());
		}
		return result;
	}

	public void changeOperatorPassword(@Valid Operator operator, 
										@NotNull(message="Old password is required") 
										@NotBlank(message = "Old password must be not blank") 
										@Size(min = 5) 
										String oldPassword, 
										@NotNull(message="New password is required") 
										@NotBlank(message = "New password must be not blank")  
										@Size(min = 5) 
										String newPassword) throws Exception {
		
				
		if(oldPassword.equals(newPassword))                                                                                                   
			throw new PreconditionException("Invalid Operator's password; Operator's new and old password must be different to each other", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());  
		
		// l'operatore con cui viene effettuata la funzionalità
		Operator currentStoredOperator = null;

		currentStoredOperator = this.getOperator(operator);
				
		this.operatorDAO.changeOperatorPassword(currentStoredOperator, oldPassword, newPassword);
				
		
		
	}

	public boolean checkOperatorMediumTypeRights(	@Valid Operator operator, 
													@NotNull(message = "mediumTypeID is required; must be grather than zero") 
													@Min(value = 1, message = "mediumTypeID must be grather than zero")
													Integer mediumTypeID) throws MAIGeSDiException {
		

		return this.operatorDAO.checkOperatorMediumTypeRights (operator, mediumTypeID);
	}



}
