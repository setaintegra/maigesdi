package it.integrasistemi.maigesdi.test;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import it.integrasistemi.maigesdi.bean.Credential;
import it.integrasistemi.maigesdi.bean.CredentialType;
import it.integrasistemi.maigesdi.bean.Document;
import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.Place;
import it.integrasistemi.maigesdi.bean.Tutor;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.controller.MediumController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MediumTests {

	
	@Test
	public void emptyTest() {
		assertEquals(true, true);
		
	}

}
