package it.integrasistemi.maigesdi.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertNull;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.HttpStatusCodeException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bsh.UtilTargetError;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.ExternalSystemCheckingResult;
import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumReportContainer;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Person;
import it.integrasistemi.maigesdi.bean.PersonValidityBody;
import it.integrasistemi.maigesdi.bean.Version;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.controller.MediumController;
import it.integrasistemi.maigesdi.controller.MediumIdentifierRangeController;
import it.integrasistemi.maigesdi.controller.MediumTypeController;
import it.integrasistemi.maigesdi.controller.OperatorController;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.integrasistemi.maigesdi.dao.MediumIdentifierRangeDAO;
import it.integrasistemi.maigesdi.dao.MediumTypeDAO;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;
import it.integrasistemi.maigesdi.test.util.MAIGeSDiCENSystemTestUtil;
import javassist.bytecode.stackmap.BasicBlock.Catch;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class MAIGeSDiCENSystemTest  extends AbstractTestNGSpringContextTests{
	
	private static final Logger logger = LoggerFactory.getLogger(MAIGeSDiCENSystemTest.class);

	private static final boolean ESEGUI_I_TEST = true;
	
	@Autowired
	private TestBaseUtil testUtil;
	
	@Autowired
	private MediumController mediumController;
	
	@Autowired
	private OperatorController operatorController;
	
	@Autowired
	private MediumDAO mediumDao;

	@Autowired
	private MediumTypeDAO mediumTypeDao;
	
	@Autowired
	private UtilityService utilityService;
		
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private MediumIdentifierRangeDAO mediumIdentifierRangeDAO;
	
	private MediumType mediumTypeCreated;
	private List<Medium> mediumCreated = new ArrayList<Medium>();
	private MediumIdentifierRange mediumIdentifierRangeCreated;
	private Operator operatorUpdated;
	
	
	@BeforeTest
	@Parameters({"username", 
				 "password",
				 "MAIGeSDiCEN.MediumType.OrganizationID" 
				})
	public void checkParameter( String username, 
								String password, 
								Integer organizationID) throws Exception {
		
		if(username == null || username.isEmpty()) {
			logger.error("Parameter username not configurated in TestNG-config.xml");
			throw new Exception("Parameter username not configurated in TestNG-config.xml");
		}
		
		if(password == null || password.isEmpty()) {
			logger.error("Parameter password not configurated in TestNG-config.xml");
			throw new Exception("Parameter password not valorized in TestNG-config.xml");
		}
		if(organizationID == null || organizationID == 0) {
			logger.error("Parameter MAIGeSDiCEN.MediumType.OrganizationID not configurated in TestNG-config.xml");
			throw new Exception("Parameter MAIGeSDiCEN.MediumType.OrganizationID not configurated");
		}		
		
		
	}
	
	//chiamata di test al CEN
	@Test(enabled = ESEGUI_I_TEST)
	@Parameters({"username", "password"})
	public void testMAIPOLICEIsActive(String username, String password) throws Exception {
		//chiamata preliminare al CEN per capire se è attivo, si fa una chiamata a verifica motivi ostativi

		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Chiamata prelimare al CEN per evitare che il primo test vada in errore per inattività di MAIPOLICE");
		logger.info(" ------------------------------------------------------------------------------------");
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems/{id}/authorizations", 2)
				.param("username", username)
				.param("password", password)
				.param("givenName", "Mario")
				.param("familyName","Rossi")
				.param("fiscalCodeBirthPlace", "H501")
				.param("birthDate", "1970-01-01")
				.param("gender", "M")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);

		ResultActions resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Risultato chiamata prelimare al CEN: " + resultActions.andReturn().getResponse());
		logger.info(" ------------------------------------------------------------------------------------");
	}
	
	
	//GENERO IL MEDIUM TYPE
	@Test(enabled = ESEGUI_I_TEST, dependsOnMethods="testMAIPOLICEIsActive")
	@Parameters({"username", 
		 		 "password",
		 		 "MAIGeSDiCEN.MediumType.OrganizationID"})
	public void addMediumTypeTest(String username, 
			  					  String password, 
			  					  Integer organizationID) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumTypeTest");
		logger.info("====================================================================================");

		String inputMediumTypeJSON;
		String resultMediumTypeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		
		MediumType resultMediumType;
		MediumType inputMediumType = testUtil.generateMediumType(Utility.EXTERNAL_SYSTEM_MAIGESDI_CEN, organizationID);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium type for medium handled by MAIGeSDICENSystem");
		logger.info(" Description: Create a new mediumType with its configuration");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MediumType created ");
		logger.info(" Http status code expected:  201");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		inputMediumTypeJSON = this.objectMapper.writeValueAsString(inputMediumType);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/mediumTypes")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(inputMediumTypeJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isCreated());
				
		resultMvc = resultActions.andReturn();
		resultMediumTypeJSON = resultMvc.getResponse().getContentAsString();

		resultMediumType = objectMapper.readValue(resultMediumTypeJSON, MediumType.class);
		
		assertNotNull(resultMediumType);
		assertNotNull(resultMediumType.getID());
		assertThat(resultMediumType.getID() > 0);
		assertEquals(inputMediumType.getName().toUpperCase(), resultMediumType.getName().toUpperCase());
		assertEquals(inputMediumType.getNumberDaysValidity(), resultMediumType.getNumberDaysValidity());
		assertEquals(inputMediumType.getSeriesCode(), resultMediumType.getSeriesCode());
		
		assertNotNull(inputMediumType.getActivity());
		assertEquals(inputMediumType.getActivity().size(), resultMediumType.getActivity().size());	
		
		assertNotNull(inputMediumType.getExternalSystem());
		assertEquals(inputMediumType.getExternalSystem().getID(), resultMediumType.getExternalSystem().getID());	
		
		
		this.mediumTypeCreated = resultMediumType;
		
	}
	
	//AGGIORNO L'UTENTE CONFIGURATO
	@Test(dependsOnMethods="addMediumTypeTest", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
		 		 "password"
				})
	public void updateOperator(String username, 
			  					  String password
							  ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start updateOperator");
		logger.info("====================================================================================");
		
		//ResponseEntity<?> result;
		Operator storeOperator;
		List<Operator> listOperator;
		Operator resultOperator;
		
		String storedOperatorJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultOperatorJSON;
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get all operators");
		logger.info(" Description: get alla operators from system, from this set it choose current operator");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: All Operators returned correctly");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Request operatorController.getOperators");

		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/operators")
				.param("username", username)
				.param("password", password)
				.contentType(MediaType.APPLICATION_JSON);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();
		
		listOperator = Arrays.asList(objectMapper.readValue(resultOperatorJSON, Operator[].class));
		
		assertNotNull(listOperator);
		assertThat(listOperator.size() > 0);
		
		//ricerco l'peratore
		storeOperator = listOperator.stream().filter(x -> x.getUsername().equals(username)).findFirst().get();
		
		assertNotNull(storeOperator);
		assertEquals(username, storeOperator.getUsername());
		
		storeOperator.getMediumTypes().add(new Identity(this.mediumTypeCreated.getID(), null));
		storeOperator.setPassword(password);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update operator rights after medium type creation");
		logger.info(" Description: Add rights to current operator to handle new medium type");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected:  Operator updated");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Request operatorController.updateOperator");
		logger.info("------------------------------------------------------------------------------------");
		
		storedOperatorJSON = this.objectMapper.writeValueAsString(storeOperator);
		
		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/operators")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(storedOperatorJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();

		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);
		
		assertNotNull(resultOperator);
		assertEquals(storeOperator.getID(), resultOperator.getID());
		assertEquals(storeOperator.getUsername(), resultOperator.getUsername());
		assertEquals(storeOperator.getMediumTypes().size(), resultOperator.getMediumTypes().size());
		assertEquals(storeOperator.getProfileID(), resultOperator.getProfileID());
		
		resultOperator.setPassword(password);
		this.operatorUpdated = resultOperator;
		
	}
	
	
	
	//GENERO LA SPEZZATA PER questo tiposupporto
	
	//Creo la spezzata per il tiposupporto creato
	@Test(dependsOnMethods="updateOperator", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumIdenfierRange(String username, 
									   String password
									  ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumIdenfierRange");
		logger.info("====================================================================================");
		
		//ResponseEntity<?> result;

		String mediumIdentifierRangeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		MediumIdentifierRange mediumIdentifierRange = new MediumIdentifierRange();
		
		MediumIdentifierRange resultMediumIdentifierRange;
		
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
	
		mediumIdentifierRange.setMediumType(new Identity(this.mediumTypeCreated.getID(), null));
		mediumIdentifierRange.setStart("99999996");
		mediumIdentifierRange.setEnd("99999998");
		mediumIdentifierRange.setOrganization(new Identity(this.mediumTypeCreated.getOrganizationID(), null));

		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Generate and insert a MediumIdentifierRange for next test");
		logger.info(" Description: Generate a Medium identifier range for medium type created in previously test");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MeediumIdentifierRange created  ");
		logger.info(" Http status code expected:  201 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumIdentifierRangeJSON = this.objectMapper.writeValueAsString(mediumIdentifierRange);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/mediumIdentifierRanges")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumIdentifierRangeJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isCreated());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();

		resultMediumIdentifierRange = objectMapper.readValue(resultJSON, MediumIdentifierRange.class);
		
		assertNotNull(resultMediumIdentifierRange);
		assertNotNull(resultMediumIdentifierRange.getID());
		assertThat(resultMediumIdentifierRange.getID() > 0);
		
		assertEquals(mediumIdentifierRange.getMediumType().getID() ,resultMediumIdentifierRange.getMediumType().getID());
		assertEquals(mediumIdentifierRange.getOrganization().getID() ,resultMediumIdentifierRange.getOrganization().getID());
		assertEquals(mediumIdentifierRange.getStart() ,resultMediumIdentifierRange.getStart());
		assertEquals(mediumIdentifierRange.getEnd() ,resultMediumIdentifierRange.getEnd());
		
		this.mediumIdentifierRangeCreated = resultMediumIdentifierRange;
		
	}
		
	
	//CREO I MEDIUM
	
	//Creo il medium generando l'identificativo (versione proprietario maggiorenne)
	@Test(dependsOnMethods="addMediumIdenfierRange", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumPhisicalAdultPersonWithMediumIdenfier(String username, 
														  String password
														 ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumPhisicalPersonWithMediumIdenfier adult version");
		logger.info("====================================================================================");

		Medium mediumResult;
		List<Medium> listMediumsResult;
	
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		MediumPost mediumPost = new MediumPost();
		mediumPost.setActivityCode("NEW_PHISICAL_PERSON");
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium with mediumIdentifier generated");
		logger.info(" Description: create a new medium of the medium type previously created, with a identifier generate");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		
		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		MAIGeSDiCENSystemTestUtil.compareMediumsDataWithoutIdentifier(mediumResult, medium);
		
		assertEquals(Utility.TSSD_ATTIVO, mediumResult.getState().getID().intValue());
		assertEquals(Utility.TSFO_SUPPORTO_PRODOTTO, mediumResult.getOperativeFlow().getID().intValue());
					
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get photo of the medium previously created");
		logger.info(" Description: get the photo provided in the preoiously test, check if this photo is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the photo associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/photo/{mediumID}", mediumResult.getID())
												.param("username", username)
												.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String foto = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getPhoto(), foto);

		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get sign of the medium previously created");
		logger.info(" Description: get the sign provided in the preoiously test, check if this sign is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the sign associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/sign/{mediumID}", mediumResult.getID())
											.param("username", username)
											.param("password", password);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String sign = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getSign(), sign);

		if (medium.getTutor() != null) {
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: get tutor sign of the medium previously created");
			logger.info(" Description: get the tutor sign provided in the preoiously test, check if this tutor sign is equal than the provided one ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: the tutor sign associated with the medium created ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info(" ------------------------------------------------------------------------------------");
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/tutorsign/{mediumID}", mediumResult.getID())
												.param("username", username)
												.param("password", password);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
			
			resultMvc = resultActions.andReturn();
			
			String tutorSign = resultMvc.getResponse().getContentAsString();
			
			assertEquals(medium.getTutor().getSign(), tutorSign);
		}
	}
	
	//Creo il medium generando l'identificativo (versione proprietario minorenne)
	@Test(dependsOnMethods="addMediumPhisicalAdultPersonWithMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumPhisicalMinorPersonWithMediumIdenfier(String username, 
														  String password
														 ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumPhisicalPersonWithMediumIdenfier minor version");
		logger.info("====================================================================================");

		Medium mediumResult;
		List<Medium> listMediumsResult;
	
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		Medium personValidityResult;
		
		MediumPost mediumPost = new MediumPost();
		mediumPost.setActivityCode("NEW_PHISICAL_PERSON");
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		PersonValidityBody personValidityBody = new PersonValidityBody();
		personValidityBody.setMediumTypeID(this.mediumTypeCreated.getID());
		personValidityBody.setPersonData(new Person(medium.getGivenName(), 
													medium.getFamilyName(), 
													medium.getBirthPlace().getFiscalCode(), 
													medium.getBirthDate(), 
													medium.getGender())	);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: verifica possibilità creare un nuovo medium per la quintupla generata");
		logger.info(" Description: si contatta il sevizio persons/validity per verificare se la quintupla generata nel medium sia valida");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: person valid ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		resultActions = MAIGeSDiCENSystemTestUtil.chiamataMediumPersonsValidity(objectMapper, mockMvc, username, password, personValidityBody);
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		assertThat(resultJSON.isEmpty());

		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium with mediumIdentifier generated");
		logger.info(" Description: create a new medium of the medium type previously created, with a identifier generate");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		
		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		assertNotNull(mediumResult);
		assertNotNull(mediumResult.getID());
		assertTrue(mediumResult.getID()>0);
		
		this.mediumCreated.add(mediumResult);
		MAIGeSDiCENSystemTestUtil.compareMediumsDataWithoutIdentifier(mediumResult, medium);
		
		assertEquals(Utility.TSSD_ATTIVO, mediumResult.getState().getID().intValue());
		assertEquals(Utility.TSFO_SUPPORTO_PRODOTTO, mediumResult.getOperativeFlow().getID().intValue());
					
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get photo of the medium previously created");
		logger.info(" Description: get the photo provided in the preoiously test, check if this photo is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the photo associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/photo/{mediumID}", mediumResult.getID())
												.param("username", username)
												.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String foto = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getPhoto(), foto);

		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get sign of the medium previously created");
		logger.info(" Description: get the sign provided in the preoiously test, check if this sign is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the sign associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/sign/{mediumID}", mediumResult.getID())
											.param("username", username)
											.param("password", password);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String sign = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getSign(), sign);

		if (medium.getTutor() != null) {
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: get tutor sign of the medium previously created");
			logger.info(" Description: get the tutor sign provided in the preoiously test, check if this tutor sign is equal than the provided one ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: the tutor sign associated with the medium created ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info(" ------------------------------------------------------------------------------------");
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/tutorsign/{mediumID}", mediumResult.getID())
												.param("username", username)
												.param("password", password);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
			
			resultMvc = resultActions.andReturn();
			
			String tutorSign = resultMvc.getResponse().getContentAsString();
			
			assertEquals(medium.getTutor().getSign(), tutorSign);
		}
		
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: verifica Impossibilita creare un nuovo medium per la quintupla usata per la creazione precendente");
		logger.info(" Description: si contatta il sevizio persons/validity per verificare se la quintupla generata nel medium non sia più valida");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: person not valid, medium just created returned");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		resultActions = MAIGeSDiCENSystemTestUtil.chiamataMediumPersonsValidity(objectMapper, mockMvc, username, password, personValidityBody);
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		//assertThat(resultJSON.isEmpty());
		personValidityResult = objectMapper.readValue(resultJSON, Medium.class);
		
		assertNotNull(personValidityResult);
		assertNotNull(personValidityResult.getID());
		assertTrue(personValidityResult.getID()>0);
		MAIGeSDiCENSystemTestUtil.compareMediumsGeneratedWithIdentifier(personValidityResult, medium);
	
	}
	
	//Creo il medium generando l'identificativo e non valorrizando la data di scadenza 
	@Test(dependsOnMethods="addMediumPhisicalMinorPersonWithMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumPhisicalWhitoutValidToWithMediumIdenfier(String username, 
														  String password
														 ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumPhisicalWhitoutValidToWithMediumIdenfier");
		logger.info("====================================================================================");

		Medium mediumResult;
		List<Medium> listMediumsResult;
	
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		MediumPost mediumPost = new MediumPost();
		mediumPost.setActivityCode("NEW_PHISICAL_PERSON");
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		medium.setValidTo(null);
		Date aspectedValidTo = this.testUtil.addDaysToToday(this.mediumTypeCreated.getNumberDaysValidity());
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium with mediumIdentifier generated and without validTo parameter");
		logger.info(" Description: create a new medium of the medium type previously created, with a identifier generated and without validTo");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created with validTo valorized");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		
		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		assertNotNull(mediumResult);
		assertNotNull(mediumResult.getID());
		assertTrue(mediumResult.getID()>0);
		
		this.mediumCreated.add(mediumResult);
		//compareMediumsDataWithoutIdentifier(mediumResult, medium);
		
		
		assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
		assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
		assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
		assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
		assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
		assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
		//assertEquals(medium.getBirthDate() 					, mediumResult.getBirthDate() );
		assertEquals(medium.getGender() 					, mediumResult.getGender() );
		assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
		assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
		assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
		assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
		
		assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
		assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
		assertThat(mediumResult.getValidTo().compareTo( aspectedValidTo));
		
		assertEquals(medium.getPhoneNumber()				, mediumResult.getPhoneNumber());
		assertEquals(medium.getMobileNumber()				, mediumResult.getMobileNumber());
		assertEquals(medium.getDenomination() 				, mediumResult.getDenomination());
		assertEquals(medium.geteMail()		 				, mediumResult.geteMail());
		
		if(medium.getDocument() != null) {
			assertEquals(medium.getDocument().getDocumentType() , mediumResult.getDocument().getDocumentType());
			assertEquals(medium.getDocument().getAuthority() , mediumResult.getDocument().getAuthority());
			assertEquals(medium.getDocument().getCode() , mediumResult.getDocument().getCode());
			assertThat(mediumResult.getDocument().getReleaseDate().compareTo(medium.getDocument().getReleaseDate()));
			assertThat(mediumResult.getDocument().getExpirationDate().compareTo(medium.getDocument().getExpirationDate()));
		}
		
		if(medium.getTutor() != null) {
			assertEquals(medium.getTutor().getGivenName()   , mediumResult.getTutor().getGivenName() );
			assertEquals(medium.getTutor().getFamilyName()  , mediumResult.getTutor().getFamilyName() );
			assertEquals(medium.getTutor().getFiscalCode() 	, mediumResult.getTutor().getFiscalCode() );
			assertThat(mediumResult.getTutor().getBirthDate().compareTo(medium.getTutor().getBirthDate()));
			assertEquals(medium.getTutor().getBirthPlace(), mediumResult.getTutor().getBirthPlace());
			assertEquals(medium.getTutor().getAddress1()    , mediumResult.getTutor().getAddress1() );
			assertEquals(medium.getTutor().getAddress2()    , mediumResult.getTutor().getAddress2() );
			assertEquals(medium.getTutor().getAddress3()    , mediumResult.getTutor().getAddress3()  );
			
			if(medium.getTutor().getDocument() != null) {
				assertEquals(medium.getTutor().getDocument().getDocumentType() , mediumResult.getTutor().getDocument().getDocumentType());
				assertEquals(medium.getTutor().getDocument().getAuthority() , mediumResult.getTutor().getDocument().getAuthority());
				assertEquals(medium.getTutor().getDocument().getCode() , mediumResult.getTutor().getDocument().getCode());
				assertThat(mediumResult.getTutor().getDocument().getReleaseDate().compareTo(medium.getTutor().getDocument().getReleaseDate()));
				assertThat(mediumResult.getTutor().getDocument().getExpirationDate().compareTo(medium.getTutor().getDocument().getExpirationDate()));
			}
						
		}
		
		assertEquals(Utility.TSSD_ATTIVO, mediumResult.getState().getID().intValue());
		assertEquals(Utility.TSFO_SUPPORTO_PRODOTTO, mediumResult.getOperativeFlow().getID().intValue());
					
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get photo of the medium previously created");
		logger.info(" Description: get the photo provided in the preoiously test, check if this photo is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the photo associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/photo/{mediumID}", mediumResult.getID())
												.param("username", username)
												.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String foto = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getPhoto(), foto);

		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get sign of the medium previously created");
		logger.info(" Description: get the sign provided in the preoiously test, check if this sign is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the sign associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/sign/{mediumID}", mediumResult.getID())
											.param("username", username)
											.param("password", password);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String sign = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getSign(), sign);

		if (medium.getTutor() != null) {
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: get tutor sign of the medium previously created");
			logger.info(" Description: get the tutor sign provided in the preoiously test, check if this tutor sign is equal than the provided one ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: the tutor sign associated with the medium created ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info(" ------------------------------------------------------------------------------------");
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/tutorsign/{mediumID}", mediumResult.getID())
												.param("username", username)
												.param("password", password);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
			
			resultMvc = resultActions.andReturn();
			
			String tutorSign = resultMvc.getResponse().getContentAsString();
			
			assertEquals(medium.getTutor().getSign(), tutorSign);
		}
	}
	
	
	
	
	//Creo il medium prendendo il medium dalla spezzata precedentemente creata
	@Test(dependsOnMethods="addMediumPhisicalWhitoutValidToWithMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumPhisicalPersonWithoutMediumIdenfier(String username, 
														  	 String password
														 	) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumPhisicalPersonWithoutMediumIdenfier");
		logger.info("====================================================================================");
		//ResponseEntity<?> result;
		
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
					
		Medium mediumResult;
		List<Medium> listMediumsResult;
		MediumPost mediumPost = new MediumPost();
		 
		//calcolo della prima parte dell'identificativo(tutto tranne il codice di luhn) 
		String typographicNumber =	("00000000" + this.mediumIdentifierRangeCreated.getStart());
        typographicNumber = typographicNumber.substring(typographicNumber.length()-8, typographicNumber.length());;
        String prefixMediumIndetifierExpected = this.mediumTypeCreated.getOrganizationCode()+ typographicNumber;
        
        mediumPost.setActivityCode("NEW_PHISICAL_PERSON");

		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		medium.setMediumIdentifier(null);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium without mediumIdentifier");
		logger.info(" Description: create a new medium of the medium type previously created, by reserving a medium from mediumIdentifierRange previously created");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));

		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		assertNotNull(mediumResult);
		
		assertNotNull(mediumResult.getID());
		assertTrue(mediumResult.getID()>0);
		
		this.mediumCreated.add(mediumResult);
				
		MAIGeSDiCENSystemTestUtil.compareMediumsGeneratedFromRange(prefixMediumIndetifierExpected,mediumResult, medium);
		
		assertEquals(Utility.TSSD_ATTIVO, mediumResult.getState().getID().intValue());
		assertEquals(Utility.TSFO_SUPPORTO_PRODOTTO, mediumResult.getOperativeFlow().getID().intValue());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get photo of the medium previously created");
		logger.info(" Description: get the photo provided in the preoiously test, check if this photo is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the photo associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/photo/{mediumID}", mediumResult.getID())
				.param("username", username)
				.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String foto = resultMvc.getResponse().getContentAsString();
				
		assertEquals(medium.getPhoto(), foto);
	}
	
	//CREO I MEDIUM
	
	//Creo il medium da web (con controllo richiesto e stato finale "in attesa conferma BO") generando l'identificativo
	@Test(dependsOnMethods="addMediumPhisicalPersonWithoutMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumFromWebPhisicalPersonWithMediumIdenfier(String username, 
														  String password
														 ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumFromWebPhisicalPersonWithMediumIdenfier");
		logger.info("====================================================================================");
		//ResponseEntity<?> result;
		
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		Medium mediumResult;
		List<Medium> listMediumsResult;
		MediumPost mediumPost = new MediumPost();
		mediumPost.setActivityCode("NEW_WEB_PHISICAL_PERSON");
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium from WEB with mediumIdentifier generated");
		logger.info(" Description: create a new medium from web of the medium type previously created, with a identifier generate");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		
		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		MAIGeSDiCENSystemTestUtil.compareMediumsDataWithoutIdentifier(mediumResult, medium);
		
		assertEquals(Utility.TSSD_IN_LAVORAZIONE, mediumResult.getState().getID().intValue());
		assertEquals(Utility.TSFO_ATTESA_CONFERMA_BO, mediumResult.getOperativeFlow().getID().intValue());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get photo of the medium previously created");
		logger.info(" Description: get the photo provided in the preoiously test, check if this photo is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the photo associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/photo/{mediumID}", mediumResult.getID())
				.param("username", username)
				.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String foto = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getPhoto(), foto);


	}
	
	//Creo il medium  da web (con controllo richiesto e stato finale "in attesa conferma BO") prendendo il medium dalla spezzata precedentemente creata
	@Test(dependsOnMethods="addMediumFromWebPhisicalPersonWithMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void addMediumFromWebPhisicalPersonWithoutMediumIdenfier(String username, 
														  	 String password
														 	) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumFromWebPhisicalPersonWithoutMediumIdenfier");
		logger.info("====================================================================================");
		
		Medium mediumResult;

		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		List<Medium> listMediumsResult;
		MediumPost mediumPost = new MediumPost();
		 
		//calcolo della prima parte dell'identificativo(tutto tranne il codice di luhn) 
		String typographicNumber =	("00000000" + this.mediumIdentifierRangeCreated.getStart());
	    typographicNumber = typographicNumber.substring(typographicNumber.length()-8, typographicNumber.length());;
	    String prefixMediumIndetifierExpected = this.mediumTypeCreated.getOrganizationCode()+ typographicNumber;
	    
	    mediumPost.setActivityCode("NEW_WEB_PHISICAL_PERSON");

		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		medium.setMediumIdentifier(null);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium, from WEB, without mediumIdentifier");
		logger.info(" Description: create a new medium, from web, of the medium type previously created, by reserving a medium from mediumIdentifierRange previously created");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print()).andDo(log());
		
		//logger.debug(resultActions);
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		
		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		assertNotNull(mediumResult);
		
		assertNotNull(mediumResult.getID());
		assertTrue(mediumResult.getID()>0);
		
		this.mediumCreated.add(mediumResult);
					
		MAIGeSDiCENSystemTestUtil.compareMediumsGeneratedFromRange(prefixMediumIndetifierExpected, mediumResult, medium);
		
		assertEquals(Utility.TSSD_IN_LAVORAZIONE, mediumResult.getState().getID().intValue());
		assertEquals(Utility.TSFO_ATTESA_CONFERMA_BO, mediumResult.getOperativeFlow().getID().intValue());

		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get photo of the medium previously created");
		logger.info(" Description: get the photo provided in the preoiously test, check if this photo is equal than the provided one ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: the photo associated with the medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/photo/{mediumID}", mediumResult.getID())
				.param("username", username)
				.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		String foto = resultMvc.getResponse().getContentAsString();
		
		assertEquals(medium.getPhoto(), foto);
	}
		
		
		@Test(dependsOnMethods="addMediumFromWebPhisicalPersonWithoutMediumIdenfier", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"})
		public void searchMedium(String username, 
			  	 				 String password
			 					) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start searchMedium");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;
			Medium mediumResult;
			Medium inputMedium;
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
		
			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Integer mediumID = this.mediumCreated.get(0).getID();
			
			mediumPost.setActivityCode("SEARCH");
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: GET medium by ID");
			logger.info(" Description: GET medium by ID on MAIGeSDi ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected:  Medium correctly returned");
			logger.info(" Http status code expected:  200");
			logger.info(" MAIGeSDi error code expected: no error ");
		    logger.info("------------------------------------------------------------------------------------");

			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/{mediumID}", mediumID)
					.param("username", username)
					.param("password", password);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();
			mediumResult = objectMapper.readValue(resultJSON, Medium.class);

			
			//ricerca medium a partire dalla sestupletta
			inputMedium = new Medium();
			inputMedium.setGivenName(mediumResult.getGivenName());
			inputMedium.setFamilyName(mediumResult.getFamilyName());
			inputMedium.setBirthDate(mediumResult.getBirthDate());
			inputMedium.setBirthPlace(mediumResult.getBirthPlace());
			inputMedium.setGender(mediumResult.getGender());
			inputMedium.setMediumType(mediumResult.getMediumType());
			inputMedium.setMediumIdentifier(mediumResult.getMediumIdentifier());
			
		    mediumPost.setMedium(inputMedium);
		    
		    logger.info("------------------------------------------------------------------------------------");
		    logger.info(" Test: Search medium by custom filter");
		    logger.info(" Description: Serch the same medium, taken previously, with custom filter like sextain");
		    logger.info(" ------------------------------------------------------------------------------------");
		    logger.info(" Result expected: Medium correctly found");
		    logger.info(" Http status code expected: 200 ");
		    logger.info(" MAIGeSDi error code expected: no error ");
		    logger.info("------------------------------------------------------------------------------------");


		    mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();
			
			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size() >= 1);
		    assertThat(listMediumsResult.stream().anyMatch(t -> t.getID().equals(mediumID)));

		} 
		

		@Test(dependsOnMethods="searchMedium", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"})
		public void verifyMedium(String username, 
			  	 							 String password
			 								) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start verifyMedium");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;
			Medium mediumResult;

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
						
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_ATTIVO).findFirst().get();
			mediumPost.setActivityCode("VERIFY");
			mediumPost.setMedium(medium);
		    
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: verify medium in MAIGeSDi an on CEN ");
			logger.info(" Description: Verify if a specific medium exist in MAIGeSDi e on CEN");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium existing correcttly ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			logger.info("medium: " + medium);
		   
			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));


			assertNotNull(listMediumsResult);
		    assertThat(listMediumsResult.size() >= 1);
		    mediumResult = listMediumsResult.stream().filter(t -> t.getID().equals(medium.getID())).findFirst().get();
		    assertNotNull(mediumResult);
		    assertEquals(medium.getID(), mediumResult.getID());
		    
		    if(utilityService.isDebugMode())
		    	assertThat(Integer.parseInt(Utility.IUA_FITTIZIO) == mediumResult.getAuthorizationID());
		    else {
		    	assertNotEquals(medium.getAuthorizationID(), mediumResult.getAuthorizationID());
		    }


		} 
		
		
		@Test(dependsOnMethods="verifyMedium", enabled = ESEGUI_I_TEST)
		//@Test(dependsOnMethods="searchMedium")
		@Parameters({"username", 
					 "password"
					})
		public void modifyMedium(String username,
								 String password) throws Exception {
	
			logger.info("====================================================================================");
			logger.info(" Start modifyMedium");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;
			ErrorDetails errorResult;
			List<Medium> listMediumsResult;
			Medium mediumResult =null;
			MediumPost mediumPost = new MediumPost();

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_ATTIVO).findFirst().get();
			mediumPost.setActivityCode("MODIFY");
		
			// 1  test di aggiornamento della quintupletta su una TDT
			//risultato atteso: errore
			
			//variabile di appoggio
			String givenName = medium.getGivenName();
			
			medium.setGivenName("brazorf");
				
		    mediumPost.setMedium(medium);
			
		    logger.info("------------------------------------------------------------------------------------");
		    logger.info(" Test: Edit an existing medium handled in MAIGeSDi and on the CEN");
		    logger.info(" Description: Edit one parameter (given name) of a medium sextain, this medium is handled by MAIGeSDi and CEN (TdT) ");
		    logger.info(" ------------------------------------------------------------------------------------");
		    logger.info(" Result expected: Error, it's not allowed change the sextain of a MAIGeSDiCEN medium  ");
		    logger.info(" Http status code expected: 403 ");
		    logger.info(" MAIGeSDi error code expected: 312 ");
		    logger.info("------------------------------------------------------------------------------------");

		    mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isForbidden());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			errorResult = objectMapper.readValue(resultJSON, ErrorDetails.class);

			assertEquals(errorResult.getCode(), Utility.ErrorType.ET_MEDIUM_UPDATING_NOT_ALLOWED_PERSON_DATA_NOT_EDITABLE.getValue());
			
			medium.setGivenName(givenName);
			
			// 2 test di aggiornamento data di scadenza 
			//risultato atteso: errore
			
			//variabile d'appoggio per la data di scadenza
			Date originalValidTo = medium.getValidTo();
			
			medium.setValidTo(TestBaseUtil.getDateFormatted(11, 11, 2050) );
			
			logger.info("------------------------------------------------------------------------------------");
		    logger.info(" Test: Edit an existing medium handled in MAIGeSDi and on the CEN");
		    logger.info(" Description: Edit validity date of a medium is handled by MAIGeSDi and CEN (TdT) ");
		    logger.info(" ------------------------------------------------------------------------------------");
		    logger.info(" Result expected: Error, it's not allowed change validity date of a MAIGeSDiCEN medium  ");
		    logger.info(" Http status code expected: 403 ");
		    logger.info(" MAIGeSDi error code expected: 311 ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isForbidden());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			errorResult = objectMapper.readValue(resultJSON, ErrorDetails.class);

			assertEquals(errorResult.getCode(), Utility.ErrorType.ET_MEDIUM_UPDATING_NOT_ALLOWED_EXPIRATION_DATE_NOT_EDITABLE.getValue());
			
			medium.setValidTo(originalValidTo);
			
			// 3 test modifica dati esterni alla quintupletta
			// risultato atteso: OK
			String randomEMail = "test@test.com";
			String realEMail = medium.geteMail();
			medium.seteMail(randomEMail);
			
			logger.info("------------------------------------------------------------------------------------");
		    logger.info(" Test: Edit an existing medium handled in MAIGeSDi and on the CEN");
		    logger.info(" Description: Edit a parameter that not are in sextain or validity date of a medium that is handled by MAIGeSDi and CEN (TdT)");
		    logger.info(" ------------------------------------------------------------------------------------");
		    logger.info(" Result expected: Medium updated correctly  ");
		    logger.info(" Http status code expected: 200 ");
		    logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			
			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult);
					
			assertEquals(mediumResult.geteMail(), randomEMail);
			
			//RIPRISTINO I VALORI ORIGINALI
			medium.seteMail(realEMail);
			
			logger.info("------------------------------------------------------------------------------------");
		    logger.info(" Test: Edit an existing medium handled in MAIGeSDi and on the CEN");
		    logger.info(" Description: Edit the last edited medium from previously test, it will be restored its original state");
		    logger.info(" ------------------------------------------------------------------------------------");
		    logger.info(" Result expected: Medium updated correctly  ");
		    logger.info(" Http status code expected: 200 ");
		    logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult);
					
			assertEquals(mediumResult.geteMail(), realEMail);
			
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName()					, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			
			assertEquals(medium.getPhoneNumber()				, mediumResult.getPhoneNumber());
			assertEquals(medium.getMobileNumber()				, mediumResult.getMobileNumber());
			assertEquals(medium.getDenomination() 				, mediumResult.getDenomination());
			
			if(medium.getDocument() != null) {
				assertEquals(medium.getDocument().getDocumentType() , mediumResult.getDocument().getDocumentType());
				assertEquals(medium.getDocument().getAuthority() , mediumResult.getDocument().getAuthority());
				assertEquals(medium.getDocument().getCode() , mediumResult.getDocument().getCode());
				assertThat(mediumResult.getDocument().getReleaseDate().compareTo(medium.getDocument().getReleaseDate()));
				assertThat(mediumResult.getDocument().getExpirationDate().compareTo(medium.getDocument().getExpirationDate()));
			}
			
			if(medium.getTutor() != null) {
				assertEquals(medium.getTutor().getGivenName()   , mediumResult.getTutor().getGivenName() );
				assertEquals(medium.getTutor().getFamilyName()  , mediumResult.getTutor().getFamilyName() );
				assertEquals(medium.getTutor().getFiscalCode() 	, mediumResult.getTutor().getFiscalCode() );
				assertThat(mediumResult.getTutor().getBirthDate().compareTo(medium.getTutor().getBirthDate()));
				assertEquals(medium.getTutor().getBirthPlace(), mediumResult.getTutor().getBirthPlace());
				assertEquals(medium.getTutor().getAddress1()    , mediumResult.getTutor().getAddress1() );
				assertEquals(medium.getTutor().getAddress2()    , mediumResult.getTutor().getAddress2() );
				assertEquals(medium.getTutor().getAddress3()    , mediumResult.getTutor().getAddress3()  );
				
				if(medium.getTutor().getDocument() != null) {
					assertEquals(medium.getTutor().getDocument().getDocumentType() , mediumResult.getTutor().getDocument().getDocumentType());
					assertEquals(medium.getTutor().getDocument().getAuthority() , mediumResult.getTutor().getDocument().getAuthority());
					assertEquals(medium.getTutor().getDocument().getCode() , mediumResult.getTutor().getDocument().getCode());
					assertThat(mediumResult.getTutor().getDocument().getReleaseDate().compareTo(medium.getTutor().getDocument().getReleaseDate()));
					assertThat(mediumResult.getTutor().getDocument().getExpirationDate().compareTo(medium.getTutor().getDocument().getExpirationDate()));
				}
							
			}
			
		} 
		
		@Test(dependsOnMethods="modifyMedium", enabled = ESEGUI_I_TEST)
//		@Test(dependsOnMethods="searchMedium")
		@Parameters({"username", 
					 "password"
					})
		public void disableMedium(String username,
								  String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start disableMedium");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;
			
			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Integer disableStateID;
			
			Medium mediumResult;
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_ATTIVO).findFirst().get();
			
			disableStateID = testUtil.getRandomDisablingState(true);
			
			medium.setState(new Identity(disableStateID, null));
			
			mediumPost.setActivityCode("DISABLE");
			
			logger.debug("Medium taken from list of generated: "+medium);
			mediumPost.setMedium(medium);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Disable active medium medium ");
			logger.info(" Description: Disabled a valid medium, this operation will disable medium on CEN and change its state with a random valid one");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium correctly disabled on MAIGeSDi and on CEN");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult);
			
//		
			mediumResult = listMediumsResult.get(0);
			
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			
			this.mediumCreated.removeIf(x -> x.getID() == medium.getID());
			this.mediumCreated.add(mediumResult);
			
			MAIGeSDiCENSystemTestUtil.compareDisabledMedium(disableStateID, mediumResult, medium);
		
		}

		
		
		@Test(dependsOnMethods="disableMedium", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void disableWebMedium(String username,
								  String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start disableWebMedium");
			logger.info("====================================================================================");
			
			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			
			Medium mediumResult;
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			Integer disableStateID;
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_IN_LAVORAZIONE 
																&& x.getOperativeFlow().getID() == Utility.TSFO_ATTESA_CONFERMA_BO).findFirst().get();
			
			logger.debug("Medium taken from list of generated: "+medium);
			disableStateID = testUtil.getRandomDisablingState(true);
			
			medium.setState(new Identity(disableStateID, null));
			
			mediumPost.setActivityCode("DISABLE");
			
			mediumPost.setMedium(medium);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Disable in progress medium ");
			logger.info(" Description: Disabled an in progress medium, with state IN_LAVORAZIONE and operative flow ATTESA_CONFERMA_BO, this operation not go on CEN ");
			logger.info(" 			   the result is a medium with state with a random valid one and Operative flow 'ATTESA_CONFERMA_BO'");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium correctly disabled on MAIGeSDi");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult);
			
//		
			mediumResult = listMediumsResult.get(0);
			
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			
			this.mediumCreated.removeIf(x -> x.getID() == medium.getID());
			this.mediumCreated.add(mediumResult);
			
			MAIGeSDiCENSystemTestUtil.compareDisabledMedium(disableStateID, mediumResult, medium);
			assertThat(Utility.TSFO_ATTESA_CONFERMA_BO == mediumResult.getOperativeFlow().getID());
		} 
		
		@Test(dependsOnMethods="disableWebMedium", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void restoreMedium(String username,
								  String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start restoreMedium");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			
			Medium mediumResult;
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
					
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() >= Utility.TSSD_RINUNCIATO)
													   .filter(x -> x.getOperativeFlow().getID() == Utility.TSFO_SUPPORTO_PRODOTTO).findFirst().get();
			
			mediumPost.setActivityCode("RESTORE");
			
			logger.debug("Medium taken from list of generated: "+medium);
			
			medium.setValidTo(TestBaseUtil.getDateFormatted(23,04,2022) );
			mediumPost.setMedium(medium);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Restore a disabled medium");
			logger.info(" Description: Restore a medium disabled on CEN");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium restored ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");


			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult);
			
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			
			this.mediumCreated.removeIf(x -> x.getID() == medium.getID());
			this.mediumCreated.add(mediumResult);
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			
			assertEquals(medium.getPhoneNumber()				, mediumResult.getPhoneNumber());
			assertEquals(medium.getMobileNumber()				, mediumResult.getMobileNumber());
			assertEquals(medium.getDenomination() 				, mediumResult.getDenomination());
			
			if(medium.getDocument() != null) {
				assertEquals(medium.getDocument().getDocumentType() , mediumResult.getDocument().getDocumentType());
				assertEquals(medium.getDocument().getAuthority() , mediumResult.getDocument().getAuthority());
				assertEquals(medium.getDocument().getCode() , mediumResult.getDocument().getCode());
				assertThat(mediumResult.getDocument().getReleaseDate().compareTo(medium.getDocument().getReleaseDate()));
				assertThat(mediumResult.getDocument().getExpirationDate().compareTo(medium.getDocument().getExpirationDate()));
			}
			
			if(medium.getTutor() != null) {
				assertEquals(medium.getTutor().getGivenName()   , mediumResult.getTutor().getGivenName() );
				assertEquals(medium.getTutor().getFamilyName()  , mediumResult.getTutor().getFamilyName() );
				assertEquals(medium.getTutor().getFiscalCode() 	, mediumResult.getTutor().getFiscalCode() );
				assertThat(mediumResult.getTutor().getBirthDate().compareTo(medium.getTutor().getBirthDate()));
				assertEquals(medium.getTutor().getBirthPlace(), mediumResult.getTutor().getBirthPlace());
				assertEquals(medium.getTutor().getAddress1()    , mediumResult.getTutor().getAddress1() );
				assertEquals(medium.getTutor().getAddress2()    , mediumResult.getTutor().getAddress2() );
				assertEquals(medium.getTutor().getAddress3()    , mediumResult.getTutor().getAddress3()  );
				
				if(medium.getTutor().getDocument() != null) {
					assertEquals(medium.getTutor().getDocument().getDocumentType() , mediumResult.getTutor().getDocument().getDocumentType());
					assertEquals(medium.getTutor().getDocument().getAuthority() , mediumResult.getTutor().getDocument().getAuthority());
					assertEquals(medium.getTutor().getDocument().getCode() , mediumResult.getTutor().getDocument().getCode());
					assertThat(mediumResult.getTutor().getDocument().getReleaseDate().compareTo(medium.getTutor().getDocument().getReleaseDate()));
					assertThat(mediumResult.getTutor().getDocument().getExpirationDate().compareTo(medium.getTutor().getDocument().getExpirationDate()));
				}
							
			}
			
			assertThat(Utility.TSSD_ATTIVO == mediumResult.getState().getID());
			
		} 
		
		
		@Test(dependsOnMethods="restoreMedium", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void activeOnExternalSystemAnExistinMediumAndExternalSystemCheck(String username,
								  						  						String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start activeOnExternalSystemAnExistinMediumAndExternalSystemCheck");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			
			ExternalSystemCheckingResult vroResult;
			Medium mediumResult;
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			Medium medium; 
			Identity stato;
			Identity flusso; 
			Integer risultatoInserimento;
			boolean generaNuovoMedium = true;
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			// TEST CASE
			// 1 genero un medium nuovo
			// 2 verifico che l'anagrafica generata sia valida per il VRO
			// 3 verifico che il suopporto generato sia presente al VRO
			// 3a se non è presente lo creo solo sul db di MAIGeSDi e poi utilizzo l'attivita ACTIVE_ON_EXTERNALSYSTEM
			// 3b altrimenti torno al punto 1
			// 4 una volta terminata l'esecuzione dell'attività ACTIVE_ON_EXTERNALSYSTEM verifico la presenza del medium al VRO
			
			
			while(generaNuovoMedium) {
				// 1 genero un medium nuovo
				medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
				
				stato = new Identity(Utility.TSSD_ATTIVO, null);
				flusso = new Identity(Utility.TSFO_SUPPORTO_PRODOTTO, null);
				medium.setState(stato);
				medium.setOperativeFlow(flusso);
							
				//verifico la validità dell'anagrafica al VRO
				
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Test: Check validity of the person for external system (VRO)");
				logger.info(" Description: Check validity of person data generated for the external system (VRO)");
				logger.info(" ------------------------------------------------------------------------------------");
				logger.info(" Result expected: Person data valid ");
				logger.info(" Http status code expected: 200 ");
				logger.info(" MAIGeSDi error code expected: no error ");
				logger.info("------------------------------------------------------------------------------------");
				
				requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems/{id}/authorizations", this.mediumTypeCreated.getExternalSystem().getID())
						.param("username", username)
						.param("password", password)
						.param("givenName", medium.getGivenName())
						.param("familyName", medium.getFamilyName())
						.param("fiscalCodeBirthPlace", medium.getBirthPlace().getFiscalCode())
						.param("birthDate", TestBaseUtil.convertDateToString(medium.getBirthDate()))
						.param("gender", medium.getGender())
						.accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON);
	
				resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
				resultActions.andExpect(status().isOk());
				resultMvc = resultActions.andReturn();
				resultJSON = resultMvc.getResponse().getContentAsString();
	
				vroResult = objectMapper.readValue(resultJSON, ExternalSystemCheckingResult.class);
				
				assertNotNull(vroResult);
				assertThat(vroResult.isResult());
				assertThat(Integer.parseInt(vroResult.getResultDescription()) > 0);
				
				
				// 3 verifico che il suopporto generato sia presente al VRO
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Test: Check if exist a random generated medium on external system (VRO)");
				logger.info(" Description: Check if exist a medium on ExternalSystem with provided random data ");
				logger.info(" ------------------------------------------------------------------------------------");
				logger.info(" Result expected: medium not recognized ");
				logger.info(" Http status code expected: 200 ");
				logger.info(" MAIGeSDi error code expected: no error ");
				logger.info("------------------------------------------------------------------------------------");
				
				
				requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems/{id}/validity", this.mediumTypeCreated.getExternalSystem().getID())
						.param("username", username)
						.param("password", password)
						.param("mediumIdentifier", medium.getMediumIdentifier())
						.param("givenName", medium.getGivenName())
						.param("familyName", medium.getFamilyName())
						.param("fiscalCodeBirthPlace", medium.getBirthPlace().getFiscalCode())
						.param("birthDate", TestBaseUtil.convertDateToString(medium.getBirthDate()))
						.param("gender", medium.getGender())
						.param("validTo", TestBaseUtil.convertDateToString(medium.getValidTo()))
						.accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON);
	
				resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
				resultActions.andExpect(status().isOk());
				resultMvc = resultActions.andReturn();
				resultJSON = resultMvc.getResponse().getContentAsString();
	
				vroResult = objectMapper.readValue(resultJSON, ExternalSystemCheckingResult.class);
				assertNotNull(vroResult);
				
				//se gia esiste devo iniziare da capo
				if(vroResult.isResult() && !utilityService.isDebugMode()) {
					
					assertTrue(vroResult.isResult());
					//cancello il medium
					//this.mediumDao.deleteMedium(medium);
					
					
				}else {
					//non esiste e procedo con il test
					generaNuovoMedium = false;
					
					try {
						risultatoInserimento = this.mediumDao.addMedium(operatorUpdated, medium);
						
						// 2 verifico che l'anagrafica generata sia valida per il VRO
						//ho inserito il medium
						if(risultatoInserimento == 1)
						{
										
							assertNotNull(vroResult);
							assertFalse(vroResult.isResult());
									
							logger.info("------------------------------------------------------------------------------------");
							logger.info(" Test: Active an MAIGeSDi existing active medium on external system (VRO)");
							logger.info(" Description: Active an MAIGeSDi existing active medium on CEN");
							logger.info(" ------------------------------------------------------------------------------------");
							logger.info(" Result expected: Medium activate ");
							logger.info(" Http status code expected: 200 ");
							logger.info(" MAIGeSDi error code expected: no error ");
							logger.info("------------------------------------------------------------------------------------");
			
							mediumPost.setActivityCode("ACTIVATE_ON_EXTERNALSYSTEM");
							mediumPost.setMedium(medium);
							mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
							
							requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
																	.param("username", username)
																	.param("password", password)
																	.accept(MediaType.APPLICATION_JSON)
																	.content(mediumPostJSON)
																	.contentType(MediaType.APPLICATION_JSON);
							
							resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
							
							resultActions.andExpect(status().isOk());
									
							resultMvc = resultActions.andReturn();
							
							resultJSON = resultMvc.getResponse().getContentAsString();
			
							listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
							
							assertNotNull(listMediumsResult);
							assertThat(listMediumsResult.size()>0);
							mediumResult = listMediumsResult.get(0);
							assertNotNull(mediumResult);
							
							this.mediumCreated.add(mediumResult);
							
							assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
							assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
							assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
							assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
							assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
							assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
							assertEquals(medium.getGender() 					, mediumResult.getGender() );
							assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
							assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
							assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
							assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
							assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
							assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
							assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
							assertNotNull(mediumResult.getAuthorizationID());
							
							assertEquals(medium.getPhoneNumber()				, mediumResult.getPhoneNumber());
							assertEquals(medium.getMobileNumber()				, mediumResult.getMobileNumber());
							assertEquals(medium.getDenomination() 				, mediumResult.getDenomination());
							
							if(medium.getDocument() != null) {
								assertEquals(medium.getDocument().getDocumentType() , mediumResult.getDocument().getDocumentType());
								assertEquals(medium.getDocument().getAuthority() , mediumResult.getDocument().getAuthority());
								assertEquals(medium.getDocument().getCode() , mediumResult.getDocument().getCode());
								assertThat(mediumResult.getDocument().getReleaseDate().compareTo(medium.getDocument().getReleaseDate()));
								assertThat(mediumResult.getDocument().getExpirationDate().compareTo(medium.getDocument().getExpirationDate()));
							}
							
							if(medium.getTutor() != null) {
								assertEquals(medium.getTutor().getGivenName()   , mediumResult.getTutor().getGivenName() );
								assertEquals(medium.getTutor().getFamilyName()  , mediumResult.getTutor().getFamilyName() );
								assertEquals(medium.getTutor().getFiscalCode() 	, mediumResult.getTutor().getFiscalCode() );
								assertThat(mediumResult.getTutor().getBirthDate().compareTo(medium.getTutor().getBirthDate()));
								assertEquals(medium.getTutor().getBirthPlace(), mediumResult.getTutor().getBirthPlace());
								assertEquals(medium.getTutor().getAddress1()    , mediumResult.getTutor().getAddress1() );
								assertEquals(medium.getTutor().getAddress2()    , mediumResult.getTutor().getAddress2() );
								assertEquals(medium.getTutor().getAddress3()    , mediumResult.getTutor().getAddress3()  );
								
								if(medium.getTutor().getDocument() != null) {
									assertEquals(medium.getTutor().getDocument().getDocumentType() , mediumResult.getTutor().getDocument().getDocumentType());
									assertEquals(medium.getTutor().getDocument().getAuthority() , mediumResult.getTutor().getDocument().getAuthority());
									assertEquals(medium.getTutor().getDocument().getCode() , mediumResult.getTutor().getDocument().getCode());
									assertThat(mediumResult.getTutor().getDocument().getReleaseDate().compareTo(medium.getTutor().getDocument().getReleaseDate()));
									assertThat(mediumResult.getTutor().getDocument().getExpirationDate().compareTo(medium.getTutor().getDocument().getExpirationDate()));
								}
											
							}
							
							assertThat(Utility.TSSD_ATTIVO == mediumResult.getState().getID());
							
							
							// 4 una volta terminata l'esecuzione dell'attività ACTIVE_ON_EXTERNALSYSTEM verifico la presenza del medium al VRO
							logger.info("------------------------------------------------------------------------------------");
							logger.info(" Test: Check if exist the medium just activated on External system (VRO)");
							logger.info(" Description: Check if exist a medium on ExternalSystem with provided data ");
							logger.info(" ------------------------------------------------------------------------------------");
							logger.info(" Result expected: medium recognized ");
							logger.info(" Http status code expected: 200 ");
							logger.info(" MAIGeSDi error code expected: no error ");
							logger.info("------------------------------------------------------------------------------------");
							
							
							requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems/{id}/validity", this.mediumTypeCreated.getExternalSystem().getID())
									.param("username", username)
									.param("password", password)
									.param("mediumIdentifier", mediumResult.getMediumIdentifier())
									.param("givenName", mediumResult.getGivenName())
									.param("familyName", mediumResult.getFamilyName())
									.param("fiscalCodeBirthPlace", mediumResult.getBirthPlace().getFiscalCode())
									.param("birthDate", TestBaseUtil.convertDateToString(mediumResult.getBirthDate()))
									.param("gender", mediumResult.getGender())
									.param("validTo", TestBaseUtil.convertDateToString(mediumResult.getValidTo()))
									.accept(MediaType.APPLICATION_JSON)
									.contentType(MediaType.APPLICATION_JSON);
				
							resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
							resultActions.andExpect(status().isOk());
							resultMvc = resultActions.andReturn();
							resultJSON = resultMvc.getResponse().getContentAsString();
				
							vroResult = objectMapper.readValue(resultJSON, ExternalSystemCheckingResult.class);
							assertNotNull(vroResult);
							assertThat(Integer.getInteger(vroResult.getResultDescription()) > 0);
							
							
					
						}else {
							generaNuovoMedium = false;
							logger.error("Problemi ad inserire nel DB il medium generato per il test");
						}
					}catch(Exception e) {
						generaNuovoMedium = false;
						logger.error("Problemi ad inserire nel DB il medium generato per il test");
					}
				}
			}
			
		} 
		
		@Test(dependsOnMethods="activeOnExternalSystemAnExistinMediumAndExternalSystemCheck", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void reportMediumWithAllFilters(String username,
								  	 String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start reportMediumWithAllFilters");
			logger.info("====================================================================================");
			

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Medium mediumResult;
			MediumReportContainer result;
			
			
			//ricerche per dati relativi al supporto
			//ricerche relative dell'intestatario del medium
			//ricerche relative all'operatore
			//* ricerche miste
			
			Medium medium = this.mediumCreated.get(0);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Search a set of medium by a provided data (all filter version) ");
			logger.info(" Description: Search a set of medium with the medium's data just created ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: medium found ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/report")
					.param("username", username)
					.param("password", password)
				
					.param("mediumTypeID"              , medium.getMediumType().getID().toString())         
					.param("mediumIdentifier"          , medium.getMediumIdentifier())    
					.param("releaseDateFrom"           , TestBaseUtil.convertDateToString(this.testUtil.subtractDaysToDate(new Date(), 2)))     
					.param("releaseDateTo"             , TestBaseUtil.convertDateToString(this.testUtil.addDaysToDate(new Date(), 2)))            
					.param("expiryDateFrom"            , TestBaseUtil.convertDateToString(this.testUtil.subtractDaysToDate(medium.getValidTo(), 2)))               
					.param("expiryDateTo"              , TestBaseUtil.convertDateToString(this.testUtil.addDaysToDate(medium.getValidTo(), 2)))                  
					.param("stateTypeID"               , medium.getState().getID().toString())          
					.param("operativeFlowTypeID"       , medium.getOperativeFlow().getID().toString())  
					.param("lastTransitionOperatorID"  , this.operatorUpdated.getID().toString())
					.param("givenName"                 , medium.getGivenName())
					.param("familyName"                , medium.getFamilyName())
					.param("fiscalCodeBirthPlace"      , medium.getBirthPlace().getFiscalCode())
					.param("birthDate"                 , TestBaseUtil.convertDateToString(medium.getBirthDate()))
					.param("emitterOperatorID"         , this.operatorUpdated.getID().toString())   
					.param("offset"                    , "0")   
					.param("maxResults"                , "100")					
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();

			result = objectMapper.readValue(resultJSON, MediumReportContainer.class);
			
			assertNotNull(result);
			assertThat(result.getPagination().getTotal()>0);	
			assertNotNull(result.getItems());
			
			mediumResult = result.getItems().stream().filter(x -> x.getID().equals(medium.getID())).findFirst().get();
			assertNotNull(mediumResult);
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			assertNotNull(mediumResult.getAuthorizationID());
			
			
		}
		
		@Test(dependsOnMethods="reportMediumWithAllFilters", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void reportMediumWithMediumFilters(String username,
								  	 String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start reportMediumWithMediumFilters");
			logger.info("====================================================================================");
			

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Medium mediumResult;
			MediumReportContainer result;
			
			
			//*ricerche per dati relativi al supporto
			//ricerche relative dell'intestatario del medium
			//ricerche relative all'operatore
			//ricerche miste
			
			Medium medium = this.mediumCreated.get(0);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Search a set of medium by a provided data (medium filter version) ");
			logger.info(" Description: Search a set of medium with the medium's data just created ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: medium found ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/report")
					.param("username", username)
					.param("password", password)
				
					.param("mediumTypeID"              , medium.getMediumType().getID().toString())         
					.param("mediumIdentifier"          , medium.getMediumIdentifier())    
					.param("releaseDateFrom"           , TestBaseUtil.convertDateToString(this.testUtil.subtractDaysToDate(new Date(), 2)))     
					.param("releaseDateTo"             , TestBaseUtil.convertDateToString(this.testUtil.addDaysToDate(new Date(), 2)))            
					.param("expiryDateFrom"            , TestBaseUtil.convertDateToString(this.testUtil.subtractDaysToDate(medium.getValidTo(), 2)))               
					.param("expiryDateTo"              , TestBaseUtil.convertDateToString(this.testUtil.addDaysToDate(medium.getValidTo(), 2)))                  
					.param("stateTypeID"               , medium.getState().getID().toString())          
					.param("operativeFlowTypeID"       , medium.getOperativeFlow().getID().toString())  
					.param("lastTransitionOperatorID"  , this.operatorUpdated.getID().toString())
					.param("offset"                    , "0")   
					.param("maxResults"                , "100")					
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();

			result = objectMapper.readValue(resultJSON, MediumReportContainer.class);
			
			assertNotNull(result);
			assertThat(result.getPagination().getTotal()>0);	
			assertNotNull(result.getItems());
			
			mediumResult = result.getItems().stream().filter(x -> x.getID().equals(medium.getID())).findFirst().get();
			assertNotNull(mediumResult);
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			assertNotNull(mediumResult.getAuthorizationID());
			
			
		}
		
		@Test(dependsOnMethods="reportMediumWithMediumFilters", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void reportMediumWithMediumOwnerFilters(String username,
								  	 String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start reportMediumWithMediumOwnerFilters");
			logger.info("====================================================================================");
			

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Medium mediumResult;
			MediumReportContainer result;
			
			
			//ricerche per dati relativi al supporto
			//*ricerche relative dell'intestatario del medium
			//ricerche relative all'operatore
			//ricerche miste
			
			Medium medium = this.mediumCreated.get(0);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Search a set of medium by a provided data (medium owner filter version) ");
			logger.info(" Description: Search a set of medium with the medium's data just created ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: medium found ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/report")
					.param("username", username)
					.param("password", password)
				
					.param("givenName"                 , medium.getGivenName())
					.param("familyName"                , medium.getFamilyName())
					.param("fiscalCodeBirthPlace"      , medium.getBirthPlace().getFiscalCode())
					.param("birthDate"                 , TestBaseUtil.convertDateToString(medium.getBirthDate()))
					.param("offset"                    , "0")   
					.param("maxResults"                , "100")					
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();

			result = objectMapper.readValue(resultJSON, MediumReportContainer.class);
			
			assertNotNull(result);
			assertThat(result.getPagination().getTotal()>0);	
			assertNotNull(result.getItems());
			
			mediumResult = result.getItems().stream().filter(x -> x.getID().equals(medium.getID())).findFirst().get();
			assertNotNull(mediumResult);
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			assertNotNull(mediumResult.getAuthorizationID());
			
			
		}
		
		@Test(dependsOnMethods="reportMediumWithMediumOwnerFilters", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void reportMediumWithOperatorEmitterFilters(String username,
								  	 String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start reportMediumWithAllFilters");
			logger.info("====================================================================================");
			

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Medium mediumResult;
			MediumReportContainer result;
			
			
			//ricerche per dati relativi al supporto
			//ricerche relative dell'intestatario del medium
			//ricerche relative all'operatore
			//* ricerche miste
			
			Medium medium = this.mediumCreated.get(0);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Search a set of medium by a provided data (operator emitter filter version) ");
			logger.info(" Description: Search a set of medium with the medium's data just created ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: medium found ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/report")
					.param("username", username)
					.param("password", password)
				
					.param("emitterOperatorID"         , this.operatorUpdated.getID().toString())   
					.param("offset"                    , "0")   
					.param("maxResults"                , "1000")					
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();

			result = objectMapper.readValue(resultJSON, MediumReportContainer.class);
			
			assertNotNull(result);
			assertThat(result.getPagination().getTotal()>0);	
			assertNotNull(result.getItems());
			
			mediumResult = result.getItems().stream().filter(x -> x.getID().equals(medium.getID())).findFirst().get();
			assertNotNull(mediumResult);
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			assertNotNull(mediumResult.getAuthorizationID());
			
			
		}
		
		@Test(dependsOnMethods="reportMediumWithOperatorEmitterFilters", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void reportMediumWithMixedFilters(String username,
								  	 String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start reportMediumWithMixedFilters");
			logger.info("====================================================================================");
			

			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Medium mediumResult;
			MediumReportContainer result;
			
			
			//ricerche per dati relativi al supporto
			//ricerche relative dell'intestatario del medium
			//ricerche relative all'operatore
			//* ricerche miste
			
			Medium medium = this.mediumCreated.get(0);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Search a set of medium by a provided data (mixed filter version) ");
			logger.info(" Description: Search a set of medium with the medium's data just created ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: medium found ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			
			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/report")
					.param("username", username)
					.param("password", password)
				
					.param("expiryDateFrom"            , TestBaseUtil.convertDateToString(this.testUtil.subtractDaysToDate(medium.getValidTo(), 2)))               
					.param("expiryDateTo"              , TestBaseUtil.convertDateToString(this.testUtil.addDaysToDate(medium.getValidTo(), 2)))                  
					.param("stateTypeID"               , medium.getState().getID().toString())          
					.param("givenName"                 , medium.getGivenName())
					.param("emitterOperatorID"         , this.operatorUpdated.getID().toString())   
					.param("offset"                    , "0")   
					.param("maxResults"                , "1000")					
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();

			result = objectMapper.readValue(resultJSON, MediumReportContainer.class);
			
			assertNotNull(result);
			assertThat(result.getPagination().getTotal()>0);	
			assertNotNull(result.getItems());
			
			mediumResult = result.getItems().stream().filter(x -> x.getID().equals(medium.getID())).findFirst().get();
			assertNotNull(mediumResult);
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
			assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
			assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
			assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
			assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
			assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
			assertNotNull(mediumResult.getAuthorizationID());
			
			
		}
		
		@Test(dependsOnMethods="reportMediumWithMixedFilters", enabled = ESEGUI_I_TEST)
		@Parameters({"username", 
					 "password"
					})
		public void disableValidMediumAndCreateNewWithSamePersonData(String username,
			  	 								 					 String password) throws Exception {
			
		logger.info("====================================================================================");
		logger.info(" Start disableValidMediumAndCreateNewWithSamePersonData");
		logger.info("====================================================================================");
		
		
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		Medium mediumResult;
		MediumPost mediumPost = new MediumPost();
		List<Medium> listMediumsResult;
		Medium personValidityResult;
		
		// TEST CHE REPLICA LA LOGICA DEL SITO
		// 1 prendo un medium attivo già creato in questi test
		// 2 verifico che per quell'anagrafica non sia possibile ricreare un nuovbo medium
		// 3 disabilito il medium attivo scegliendo uno degli stati di disabilitazione casualmente
		// 4 verifico nuovamente se sia possibile creare un nuovo medium con quell'anagrafica, questa volta mi aspetto che sia possibile
		// 5 creo il nuovo medium con la stssa anagrafica
				
		assertNotNull(this.mediumCreated);
		assertNotNull(this.mediumCreated.get(0));
		assertThat(this.mediumCreated.get(0).getID() > 0);
		// 1 prendo un medium attivo già creato in questi test
		Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_ATTIVO).findFirst().get();
		
		// 2 verifico che per quell'anagrafica non sia possibile ricreare un nuovbo medium
		PersonValidityBody personValidityBody = new PersonValidityBody();
		personValidityBody.setMediumTypeID(this.mediumTypeCreated.getID());
		personValidityBody.setPersonData(new Person(medium.getGivenName(), 
													medium.getFamilyName(), 
													medium.getBirthPlace().getFiscalCode(), 
													medium.getBirthDate(), 
													medium.getGender())	);

		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: verifica Impossibilita creare un nuovo medium per la quintupla usata per la creazione precendente");
		logger.info(" Description: si contatta il sevizio persons/validity per verificare se la quintupla generata nel medium non sia più valida");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: person not valid, medium just created returned");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		resultActions = MAIGeSDiCENSystemTestUtil.chiamataMediumPersonsValidity(objectMapper, mockMvc, username, password, personValidityBody);
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();

		personValidityResult = objectMapper.readValue(resultJSON, Medium.class);
		
		assertNotNull(personValidityResult);
		assertNotNull(personValidityResult.getID());
		assertTrue(personValidityResult.getID()>0);
//		MAIGeSDiCENSystemTestUtil.
		MAIGeSDiCENSystemTestUtil.compareMediumsGeneratedWithIdentifier(personValidityResult, medium);
		
		// 3 disabilito il medium attivo scegliendo uno degli stati di disabilitazione casualmente
		
		Integer disableStateID = testUtil.getRandomDisablingState(true);
		
		medium.setState(new Identity(disableStateID, null));
		
		mediumPost.setActivityCode("DISABLE");
		
		logger.debug("Medium taken from list of generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Disable active medium medium ");
		logger.info(" Description: Disabled a valid medium, this operation will disable medium on CEN and change its state with a random valid one");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: Medium correctly disabled on MAIGeSDi and on CEN");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();

		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		
		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		mediumResult = listMediumsResult.get(0);
		assertNotNull(mediumResult);
		
//	
		mediumResult = listMediumsResult.get(0);
		
		assertNotNull(mediumResult.getID());
		assertTrue(mediumResult.getID()>0);
		
		this.mediumCreated.removeIf(x -> x.getID() == medium.getID());
		this.mediumCreated.add(mediumResult);
		
		MAIGeSDiCENSystemTestUtil.compareDisabledMedium(disableStateID, mediumResult, medium);
				
		// 4 verifico nuovamente se sia possibile creare un nuovo medium con quell'anagrafica, questa volta mi aspetto che sia possibile
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: verifica possibilità creare un nuovo medium per la quintupla generata");
		logger.info(" Description: si contatta il sevizio persons/validity per verificare se la quintupla generata nel medium sia valida");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: person valid ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		resultActions = MAIGeSDiCENSystemTestUtil.chiamataMediumPersonsValidity(objectMapper, mockMvc, username, password, personValidityBody);
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		assertThat(resultJSON.isEmpty());
		
		// 5 creo il nuovo medium con la stssa anagrafica usando una spezzata
		
		//calcolo della prima parte dell'identificativo(tutto tranne il codice di luhn) 
		String typographicNumber =	("00000000" + this.mediumIdentifierRangeCreated.getStart());
        typographicNumber = typographicNumber.substring(typographicNumber.length()-8, typographicNumber.length());;
        String prefixMediumIndetifierExpected = this.mediumTypeCreated.getOrganizationCode()+ typographicNumber;
        
        mediumPost.setActivityCode("NEW_PHISICAL_PERSON");
        medium.setMediumIdentifier(null);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Create a new medium without mediumIdentifier");
		logger.info(" Description: create a new medium of the medium type previously created, by reserving a medium from mediumIdentifierRange previously created");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: medium created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));

		assertNotNull(listMediumsResult);
		assertThat(listMediumsResult.size()>0);
		
		mediumResult = listMediumsResult.get(0);
		assertNotNull(mediumResult);
		
		assertNotNull(mediumResult.getID());
		assertTrue(mediumResult.getID()>0);
		
		this.mediumCreated.add(mediumResult);
				
		MAIGeSDiCENSystemTestUtil.compareMediumsGeneratedFromRange(prefixMediumIndetifierExpected,mediumResult, medium);
	
	}


		
	
		
		@Test(enabled = false )
		@Parameters({"username", 
					 "password"
					})
		public void disabilitaMediumBloccatoAlCEN(String username,
								  String password) throws Exception {
			logger.info("====================================================================================");
			logger.info(" Start disabilitaMediumBloccatoAlCEN");
			logger.info("====================================================================================");
			//ResponseEntity<?> result;
			
			String mediumPostJSON;
			RequestBuilder requestBuilder;
			ResultActions resultActions;
			MvcResult resultMvc;
			String resultJSON;
			Integer disableStateID;

			Medium mediumDaDisabilitare;
			Medium mediumResult;
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			
			Integer mediumID = 2428760;
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: GET medium by ID");
			logger.info(" Description: GET medium by ID on MAIGeSDi ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected:  Medium correctly returned");
			logger.info(" Http status code expected:  200");
			logger.info(" MAIGeSDi error code expected: no error ");
		    logger.info("------------------------------------------------------------------------------------");

			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/{mediumID}", mediumID)
					.param("username", username)
					.param("password", password);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isOk());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();
			mediumDaDisabilitare = objectMapper.readValue(resultJSON, Medium.class);

			assertEquals(mediumID, mediumDaDisabilitare.getID());
			
			disableStateID = Utility.TSSD_RINUNCIATO;
			mediumDaDisabilitare.setState(new Identity(disableStateID, null));
			
			mediumPost.setActivityCode("DISABLE");
			
			mediumPost.setMedium(mediumDaDisabilitare);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Disable active medium medium ");
			logger.info(" Description: Disabled a valid medium, this operation will disable medium on CEN and change its state with a random valid one");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium correctly disabled on MAIGeSDi and on CEN");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult);

			mediumResult = listMediumsResult.get(0);
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			assertThat(disableStateID == mediumResult.getState().getID());
		
		} 
	
	@AfterClass
	@Parameters({"username", "password"})
	public void restoreSystemStateAndDeleteGeneratedObject(String username, String password) {
		logger.info("====================================================================================");
		logger.info(" Start restoreSystemStateAndDeleteGeneratedObject");
		logger.info("====================================================================================");
		
		ResponseEntity<?> result ;
		if(this.mediumCreated != null && this.mediumCreated.size() > 0) {
			MediumPost mediumPost;
			Long numberMediumActive = this.mediumCreated.stream().filter(x -> x.getState().getID().equals(Utility.TSSD_ATTIVO)).count();
			logger.info("Number of Medium created: {}, of whitch active. {}",this.mediumCreated.size(), numberMediumActive );
			Identity statoDiDisabilitazione = new Identity(Utility.TSSD_RINUNCIATO, null);
			
			for(Medium m : this.mediumCreated) {
				try {
					
					if(m.getState().getID() == Utility.TSSD_ATTIVO && m.getOperativeFlow().getID() == Utility.TSFO_SUPPORTO_PRODOTTO)
					{
						logger.debug("Medium created for test, disabling on CEN required: "+ m);
						mediumPost = new MediumPost();
						mediumPost.setActivityCode("DISABLE");
						m.setState(statoDiDisabilitazione);
						mediumPost.setMedium(m);

						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Request mediumController.handleMediumActivity");
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Username: "+username);
						logger.info(" Password: "+password);
						logger.info(" MediumPost: "+mediumPost);
						logger.info("------------------------------------------------------------------------------------");
						
						result = this.mediumController.handleMediumActivity(username, password, "DisablingMediumAfterTest", mediumPost);

						if(!result.getStatusCode().equals(HttpStatus.OK))
							throw new Exception("Deleting medium problem "+ result);
						
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Response mediumController.handleMediumActivity");
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Result: "+result);
						logger.info("------------------------------------------------------------------------------------");
						
						if(result != null && result.getStatusCode().equals(HttpStatus.OK)) {
							logger.debug("Medium disabled on CEN: "+ m);
							this.mediumDao.deleteMedium(m);
							logger.debug("Medium deleted "+ m);
						}else {
							logger.debug("Medium not disabled on CEN, disable it manually on CEN and delete from DB; Medium : "+ m);
						}
					}else {
						logger.debug("Medium disabled on CEN: "+ m);
						this.mediumDao.deleteMedium(m);
						logger.debug("Medium deleted "+m);
					}
					
				} catch (Exception e) {//MAIGeSDiException e) {
					logger.error("Deleting medium error "+ m, e);
					
					e.printStackTrace();
				}
			}
		}
		
		if(this.mediumIdentifierRangeCreated != null && this.mediumIdentifierRangeCreated.getID() > 0) {
			try {
				logger.debug("MediumIdentifierRange created: "+ this.mediumIdentifierRangeCreated);
				this.mediumIdentifierRangeDAO.deleteMediumIdentifierRangeAndMediumForUnitTest(this.mediumIdentifierRangeCreated);
				logger.debug("MediumIdentifierRange and its medium deleted");
			} catch (MAIGeSDiException e) {
				logger.error("ERROR in delete MediumIdentifierRange ", e);
				
				e.printStackTrace();
			}
		}
					
		
		if(this.mediumTypeCreated != null && this.mediumTypeCreated.getID() > 0) {
			try {
				logger.debug("MediumType created: "+ this.mediumTypeCreated);
				this.mediumTypeDao.deleteMediumType(this.mediumTypeCreated);
				logger.debug("MediumType deleted");
			} catch (MAIGeSDiException e) {
				logger.error("ERROR in delete MediumType ", e);
				
				e.printStackTrace();
			}
		}
		
		//restore operator
		
		if(this.operatorUpdated != null && this.operatorUpdated.getUsername().equals(username) ) {
			
			this.operatorUpdated.getMediumTypes().remove(new Identity(this.mediumTypeCreated.getID(), null));
			
			try {
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Request operatorController.updateOperator");
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Username: "+username);
				logger.info(" Password: "+password);
				logger.info(" Operator: "+operatorUpdated);
				logger.info("------------------------------------------------------------------------------------");
				
				result = this.operatorController.updateOperator(username, password, null, this.operatorUpdated);

				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Response operatorController.updateOperator");
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Result: "+result);
				logger.info("------------------------------------------------------------------------------------");
				
				if(result != null && result.getStatusCode().equals(HttpStatus.OK)) {
					logger.debug("Operator updated: "+ this.operatorUpdated);
				}else {
					logger.debug("Operator updateding problem : "+ this.operatorUpdated);
					}
			} catch (Exception e) {
				logger.error("Restoring operatore error ", e);
				
				e.printStackTrace();
			}
			
			
		}
		
		logger.info("====================================================================================");
		logger.info(" End restoreSystemStateAndDeleteGeneratedObject");
		logger.info("====================================================================================");
		
		
	}
	
	
	
	

}
