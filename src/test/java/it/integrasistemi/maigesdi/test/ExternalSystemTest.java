package it.integrasistemi.maigesdi.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class ExternalSystemTest extends AbstractTestNGSpringContextTests{
	private static final Logger logger = LoggerFactory.getLogger(ExternalSystemTest.class);
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeTest
	@Parameters({"username", 
				 "password"
				})
	public void checkParameter(String username, 
							  String password) throws Exception {
		
		if(username == null || username.isEmpty()) {
			logger.error("Parameter username not configurated in TestNG-config.xml");
			throw new Exception("Parameter username not configurated in TestNG-config.xml");
		}
		
		if(password == null || password.isEmpty()) {
			logger.error("Parameter password not configurated in TestNG-config.xml");
			throw new Exception("Parameter password not valorized in TestNG-config.xml");
		}
	}
	
	@Test
	@Parameters({"username", 
		 		 "password"})
	public void getExteranlSystem(String username, 
			  					  String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start getExteranlSystem");
		logger.info("====================================================================================");
		//ResponseEntity<?> result;
		List<ExternalSystem> externalSystemList;
		ExternalSystem externalSystem;
		ExternalSystem resultExternalSystem;
	
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get all MAIGeSDi ExternalSystem");
		logger.info(" Description: Get all ExternalSystem with an operator with configurator profile ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: ExternalSystem retrived ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems")
				.param("username", username)
				.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		externalSystemList = Arrays.asList(objectMapper.readValue(resultJSON, ExternalSystem[].class));

		assertNotNull(externalSystemList);
		assertThat(externalSystemList.size() > 0);
		
		externalSystem = externalSystemList.get(0);
		
		assertThat(externalSystem.getID() > 0);
		assertNotNull(externalSystem.getName());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get an ExternalSystem by ID");
		logger.info(" Description: Get an ExternalSystem with an operator with configurator profile by its ID ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: ExternalSystem retrived ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems/{id}", externalSystem.getID())
				.param("username", username)
				.param("password", password);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		resultExternalSystem = objectMapper.readValue(resultJSON, ExternalSystem.class);

		assertNotNull(resultExternalSystem);
		
		assertEquals(externalSystem.getID()				, resultExternalSystem.getID());
		assertEquals(externalSystem.getDescription()	, resultExternalSystem.getDescription() );
		assertEquals(externalSystem.getName()			, resultExternalSystem.getName() );
		assertEquals(externalSystem.getUsername()		, resultExternalSystem.getUsername() );
		assertEquals(externalSystem.getURL()			, resultExternalSystem.getURL() );
		assertEquals(externalSystem.getPassword()		, resultExternalSystem.getPassword() );
		
		
		
	}

}
