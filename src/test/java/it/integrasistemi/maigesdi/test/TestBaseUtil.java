package it.integrasistemi.maigesdi.test;

import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.ActivityType;
import it.integrasistemi.maigesdi.bean.Credential;
import it.integrasistemi.maigesdi.bean.CredentialType;
import it.integrasistemi.maigesdi.bean.Document;
import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.Parameter;
import it.integrasistemi.maigesdi.bean.Place;
import it.integrasistemi.maigesdi.bean.Tutor;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.SimpleDateFormatThreadSafe;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;

@Component
public class TestBaseUtil {
	
	@Autowired
	UtilityService utilityService;
	
	static final String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static final String ABNUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static final String NUM = "0123456789";
	static SecureRandom rnd = new SecureRandom();

	public static String randomAlphaNumericString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( ABNUM.charAt( rnd.nextInt(ABNUM.length()) ) );
	   return sb.toString();
	}
	
	public static String randomNumericString( int len ){
		   StringBuilder sb = new StringBuilder( len );
		   for( int i = 0; i < len; i++ ) 
		      sb.append( NUM.charAt( rnd.nextInt(NUM.length()) ) );
		   return sb.toString();
		}
	
	public static String randomAlphaString( int len ){
		   StringBuilder sb = new StringBuilder( len );
		   for( int i = 0; i < len; i++ ) 
		      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		   return sb.toString();
		}
	
	public static Integer randomInteger(int maxNum) {
		return rnd.nextInt(maxNum);
	}
	
	public static Integer randomInteger(int minNum, int maxNum) {
		return rnd.nextInt((maxNum-minNum+1))+minNum; 
	}
	
	
	public static Date getDateFormatted(Integer giorno, Integer mese, Integer anno) {
		//DateFormat formatter = new SimpleDateFormatThreadSafe("dd/MM/yyyy");
		DateFormat formatter = new SimpleDateFormatThreadSafe("yyyy-MM-dd");
		TimeZone tz = TimeZone.getTimeZone("CET");
		formatter.setTimeZone(tz);
		Date result = null;
		Calendar cal = Calendar.getInstance();
		cal.set(anno, mese, giorno);
		Date data = cal.getTime();
		try {
			result = (formatter.parse(formatter.format(data)));
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
		return result;
		
		
	}
	
	public static String convertDateToString(Date date) {
		SimpleDateFormatThreadSafe dateFormat = new SimpleDateFormatThreadSafe("yyyy-MM-dd");

		return dateFormat.format(date);
		
	}
	
	public static boolean isValidDate(String stringDate) {
		boolean result = true;
		SimpleDateFormatThreadSafe df = new SimpleDateFormatThreadSafe("yyyy-MM-dd");
		
		try {
		    TimeZone tz = TimeZone.getTimeZone("CET");
		    df.setTimeZone(tz);
		    df.setLenient(false); // verifica la data
		
		    df.parse(stringDate);
		}catch(Exception e) {
			result = false;
		}
		
		
		return result;
	}
	
	public static Date convertDate(String date) throws ParseException {
		SimpleDateFormatThreadSafe df = new SimpleDateFormatThreadSafe("yyyy-MM-dd");
	
	    TimeZone tz = TimeZone.getTimeZone("CET");
	    df.setTimeZone(tz);
	    df.setLenient(false); // verifica la data
	
	    return df.parse(date);
	}
	
	
	public MediumType generateMediumType(Integer externalSystemID, Integer organizationID) throws Exception {
		
		MediumType result = new MediumType();
		
		
		result.setName("GeneratedName-"+randomAlphaNumericString(46));
		result.setExternalSystem(new Identity(externalSystemID, null));
		result.setOrganizationID(organizationID);
		result.setNumberDaysValidity(rnd.nextInt(1000));
		result.setSeriesCode(randomAlphaString(3));
		
		//gestione tipo credenziali
		Set<CredentialType> credentialTypes = new HashSet<CredentialType>();
		CredentialType barcode = new CredentialType(1, "BARCODE", "barcode");
		credentialTypes.add(barcode);
		result.setCredentialTypes(credentialTypes);
		
		//- gestione parametri
		Set<Activity> activitySet = null;
		
		switch (externalSystemID)
		{
		case Utility.EXTERNAL_SYSTEM_MAIGESDI:
			activitySet = getParameterForMAIGeSDiMedium();
			break;
		case Utility.EXTERNAL_SYSTEM_MAIGESDI_CEN:
			activitySet = getParameterForMAIGeSDiCENMedium();
			break;
		case Utility.EXTERNAL_SYSTEM_EXTERNAL_CEN:
			activitySet = getParameterForExternalCENMedium();
			break;
			
		default:
			throw new Exception("Id sistema esterno errato");
		
		}
		
		result.setActivity(activitySet);
		
		//System.out.println("------------RESULT " + result  );
		return result;
	}

	private Set<Activity> getParameterForExternalCENMedium() {
		Activity activity = null;
		Identity activityType = null;
		//gestione attività
		Set<Activity> activitySet = new HashSet<Activity>();
		
		activity = new Activity();
		//attivita NEW_PHISICAL_PERSON
		activity.setCode("NEW_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName("NEW");
		activity.setActivityType(activityType);
		
		Set<Parameter> parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		//attivita SEARCH
		activity = new Activity(); 
		activity.setCode("SEARCH");
		//- gestione activityType SEARCH 
		activityType = new Identity();
		activityType.setName("SEARCH");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
				
		//attivita NEW_WEB_PHISICAL_PERSON
		activity = new Activity();
		activity.setCode("NEW_WEB_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_NEW_WEB);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
		
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		//attivita DISABLE
		activity = new Activity(); 
		activity.setCode("DISABLE");
		//- gestione activityType DISABLE 
		activityType = new Identity();
		activityType.setName("DISABLE");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0, Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_STATE_TYPE_ID , null, true));
						
		activity.setParameters(parameterSet);
		activitySet.add(activity);	
	
		// VERIFY
		activity = new Activity(); 
		activity.setCode("VERIFY");
		
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_VERIFY);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, true));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, true));

		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		return activitySet;
	}

	private Set<Activity> getParameterForMAIGeSDiMedium() {
		Activity activity = null;
		Identity activityType = null;
		//gestione attività
		Set<Activity> activitySet = new HashSet<Activity>();
		
		activity = new Activity();
		//attivita NEW_PHISICAL_PERSON
		activity.setCode("NEW_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName("NEW");
		activity.setActivityType(activityType);
		
		Set<Parameter> parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		activity = new Activity();
		//attivita NEW_NOT_PHISICAL_PERSON
		activity.setCode("NEW_NOT_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName("NEW");
		activity.setActivityType(activityType);
		
		 parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		
		//attivita NEW_WEB_PHISICAL_PERSON
		activity = new Activity();
		activity.setCode("NEW_WEB_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_NEW_WEB);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		
		
		//attivita MODIFY
		activity = new Activity(); 
		activity.setCode("MODIFY");
		//- gestione activityType MODIFY 
		activityType = new Identity();
		activityType.setName("MODIFY");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, false));
		//parameterSet.add(new Parameter(0, "PHOTO, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);

		//attivita SEARCH
		activity = new Activity(); 
		activity.setCode("SEARCH");
		//- gestione activityType SEARCH 
		activityType = new Identity();
		activityType.setName("SEARCH");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, false));
		//parameterSet.add(new Parameter(0, "PHOTO, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		

		//attivita NEW_WEB_PHISICAL_PERSON
		activity = new Activity(); 
		activity.setCode("NEW_WEB_PHISICAL_PERSON");
		//- gestione activityType NEW_WEB 
		activityType = new Identity();
		activityType.setName("NEW_WEB");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);

		//attivita DISABLE
		activity = new Activity(); 
		activity.setCode("DISABLE");
		//- gestione activityType DISABLE 
		activityType = new Identity();
		activityType.setName("DISABLE");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0, Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_STATE_TYPE_ID, null, true));
						
		activity.setParameters(parameterSet);
		activitySet.add(activity);	
		
		
		//attivita NEW_WEB_PHISICAL_PERSON
		activity = new Activity(); 
		activity.setCode("RESTORE");
		//- gestione activityType NEW_WEB 
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_RESTORE);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
				
		
		return activitySet;
	}

	private Set<Activity> getParameterForMAIGeSDiCENMedium() {
		Activity activity = null;
		Identity activityType = null;
		//gestione attività
		Set<Activity> activitySet = new HashSet<Activity>();
		
		activity = new Activity();
		//attivita NEW_PHISICAL_PERSON
		activity.setCode("NEW_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName("NEW");
		activity.setActivityType(activityType);
		
		Set<Parameter> parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		//attivita NEW_WEB_PHISICAL_PERSON
		activity = new Activity();
		activity.setCode("NEW_WEB_PHISICAL_PERSON");
		//- gestione activityType NEW 
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_NEW_WEB);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID", null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_GENDER, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0, Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		
		
		//attivita MODIFY
		activity = new Activity(); 
		activity.setCode("MODIFY");
		//- gestione activityType MODIFY 
		activityType = new Identity();
		activityType.setName("MODIFY");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, true));
		//parameterSet.add(new Parameter(0, "PHOTO, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);

		//attivita SEARCH
		activity = new Activity(); 
		activity.setCode("SEARCH");
		//- gestione activityType SEARCH 
		activityType = new Identity();
		activityType.setName("SEARCH");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, false));
		//parameterSet.add(new Parameter(0, "PHOTO, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		

		//attivita NEW_WEB_PHISICAL_PERSON
		activity = new Activity(); 
		activity.setCode("NEW_WEB_PHISICAL_PERSON");
		//- gestione activityType NEW_WEB 
		activityType = new Identity();
		activityType.setName("NEW_WEB");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0, "ID, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);

		//attivita DISABLE
		activity = new Activity(); 
		activity.setCode("DISABLE");
		//- gestione activityType DISABLE 
		activityType = new Identity();
		activityType.setName("DISABLE");
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0, Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0, Utility.PARAM_STATE_TYPE_ID , null, true));
		//parameterSet.add(new Parameter(0, "IDENTIFIER", null, true));
						
		activity.setParameters(parameterSet);
		activitySet.add(activity);	
		
		
		//attivita RESTORE
		activity = new Activity(); 
		activity.setCode("RESTORE");
		//- gestione activityType RESTORE 
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_RESTORE);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_PHOTO, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_SIGN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_SIGN, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		// VERIFY
		activity = new Activity(); 
		activity.setCode("VERIFY");
		
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_VERIFY);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		//parameterSet.add(new Parameter(0,  Utility.PARAM_ID, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_IDENTIFIER, null, true));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_PIN, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GIVENNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_FAMILYNAME, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHPLACE, null, true));
		parameterSet.add(new Parameter(0,  Utility.PARAM_BIRTHDATE, null, true));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_FISCALCODE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS1, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS2, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_ADDRESS3, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_TYPE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_CODE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_RELEASEDATE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_AUTHORITY, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_DOCUMENT_EXPIRATIONDATE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_PHONENUMBER, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_MOBILENUMBER, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_EMAIL, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_RELEASEDATE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDFROM, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_VALIDTO, null, true));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_GIVENNAME, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FAMILYNAME, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHPLACE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_BIRTHDATE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_FISCALCODE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS1, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS2, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_ADDRESS3, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_TYPE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_CODE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE, null, false));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_DENOMINATION, null, false));
		parameterSet.add(new Parameter(0,  Utility.PARAM_GENDER, null, true));
//		parameterSet.add(new Parameter(0,  Utility.PARAM_PHOTO, null, false));
				
		activity.setParameters(parameterSet);
		activitySet.add(activity);
		
		//attivita ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM
		activity = new Activity(); 
		activity.setCode(Utility.ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM);
		//- gestione activityType ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM 
		activityType = new Identity();
		activityType.setName(Utility.ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM);
		activity.setActivityType(activityType);
		
		//- gestione parametri
		parameterSet = new HashSet<Parameter>();
		parameterSet.add(new Parameter(0, Utility.PARAM_ID, null, true));
						
		activity.setParameters(parameterSet);
		activitySet.add(activity);	
		
		return activitySet;
	}
	
	
	
	public Medium getMediumTestPhisicalPerson(MediumType mediumType, boolean isMaggiorenne) {
		Integer currentYear = Calendar.getInstance().get(Calendar.YEAR);
		
		Medium medium = new Medium();

		medium.setMediumType(new Identity(mediumType.getID(), null));
		
		
		String mediumIdentifier; 
		
		String codiceConi = mediumType.getOrganizationCode();
		
		if(codiceConi.equals("009")) 
			mediumIdentifier = codiceConi.concat(randomNumericString(6));
		else
			mediumIdentifier = codiceConi.concat(randomNumericString(9));
		
		
		// credenziali
		Set<Credential> credentials = new HashSet<Credential>();
		credentials.add(new Credential(new CredentialType(1, "BARCODE", null), mediumIdentifier));
		credentials.add(new Credential(new CredentialType(2, "RFID", null), "987654321"));
		medium.setCredentials(credentials);

		medium.setMediumIdentifier(mediumIdentifier);
		// medium.setID();
		medium.setPIN(randomNumericString(6));

		medium.setGivenName(randomAlphaString(30));
		medium.setFamilyName(randomAlphaString(30));
		
		if(isMaggiorenne)
			medium.setBirthDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear - 80, currentYear - 19)));
		else
			medium.setBirthDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear - 17, currentYear - 5)));
				
		medium.setGender(Utility.GENDER_MALE);
		medium.setFiscalCode(randomAlphaNumericString(16));

		medium.setAddress1(randomAlphaNumericString(50));
		medium.setAddress2(randomAlphaNumericString(50));
		medium.setAddress3(randomAlphaNumericString(30));

		medium.setPhoneNumber(randomAlphaNumericString(15));
		medium.setMobileNumber(randomAlphaNumericString(15));
		medium.seteMail(randomAlphaNumericString(40));

		medium.setReleaseDate(getDateFormatted(25, 4, 2016));
		medium.setValidFrom(getDateFormatted(25, 4, 2016));
		medium.setValidTo(getDateFormatted(25, 4, 2020));
		medium.setDenomination(randomAlphaNumericString(255));
		
		medium.setPhoto("iVBORw0KGgoAAAANSUhEUgAAAEoAAABSCAIAAAB8N9XQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABIzSURBVHhezVx7bJTXlb/fc1425pU+NmlCtlWfq0WbkKhJ2wDdJCpK0tASQkhV7Ua7K+UfIu0SgpIoilRtE6BKH6pUtWkhaUzs4HnZxqa7FSEPUsA2BfwYe2aM7QAGTJQHsTAGApn+7j137ty5Y5vXMPSno/F3H9+953fPuefe+803ZizeV35J9NmJHtbUFdjU58RSViztRlN2PMPiaSvezWKDXizjxPq8aD8uzHvLK2a6HGLFIGk/MRCOp6tj3Z+t3XnTS+03b3jzppfenFXXWd3QzeJdbkMP29zL4lnj3jKLmb4EaUzh041lQg1pO5mdVd/Bpl/rWC7ApoRrO55bdcOSf//mi5t98Izth3lZMsUSvMHyiJm+FEk70TR8b2b93tkNbzvQmwUcO8D/Tgnbdm3mB7ywFfAW1r9Jbgwpaf8yxExfvPjRtNecvXN9ktnTHBYIMce2ODNcSB6TgLPnVewqfFjsm/+7wdrcaUe7vDIyNNMXLJhgLLaPxbKspTewcadlWY7j2K4DRXHNbHxYXHfbxqfnefBV5NAncgoQtfhdqOjN+sav/y/4WppHJrRf0ulFi5m+YAE9r7HfrnubuQGQIVV1gO2nn3569uzZXAmQCTz11FNUUxF2wZMxnwVcOHx8xOjxUsRMTyleQ5o1Yep3W/F+1rqP2Z6iBRVhokgkcuDAAUniYtDT00OGlVQ9xqZf70f7WWMnjzdcTGUuSMz0lGLFMft50GfJ7HXr/8KdKo9wOAx6Bw8elPpeEmBw7uHwZ8d1A9OxWrqxgcrRY0ksWembX0i6Ic9CbAQ7MdNA7N13392/f//Q0JDU9JIwPj5Ozswc/967v3fyxPiuXM5KZC59HprpSSXlRLEE97CNPWE/gAGWXsTYY489BmKDg4MDAwOYbKRoWTCW++TsyTOB+BCfDrG+4GvZ/MpxwQujmZ5CGtPYQ81Z8QxWY7BS9MhuoAdu5aWHEJTLfdI8nqtKpKxEuiragc0DdnymYlOImZ5EsEWEZ379v1ZWYyFGbBMAwyNHjoAYcPr06TJzAzMQzJ0by52Bx/8jHKem5ksv74AlDd2mEjNdIm4U/sA/vfig7pO4wEwjbpcZUS4E42dzA7ncj3638ZYn/+BGh/iqi+1bibammOkJJM0HrCk7b10tKBE9xLfa2lriBs/s7++XWlwxnMqdOZ3LnTlzxvPD3sL5oeYM9rclqpaImS4RL5rCueY7G7b5TpWid9dddx06dIjowYYnTpyQWlw5nMvlPuW+6jBsVQNu7V9DiT2GqhOImdZEzLcUa0q7jT3MDlq2T9zuv/9+eCNxA95//32pwZXFOdDD5MYpg/kOmzZ7Zn0nS+w9zxbcTBeLG8WOuQOLW5BPNwmESslMmK7sEWVqnDp1yrcCzPKw9GNbwydhidoFMdPFwsfma/NAz81v/7HnAiS5wcFsNiu7rRQw/cY++gib3BALzHt563k2NGZaEz4wjZ205SdkMhlEEclMBJVz5zAnKg34C5TBTMFp2G3K+LFesZ0y9edipotlxmspRQ/NSVp5UE/UZYVBWlW59tzn17PmjBPFSd9UnouZJkmkI4nMP63+uevh0M2BlSCdTktaAhVYDKYGKYZAg1N/dWO/0LzEUc00STI9r+4tx/HUebu3t1fSyuPw4cOyn6sBzEBsvqEYggKWiq+98paIMRdGz2nsx+Gbjt4A3FKfcgCSV8stdXDl+EGfsZmf4cvYeenhiMWifaHGrOtY3L0RM10XK5uklcdlHnzKhXxcgHs6LNolGBaLkfYbBlAJZzkMCj3oghPipCNpCSBgfvIJtrtXH0JBDvhXzaa/Yk4ZdEx6VqI3ENvPn3fx5wEcWNkAyUwAhwPZ/NWGHtWvX/28wYWLkXZj3W6iE7XpNgRMyUkDHagrCTgLNisIJzKtgfTkCkPl5gGDjknPjg3c9lIr3QP4vi85aaiwZyKGQXuEANiqNJ5JRXmUsEPRkm8sjHSwqWua/Rl5B2PqRKdDNlwpSFUEMNwyVwAmBW24GIoQXma+2mnQkfT4opHgR4SaeJf+cJmeMihU5mhnIBRCIChA5moAQ+R7zKpe/t/yUYV6YCH/YK2LIuykbt34BrP4swYMybPPPqsfDgDQw4DJVisFFT8Ihn/S4s6BWoGZOJ2KR6MmvRSLZb1EivmfDYiYiSExuAEVeOhQClJeoXT6jY6O8gJ6+F/bxZo6C6ekPL0+N94ze1MbryaAXVjpxDt+/Hh5NytGaxhQz/PC4bBMC3ClBehC5uZBRxaqAHyubjd/tp0nVaDnxbsiy1ZTpUAggEMdjj+SVh6ImRdFjyrDf4xzEyXh/x988AEuVJvwQ3JFSgLokVTCLVQ0oQJqCG557new0wTWm7OhVXwlJSEJFUM2dj6QBrIhMY2feOIJfbWErhTuCDAa5ct0MQckYVK6wJQBKF+Hoscse1ZdlyJVoMfcawKW7FJ/lKIDDRlbFhh5bGzMMA5A/eEzGOTPMQy/4n0IINCjCGZR+XpNNUwrV67ERVtbGwYFlVevXi3KC+jo6ODNwe+Ys7B+W4FU/iplBXB0kvQmXO6QiSK0rpZ1qgwY9FCBtERlGI2cCqBSbD4oCV2RxEx74IEHqGjNmjVUmRqkTyTxCarK5hgyXrsYvEXA8a9b+qhJDyue5zmuOFsA6tnzyMjI1q1bMcbIVFoa9MDECAYAFQG4BkPcom+pqGjRokW4RpEaHdSkVU75CFghSfeOj4+TDuSrBniLUBKr2qwvmOseP0pA0fwXWiqoNDU1IamIKaA56liB+iCcOHGCMqFl6UZRtaYmmKoDAmR2FFEpPim5Y8cOXCDm4ZPMrkN5RND2WbhGclP0/IZ90nACxA3QA4ACDTZGWqYFqBuCzCrOVICpqVQZTQE0dOekHKpsDDGRV0B9ynf47AsWzrX0p+q1fbxQUAQlSW5wkNwSwUrNeL1dXjsPDLzM1fKVG+uQZROVKnrbt2+njhQ9AqkBIF+/vUDPcj0f9IqtV1O/h/stVdGsR0l0ie0YQgsspuhRx+iPhoByqEjcxKFydMiyiWwLH1MEKId6wYhjiFetWoUcJFEHK40ewxU9GwQd26Q3s243sxA5JSQ5QY8I4EQLehgwMJRN5hUleii6fHoAaFApTUiiR75KrVEpRpzXzoOqAZhL3P5F9BLdc1/epiYZ+Ehyg4PDw8OUqfxTtataBDC6gLKtzGVMHwsFCg+ATGsAjccffxxdoDsaL+zgUZOaJXDthZJz586VWQKiSe6BPHga9ObVvqHCJiDJCcisPFRQVvTeeecdopdMJqmI8gFKGpBlE5USH6gODrgGkGnUpHtRB0Mgs8Rmg/LFBHNNejdt3IYiIRySmQDlEDDwcEVqUUVOXIAbXdNxia4B0s8A7EOlai+mQNECqgMqcqAmXRDoXoLM0pTBvAsGqorpxVM3bdxKxQTJTEBmCf9EQzQN9Baxyj3yyCO4wJCTTpQP4LqUIW6EC5CP4Vrm5nJqe418ZRnqha4JUADkeetavgotFgt6zOcvU1wsPZhINpYHMtETKQpAJ6pDKgIoopr64o5SHJRRinuxR0EO4hauVSmgBhFATeTIhGCidpgyS7eeZTN3mnli+OIr2yeMnABciEZLaaBAlZVnEpAPDVQmDAXadK1a0C2gQEUG4O2YVzpbAIN18uRJmRDAqitbcdjs5Y+a9Oa88g4We6WmZCaA9QAqQhtoSUMrmxT0MBWpb7oRwDXqKJPiRroX18qYgBpvBVlQDGpc92EAU0B3BwA51IjHrFs2tJj0quv2cueaKLQMDAwcO3aM1MXiruihY6pMSboGqAK6X7x4MZIYGnyCHlowjEB45plnnn76aZm4VBQcxGEzNu6V3BQ9f1MXfRVBkMwEYL3+/n5YAPnYaquBnJoeQCNKNwJI6pbXYRjnEkCnSg4vUl16nHViKVhO7crUWw8KkUgE+Ti8yvYEkINhU9fEhI6eBPDBKBBkVrmBoQHQr8QdS33xTnoRPTgrJh7frQlIThpI9d7eXvgntQuNkYN8StI1PlUFAPQms1i5QCOIfgn/HO1gTdr7LvTHjqdv/J9fRPgLsRyYJ5JWHh9++CEV4VppDNMZqwUG8krzKcXo6ChG1mJumAVDySFX/5ZPXX2urj0UnE4coDT8E3YgbgBmIDiDD65pa0I0jABdeWBAKbBjNWfh66zofpc/jzboJfr8+F4WLHy7gOVOpweMjIxghw3/lA3nHUMmrhKUZ7pYpJb8h5fIwBPFE3edHn+ROHvL75v5GyyIoZYF6xlfOCtgwGTbfwcYGxujVcGx/Dl/fEvRkSL/JHh0sTelgrfebee/uHz99ddhQ8mpGB9//HHl51gpCpsVxAtsS2pL3m5RV/xpUuO+OdHdsDJqY/phR2J87azj74HefffdR9w4ArOtzef7fs95tQcbDQrxwLp16ySbYoD20aNHZSdXD6Qkj/QW+/L6Vm6hYjomPf6DoOrrFD1cSEIl6Ovrk51cPUA9BAscxAOhYCTRyV9FNuiY6USWvZpi4engR+d3eGkqlZKcioEOrpaLYk9LBuARk4W+9fK2wjZaFzM3kUVgvfa3W/idYpHHcrdgwQJjkSAgtNL5tfIgxfDpuyHEzZokf1W+iAgJ/+JSS7uxbq8hbdV3Msu3RAiF/dHQhIsE4irWD9lhBaHvwvhW2Z/hJyZ5sZNvpvlVEUkrnvqX9f9vBbygU/he+/Dhw5KWBsQY9FdJF6W+SCWc7uxw1Td+s2XS1+Pv3XO8uqGNbeopyk30ebFeK9EbuvUeagiAGSd0UWJYMUhtCMEZsATnNtmPGx5qP/j99iPBaBE9EWGzTjTrJbtkQ4IeFoNShtlstpIuqh5zANe/9AZLaFuwUlnWNrys7dDivjEvwX/8aBSDJyau6/rqrLtnzx7sPCWzPCppQFKDIgKTr3FOLpzeriPL2w7c0XUq2GgUp51Y97Uvvg5q8qwv1gmcHko3a6dPn8Ze9IrusNF74aGDoGcneqYyHURYb/jBtuEH2kdmbdoTaOhmyV7+82t4Mxf+Gw1YFRvWSP7NXELpW5CwIVD2MIMGi87jfB0HPfiTZyfFy+wGJV2IHmRJ+8i0P76NRU8cB8EKxTycOg2DVrzzmlgbC16LMQPQA30arx/DqkB5zxPgtmrVKsFKIuhjlF3L4k80Ef+KyJSKst7yXcPLd773g13v+Rs73GQW9Ggl4aEJ9RL7Qi3vyh7yWLFiBSKN/t4/kMlkymtA+CCNpkSE/TK21Ob7fvsr9W9L9SYTZT0wxOfDu4882DaCTYCgx+0uIq/4lVRTBj3Rgz0FdGys+JiWx48fl6pdHvTzjsK6P313Xet88ZaDG/nybROv5koUPV2WtB1a1PGRE+/kP2xWVfm/Suiy61NucAb/EjQfbihSNzU1wVe7u7uJITA6OnqxZkR9uoVaJsBw6MBj7NEnl65N3rum5V+f2/Lt4LQq33bCljvvt392N/UV6amLQYzk4Z3DSzuO3b3rPRbT1kM+G7Nw93+I72Vfv11zFw4YFjxhzGPHjoEbYgwWyQsMpKB06tQp9aRZX9kA3iwLMiu4tvXba7YsWtO6AAy/ctss/j0hakZucGP76CdvSgo6G8R0wWz8fv94oaomfF/aOMRs8fCU72xdaCROwTxDh6GrDvg5BkUmNIiZFrCYb9n8VZR10QU/bf7WmpaFnJiQtS13/GT9Q1U1vEP+bDYw64fth+/t+PC27Udv3nF0xqttobpO1oCok5qKHl8Pdx0yiHGhNQMxNjrEYpnvxHax8Oc9rhfY8b+XCdALT6968mc/ei42f23r7Wu23L62BRYr0IP1ftl6zw//baGozYdgWfsxoe2R5TtHIEs6eAR5sP3QeejxGoqVJuQAIurIpP/gCha5BmZkIT5baFh5EubBlRKAPmFYy8YcxlnUsn3X9kKhSKC6Gj4w/6EbQea5LXkyE0rLwrUtC5njwzfQ3qNv9v5499GH2/aT5nwV2MUvzuOcy9oOKEpTix3tDcfTNbEMc6txDnYcz7JCDkMEggvxjR3/0psLXXtcOPh/z4CKru/cufSrv9i86IXNdz/fPN8kM4lYFv9qgPOrmrZ850HYzaBwHnqLdw4ZNCYTOa1hz8j1ZKgX1j92+z03MizCIgnxAnYwLH/tzsVl8757w4uJlb+K/ufPmr+HGfXTLWS0Owwak4lv86cKaBBDiFi4vONIEYW24b8BBnBePXof7pYAAAAASUVORK5CYII=");
		//		medium.setPhoto(photo);
		
		medium.setSign("iVBORw0KGgoAAAANSUhEUgAAAGIAAAAqCAYAAABIro4PAAAKCWlDQ1BJQ0MgUHJvZmlsZQAASImFlgdUFNcexu/M9kZbWHrvvcMC0nuTXkVlWWCpKywsIHYkqEBEEREBRZBQFYyGGgsiigURUMCCZpEgoDyDBVBReQMkMcl7573/nHvu73x77zf/uTvnzAcAyYORlJQACwCQyE7l+DrbyQWHhMrhJgAMBIEwkACKDGZKkq23twdA6o/577UwAqCV+b72itd//v4/SzAyKoUJAOSNMJ2ZxElFeD/C/umpSSs8hrAwB2kK4bkVZq0yjF7hiDUWW13j72uPsBYAeDKDwWEBQKQjulwak4X4EIMR1mNHxrIRXvG3YsYwIhG+ibBWdAI3A+H3K2sSE7cgOkkJYbWIv3iy/uYf8ac/g8H6kxMTuMzfn2vlRMhR7AA/ZJZAhhSIBjogAXBBBpADSYADtiBKLKJEIWf/3/fRV/fZIyuTwFZkRyxggRiQiux3+ouX36pTKkgHDGRNFKJ4IJf9yv+4ZvmOtuoK0W5/05K7ADDLRUTWN42hCEDHCwCoC980xbdIO4cAuDTA5HLS1rSVowcYQAT8yBsiDmSAIlAD2sAAmAALYAMcgRvwAv4gBGwCTKTfRKSrdLAd7AE5IA8cAkdBKagAp0EdOAvOgzZwEVwFN8AdMACGwRPAA5PgFZgDC2AJgiAcRIGokDgkCylDmpABRIesIEfIA/KFQqBwiAWxIS60HdoL5UGFUClUCdVDP0Id0FXoFjQIPYLGoRnoLfQJRsFkWBiWhlVgXZgO28LusD+8EWbByXAmnA0fhEvgKvgM3Apfhe/AwzAPfgXPowCKhKKh5FHaKDrKHuWFCkVFozionahcVDGqCtWE6kT1ou6jeKhZ1Ec0Fk1Fy6G10RZoF3QAmolORu9E56NL0XXoVnQP+j56HD2H/oqhYKQwmhhzjCsmGMPCpGNyMMWYGkwL5jpmGDOJWcBisTSsKtYU64INwcZht2HzsSewzdgu7CB2AjuPw+HEcZo4S5wXjoFLxeXgjuPO4K7ghnCTuA94El4Wb4B3wofi2fgsfDG+AX8ZP4Sfwi8RBAjKBHOCFyGSsJVQQKgmdBLuESYJS0RBoirRkuhPjCPuIZYQm4jXiWPEdyQSSYFkRvIhxZJ2k0pI50g3SeOkj2QhsgbZnhxG5pIPkmvJXeRH5HcUCkWFYkMJpaRSDlLqKdcozygf+Kh8OnyufJF8u/jK+Fr5hvhe8xP4lflt+TfxZ/IX81/gv8c/K0AQUBGwF2AI7BQoE+gQGBWYF6QK6gt6CSYK5gs2CN4SnBbCCakIOQpFCmULnRa6JjRBRVEVqfZUJnUvtZp6nTopjBVWFXYVjhPOEz4r3C88JyIkYiQSKJIhUiZySYRHQ9FUaK60BFoB7TxthPZJVFrUVjRK9IBok+iQ6KKYpJiNWJRYrliz2LDYJ3E5cUfxePHD4m3iTyXQEhoSPhLpEiclrkvMSgpLWkgyJXMlz0s+loKlNKR8pbZJnZbqk5qXlpF2lk6SPi59TXpWhiZjIxMnUyRzWWZGliprJRsrWyR7RfalnIicrVyCXIlcj9ycvJS8izxXvlK+X35JQVUhQCFLoVnhqSJRka4YrVik2K04pySr5Km0XalR6bEyQZmuHKN8TLlXeVFFVSVIZZ9Km8q0qpiqq2qmaqPqmBpFzVotWa1K7YE6Vp2uHq9+Qn1AA9Yw1ojRKNO4pwlrmmjGap7QHNTCaJlpsbWqtEa1ydq22mnajdrjOjQdD50snTad17pKuqG6h3V7db/qGesl6FXrPdEX0nfTz9Lv1H9roGHANCgzeGBIMXQy3GXYbvjGSNMoyuik0UNjqrGn8T7jbuMvJqYmHJMmkxlTJdNw03LTUbow3ZueT79phjGzM9tldtHso7mJear5efPfLLQt4i0aLKbXqa6LWle9bsJSwZJhWWnJs5KzCrc6ZcWzlrdmWFdZP7dRtIm0qbGZslW3jbM9Y/vaTs+OY9dit2hvbr/DvssB5eDskOvQ7yjkGOBY6vjMScGJ5dToNOds7LzNucsF4+Lucthl1FXalela7zrnZuq2w63Hnezu517q/txDw4Pj0ekJe7p5HvEcW6+8nr2+zQt4uXod8Xrqreqd7P2zD9bH26fM54Wvvu92314/qt9mvwa/BX87/wL/JwFqAdyA7kD+wLDA+sDFIIegwiBesG7wjuA7IRIhsSHtobjQwNCa0PkNjhuObpgMMw7LCRvZqLoxY+OtTRKbEjZd2sy/mbH5QjgmPCi8Ifwzw4tRxZiPcI0oj5hj2jOPMV9F2kQWRc5EWUYVRk1FW0YXRk+zLFlHWDMx1jHFMbOx9rGlsW/iXOIq4hbjveJr45cTghKaE/GJ4YkdbCF2PLtni8yWjC2DSZpJOUm8ZPPko8lzHHdOTQqUsjGlPVUY+Xj2cdW433HH06zSytI+pAemX8gQzGBn9G3V2Hpg61SmU+YP29DbmNu6t8tv37N9fIftjsqd0M6Ind27FHdl75rc7by7bg9xT/yeu1l6WYVZ7/cG7e3Mls7enT3xnfN3jTl8OZyc0X0W+yr2o/fH7u8/YHjg+IGvuZG5t/P08orzPucz829/r/99yffLB6MP9heYFJw8hD3EPjRy2PpwXaFgYWbhxBHPI61FckW5Re+Pbj56q9iouOIY8Rj3GK/Eo6T9uNLxQ8c/l8aUDpfZlTWXS5UfKF88EXli6KTNyaYK6Yq8ik+nYk89rHSubK1SqSo+jT2ddvpFdWB17w/0H+prJGryar7Usmt5db51PfWm9fUNUg0FjXAjt3HmTNiZgbMOZ9ubtJsqm2nNeefAOe65lz+G/zhy3v189wX6haaflH8qb6G25LZCrVtb59pi2njtIe2DHW4d3Z0WnS0/6/xce1H+YtklkUsFl4mXsy8vX8m8Mt+V1DV7lXV1ontz95Nrwdce9Pj09F93v37zhtONa722vVduWt68eMv8Vsdt+u22OyZ3WvuM+1ruGt9t6Tfpb71neq99wGygc3Dd4OUh66Gr9x3u33jg+uDO8PrhwZGAkYejYaO8h5EPpx8lPHrzOO3x0pPdY5ix3KcCT4ufST2r+kX9l2aeCe/SuMN433O/508mmBOvfk359fNk9gvKi+Ip2an6aYPpizNOMwMvN7ycfJX0amk251+C/yp/rfb6p99sfuubC56bfMN5s/w2/534u9r3Ru+7573nny0kLiwt5n4Q/1D3kf6x91PQp6ml9M+4zyVf1L90fnX/OracuLycxOAwVqMAChlwdDQAb2sBoIQg2WEAyUIb1jLX73kG+kuy+YPBC71vfG/dWi5bLRMAam0ACNgNgAeSUU4iQxlhMjKvRER/GwAbGv45fq+UaEODtXuQOUg0+bC8/E4aAFwnAF84y8tLJ5aXv1QjzT4CoCv5//b2D17LgyuFRVLyKYUVuqu4D/yz/g23l73aaf/IJQAAAAlwSFlzAAAEnQAABJ0BfDRroQAAAgRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjIwMjwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj40Njg8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KI/eBCAAAEb9JREFUeAHNmgeM1cW3x2dZECyAGAVEUQLGltg1mthAUSMoecQGJj7yVyMYC0T9g1GxgJq/aNQXMYrG/pSiWED/CogYELuURxPbUkSagCIi0uadz9n7/TH3t/cuu4Dmf3bvnZnT5sycmTPtVmyNW2NFqAi7AmKMmZqKim06hU9xGWMhA4/o+Tws0PJ48ed11VaWDtJSkOosx4Oc7JGOfHl7eNGVVlhlpS0SRyGFTR9QmcFIW597uZAvolOoBVR9pq8W3r+KJBvQn9pRDg+faOX44SkHqYx4GipTKlVl9G8DG5GlFKRy5qrMwBS/PbmUV3l08b8jstLxl6fbJv1OV1XDEXS+Gk++QYMGHrg2b94cfv7557B69erw+++/hy1btoTGjRuH3XffPTRv3jw0a9Ys7Lnnnpkslm3dujUzEJ153eD4807POKtHm9tgDUUHNgCpfMK+S7PUUQqEV984D6w74AxvB4IFWS/bV1HN6jywlZUNwp9//hmmTJ4SRr02KkyfNj2sXbvWO2TTpk2hUaNGYfWa1aFZ02ahQ4cO4bTTTgvHHntsOOyww8JBBx0U9thjj2p7TZn0umMLToFY1DAru1FGX7p0acD5bdu2zWTdcUbbVaCmo1f5vG7Zl9KFy/NS3h5fSk/la6wRMNJplZWV3hl333NPWG8zoEePHqF9+/Y+6pkFAHzrf18f1vyyJixcuNA/n3zySRg1alTo3r17OPvss8Ppp5/uTmK2AOiXMWnHglOHkK5Zsyb870svhS5du7r8lq1bLDza7NzFjlCdblyJL9WX2lyCzVHiEV2yKivN80nY8NvAQo4XVq1aFS+44IL4/PPPR5sV2xi2k1u/fn1ctGhRnDB+Qrz99ttj+w7tY9euXePLL78cq6qqovRLDWV9zLGOtpng6ffffx+feOKJCD79SDZNt0dPecmLP80Ll6Z5OuVyIDnoypOmkOLTfNGMMIFgu9nQwELSo48+6qOvb9++7jAT8jTvZZcpjGZoKR3a8hXLw5xZc8LYt8eGKVOmhJNOOslny1FHHRVatmwZGjbctkxJFxWRZ1a+9tpr4dxzz/U1CNs8tJaYFfALUhuEgw4+TUVL01Q21QlPOVqKT3WVyud1imdbLxgGJpywYMGC8OGHH4Znn33W+YjVaYdJWKkMIUWHPqwHrVu19k/HTh3Dkh+XhHlfzwuvv/56GNB/QOh0VqdwwgknhGOOOSa0bt067L333r7uuB0mO2fOHF+TWGvAaXFTvX9nqjZSp9tSqDzF18Ue9VGet4YjYPjyyy9Dt27dwj777OO7I+1a8sLlylSmCt0phVl20MG2gO+5RxgzZkz41wP/Ck2aNHGHjxs3Lnz77bfh0EMPDQcccIDLbtiwwWfEjTfe6IOAGVnfRpezb0fwtKNG/Tuwb0idmNqROQIGOpx03rx54fzzz6/ms8pqc0TeOJVTwysqK9yhM2fODJ07d/ZQww5rr732Ch07dgy2rvi2mO0xuzFGfvOmzcPB7Q72LbJsSw3P51Wv8Gn94ERXKr6dSm2SsvWWTuoE8uV8HdDzvJkjpMA7ZdWqsO+++7o8+/ydASoltE2dOjUwugkzDz74oDsBPE4Gx+fAAw+sURUzobaBUEPgL0Koc/PqhVfH5unlypITPXOERpDtYMJm+2iLKsYdSdHJh/WF8wAdOmLECD9jbNm8JVQ2qPQRpY1Avg6MRUZ0BkVFgzIDg8FYhpTXu0Nl6bc034n1dYL3tYwt2Jw5IlWe5utrdDmj3nrrrXDJJZf4ucINoUPtn7NBbVDXGUGI4F+2K61N9/ZoqQ5vF84AknrKtRee2sDthaHAV6MXGIEsooSNnQWMRN/cuXP9kHf11Ve7SneEjXaAfL4xwpEiz+HuqaeeCtOnT89kPFPiK6+rBMvfgyqM9LpWljlC3uf+iEWUqwxBXRuX8rHnRyeh7umnnw4DBgzwc4McDG/Kr7pIZQspThh0z6DQu3fv8M0336RsRXnJFCH/rkJu9DOLFU4xoba2ysTMESDoNOJ5u3btwpIlS5ynXGdJgVJVRurhxM4jjOYZM2aE3377LVx44YXOin7w+tCBfPKGYwvw8ccfh0f/59Fw//33BzvpO05faZ15O/O0PF06SqWlZH19Yo3iz+wtAisiQxugqW3ioQyUswGZbI2AURVw6uVAx1ZTSqBvD1QRelb9vCpULagKjzzyiM8EwtNuu+2WXQRKF8a3aNHCT87CIc8f+s4888wwe/bs0M4GB/dV4GQn/GmdtZWheUeZXv7rCtKfymCbQ2EmoFedz/b7l19+cbu4DOWAvGz5stBm/zZ+LsrXq7YUOUIL52GHHR7eeOMNv/Tbf//9w9Yt5mk7C5QCDFXncCUBrPttXRg+Ynh4yS7t7H4p9O3XN4x5a0xotFsjn3GsQWxXmR377bef39hyjS5wfTaKNm7cGD7//PNw5JFHuhOYJaoDHjpAZWRTOlf1hMcmuzdxHvg1qMgDpHzc8eb8FITPy0hOvLKBkPvFF1+E8ePHe99xZrrlllv89pptOXnJqvOlw+2xryKwxnj5gw8+iK+88ornzctFPGnBDInmqMhln8Vwv8DjknDTxk1x2JPD4rBhw5wdvdlna3UdeT0qy4Zp06bRY35hCM0am12middGX5w8ZXK08OeodevW+UUlF402AKK9n7gMRHNOXL58ebUdm4ttkG3UofqRsZkcZ82aRdbx3l5rMzzwAosXL/bLyffee8/zP/30U7zvvvvc9patWvolKHzISF4peKBojcBD7i0bJZx8f/zxR793YuTi8VIAnr39rP+bFSZNmuSjjhC0afOmMPXjqaFTp04uhl5Gl48+m9qMVjMm+6S64TOjw9ixYx19yimnpGTXwTsJV+4XXXSRv5ewweBG4I477vAz0EMPPRT69e3nIY86WfSh8c6Bfj7Lli0Ln332WVi+bLmXrT8yGtf6jz/+uM9G7dZkBHx8mI3Y8OSTT4auXbqG8847zw+lXA3ZgHD2kSNG+hmK9njfSkk+NYVFgKfkaXNEfOCBB6LtoJwHPF71j40oZgLAqLv++usj/AJG0Q033BAtvNQYBfnRIBnw6AYYiWZrtB2Xl8Gntn333XdOv+yyy+Iff/wR7WY33nvvvUU2IIgMMxRbGLEAs+Ldd9+NvXr1ch129+V4vuD96KOPou3ynHblP/6RzTa1HZ2Ahe/Yp3efyKwE6B+7I4t2c+CygwYNikQT8ZPmPy5oX3i2CMQoZ8yfPz8OeXBIrKqqKuJTwS7rosW/aCPLUQpjw4cPj2++8abj0CVjJFcqVWeTDh402BtjuzdnBQfILjoLR2GfXRrGkSNHeqPFs3nT5qxsIzYLb3bVEkePHh1XrlwZ7ZTvOnAqQF12XnGd02dMd9qECROcRrtkHwjCtj1+bXNCoT7eT26++ebY+ezOceGChS6LLaXar76GqWixZrZo+jDtjNFvRHv26OkLLwvqEUccEZo2bepTnRtTQsS1117rr3dmqE9t5L7++utwxRVXoLJOuxSzxac7IQO9A+8cGMyZoU2bNm4HdqEXOos4VyVsBn744QffjV166aVOT21oWNkwmMN8K96/f//wzDPP+A6u+39193BqnRzuvvtufwG0GRzGjR8XelzWw8ML4YbtssKi+oWUjczll1/uoZv3eqCyYaUfWrk9thkXbrrppsBts2jYDiBPW1Pwsn2VBUaARuCGPzb49H/xxRfjCy+84NOc0QgPoBFDntF25513+uJI2YwgKQnQ8p+HH344dmjfIa5YscJlqAMezTaLy9EemOJzzz0XyQOyFT7ZbGtAvOaaa3y2DhkyJE6aNMl5+bJdjY94Ri2zJA3Bi39c7LTJkyc7P/pUt2bi3DlznUYos/UnPvbYY14Xs80evDz02Y4v2ptKNmvy7UzLNWZE6ilGH2ACoXGTxr6As4inYNY4nQXdOsNJ9szqDz1sUZHVaIIIfwqiwUd9HCQZTYw6trboZGO5xejUwbbUnm99q8gbOiMWHmTZftu8ytTb86zPFlsb/H2FHzZQDzagn1k08YOJvhHp16+f8yL89ti3g60f/poILx/qZvNC+22Q+WbknXfeCbZjChMnTvQ7tIsvvjh0u7BbGP366MBZDFvZyrMFpy1HH3101hc1ZoZVsl0w431EMjIYeXyUT4U1Elk433yzen2AF3mBdCkVHj7ALgd9NGo2MBIlz7bwrrvucjrxHYBOvdInG5gp1qfx+OOPj3YgdF42DsC06dXbYuisH8iqfrbg4O0XK87LF3QW+FtvvdVpdkiNbO/ff//9yOy12wPntd1V7NOnT403fuQ//fRTjyjSp1R24+1dBlusQ4BXX301q1QNVCWqWKnwpHSizYZo20ZHq1NJJ1lYue6667wjLK77bkwycrbq4ocPnCPoUM4igELLr7/+Gq+66iqnyQnoR5YwQyhjwQXYLRKehg4dGu3t3mUIYYBtb6PNiEj4A5Bnkc4v7l6vjUPqeM5CKQ4F1H6lNc4RZny9wHT61CXVqsxixYIKFNMdVeOrWjb4ndRXX32VvQ6yYeBsYB3ve33eNHjftlHnb9vWeA9J+WnOlTthg/S4444rCo/mUF+0WewJP5IltNks9lte3tH5SdBtt93mGwdzajj11FPdbuxhE8Hizk+FWrVq5XjC1sJFi8KJJ57oZWxHN3oJl+TpFx7eUjBHVIe+FJnm6RyE6wUFdvO+N+Css85ycelSh+d1qh52Y7yV82ZuC34gtnMQo+M3/rkx9OjZw38JQuOpg8YK0E2Zg9iVV14ZbFSHLl26iJy15eSTT/ZDHT9WANBD/Kdj7UziuMGDB4eePXuGgQMH+i4L5OxZs52GjYcffrg7mDptxPugsNATOtvvuPgBBJ2rNiFEXodCdlUAsjgJmxlQIEoCU6YuoKlFyvQE+D0S8RwA57pMXcqrvDPZl/NYSjiwbanHY2IwUxsdhAd2LHYi97L0So/qXrlipZ8DVJZe6tEBlDx011E4lNrPhzz0sCtk1ycgpABcobDT4ioH4GwADf2sPYTUmTNmZjQy0NwOs5lQpoOxbEMnaxJQ1hFOredXVnFBLu2EvCpoKb02WWK3FlrxpbLoFl71UM7zQJMDlCdlke31372itqTg6GR3VAk9coA6tKqqKtp7SeSeC5CcF+wrtcOeiB0NjkOoHFH0AzOfMzvxZTW4tFKmXm0AXzqF4XVZU8P9lRlb/ZRaCHnWQJ/O0pnKqk7tXsu+bUu4UBc6+MloZUVlaNa8WfV22XDg/Sq+oBD9slf1Yh+h5d/v/DssXbY02CagKCyJjyrhRV7hVIfVM+2an3bVeo5IbK5TVhUrrZNQjsllCx3vCx3O4pLQ0tocm9VZj2VNMi32buFW2Ej3jhIepOoGl+KdZjhg/jfzwxlnnOH5cl/Iyn7ON/yAW05wWjnBuuI1UkgBlfPywqdpnqdUWY1XCk+al0xd9cIHpPzKa7SqrFR15FM6ll2Qxf7Q9sC2RWRkNQtwMDaD4zC69te1/kMK7aag1R47ilT/ZxUYqfUFOgJQKnk6Qh/h6pryGsfLIWENQI9AzmBXZucXf+5lJvTu07uaxa6fxL9LQ5MM+E9PaXzeGbLZOwZ/WX+W4gGnzpMMT728OhLrBfBohrE1HvrY0HDxJReHc845x/X6+pesobvUEaUMTw0jX44nj88aa53CFBZk+G0DT6RaU3Ugab6uVDCjbasyJdeYOdwh4QTuyHRGkIBt4f1Oy3ZG4Z/9/xkOOeSQbDH39aLgcPh3eteUGa7akzTrtASX5xdPffGJSs+m8tKZ8kAHL75SPPCLnsoq7zJJ50knr34cQrnkhId1g2cAfs3Or1c4WGrG4ADVndb1lzlClakRStPKwYmvvnjpU5rKS6dopNDBi68Uj/hSuTTv8jY7tT6hS1tY3mXofE72nLZ5R+HthpM2AJ/qVCpboO+0I1BSHyiqnI4h7JQIAzK2Nt15XbXx1pWW6kxl3Ak4s7BJSMMlMtBL2cwvYBApRUv179I1IlVc53wJJ9RVdnuNq6ue+vClDpCc7GDUO7B+FdpVl4MlMv8PDeP3JXoq5XkAAAAASUVORK5CYII=");
		
		// Place
		Place place = new Place();
		place.setFiscalCode("H501");
		medium.setBirthPlace(place);

		// Documnet
		Document doc = new Document();
		doc.setDocumentType(randomAlphaString(26));
		doc.setCode(randomAlphaNumericString(15));
		doc.setReleaseDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear - 10, currentYear - 1)));
		doc.setAuthority(randomAlphaString(50));
		doc.setExpirationDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear+1, currentYear+10)));
					
		
		medium.setDocument(doc);

		// Tutor
		if(UtilityService.getAge(medium.getBirthDate()) < 18) {
			Tutor tutor = new Tutor();
			
			tutor.setGivenName(randomAlphaString(20));
			tutor.setFamilyName(randomAlphaString(20));
			tutor.setBirthDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(1920, currentYear - 19)));
			tutor.setFiscalCode(randomAlphaNumericString(16));
			tutor.setAddress1(randomAlphaNumericString(50));
			tutor.setAddress2(randomAlphaNumericString(50));
			tutor.setAddress3(randomAlphaNumericString(30));
			tutor.setBirthPlace(randomAlphaString(30));
			tutor.setSign("iVBORw0KGgoAAAANSUhEUgAAAE0AAAAfCAYAAACiT0BMAAAKCWlDQ1BJQ0MgUHJvZmlsZQAASImFlgdUFNcexu/M9kZbWHrvvcMC0nuTXkVlWWCpKywsIHYkqEBEEREBRZBQFYyGGgsiigURUMCCZpEgoDyDBVBReQMkMcl7573/nHvu73x77zf/uTvnzAcAyYORlJQACwCQyE7l+DrbyQWHhMrhJgAMBIEwkACKDGZKkq23twdA6o/577UwAqCV+b72itd//v4/SzAyKoUJAOSNMJ2ZxElFeD/C/umpSSs8hrAwB2kK4bkVZq0yjF7hiDUWW13j72uPsBYAeDKDwWEBQKQjulwak4X4EIMR1mNHxrIRXvG3YsYwIhG+ibBWdAI3A+H3K2sSE7cgOkkJYbWIv3iy/uYf8ac/g8H6kxMTuMzfn2vlRMhR7AA/ZJZAhhSIBjogAXBBBpADSYADtiBKLKJEIWf/3/fRV/fZIyuTwFZkRyxggRiQiux3+ouX36pTKkgHDGRNFKJ4IJf9yv+4ZvmOtuoK0W5/05K7ADDLRUTWN42hCEDHCwCoC980xbdIO4cAuDTA5HLS1rSVowcYQAT8yBsiDmSAIlAD2sAAmAALYAMcgRvwAv4gBGwCTKTfRKSrdLAd7AE5IA8cAkdBKagAp0EdOAvOgzZwEVwFN8AdMACGwRPAA5PgFZgDC2AJgiAcRIGokDgkCylDmpABRIesIEfIA/KFQqBwiAWxIS60HdoL5UGFUClUCdVDP0Id0FXoFjQIPYLGoRnoLfQJRsFkWBiWhlVgXZgO28LusD+8EWbByXAmnA0fhEvgKvgM3Apfhe/AwzAPfgXPowCKhKKh5FHaKDrKHuWFCkVFozionahcVDGqCtWE6kT1ou6jeKhZ1Ec0Fk1Fy6G10RZoF3QAmolORu9E56NL0XXoVnQP+j56HD2H/oqhYKQwmhhzjCsmGMPCpGNyMMWYGkwL5jpmGDOJWcBisTSsKtYU64INwcZht2HzsSewzdgu7CB2AjuPw+HEcZo4S5wXjoFLxeXgjuPO4K7ghnCTuA94El4Wb4B3wofi2fgsfDG+AX8ZP4Sfwi8RBAjKBHOCFyGSsJVQQKgmdBLuESYJS0RBoirRkuhPjCPuIZYQm4jXiWPEdyQSSYFkRvIhxZJ2k0pI50g3SeOkj2QhsgbZnhxG5pIPkmvJXeRH5HcUCkWFYkMJpaRSDlLqKdcozygf+Kh8OnyufJF8u/jK+Fr5hvhe8xP4lflt+TfxZ/IX81/gv8c/K0AQUBGwF2AI7BQoE+gQGBWYF6QK6gt6CSYK5gs2CN4SnBbCCakIOQpFCmULnRa6JjRBRVEVqfZUJnUvtZp6nTopjBVWFXYVjhPOEz4r3C88JyIkYiQSKJIhUiZySYRHQ9FUaK60BFoB7TxthPZJVFrUVjRK9IBok+iQ6KKYpJiNWJRYrliz2LDYJ3E5cUfxePHD4m3iTyXQEhoSPhLpEiclrkvMSgpLWkgyJXMlz0s+loKlNKR8pbZJnZbqk5qXlpF2lk6SPi59TXpWhiZjIxMnUyRzWWZGliprJRsrWyR7RfalnIicrVyCXIlcj9ycvJS8izxXvlK+X35JQVUhQCFLoVnhqSJRka4YrVik2K04pySr5Km0XalR6bEyQZmuHKN8TLlXeVFFVSVIZZ9Km8q0qpiqq2qmaqPqmBpFzVotWa1K7YE6Vp2uHq9+Qn1AA9Yw1ojRKNO4pwlrmmjGap7QHNTCaJlpsbWqtEa1ydq22mnajdrjOjQdD50snTad17pKuqG6h3V7db/qGesl6FXrPdEX0nfTz9Lv1H9roGHANCgzeGBIMXQy3GXYbvjGSNMoyuik0UNjqrGn8T7jbuMvJqYmHJMmkxlTJdNw03LTUbow3ZueT79phjGzM9tldtHso7mJear5efPfLLQt4i0aLKbXqa6LWle9bsJSwZJhWWnJs5KzCrc6ZcWzlrdmWFdZP7dRtIm0qbGZslW3jbM9Y/vaTs+OY9dit2hvbr/DvssB5eDskOvQ7yjkGOBY6vjMScGJ5dToNOds7LzNucsF4+Lucthl1FXalela7zrnZuq2w63Hnezu517q/txDw4Pj0ekJe7p5HvEcW6+8nr2+zQt4uXod8Xrqreqd7P2zD9bH26fM54Wvvu92314/qt9mvwa/BX87/wL/JwFqAdyA7kD+wLDA+sDFIIegwiBesG7wjuA7IRIhsSHtobjQwNCa0PkNjhuObpgMMw7LCRvZqLoxY+OtTRKbEjZd2sy/mbH5QjgmPCi8Ifwzw4tRxZiPcI0oj5hj2jOPMV9F2kQWRc5EWUYVRk1FW0YXRk+zLFlHWDMx1jHFMbOx9rGlsW/iXOIq4hbjveJr45cTghKaE/GJ4YkdbCF2PLtni8yWjC2DSZpJOUm8ZPPko8lzHHdOTQqUsjGlPVUY+Xj2cdW433HH06zSytI+pAemX8gQzGBn9G3V2Hpg61SmU+YP29DbmNu6t8tv37N9fIftjsqd0M6Ind27FHdl75rc7by7bg9xT/yeu1l6WYVZ7/cG7e3Mls7enT3xnfN3jTl8OZyc0X0W+yr2o/fH7u8/YHjg+IGvuZG5t/P08orzPucz829/r/99yffLB6MP9heYFJw8hD3EPjRy2PpwXaFgYWbhxBHPI61FckW5Re+Pbj56q9iouOIY8Rj3GK/Eo6T9uNLxQ8c/l8aUDpfZlTWXS5UfKF88EXli6KTNyaYK6Yq8ik+nYk89rHSubK1SqSo+jT2ddvpFdWB17w/0H+prJGryar7Usmt5db51PfWm9fUNUg0FjXAjt3HmTNiZgbMOZ9ubtJsqm2nNeefAOe65lz+G/zhy3v189wX6haaflH8qb6G25LZCrVtb59pi2njtIe2DHW4d3Z0WnS0/6/xce1H+YtklkUsFl4mXsy8vX8m8Mt+V1DV7lXV1ontz95Nrwdce9Pj09F93v37zhtONa722vVduWt68eMv8Vsdt+u22OyZ3WvuM+1ruGt9t6Tfpb71neq99wGygc3Dd4OUh66Gr9x3u33jg+uDO8PrhwZGAkYejYaO8h5EPpx8lPHrzOO3x0pPdY5ix3KcCT4ufST2r+kX9l2aeCe/SuMN433O/508mmBOvfk359fNk9gvKi+Ip2an6aYPpizNOMwMvN7ycfJX0amk251+C/yp/rfb6p99sfuubC56bfMN5s/w2/534u9r3Ru+7573nny0kLiwt5n4Q/1D3kf6x91PQp6ml9M+4zyVf1L90fnX/OracuLycxOAwVqMAChlwdDQAb2sBoIQg2WEAyUIb1jLX73kG+kuy+YPBC71vfG/dWi5bLRMAam0ACNgNgAeSUU4iQxlhMjKvRER/GwAbGv45fq+UaEODtXuQOUg0+bC8/E4aAFwnAF84y8tLJ5aXv1QjzT4CoCv5//b2D17LgyuFRVLyKYUVuqu4D/yz/g23l73aaf/IJQAAAAlwSFlzAAAEnQAABJ0BfDRroQAAAgRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjE0NjwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4zNjg8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KvlZgpAAAE4NJREFUaAW1WXd41VWafm/uTafXBAhVIPSAKEiRvjCACqMIihR1HxVl9HEsYJnRRVCx7Lqziw+uZQcQlbFABBHQUREcOqEEUAIhtNATQiC97Pt+5/5uboK4z/6xJ9x7f79TvvOV9yvn4Ms8crDS5/PB+yDYKisr7cnni7Ax110ZenbjXBfhQwTXV1ZUQn8RERHBdT5wMhCkEyQb+uEIh0mbf0ZL0/mnJl7UF86TcaM5olmj+So9/t123jonQY3JfP01GlzJv3KOOZ4Av2baeyDgQ0VFBcrLNe5D4GqSNXuqFGVEJKT+yJHjX89VgnjCGpVrcV1zC71rbpCMaOjPU6I33ZSraeGKC9vD8RHW4S2s8Ru+3qNZbYr4IAikwJKSEmzZsh1+vx89e/ZEZGQkHCyqrah6EXH9VREmtaBgEUSYeHeCOUZtfrhApokqeuFPBsCgcpzGpPwK20sKU6vguCxcUVFuY9qwGnlNCvKjR0fHPf3Wd4W8oppuRUS0nTQe25pSWFiIuXPnYtiwEVi7dq3NuSbSQtYwLrXc28URv4opY/4aY1dNDnZ4nNfQRJD10CohKCRQNTO5KSFeQyv+t4dryGJ8CCQ0GI0VFxeLs2fPICcn1wgq9CgUXVNpVdtWbVDV91tPml/N/L8x2ZtXpSZ7ImPeiBmLLwy8IToeR6GO/4cHGaKoqAipqalIT9+HOyfeiYEDB6KcyvSFJwLnhrIqxbYvA22QJddfnb/wPgnqBKtmed81RGS3Qq4tMYVorYtjRsfjofqG7s22sS/3Tlra0z4hmhxyJK+ioFhVvWliBeeX89fHgF+J2Oja2L59JwYNvhm1akdh+eep6D+gP2NccVVMM9HMXTwhq5QgZbpP9a28N0HZea82F7zdr+Naz9dq3l4ad8+KZ/YXPlRzudyVfd6n2rC3ndYHE5QXkyWeusWv1+fWVm2m/oA/EvmXr+Bvf/tMs/Gvb76Ffv1usqTgo4tWuacUwyneno6Y+zal8NGVE86ibsStiI6OZsnhQ1lZmUNo+OL/03M4B0oCKmGqVFMNwUG6UoRr4WupHMnDrgiiShkvEGAJYVPU7xJMaWnpVfxqTVR0LDZs+A4L3n4LY8bciuHDh1tSsn04HoiI8HNhcFutCDURd5kzEAgYcQnhGHe/Ykg+vm/fftSuVQctWiShtLTEKVdokMDGaIho6MG29DYO9sptvHpPgkZy36LiYvJRAX+w/gsRCD5Yjchn8eUhSPwGApHs9NOQpQzmZ3HyZDYuXLjAOFWAmJhoJCcnk98WZmiR0h6+iEruE40zp89h0V8X2w4zZz6EhIQEoqzU5DIkhvN9lSVJSOgqLCxCXl4eGjVqSGYcOM2SjFeXLuXhkUcexuBBw/D888/ZeAWLQPMMKqxm+LhqD2NNCHd/ns+dOnUK5y+cRycKpzVlpOntHVxiPypLZBdFKY1L2UL8uXPnsHvPPuzatROrv16DHdu3hS+z52XLPsaQIUO4JorvRDWVHAhEYdOmzfjs82V44snH0bt3StCD3HLxEmHCE0HavGZTXyAyQCKbcEPvvqY4IVOVsXMdKbQQB/ZnoGXLpOByojP0FERaiHAQ0qH3qx+EnCgKvm7dOgwePJy10TpThFDnYFt9jfgX1eioaKK8FAcOHMBHH32EmX94FLffPh4vvfQSrmuXjAUL3sHKlWvw00+bsXXrZrz++nxMnHgXli9fboWrqCpenTt7Hp988infYnHbrbegdu1Yk1fjXgvFNCW5KsU54aQggg2HMg7jd6NHIT6uFt+JPgpmcKZ9z529YLR639CbwvlRyDSt6lkC2rHERHJI8pSpBVerTz1cwdhIx6TiYjSNhsrnXo43D6Xhsc7vjzAX3L1nF5avSMXSJUtwmoInd+yAeXPnYMCAgWjbth3q169LrwlQRhm8nO7ZCbXi6+DBB2egVevW6N+vL/fxYxsR+eXKT/Hkk0/bnKJiuSVdPYzjgDHiSUDuxKAHVSms4IqQdAAjho9AXGw8NyQRKY1E9Ld/389agCZNmlDgcipMhYRzGU9Jin2mEtHnU6jUsJVOoSRpQPKTweLiCpw/7wrKLl0602UiiaIyoy1k+YU6/sqoJ04ex/vvvYu3/n2BUevSuSOefe55c7ukpMSQuxYXF9i4wg2jF/y+aIwbdwe++eY7LFz4Nnqm9GBvNFZ/tdrmjR49kkqtTZnKgjqxbkej6pHMk3NlKylF1oika+bm5tK/v0CPHt1N2eGZ9MqVy9i6bSseemAGGtSrHwqqCmRWPZNB0fSU5/aqerPQ4KxkQ1Kp9s/Pz8PmLZvQrUtXNGvWzIwhZKuJXkU5xeYeG3/aiG7dUvAXKiw+NgqzZj1F1/wY99wzGc1bNCM/FSghUsrKyilThHkAIxHXVyIyKmAIVdW/YvlK5F+6jIxDGfjrog8wdeo0tG/fnqh0h3TxGd4ipAQv64QGJGgwc27YsJEpdyhatWrFPpc1y8m0Au6RI1lYtmwZbrjxBqbpqNC4IBO+URBEJrDQqYyruCg3DvgD1edS0du2bceatWtYWA6mWzVAKQ/N4U20A1zbkGP9bupLg/Y0VEy55x4Kex2nUqZyV/4o5Midy6UtYlyojY6KtUTxwX+/iw+XLsbIkSMRxb4tm7fYNkOHDkG9evWMRxnfvDGMgQhL8SRsQgYVKquqrlHG/OCD9zF06FCoFpOLqElYMZOenm7v7a8To1QNla0mI7hk4dAh2mYcKlsupeQSFxePMqbxnJwcQ6VDeQTDwRX8uP5Ho9O3Tx+WB4ptjHM0ktcUAoqpyO49ehBds5G2Ow1Tpt+LhKYJKCi4wr2kMK2SQSLIexzXx7Dfh9ycPPz9u+8xc+ZMvPDCC+jRvSteefkVnDlzFv/25n8SHG2RkpJCGbWW/DuRvK1NgQHVUmI4XJvSnZjMZm2zb98BWlJEVPOUmavIhQoLCrBz5w7c1KevxTOrz0hH86KiIkmPNRzRpDXq00fKU/mieinzcCY2btyI1atW4dX5r2Hw4EHG5ImTR7HyyzVo27oNOnXqZMr2OJbixadnkPLySFO6xrvSlaOiomjMEsY8FbIsQiqjrG48z8Rw7PgxpO3ahbVr1mHNuq9Rt35t/OU/3sTo0WPRvFkrzH/1dRzPzsBTT81Cq5atUPYrha/Hh2VPysIWhBmfpFwxtjMtDa3plq1btzZkSRGKJyoaL17MwaLFS/HM7GfQqHEjjpfT+kW4xGx36NAhXMy7iI4dk8lQM8bFi1DddezYMWzdsoVp/lO+52hTa/mX82w/P48vKhlOnD6Exx57FInNEizuKLZUllUa2i2Q09WEWDUV1motWrRkAVqGS4yHopebcxFZWdmWxL4jsrbtcK43eNAQvLNwIfr0vRFt2rTiSh+RvZ6lzddGp1+/PghEsayy0w21QuUopCDsDG1Kk8Kc4mwdXTOAKwWFrGu+wrjx4xEfX4tTWD+xFqqoiLT3Hcd32eQmTZsgfV869jPDZjHGHfj5Z65LtbHbb7+DWSkF/2Cdt3r1V444vydMmIDrr++F1m1aomNyeyQmJJIDH12rlIklzeb1IYJj42N5Iii05KAY9ssvv2D7th1MDs1Rr35DXDh/gXFoGxNGCoN5KhYtWoKDvxzGlm3OvUUopXsPi40zZ86gETsiITHByg+5n5CbmXkYI0eNRN26rsRp3KQh92M44SfUIpRvPT8lp0eyMhzOqDVZUZqNjY3DzwcO0mWGYD2toCOH4tvly5ctm2ZnZ1MJq829jh8/HqKth4SEpvwk0g0LKORBG6sVH4f77/9ni0FdunRhIdzSjjLUAxFWavEpMhBjAv9u1Hh06toG//XOO2jW3CGNHmnF6+qvvsbdk6dU2y+ley9zwYYNG6ADa7PExES0aJ5k2TORCqpXt54F9djYWFOSUFtWVsJ9lcwCXFvKc+ZGK3JPnzmFOXP+Bd26dzPkexvVDF9BpLngLqiKYFFRIY8fu1C3Tn2cOHECaXTTtF1pWLVyJeORc6vmzZtbOfDAAw9Y4JRS58+fb0xo7ZgxYzBt2nS7Im7bti0tWdfiZAFjYfapbFy5nM9Y2JiIqWOxTIzt3ZuOvPzTmPHQfAqfwKzJmtCveAvSLcOIESOw/ofvGWtPmWHS6Zp/ePQRvPjnFzBt+nRLGjH0hghaw3kvsyazqEqOouIrVvErvCi+qpVxTM+jiLSbWNyW0YDxSlB0TQ9AmleFMueuASFL7nj+3HksXvIhLufnMx7lIYOngLxLuZg8ebLWoX///pjODJXSMwUx0THmYrNmzcKkiZPIjLvh2Ju+lwF1Ph5//HFW2g+iYcOGVM4VnDp9ivfsW8y9Nm36BxRj1N588zUKO8Wsmk8lquZTWdCtWzcTppyFtJTpwodOIn6ioDtdvhfHmahOnRYZunkrNCLSinm4L69ksqLQYAxU6aH1Slwio4pRmVdN/ZGkUU6FypC1GIIqfSq/VFbZFJvjnqp/B6wUoG+rFfMItDNtJ5KSWuJo1hE88cQTvEfqR+EbmdvFxcXxLFabiNhr87t27WrWUpA+efIkvln3jfXLCN9++y3T+Bme9X6ikr6zfn0NGzacd+7zWE+1Y3bsYApT1jt0KBPvv/8uHnv0j0RgU0NWuIVVbNtJgDTEcwQF1KFcrVYtooOoUfUu5Egh+khNrrmSSnFbCcSKXYYizfVQV1TM41/Aq8m8dcHlNV7takj+3ZjHoNmzZ1l8OXr0KFJXrCRsR7Hc6GHuKriW8ppFddDevbuNWqNGjSwGVjJzbty4gSXITsau+7BkyWKcPn0muCPoplNwY58beWPRKRjE69EN4qATRRndThXC0azjNr9L12TWcCwV6JrKWOJN10KqtSpY4WcczGDsO4imrMmyjhyxNbGxMRReiBKChDB9PEndr1OYYmgFE1oMk8h5fLH8Cxbl0Rg0cADatWuLEoYmHdq1Vh5oNLScz86A6uNtirKijhhlpbrqBeNYPZw+JTeBVeO6xSgrLyZTIkb4cp0C5rBh/8QYEmfQz8m9wITxra2ZNnUK7p0+DTt27OT6+mjfoZ0F51geV4RAPy2thPLDD1vx2adfMOb1wh23T0BW5glb3759a8YjxqGKIs6P4sE9mh5Qgl9+3o+NDNjPPPeMzdPX9T2vt+f4+PigkFJaUFnkk6kthDr1C60BfxTOncnFe+99gFdem2frm/P78w0/oFO3rmYsIZrYpGocrQCLYhnQEMy5LhFokNqUFaQkBfIJE+5k1qnrXIHa1xQfY4r+syEj4yBdK9nqJlXNSg6pqV8zgD9gxy25m9K7WYrfZjPSKCwoQnb2SXzOs+x8FrRqixYvQi8q7tixLLRs3JkIam4H9shIIosW2rM7HatWfcX494bN//248Wh3XTueHArw5ZerrE9HHnmCPlrjtZAC2WHuTXfMybmAP/3pRXy0bAnuGP97Xp7G0zOW4Plnn8NCHvybJ7WgRzEmUl7d4Yn5cDqi7ZRGhbmEwKsdVuwK1BMmTDSl6AxHMEqn5gLKRnv27MG0qX0sGxLwTiskVrt2HZYrsaJrTQmipLgMBaR5+PAhovFHvPzyXBsbPOhmTJo0kQlmgM1J/XIlBg8dzJuUWkw0tTj/CFasWMES4EWb//CMhzF27CgrmLWHBLlr8iQWydnBE0lpEG1k9lea5JNSL+blmsIS6d7JHToQ9VfQuG597Nq0DUuXLsXQYcN4ldQWfpYjsSyVRM0uCPjrKc+ypwgquAoh8vW0tF14+unZnETLEUlmQcYtZSFZeP++g3Q9WpeM88LACsap0+7Ga6+/YTVO2zZtcJlnSGXizMwsrF33dyaGtSbKfffdb3fuPbp3IaoamwtmZR1lhj2B6JhIxrkCbNq8GW+/vYD104+47bbbMJUx8YbevQ0VKgc8BXTltVG3rp0taei22K7Xf0Vh6hKvWpvEGnHZJx9j4YK3MXf+q6HZ17VqgxfmzLPPpIkTLIyMGDkCt9wyFrE8/8rDDDhUvN2nSYNiRL9ngxkpMbEZ3wVrWShE21zNmGCnHEHE4mmRmQ/PBCsETJkyrWpy8GncuLE8X87jjUQ/tKFCY1g8g0ZQYamQEB0dhd69ruetyREicR6WfvShrXzjjfkYe8sYIqmRxRpldzElI0oC3X4o9rDT9V21c1iHhGHz0/Cjx4zCjTTCmdNnEc2D/A/fr8fjs/+IHp072a3x7u3bcYBIX/rxJ3h53hzcffddNHBTu53RvlSaI+ygV8nYctQ6oqPdrYLQ5TCqbh113GWebjiG8X9pdDWti8kWSUl4ae4cPDTjQeRevGhCxDAz1W9YD3Xq1GFGjLPrHCG6tLTIIO+usCusnpt+3zTePDxqe0+ZMhm33nYLbr55AJOCjxm9kJeGAUQozVoTT8xn5E3JUgb/7aZxp1ztL+WrTKmf3BFRPInUr9cAK1KXY/2mDWjBKqJLt85oxOPhGdauzz73ZyvuVXcqTusU4cvMPKh8ak3ZTceirVu2YeDNA3km5P/CBP93SVdBUtBRupLcUP+r8yoLWZUdinNqWh9+heMJo1+5j2jIOEKKISQoq4f0w4cPUzYfWrZKsqt13QR7HiD6zrB6qvEcpjMPBDXniA7/WRPCL7OYVjGsJhl2796Du+68F5cKLtg+nVlDCtD5+QU8h5eYwXv26okGDRrgfwBdpuvJ2Qp4qgAAAABJRU5ErkJggg==");
					
			// Tutor document
			doc = new Document();
			doc.setDocumentType(randomAlphaString(26));
			doc.setCode(randomAlphaNumericString(15));
			doc.setReleaseDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear - 10, currentYear - 1)));
			
			doc.setAuthority(randomAlphaString(16));
			doc.setExpirationDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear+1, currentYear+10)));
						
			tutor.setDocument(doc);
			medium.setTutor(tutor);
		}
		
		return medium;
	}
	
	/**
	 * metodo per generare un medium in base ai parametri passati
	 * @param mediumTypeID
	 * @param isMediumRequired
	 * @param parameters
	 * @return
	 */
	public Medium generateMediumFromParameters(	Integer mediumTypeID, 
												boolean isMediumRequired,
												Set<Parameter> parameters) {
		
		Integer currentYear = Calendar.getInstance().get(Calendar.YEAR);
		
		Medium medium = new Medium();

		medium.setMediumType(new Identity(mediumTypeID, null));
				
		String mediumIdentifier = randomNumericString(12);
		
		// Documnet
		Document doc =null;
				
		Date birthday = getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(1920, currentYear -1));
			
		Tutor tutor = null;
		Document tdoc = null;
		// Tutor
		if(this.utilityService.getAge(birthday) < 18) {
			tutor = new Tutor();
			// Tutor document
			 tdoc = new Document();			
									
		}
		
		// per ogni parametro fornito valorizzo la variabile del medium 
				// valorizzato nel medium passato
		for (Parameter p : parameters) {

			switch (p.getCode()) {
									
			// mapping ID -> medium.getID()
			case Utility.PARAM_ID:
				
				break;
			// mapping IDENTIFIER -> .medium.getMediumIdentifier()
			case Utility.PARAM_IDENTIFIER:
				//indeficativo va generato o si prende da una spezzata?
				if( isMediumRequired)
					medium.setMediumIdentifier(mediumIdentifier);
				break;
			// mapping  PIN -> 
			case Utility.PARAM_PIN:
				medium.setPIN(randomNumericString(6));
				break;
			// mapping GIVENNAME -> medium.getGivenName 
			case Utility.PARAM_GIVENNAME:
				medium.setGivenName(randomAlphaString(30));
				
				break;
			// mapping FAMILYNAME -> medium.getFamilyName()
			case Utility.PARAM_FAMILYNAME:
				medium.setFamilyName(randomAlphaString(30));
				break;
				// mapping BIRTHPLACE -> medium.getBirthPlace().getFiscalCode()
			case Utility.PARAM_BIRTHPLACE:
				// Place
				Place place = new Place();
				place.setFiscalCode("H501");
				medium.setBirthPlace(place);
				break;
				// mapping BIRTHDATE -> medium.getBirthDate()
			case Utility.PARAM_BIRTHDATE:
				medium.setBirthDate(birthday);
				break;
			// mapping GENDER -> medium.getBirthDate()
			case Utility.PARAM_GENDER:
				medium.setGender(Utility.GENDER_MALE);
				break;
				// mapping FISCALCODE -> medium.getFiscalCode()
			case Utility.PARAM_FISCALCODE:
				medium.setFiscalCode(randomAlphaNumericString(16));
				break;
				// mapping ADDRESS1 -> medium.getAddress1()
			case Utility.PARAM_ADDRESS1:
				medium.setAddress1(randomAlphaNumericString(50));
				break;
				// mapping ADDRESS2 -> medium.getAddress2()
			case Utility.PARAM_ADDRESS2:
				medium.setAddress2(randomAlphaNumericString(50));
				break;
				// mapping ADDRESS3 -> medium.getAddress3()
			case Utility.PARAM_ADDRESS3:
				medium.setAddress3(randomAlphaNumericString(30));
				break;
				// mapping DOCUMENT_TYPE -> medium.getDocument().getDocumentType() 
			case Utility.PARAM_DOCUMENT_TYPE:
				if(doc == null)
					doc = new Document();
				doc.setDocumentType(randomAlphaString(26));
				
				break;
				// mapping DOCUMENT_CODE -> medium.getDocument().getCode()
			case Utility.PARAM_DOCUMENT_CODE:
				if(doc == null)
					doc = new Document();
				doc.setCode(randomAlphaNumericString(15));
				break;
				// mapping  -> medium.getDocument().getReleaseDate()
			case Utility.PARAM_DOCUMENT_RELEASEDATE:
				if(doc == null)
					doc = new Document();
				doc.setReleaseDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear - 10, currentYear - 1)));
				
				break;
				// mapping  -> medium.getDocument().getAuthority()
			case Utility.PARAM_DOCUMENT_AUTHORITY:
				if(doc == null)
					doc = new Document();
				doc.setAuthority(randomAlphaString(50));
				
				break;
				// mapping  -> getDocument().getExpirationDate()
			case Utility.PARAM_DOCUMENT_EXPIRATIONDATE:
				if(doc == null)
					doc = new Document();
				doc.setExpirationDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear+1, currentYear+10)));
				break;
				// mapping  -> medium.getPhoneNumber()
			case Utility.PARAM_PHONENUMBER:
				medium.setPhoneNumber(randomAlphaNumericString(15));
				break;
				// mapping MOBILENUMBER -> medium.getMobileNumber()
			case Utility.PARAM_MOBILENUMBER:
				medium.setMobileNumber(randomAlphaNumericString(15));
				break;
				// mapping  -> medium.geteMail()
			case Utility.PARAM_EMAIL:
				medium.seteMail(randomAlphaNumericString(40));
				
				break;
				// mapping RELEASEDATE -> medium.getReleaseDate()
			case Utility.PARAM_RELEASEDATE:
				medium.setReleaseDate(getDateFormatted(25, 4, 2016));
				
				break;
				// mapping  -> medium.getValidFrom()
			case Utility.PARAM_VALIDFROM:
				medium.setValidFrom(getDateFormatted(25, 4, 2016));
				
				break;
				// mapping  -> medium.getValidTo()
			case Utility.PARAM_VALIDTO:
				medium.setValidTo(getDateFormatted(25, 4, 2020));
				
				break;
				// mapping  -> (medium.getTutor().getGivenName()
			case Utility.PARAM_TUTOR_GIVENNAME:
				tutor.setGivenName(randomAlphaString(20));
				
				break;
				// mapping  -> medium.getTutor().getFamilyName 
			case Utility.PARAM_TUTOR_FAMILYNAME:
				tutor.setFamilyName(randomAlphaString(20));
				
				break;
			// mapping  -> medium.getTutor().getBirthPlace()
			case Utility.PARAM_TUTOR_BIRTHPLACE:
				tutor.setBirthPlace(randomAlphaString(30));
				break;
				// mapping TUTOR_BIRTHDATE -> medium.getTutor().getBirthDate()
			case Utility.PARAM_TUTOR_BIRTHDATE:
				tutor.setBirthDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(1920, currentYear - 19)));
				break;
				// mapping TUTOR_FISCALCODE -> medium.getTutor().getFiscalCode()
			case Utility.PARAM_TUTOR_FISCALCODE:
				tutor.setFiscalCode(randomAlphaNumericString(16));
				break;
				// mapping TUTOR_ADDRESS1 -> medium.getTutor().getAddress1())
			case Utility.PARAM_TUTOR_ADDRESS1:
				tutor.setAddress1(randomAlphaNumericString(50));
				break;
				// mapping TUTOR_ADDRESS2 -> medium.getTutor().getAddress2()
			case Utility.PARAM_TUTOR_ADDRESS2:
				tutor.setAddress2(randomAlphaNumericString(50));
				break;
				// mapping TUTOR_ADDRESS3 -> medium.getTutor().getAddress3()
			case Utility.PARAM_TUTOR_ADDRESS3:
				tutor.setAddress3(randomAlphaNumericString(30));
				break;
				// mapping TUTOR_DOCUMENT_TYPE -> medium.getTutor().getDocument().getDocumentType()
			case Utility.PARAM_TUTOR_DOCUMENT_TYPE:
				tdoc.setDocumentType(randomAlphaString(26));
				break;
				// mapping TUTOR_DOCUMENT_CODE -> medium.getTutor().getDocument().getCode()
			case Utility.PARAM_TUTOR_DOCUMENT_CODE:
				tdoc.setCode(randomAlphaNumericString(15));
				break;
				// mapping TUTOR_DOCUMENT_RELEASEDATE -> medium.getTutor().getDocument().getReleaseDate()
			case Utility.PARAM_TUTOR_DOCUMENT_RELEASEDATE:
				tdoc.setReleaseDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear - 10, currentYear - 1)));
				break;
				// mapping TUTOR_DOCUMENT_AUTHORITY -> medium.getTutor().getDocument().getAuthority()
			case Utility.PARAM_TUTOR_DOCUMENT_AUTHORITY:
				tdoc.setAuthority(randomAlphaString(50));
				break;
				// mapping TUTOR_DOCUMENT_EXPIRATIONDATE -> medium.getTutor().getDocument().getExpirationDate()
			case Utility.PARAM_TUTOR_DOCUMENT_EXPIRATIONDATE:
				tdoc.setExpirationDate(getDateFormatted(randomInteger(30),randomInteger(12),randomInteger(currentYear+1, currentYear+10)));
				break;
				// mapping DENOMINATION -> medium.getDenomitaion())
			case Utility.PARAM_DENOMINATION:
				medium.setDenomination(randomAlphaNumericString(255));
				break;
				
			case Utility.PARAM_PHOTO:
				medium.setPhoto("iVBORw0KGgoAAAANSUhEUgAAAEoAAABSCAIAAAB8N9XQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABIzSURBVHhezVx7bJTXlb/fc1425pU+NmlCtlWfq0WbkKhJ2wDdJCpK0tASQkhV7Ua7K+UfIu0SgpIoilRtE6BKH6pUtWkhaUzs4HnZxqa7FSEPUsA2BfwYe2aM7QAGTJQHsTAGApn+7j137ty5Y5vXMPSno/F3H9+953fPuefe+803ZizeV35J9NmJHtbUFdjU58RSViztRlN2PMPiaSvezWKDXizjxPq8aD8uzHvLK2a6HGLFIGk/MRCOp6tj3Z+t3XnTS+03b3jzppfenFXXWd3QzeJdbkMP29zL4lnj3jKLmb4EaUzh041lQg1pO5mdVd/Bpl/rWC7ApoRrO55bdcOSf//mi5t98Izth3lZMsUSvMHyiJm+FEk70TR8b2b93tkNbzvQmwUcO8D/Tgnbdm3mB7ywFfAW1r9Jbgwpaf8yxExfvPjRtNecvXN9ktnTHBYIMce2ODNcSB6TgLPnVewqfFjsm/+7wdrcaUe7vDIyNNMXLJhgLLaPxbKspTewcadlWY7j2K4DRXHNbHxYXHfbxqfnefBV5NAncgoQtfhdqOjN+sav/y/4WppHJrRf0ulFi5m+YAE9r7HfrnubuQGQIVV1gO2nn3569uzZXAmQCTz11FNUUxF2wZMxnwVcOHx8xOjxUsRMTyleQ5o1Yep3W/F+1rqP2Z6iBRVhokgkcuDAAUniYtDT00OGlVQ9xqZf70f7WWMnjzdcTGUuSMz0lGLFMft50GfJ7HXr/8KdKo9wOAx6Bw8elPpeEmBw7uHwZ8d1A9OxWrqxgcrRY0ksWembX0i6Ic9CbAQ7MdNA7N13392/f//Q0JDU9JIwPj5Ozswc/967v3fyxPiuXM5KZC59HprpSSXlRLEE97CNPWE/gAGWXsTYY489BmKDg4MDAwOYbKRoWTCW++TsyTOB+BCfDrG+4GvZ/MpxwQujmZ5CGtPYQ81Z8QxWY7BS9MhuoAdu5aWHEJTLfdI8nqtKpKxEuiragc0DdnymYlOImZ5EsEWEZ379v1ZWYyFGbBMAwyNHjoAYcPr06TJzAzMQzJ0by52Bx/8jHKem5ksv74AlDd2mEjNdIm4U/sA/vfig7pO4wEwjbpcZUS4E42dzA7ncj3638ZYn/+BGh/iqi+1bibammOkJJM0HrCk7b10tKBE9xLfa2lriBs/s7++XWlwxnMqdOZ3LnTlzxvPD3sL5oeYM9rclqpaImS4RL5rCueY7G7b5TpWid9dddx06dIjowYYnTpyQWlw5nMvlPuW+6jBsVQNu7V9DiT2GqhOImdZEzLcUa0q7jT3MDlq2T9zuv/9+eCNxA95//32pwZXFOdDD5MYpg/kOmzZ7Zn0nS+w9zxbcTBeLG8WOuQOLW5BPNwmESslMmK7sEWVqnDp1yrcCzPKw9GNbwydhidoFMdPFwsfma/NAz81v/7HnAiS5wcFsNiu7rRQw/cY++gib3BALzHt563k2NGZaEz4wjZ205SdkMhlEEclMBJVz5zAnKg34C5TBTMFp2G3K+LFesZ0y9edipotlxmspRQ/NSVp5UE/UZYVBWlW59tzn17PmjBPFSd9UnouZJkmkI4nMP63+uevh0M2BlSCdTktaAhVYDKYGKYZAg1N/dWO/0LzEUc00STI9r+4tx/HUebu3t1fSyuPw4cOyn6sBzEBsvqEYggKWiq+98paIMRdGz2nsx+Gbjt4A3FKfcgCSV8stdXDl+EGfsZmf4cvYeenhiMWifaHGrOtY3L0RM10XK5uklcdlHnzKhXxcgHs6LNolGBaLkfYbBlAJZzkMCj3oghPipCNpCSBgfvIJtrtXH0JBDvhXzaa/Yk4ZdEx6VqI3ENvPn3fx5wEcWNkAyUwAhwPZ/NWGHtWvX/28wYWLkXZj3W6iE7XpNgRMyUkDHagrCTgLNisIJzKtgfTkCkPl5gGDjknPjg3c9lIr3QP4vi85aaiwZyKGQXuEANiqNJ5JRXmUsEPRkm8sjHSwqWua/Rl5B2PqRKdDNlwpSFUEMNwyVwAmBW24GIoQXma+2mnQkfT4opHgR4SaeJf+cJmeMihU5mhnIBRCIChA5moAQ+R7zKpe/t/yUYV6YCH/YK2LIuykbt34BrP4swYMybPPPqsfDgDQw4DJVisFFT8Ihn/S4s6BWoGZOJ2KR6MmvRSLZb1EivmfDYiYiSExuAEVeOhQClJeoXT6jY6O8gJ6+F/bxZo6C6ekPL0+N94ze1MbryaAXVjpxDt+/Hh5NytGaxhQz/PC4bBMC3ClBehC5uZBRxaqAHyubjd/tp0nVaDnxbsiy1ZTpUAggEMdjj+SVh6ImRdFjyrDf4xzEyXh/x988AEuVJvwQ3JFSgLokVTCLVQ0oQJqCG557new0wTWm7OhVXwlJSEJFUM2dj6QBrIhMY2feOIJfbWErhTuCDAa5ct0MQckYVK6wJQBKF+Hoscse1ZdlyJVoMfcawKW7FJ/lKIDDRlbFhh5bGzMMA5A/eEzGOTPMQy/4n0IINCjCGZR+XpNNUwrV67ERVtbGwYFlVevXi3KC+jo6ODNwe+Ys7B+W4FU/iplBXB0kvQmXO6QiSK0rpZ1qgwY9FCBtERlGI2cCqBSbD4oCV2RxEx74IEHqGjNmjVUmRqkTyTxCarK5hgyXrsYvEXA8a9b+qhJDyue5zmuOFsA6tnzyMjI1q1bMcbIVFoa9MDECAYAFQG4BkPcom+pqGjRokW4RpEaHdSkVU75CFghSfeOj4+TDuSrBniLUBKr2qwvmOseP0pA0fwXWiqoNDU1IamIKaA56liB+iCcOHGCMqFl6UZRtaYmmKoDAmR2FFEpPim5Y8cOXCDm4ZPMrkN5RND2WbhGclP0/IZ90nACxA3QA4ACDTZGWqYFqBuCzCrOVICpqVQZTQE0dOekHKpsDDGRV0B9ynf47AsWzrX0p+q1fbxQUAQlSW5wkNwSwUrNeL1dXjsPDLzM1fKVG+uQZROVKnrbt2+njhQ9AqkBIF+/vUDPcj0f9IqtV1O/h/stVdGsR0l0ie0YQgsspuhRx+iPhoByqEjcxKFydMiyiWwLH1MEKId6wYhjiFetWoUcJFEHK40ewxU9GwQd26Q3s243sxA5JSQ5QY8I4EQLehgwMJRN5hUleii6fHoAaFApTUiiR75KrVEpRpzXzoOqAZhL3P5F9BLdc1/epiYZ+Ehyg4PDw8OUqfxTtataBDC6gLKtzGVMHwsFCg+ATGsAjccffxxdoDsaL+zgUZOaJXDthZJz586VWQKiSe6BPHga9ObVvqHCJiDJCcisPFRQVvTeeecdopdMJqmI8gFKGpBlE5USH6gODrgGkGnUpHtRB0Mgs8Rmg/LFBHNNejdt3IYiIRySmQDlEDDwcEVqUUVOXIAbXdNxia4B0s8A7EOlai+mQNECqgMqcqAmXRDoXoLM0pTBvAsGqorpxVM3bdxKxQTJTEBmCf9EQzQN9Baxyj3yyCO4wJCTTpQP4LqUIW6EC5CP4Vrm5nJqe418ZRnqha4JUADkeetavgotFgt6zOcvU1wsPZhINpYHMtETKQpAJ6pDKgIoopr64o5SHJRRinuxR0EO4hauVSmgBhFATeTIhGCidpgyS7eeZTN3mnli+OIr2yeMnABciEZLaaBAlZVnEpAPDVQmDAXadK1a0C2gQEUG4O2YVzpbAIN18uRJmRDAqitbcdjs5Y+a9Oa88g4We6WmZCaA9QAqQhtoSUMrmxT0MBWpb7oRwDXqKJPiRroX18qYgBpvBVlQDGpc92EAU0B3BwA51IjHrFs2tJj0quv2cueaKLQMDAwcO3aM1MXiruihY6pMSboGqAK6X7x4MZIYGnyCHlowjEB45plnnn76aZm4VBQcxGEzNu6V3BQ9f1MXfRVBkMwEYL3+/n5YAPnYaquBnJoeQCNKNwJI6pbXYRjnEkCnSg4vUl16nHViKVhO7crUWw8KkUgE+Ti8yvYEkINhU9fEhI6eBPDBKBBkVrmBoQHQr8QdS33xTnoRPTgrJh7frQlIThpI9d7eXvgntQuNkYN8StI1PlUFAPQms1i5QCOIfgn/HO1gTdr7LvTHjqdv/J9fRPgLsRyYJ5JWHh9++CEV4VppDNMZqwUG8krzKcXo6ChG1mJumAVDySFX/5ZPXX2urj0UnE4coDT8E3YgbgBmIDiDD65pa0I0jABdeWBAKbBjNWfh66zofpc/jzboJfr8+F4WLHy7gOVOpweMjIxghw3/lA3nHUMmrhKUZ7pYpJb8h5fIwBPFE3edHn+ROHvL75v5GyyIoZYF6xlfOCtgwGTbfwcYGxujVcGx/Dl/fEvRkSL/JHh0sTelgrfebee/uHz99ddhQ8mpGB9//HHl51gpCpsVxAtsS2pL3m5RV/xpUuO+OdHdsDJqY/phR2J87azj74HefffdR9w4ArOtzef7fs95tQcbDQrxwLp16ySbYoD20aNHZSdXD6Qkj/QW+/L6Vm6hYjomPf6DoOrrFD1cSEIl6Ovrk51cPUA9BAscxAOhYCTRyV9FNuiY6USWvZpi4engR+d3eGkqlZKcioEOrpaLYk9LBuARk4W+9fK2wjZaFzM3kUVgvfa3W/idYpHHcrdgwQJjkSAgtNL5tfIgxfDpuyHEzZokf1W+iAgJ/+JSS7uxbq8hbdV3Msu3RAiF/dHQhIsE4irWD9lhBaHvwvhW2Z/hJyZ5sZNvpvlVEUkrnvqX9f9vBbygU/he+/Dhw5KWBsQY9FdJF6W+SCWc7uxw1Td+s2XS1+Pv3XO8uqGNbeopyk30ebFeK9EbuvUeagiAGSd0UWJYMUhtCMEZsATnNtmPGx5qP/j99iPBaBE9EWGzTjTrJbtkQ4IeFoNShtlstpIuqh5zANe/9AZLaFuwUlnWNrys7dDivjEvwX/8aBSDJyau6/rqrLtnzx7sPCWzPCppQFKDIgKTr3FOLpzeriPL2w7c0XUq2GgUp51Y97Uvvg5q8qwv1gmcHko3a6dPn8Ze9IrusNF74aGDoGcneqYyHURYb/jBtuEH2kdmbdoTaOhmyV7+82t4Mxf+Gw1YFRvWSP7NXELpW5CwIVD2MIMGi87jfB0HPfiTZyfFy+wGJV2IHmRJ+8i0P76NRU8cB8EKxTycOg2DVrzzmlgbC16LMQPQA30arx/DqkB5zxPgtmrVKsFKIuhjlF3L4k80Ef+KyJSKst7yXcPLd773g13v+Rs73GQW9Ggl4aEJ9RL7Qi3vyh7yWLFiBSKN/t4/kMlkymtA+CCNpkSE/TK21Ob7fvsr9W9L9SYTZT0wxOfDu4882DaCTYCgx+0uIq/4lVRTBj3Rgz0FdGys+JiWx48fl6pdHvTzjsK6P313Xet88ZaDG/nybROv5koUPV2WtB1a1PGRE+/kP2xWVfm/Suiy61NucAb/EjQfbihSNzU1wVe7u7uJITA6OnqxZkR9uoVaJsBw6MBj7NEnl65N3rum5V+f2/Lt4LQq33bCljvvt392N/UV6amLQYzk4Z3DSzuO3b3rPRbT1kM+G7Nw93+I72Vfv11zFw4YFjxhzGPHjoEbYgwWyQsMpKB06tQp9aRZX9kA3iwLMiu4tvXba7YsWtO6AAy/ctss/j0hakZucGP76CdvSgo6G8R0wWz8fv94oaomfF/aOMRs8fCU72xdaCROwTxDh6GrDvg5BkUmNIiZFrCYb9n8VZR10QU/bf7WmpaFnJiQtS13/GT9Q1U1vEP+bDYw64fth+/t+PC27Udv3nF0xqttobpO1oCok5qKHl8Pdx0yiHGhNQMxNjrEYpnvxHax8Oc9rhfY8b+XCdALT6968mc/ei42f23r7Wu23L62BRYr0IP1ftl6zw//baGozYdgWfsxoe2R5TtHIEs6eAR5sP3QeejxGoqVJuQAIurIpP/gCha5BmZkIT5baFh5EubBlRKAPmFYy8YcxlnUsn3X9kKhSKC6Gj4w/6EbQea5LXkyE0rLwrUtC5njwzfQ3qNv9v5499GH2/aT5nwV2MUvzuOcy9oOKEpTix3tDcfTNbEMc6txDnYcz7JCDkMEggvxjR3/0psLXXtcOPh/z4CKru/cufSrv9i86IXNdz/fPN8kM4lYFv9qgPOrmrZ850HYzaBwHnqLdw4ZNCYTOa1hz8j1ZKgX1j92+z03MizCIgnxAnYwLH/tzsVl8757w4uJlb+K/ufPmr+HGfXTLWS0Owwak4lv86cKaBBDiFi4vONIEYW24b8BBnBePXof7pYAAAAASUVORK5CYII=");
				break;
				// mapping  -> not found Exception
				
			case Utility.PARAM_SIGN:
				medium.setSign("iVBORw0KGgoAAAANSUhEUgAAAGIAAAAqCAYAAABIro4PAAAKCWlDQ1BJQ0MgUHJvZmlsZQAASImFlgdUFNcexu/M9kZbWHrvvcMC0nuTXkVlWWCpKywsIHYkqEBEEREBRZBQFYyGGgsiigURUMCCZpEgoDyDBVBReQMkMcl7573/nHvu73x77zf/uTvnzAcAyYORlJQACwCQyE7l+DrbyQWHhMrhJgAMBIEwkACKDGZKkq23twdA6o/577UwAqCV+b72itd//v4/SzAyKoUJAOSNMJ2ZxElFeD/C/umpSSs8hrAwB2kK4bkVZq0yjF7hiDUWW13j72uPsBYAeDKDwWEBQKQjulwak4X4EIMR1mNHxrIRXvG3YsYwIhG+ibBWdAI3A+H3K2sSE7cgOkkJYbWIv3iy/uYf8ac/g8H6kxMTuMzfn2vlRMhR7AA/ZJZAhhSIBjogAXBBBpADSYADtiBKLKJEIWf/3/fRV/fZIyuTwFZkRyxggRiQiux3+ouX36pTKkgHDGRNFKJ4IJf9yv+4ZvmOtuoK0W5/05K7ADDLRUTWN42hCEDHCwCoC980xbdIO4cAuDTA5HLS1rSVowcYQAT8yBsiDmSAIlAD2sAAmAALYAMcgRvwAv4gBGwCTKTfRKSrdLAd7AE5IA8cAkdBKagAp0EdOAvOgzZwEVwFN8AdMACGwRPAA5PgFZgDC2AJgiAcRIGokDgkCylDmpABRIesIEfIA/KFQqBwiAWxIS60HdoL5UGFUClUCdVDP0Id0FXoFjQIPYLGoRnoLfQJRsFkWBiWhlVgXZgO28LusD+8EWbByXAmnA0fhEvgKvgM3Apfhe/AwzAPfgXPowCKhKKh5FHaKDrKHuWFCkVFozionahcVDGqCtWE6kT1ou6jeKhZ1Ec0Fk1Fy6G10RZoF3QAmolORu9E56NL0XXoVnQP+j56HD2H/oqhYKQwmhhzjCsmGMPCpGNyMMWYGkwL5jpmGDOJWcBisTSsKtYU64INwcZht2HzsSewzdgu7CB2AjuPw+HEcZo4S5wXjoFLxeXgjuPO4K7ghnCTuA94El4Wb4B3wofi2fgsfDG+AX8ZP4Sfwi8RBAjKBHOCFyGSsJVQQKgmdBLuESYJS0RBoirRkuhPjCPuIZYQm4jXiWPEdyQSSYFkRvIhxZJ2k0pI50g3SeOkj2QhsgbZnhxG5pIPkmvJXeRH5HcUCkWFYkMJpaRSDlLqKdcozygf+Kh8OnyufJF8u/jK+Fr5hvhe8xP4lflt+TfxZ/IX81/gv8c/K0AQUBGwF2AI7BQoE+gQGBWYF6QK6gt6CSYK5gs2CN4SnBbCCakIOQpFCmULnRa6JjRBRVEVqfZUJnUvtZp6nTopjBVWFXYVjhPOEz4r3C88JyIkYiQSKJIhUiZySYRHQ9FUaK60BFoB7TxthPZJVFrUVjRK9IBok+iQ6KKYpJiNWJRYrliz2LDYJ3E5cUfxePHD4m3iTyXQEhoSPhLpEiclrkvMSgpLWkgyJXMlz0s+loKlNKR8pbZJnZbqk5qXlpF2lk6SPi59TXpWhiZjIxMnUyRzWWZGliprJRsrWyR7RfalnIicrVyCXIlcj9ycvJS8izxXvlK+X35JQVUhQCFLoVnhqSJRka4YrVik2K04pySr5Km0XalR6bEyQZmuHKN8TLlXeVFFVSVIZZ9Km8q0qpiqq2qmaqPqmBpFzVotWa1K7YE6Vp2uHq9+Qn1AA9Yw1ojRKNO4pwlrmmjGap7QHNTCaJlpsbWqtEa1ydq22mnajdrjOjQdD50snTad17pKuqG6h3V7db/qGesl6FXrPdEX0nfTz9Lv1H9roGHANCgzeGBIMXQy3GXYbvjGSNMoyuik0UNjqrGn8T7jbuMvJqYmHJMmkxlTJdNw03LTUbow3ZueT79phjGzM9tldtHso7mJear5efPfLLQt4i0aLKbXqa6LWle9bsJSwZJhWWnJs5KzCrc6ZcWzlrdmWFdZP7dRtIm0qbGZslW3jbM9Y/vaTs+OY9dit2hvbr/DvssB5eDskOvQ7yjkGOBY6vjMScGJ5dToNOds7LzNucsF4+Lucthl1FXalela7zrnZuq2w63Hnezu517q/txDw4Pj0ekJe7p5HvEcW6+8nr2+zQt4uXod8Xrqreqd7P2zD9bH26fM54Wvvu92314/qt9mvwa/BX87/wL/JwFqAdyA7kD+wLDA+sDFIIegwiBesG7wjuA7IRIhsSHtobjQwNCa0PkNjhuObpgMMw7LCRvZqLoxY+OtTRKbEjZd2sy/mbH5QjgmPCi8Ifwzw4tRxZiPcI0oj5hj2jOPMV9F2kQWRc5EWUYVRk1FW0YXRk+zLFlHWDMx1jHFMbOx9rGlsW/iXOIq4hbjveJr45cTghKaE/GJ4YkdbCF2PLtni8yWjC2DSZpJOUm8ZPPko8lzHHdOTQqUsjGlPVUY+Xj2cdW433HH06zSytI+pAemX8gQzGBn9G3V2Hpg61SmU+YP29DbmNu6t8tv37N9fIftjsqd0M6Ind27FHdl75rc7by7bg9xT/yeu1l6WYVZ7/cG7e3Mls7enT3xnfN3jTl8OZyc0X0W+yr2o/fH7u8/YHjg+IGvuZG5t/P08orzPucz829/r/99yffLB6MP9heYFJw8hD3EPjRy2PpwXaFgYWbhxBHPI61FckW5Re+Pbj56q9iouOIY8Rj3GK/Eo6T9uNLxQ8c/l8aUDpfZlTWXS5UfKF88EXli6KTNyaYK6Yq8ik+nYk89rHSubK1SqSo+jT2ddvpFdWB17w/0H+prJGryar7Usmt5db51PfWm9fUNUg0FjXAjt3HmTNiZgbMOZ9ubtJsqm2nNeefAOe65lz+G/zhy3v189wX6haaflH8qb6G25LZCrVtb59pi2njtIe2DHW4d3Z0WnS0/6/xce1H+YtklkUsFl4mXsy8vX8m8Mt+V1DV7lXV1ontz95Nrwdce9Pj09F93v37zhtONa722vVduWt68eMv8Vsdt+u22OyZ3WvuM+1ruGt9t6Tfpb71neq99wGygc3Dd4OUh66Gr9x3u33jg+uDO8PrhwZGAkYejYaO8h5EPpx8lPHrzOO3x0pPdY5ix3KcCT4ufST2r+kX9l2aeCe/SuMN433O/508mmBOvfk359fNk9gvKi+Ip2an6aYPpizNOMwMvN7ycfJX0amk251+C/yp/rfb6p99sfuubC56bfMN5s/w2/534u9r3Ru+7573nny0kLiwt5n4Q/1D3kf6x91PQp6ml9M+4zyVf1L90fnX/OracuLycxOAwVqMAChlwdDQAb2sBoIQg2WEAyUIb1jLX73kG+kuy+YPBC71vfG/dWi5bLRMAam0ACNgNgAeSUU4iQxlhMjKvRER/GwAbGv45fq+UaEODtXuQOUg0+bC8/E4aAFwnAF84y8tLJ5aXv1QjzT4CoCv5//b2D17LgyuFRVLyKYUVuqu4D/yz/g23l73aaf/IJQAAAAlwSFlzAAAEnQAABJ0BfDRroQAAAgRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjIwMjwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj40Njg8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KI/eBCAAAEb9JREFUeAHNmgeM1cW3x2dZECyAGAVEUQLGltg1mthAUSMoecQGJj7yVyMYC0T9g1GxgJq/aNQXMYrG/pSiWED/CogYELuURxPbUkSagCIi0uadz9n7/TH3t/cuu4Dmf3bvnZnT5sycmTPtVmyNW2NFqAi7AmKMmZqKim06hU9xGWMhA4/o+Tws0PJ48ed11VaWDtJSkOosx4Oc7JGOfHl7eNGVVlhlpS0SRyGFTR9QmcFIW597uZAvolOoBVR9pq8W3r+KJBvQn9pRDg+faOX44SkHqYx4GipTKlVl9G8DG5GlFKRy5qrMwBS/PbmUV3l08b8jstLxl6fbJv1OV1XDEXS+Gk++QYMGHrg2b94cfv7557B69erw+++/hy1btoTGjRuH3XffPTRv3jw0a9Ys7Lnnnpkslm3dujUzEJ153eD4807POKtHm9tgDUUHNgCpfMK+S7PUUQqEV984D6w74AxvB4IFWS/bV1HN6jywlZUNwp9//hmmTJ4SRr02KkyfNj2sXbvWO2TTpk2hUaNGYfWa1aFZ02ahQ4cO4bTTTgvHHntsOOyww8JBBx0U9thjj2p7TZn0umMLToFY1DAru1FGX7p0acD5bdu2zWTdcUbbVaCmo1f5vG7Zl9KFy/NS3h5fSk/la6wRMNJplZWV3hl333NPWG8zoEePHqF9+/Y+6pkFAHzrf18f1vyyJixcuNA/n3zySRg1alTo3r17OPvss8Ppp5/uTmK2AOiXMWnHglOHkK5Zsyb870svhS5du7r8lq1bLDza7NzFjlCdblyJL9WX2lyCzVHiEV2yKivN80nY8NvAQo4XVq1aFS+44IL4/PPPR5sV2xi2k1u/fn1ctGhRnDB+Qrz99ttj+w7tY9euXePLL78cq6qqovRLDWV9zLGOtpng6ffffx+feOKJCD79SDZNt0dPecmLP80Ll6Z5OuVyIDnoypOmkOLTfNGMMIFgu9nQwELSo48+6qOvb9++7jAT8jTvZZcpjGZoKR3a8hXLw5xZc8LYt8eGKVOmhJNOOslny1FHHRVatmwZGjbctkxJFxWRZ1a+9tpr4dxzz/U1CNs8tJaYFfALUhuEgw4+TUVL01Q21QlPOVqKT3WVyud1imdbLxgGJpywYMGC8OGHH4Znn33W+YjVaYdJWKkMIUWHPqwHrVu19k/HTh3Dkh+XhHlfzwuvv/56GNB/QOh0VqdwwgknhGOOOSa0bt067L333r7uuB0mO2fOHF+TWGvAaXFTvX9nqjZSp9tSqDzF18Ue9VGet4YjYPjyyy9Dt27dwj777OO7I+1a8sLlylSmCt0phVl20MG2gO+5RxgzZkz41wP/Ck2aNHGHjxs3Lnz77bfh0EMPDQcccIDLbtiwwWfEjTfe6IOAGVnfRpezb0fwtKNG/Tuwb0idmNqROQIGOpx03rx54fzzz6/ms8pqc0TeOJVTwysqK9yhM2fODJ07d/ZQww5rr732Ch07dgy2rvi2mO0xuzFGfvOmzcPB7Q72LbJsSw3P51Wv8Gn94ERXKr6dSm2SsvWWTuoE8uV8HdDzvJkjpMA7ZdWqsO+++7o8+/ydASoltE2dOjUwugkzDz74oDsBPE4Gx+fAAw+sURUzobaBUEPgL0Koc/PqhVfH5unlypITPXOERpDtYMJm+2iLKsYdSdHJh/WF8wAdOmLECD9jbNm8JVQ2qPQRpY1Avg6MRUZ0BkVFgzIDg8FYhpTXu0Nl6bc034n1dYL3tYwt2Jw5IlWe5utrdDmj3nrrrXDJJZf4ucINoUPtn7NBbVDXGUGI4F+2K61N9/ZoqQ5vF84AknrKtRee2sDthaHAV6MXGIEsooSNnQWMRN/cuXP9kHf11Ve7SneEjXaAfL4xwpEiz+HuqaeeCtOnT89kPFPiK6+rBMvfgyqM9LpWljlC3uf+iEWUqwxBXRuX8rHnRyeh7umnnw4DBgzwc4McDG/Kr7pIZQspThh0z6DQu3fv8M0336RsRXnJFCH/rkJu9DOLFU4xoba2ysTMESDoNOJ5u3btwpIlS5ynXGdJgVJVRurhxM4jjOYZM2aE3377LVx44YXOin7w+tCBfPKGYwvw8ccfh0f/59Fw//33BzvpO05faZ15O/O0PF06SqWlZH19Yo3iz+wtAisiQxugqW3ioQyUswGZbI2AURVw6uVAx1ZTSqBvD1QRelb9vCpULagKjzzyiM8EwtNuu+2WXQRKF8a3aNHCT87CIc8f+s4888wwe/bs0M4GB/dV4GQn/GmdtZWheUeZXv7rCtKfymCbQ2EmoFedz/b7l19+cbu4DOWAvGz5stBm/zZ+LsrXq7YUOUIL52GHHR7eeOMNv/Tbf//9w9Yt5mk7C5QCDFXncCUBrPttXRg+Ynh4yS7t7H4p9O3XN4x5a0xotFsjn3GsQWxXmR377bef39hyjS5wfTaKNm7cGD7//PNw5JFHuhOYJaoDHjpAZWRTOlf1hMcmuzdxHvg1qMgDpHzc8eb8FITPy0hOvLKBkPvFF1+E8ePHe99xZrrlllv89pptOXnJqvOlw+2xryKwxnj5gw8+iK+88ornzctFPGnBDInmqMhln8Vwv8DjknDTxk1x2JPD4rBhw5wdvdlna3UdeT0qy4Zp06bRY35hCM0am12middGX5w8ZXK08OeodevW+UUlF402AKK9n7gMRHNOXL58ebUdm4ttkG3UofqRsZkcZ82aRdbx3l5rMzzwAosXL/bLyffee8/zP/30U7zvvvvc9patWvolKHzISF4peKBojcBD7i0bJZx8f/zxR793YuTi8VIAnr39rP+bFSZNmuSjjhC0afOmMPXjqaFTp04uhl5Gl48+m9qMVjMm+6S64TOjw9ixYx19yimnpGTXwTsJV+4XXXSRv5ewweBG4I477vAz0EMPPRT69e3nIY86WfSh8c6Bfj7Lli0Ln332WVi+bLmXrT8yGtf6jz/+uM9G7dZkBHx8mI3Y8OSTT4auXbqG8847zw+lXA3ZgHD2kSNG+hmK9njfSkk+NYVFgKfkaXNEfOCBB6LtoJwHPF71j40oZgLAqLv++usj/AJG0Q033BAtvNQYBfnRIBnw6AYYiWZrtB2Xl8Gntn333XdOv+yyy+Iff/wR7WY33nvvvUU2IIgMMxRbGLEAs+Ldd9+NvXr1ch129+V4vuD96KOPou3ynHblP/6RzTa1HZ2Ahe/Yp3efyKwE6B+7I4t2c+CygwYNikQT8ZPmPy5oX3i2CMQoZ8yfPz8OeXBIrKqqKuJTwS7rosW/aCPLUQpjw4cPj2++8abj0CVjJFcqVWeTDh402BtjuzdnBQfILjoLR2GfXRrGkSNHeqPFs3nT5qxsIzYLb3bVEkePHh1XrlwZ7ZTvOnAqQF12XnGd02dMd9qECROcRrtkHwjCtj1+bXNCoT7eT26++ebY+ezOceGChS6LLaXar76GqWixZrZo+jDtjNFvRHv26OkLLwvqEUccEZo2bepTnRtTQsS1117rr3dmqE9t5L7++utwxRVXoLJOuxSzxac7IQO9A+8cGMyZoU2bNm4HdqEXOos4VyVsBn744QffjV166aVOT21oWNkwmMN8K96/f//wzDPP+A6u+39193BqnRzuvvtufwG0GRzGjR8XelzWw8ML4YbtssKi+oWUjczll1/uoZv3eqCyYaUfWrk9thkXbrrppsBts2jYDiBPW1Pwsn2VBUaARuCGPzb49H/xxRfjCy+84NOc0QgPoBFDntF25513+uJI2YwgKQnQ8p+HH344dmjfIa5YscJlqAMezTaLy9EemOJzzz0XyQOyFT7ZbGtAvOaaa3y2DhkyJE6aNMl5+bJdjY94Ri2zJA3Bi39c7LTJkyc7P/pUt2bi3DlznUYos/UnPvbYY14Xs80evDz02Y4v2ptKNmvy7UzLNWZE6ilGH2ACoXGTxr6As4inYNY4nQXdOsNJ9szqDz1sUZHVaIIIfwqiwUd9HCQZTYw6trboZGO5xejUwbbUnm99q8gbOiMWHmTZftu8ytTb86zPFlsb/H2FHzZQDzagn1k08YOJvhHp16+f8yL89ti3g60f/poILx/qZvNC+22Q+WbknXfeCbZjChMnTvQ7tIsvvjh0u7BbGP366MBZDFvZyrMFpy1HH3101hc1ZoZVsl0w431EMjIYeXyUT4U1Elk433yzen2AF3mBdCkVHj7ALgd9NGo2MBIlz7bwrrvucjrxHYBOvdInG5gp1qfx+OOPj3YgdF42DsC06dXbYuisH8iqfrbg4O0XK87LF3QW+FtvvdVpdkiNbO/ff//9yOy12wPntd1V7NOnT403fuQ//fRTjyjSp1R24+1dBlusQ4BXX301q1QNVCWqWKnwpHSizYZo20ZHq1NJJ1lYue6667wjLK77bkwycrbq4ocPnCPoUM4igELLr7/+Gq+66iqnyQnoR5YwQyhjwQXYLRKehg4dGu3t3mUIYYBtb6PNiEj4A5Bnkc4v7l6vjUPqeM5CKQ4F1H6lNc4RZny9wHT61CXVqsxixYIKFNMdVeOrWjb4ndRXX32VvQ6yYeBsYB3ve33eNHjftlHnb9vWeA9J+WnOlTthg/S4444rCo/mUF+0WewJP5IltNks9lte3tH5SdBtt93mGwdzajj11FPdbuxhE8Hizk+FWrVq5XjC1sJFi8KJJ57oZWxHN3oJl+TpFx7eUjBHVIe+FJnm6RyE6wUFdvO+N+Css85ycelSh+d1qh52Y7yV82ZuC34gtnMQo+M3/rkx9OjZw38JQuOpg8YK0E2Zg9iVV14ZbFSHLl26iJy15eSTT/ZDHT9WANBD/Kdj7UziuMGDB4eePXuGgQMH+i4L5OxZs52GjYcffrg7mDptxPugsNATOtvvuPgBBJ2rNiFEXodCdlUAsjgJmxlQIEoCU6YuoKlFyvQE+D0S8RwA57pMXcqrvDPZl/NYSjiwbanHY2IwUxsdhAd2LHYi97L0So/qXrlipZ8DVJZe6tEBlDx011E4lNrPhzz0sCtk1ycgpABcobDT4ioH4GwADf2sPYTUmTNmZjQy0NwOs5lQpoOxbEMnaxJQ1hFOredXVnFBLu2EvCpoKb02WWK3FlrxpbLoFl71UM7zQJMDlCdlke31372itqTg6GR3VAk9coA6tKqqKtp7SeSeC5CcF+wrtcOeiB0NjkOoHFH0AzOfMzvxZTW4tFKmXm0AXzqF4XVZU8P9lRlb/ZRaCHnWQJ/O0pnKqk7tXsu+bUu4UBc6+MloZUVlaNa8WfV22XDg/Sq+oBD9slf1Yh+h5d/v/DssXbY02CagKCyJjyrhRV7hVIfVM+2an3bVeo5IbK5TVhUrrZNQjsllCx3vCx3O4pLQ0tocm9VZj2VNMi32buFW2Ej3jhIepOoGl+KdZjhg/jfzwxlnnOH5cl/Iyn7ON/yAW05wWjnBuuI1UkgBlfPywqdpnqdUWY1XCk+al0xd9cIHpPzKa7SqrFR15FM6ll2Qxf7Q9sC2RWRkNQtwMDaD4zC69te1/kMK7aag1R47ilT/ZxUYqfUFOgJQKnk6Qh/h6pryGsfLIWENQI9AzmBXZucXf+5lJvTu07uaxa6fxL9LQ5MM+E9PaXzeGbLZOwZ/WX+W4gGnzpMMT728OhLrBfBohrE1HvrY0HDxJReHc845x/X6+pesobvUEaUMTw0jX44nj88aa53CFBZk+G0DT6RaU3Ugab6uVDCjbasyJdeYOdwh4QTuyHRGkIBt4f1Oy3ZG4Z/9/xkOOeSQbDH39aLgcPh3eteUGa7akzTrtASX5xdPffGJSs+m8tKZ8kAHL75SPPCLnsoq7zJJ50knr34cQrnkhId1g2cAfs3Or1c4WGrG4ADVndb1lzlClakRStPKwYmvvnjpU5rKS6dopNDBi68Uj/hSuTTv8jY7tT6hS1tY3mXofE72nLZ5R+HthpM2AJ/qVCpboO+0I1BSHyiqnI4h7JQIAzK2Nt15XbXx1pWW6kxl3Ak4s7BJSMMlMtBL2cwvYBApRUv179I1IlVc53wJJ9RVdnuNq6ue+vClDpCc7GDUO7B+FdpVl4MlMv8PDeP3JXoq5XkAAAAASUVORK5CYII=");
				break;
				
			case Utility.PARAM_TUTOR_SIGN:
				tutor.setSign("iVBORw0KGgoAAAANSUhEUgAAAE0AAAAfCAYAAACiT0BMAAAKCWlDQ1BJQ0MgUHJvZmlsZQAASImFlgdUFNcexu/M9kZbWHrvvcMC0nuTXkVlWWCpKywsIHYkqEBEEREBRZBQFYyGGgsiigURUMCCZpEgoDyDBVBReQMkMcl7573/nHvu73x77zf/uTvnzAcAyYORlJQACwCQyE7l+DrbyQWHhMrhJgAMBIEwkACKDGZKkq23twdA6o/577UwAqCV+b72itd//v4/SzAyKoUJAOSNMJ2ZxElFeD/C/umpSSs8hrAwB2kK4bkVZq0yjF7hiDUWW13j72uPsBYAeDKDwWEBQKQjulwak4X4EIMR1mNHxrIRXvG3YsYwIhG+ibBWdAI3A+H3K2sSE7cgOkkJYbWIv3iy/uYf8ac/g8H6kxMTuMzfn2vlRMhR7AA/ZJZAhhSIBjogAXBBBpADSYADtiBKLKJEIWf/3/fRV/fZIyuTwFZkRyxggRiQiux3+ouX36pTKkgHDGRNFKJ4IJf9yv+4ZvmOtuoK0W5/05K7ADDLRUTWN42hCEDHCwCoC980xbdIO4cAuDTA5HLS1rSVowcYQAT8yBsiDmSAIlAD2sAAmAALYAMcgRvwAv4gBGwCTKTfRKSrdLAd7AE5IA8cAkdBKagAp0EdOAvOgzZwEVwFN8AdMACGwRPAA5PgFZgDC2AJgiAcRIGokDgkCylDmpABRIesIEfIA/KFQqBwiAWxIS60HdoL5UGFUClUCdVDP0Id0FXoFjQIPYLGoRnoLfQJRsFkWBiWhlVgXZgO28LusD+8EWbByXAmnA0fhEvgKvgM3Apfhe/AwzAPfgXPowCKhKKh5FHaKDrKHuWFCkVFozionahcVDGqCtWE6kT1ou6jeKhZ1Ec0Fk1Fy6G10RZoF3QAmolORu9E56NL0XXoVnQP+j56HD2H/oqhYKQwmhhzjCsmGMPCpGNyMMWYGkwL5jpmGDOJWcBisTSsKtYU64INwcZht2HzsSewzdgu7CB2AjuPw+HEcZo4S5wXjoFLxeXgjuPO4K7ghnCTuA94El4Wb4B3wofi2fgsfDG+AX8ZP4Sfwi8RBAjKBHOCFyGSsJVQQKgmdBLuESYJS0RBoirRkuhPjCPuIZYQm4jXiWPEdyQSSYFkRvIhxZJ2k0pI50g3SeOkj2QhsgbZnhxG5pIPkmvJXeRH5HcUCkWFYkMJpaRSDlLqKdcozygf+Kh8OnyufJF8u/jK+Fr5hvhe8xP4lflt+TfxZ/IX81/gv8c/K0AQUBGwF2AI7BQoE+gQGBWYF6QK6gt6CSYK5gs2CN4SnBbCCakIOQpFCmULnRa6JjRBRVEVqfZUJnUvtZp6nTopjBVWFXYVjhPOEz4r3C88JyIkYiQSKJIhUiZySYRHQ9FUaK60BFoB7TxthPZJVFrUVjRK9IBok+iQ6KKYpJiNWJRYrliz2LDYJ3E5cUfxePHD4m3iTyXQEhoSPhLpEiclrkvMSgpLWkgyJXMlz0s+loKlNKR8pbZJnZbqk5qXlpF2lk6SPi59TXpWhiZjIxMnUyRzWWZGliprJRsrWyR7RfalnIicrVyCXIlcj9ycvJS8izxXvlK+X35JQVUhQCFLoVnhqSJRka4YrVik2K04pySr5Km0XalR6bEyQZmuHKN8TLlXeVFFVSVIZZ9Km8q0qpiqq2qmaqPqmBpFzVotWa1K7YE6Vp2uHq9+Qn1AA9Yw1ojRKNO4pwlrmmjGap7QHNTCaJlpsbWqtEa1ydq22mnajdrjOjQdD50snTad17pKuqG6h3V7db/qGesl6FXrPdEX0nfTz9Lv1H9roGHANCgzeGBIMXQy3GXYbvjGSNMoyuik0UNjqrGn8T7jbuMvJqYmHJMmkxlTJdNw03LTUbow3ZueT79phjGzM9tldtHso7mJear5efPfLLQt4i0aLKbXqa6LWle9bsJSwZJhWWnJs5KzCrc6ZcWzlrdmWFdZP7dRtIm0qbGZslW3jbM9Y/vaTs+OY9dit2hvbr/DvssB5eDskOvQ7yjkGOBY6vjMScGJ5dToNOds7LzNucsF4+Lucthl1FXalela7zrnZuq2w63Hnezu517q/txDw4Pj0ekJe7p5HvEcW6+8nr2+zQt4uXod8Xrqreqd7P2zD9bH26fM54Wvvu92314/qt9mvwa/BX87/wL/JwFqAdyA7kD+wLDA+sDFIIegwiBesG7wjuA7IRIhsSHtobjQwNCa0PkNjhuObpgMMw7LCRvZqLoxY+OtTRKbEjZd2sy/mbH5QjgmPCi8Ifwzw4tRxZiPcI0oj5hj2jOPMV9F2kQWRc5EWUYVRk1FW0YXRk+zLFlHWDMx1jHFMbOx9rGlsW/iXOIq4hbjveJr45cTghKaE/GJ4YkdbCF2PLtni8yWjC2DSZpJOUm8ZPPko8lzHHdOTQqUsjGlPVUY+Xj2cdW433HH06zSytI+pAemX8gQzGBn9G3V2Hpg61SmU+YP29DbmNu6t8tv37N9fIftjsqd0M6Ind27FHdl75rc7by7bg9xT/yeu1l6WYVZ7/cG7e3Mls7enT3xnfN3jTl8OZyc0X0W+yr2o/fH7u8/YHjg+IGvuZG5t/P08orzPucz829/r/99yffLB6MP9heYFJw8hD3EPjRy2PpwXaFgYWbhxBHPI61FckW5Re+Pbj56q9iouOIY8Rj3GK/Eo6T9uNLxQ8c/l8aUDpfZlTWXS5UfKF88EXli6KTNyaYK6Yq8ik+nYk89rHSubK1SqSo+jT2ddvpFdWB17w/0H+prJGryar7Usmt5db51PfWm9fUNUg0FjXAjt3HmTNiZgbMOZ9ubtJsqm2nNeefAOe65lz+G/zhy3v189wX6haaflH8qb6G25LZCrVtb59pi2njtIe2DHW4d3Z0WnS0/6/xce1H+YtklkUsFl4mXsy8vX8m8Mt+V1DV7lXV1ontz95Nrwdce9Pj09F93v37zhtONa722vVduWt68eMv8Vsdt+u22OyZ3WvuM+1ruGt9t6Tfpb71neq99wGygc3Dd4OUh66Gr9x3u33jg+uDO8PrhwZGAkYejYaO8h5EPpx8lPHrzOO3x0pPdY5ix3KcCT4ufST2r+kX9l2aeCe/SuMN433O/508mmBOvfk359fNk9gvKi+Ip2an6aYPpizNOMwMvN7ycfJX0amk251+C/yp/rfb6p99sfuubC56bfMN5s/w2/534u9r3Ru+7573nny0kLiwt5n4Q/1D3kf6x91PQp6ml9M+4zyVf1L90fnX/OracuLycxOAwVqMAChlwdDQAb2sBoIQg2WEAyUIb1jLX73kG+kuy+YPBC71vfG/dWi5bLRMAam0ACNgNgAeSUU4iQxlhMjKvRER/GwAbGv45fq+UaEODtXuQOUg0+bC8/E4aAFwnAF84y8tLJ5aXv1QjzT4CoCv5//b2D17LgyuFRVLyKYUVuqu4D/yz/g23l73aaf/IJQAAAAlwSFlzAAAEnQAABJ0BfDRroQAAAgRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjE0NjwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4zNjg8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KvlZgpAAAE4NJREFUaAW1WXd41VWafm/uTafXBAhVIPSAKEiRvjCACqMIihR1HxVl9HEsYJnRRVCx7Lqziw+uZQcQlbFABBHQUREcOqEEUAIhtNATQiC97Pt+5/5uboK4z/6xJ9x7f79TvvOV9yvn4Ms8crDS5/PB+yDYKisr7cnni7Ax110ZenbjXBfhQwTXV1ZUQn8RERHBdT5wMhCkEyQb+uEIh0mbf0ZL0/mnJl7UF86TcaM5olmj+So9/t123jonQY3JfP01GlzJv3KOOZ4Av2baeyDgQ0VFBcrLNe5D4GqSNXuqFGVEJKT+yJHjX89VgnjCGpVrcV1zC71rbpCMaOjPU6I33ZSraeGKC9vD8RHW4S2s8Ru+3qNZbYr4IAikwJKSEmzZsh1+vx89e/ZEZGQkHCyqrah6EXH9VREmtaBgEUSYeHeCOUZtfrhApokqeuFPBsCgcpzGpPwK20sKU6vguCxcUVFuY9qwGnlNCvKjR0fHPf3Wd4W8oppuRUS0nTQe25pSWFiIuXPnYtiwEVi7dq3NuSbSQtYwLrXc28URv4opY/4aY1dNDnZ4nNfQRJD10CohKCRQNTO5KSFeQyv+t4dryGJ8CCQ0GI0VFxeLs2fPICcn1wgq9CgUXVNpVdtWbVDV91tPml/N/L8x2ZtXpSZ7ImPeiBmLLwy8IToeR6GO/4cHGaKoqAipqalIT9+HOyfeiYEDB6KcyvSFJwLnhrIqxbYvA22QJddfnb/wPgnqBKtmed81RGS3Qq4tMYVorYtjRsfjofqG7s22sS/3Tlra0z4hmhxyJK+ioFhVvWliBeeX89fHgF+J2Oja2L59JwYNvhm1akdh+eep6D+gP2NccVVMM9HMXTwhq5QgZbpP9a28N0HZea82F7zdr+Naz9dq3l4ad8+KZ/YXPlRzudyVfd6n2rC3ndYHE5QXkyWeusWv1+fWVm2m/oA/EvmXr+Bvf/tMs/Gvb76Ffv1usqTgo4tWuacUwyneno6Y+zal8NGVE86ibsStiI6OZsnhQ1lZmUNo+OL/03M4B0oCKmGqVFMNwUG6UoRr4WupHMnDrgiiShkvEGAJYVPU7xJMaWnpVfxqTVR0LDZs+A4L3n4LY8bciuHDh1tSsn04HoiI8HNhcFutCDURd5kzEAgYcQnhGHe/Ykg+vm/fftSuVQctWiShtLTEKVdokMDGaIho6MG29DYO9sptvHpPgkZy36LiYvJRAX+w/gsRCD5Yjchn8eUhSPwGApHs9NOQpQzmZ3HyZDYuXLjAOFWAmJhoJCcnk98WZmiR0h6+iEruE40zp89h0V8X2w4zZz6EhIQEoqzU5DIkhvN9lSVJSOgqLCxCXl4eGjVqSGYcOM2SjFeXLuXhkUcexuBBw/D888/ZeAWLQPMMKqxm+LhqD2NNCHd/ns+dOnUK5y+cRycKpzVlpOntHVxiPypLZBdFKY1L2UL8uXPnsHvPPuzatROrv16DHdu3hS+z52XLPsaQIUO4JorvRDWVHAhEYdOmzfjs82V44snH0bt3StCD3HLxEmHCE0HavGZTXyAyQCKbcEPvvqY4IVOVsXMdKbQQB/ZnoGXLpOByojP0FERaiHAQ0qH3qx+EnCgKvm7dOgwePJy10TpThFDnYFt9jfgX1eioaKK8FAcOHMBHH32EmX94FLffPh4vvfQSrmuXjAUL3sHKlWvw00+bsXXrZrz++nxMnHgXli9fboWrqCpenTt7Hp988infYnHbrbegdu1Yk1fjXgvFNCW5KsU54aQggg2HMg7jd6NHIT6uFt+JPgpmcKZ9z529YLR639CbwvlRyDSt6lkC2rHERHJI8pSpBVerTz1cwdhIx6TiYjSNhsrnXo43D6Xhsc7vjzAX3L1nF5avSMXSJUtwmoInd+yAeXPnYMCAgWjbth3q169LrwlQRhm8nO7ZCbXi6+DBB2egVevW6N+vL/fxYxsR+eXKT/Hkk0/bnKJiuSVdPYzjgDHiSUDuxKAHVSms4IqQdAAjho9AXGw8NyQRKY1E9Ld/389agCZNmlDgcipMhYRzGU9Jin2mEtHnU6jUsJVOoSRpQPKTweLiCpw/7wrKLl0602UiiaIyoy1k+YU6/sqoJ04ex/vvvYu3/n2BUevSuSOefe55c7ukpMSQuxYXF9i4wg2jF/y+aIwbdwe++eY7LFz4Nnqm9GBvNFZ/tdrmjR49kkqtTZnKgjqxbkej6pHMk3NlKylF1oika+bm5tK/v0CPHt1N2eGZ9MqVy9i6bSseemAGGtSrHwqqCmRWPZNB0fSU5/aqerPQ4KxkQ1Kp9s/Pz8PmLZvQrUtXNGvWzIwhZKuJXkU5xeYeG3/aiG7dUvAXKiw+NgqzZj1F1/wY99wzGc1bNCM/FSghUsrKyilThHkAIxHXVyIyKmAIVdW/YvlK5F+6jIxDGfjrog8wdeo0tG/fnqh0h3TxGd4ipAQv64QGJGgwc27YsJEpdyhatWrFPpc1y8m0Au6RI1lYtmwZbrjxBqbpqNC4IBO+URBEJrDQqYyruCg3DvgD1edS0du2bceatWtYWA6mWzVAKQ/N4U20A1zbkGP9bupLg/Y0VEy55x4Kex2nUqZyV/4o5Midy6UtYlyojY6KtUTxwX+/iw+XLsbIkSMRxb4tm7fYNkOHDkG9evWMRxnfvDGMgQhL8SRsQgYVKquqrlHG/OCD9zF06FCoFpOLqElYMZOenm7v7a8To1QNla0mI7hk4dAh2mYcKlsupeQSFxePMqbxnJwcQ6VDeQTDwRX8uP5Ho9O3Tx+WB4ptjHM0ktcUAoqpyO49ehBds5G2Ow1Tpt+LhKYJKCi4wr2kMK2SQSLIexzXx7Dfh9ycPPz9u+8xc+ZMvPDCC+jRvSteefkVnDlzFv/25n8SHG2RkpJCGbWW/DuRvK1NgQHVUmI4XJvSnZjMZm2zb98BWlJEVPOUmavIhQoLCrBz5w7c1KevxTOrz0hH86KiIkmPNRzRpDXq00fKU/mieinzcCY2btyI1atW4dX5r2Hw4EHG5ImTR7HyyzVo27oNOnXqZMr2OJbixadnkPLySFO6xrvSlaOiomjMEsY8FbIsQiqjrG48z8Rw7PgxpO3ahbVr1mHNuq9Rt35t/OU/3sTo0WPRvFkrzH/1dRzPzsBTT81Cq5atUPYrha/Hh2VPysIWhBmfpFwxtjMtDa3plq1btzZkSRGKJyoaL17MwaLFS/HM7GfQqHEjjpfT+kW4xGx36NAhXMy7iI4dk8lQM8bFi1DddezYMWzdsoVp/lO+52hTa/mX82w/P48vKhlOnD6Exx57FInNEizuKLZUllUa2i2Q09WEWDUV1motWrRkAVqGS4yHopebcxFZWdmWxL4jsrbtcK43eNAQvLNwIfr0vRFt2rTiSh+RvZ6lzddGp1+/PghEsayy0w21QuUopCDsDG1Kk8Kc4mwdXTOAKwWFrGu+wrjx4xEfX4tTWD+xFqqoiLT3Hcd32eQmTZsgfV869jPDZjHGHfj5Z65LtbHbb7+DWSkF/2Cdt3r1V444vydMmIDrr++F1m1aomNyeyQmJJIDH12rlIklzeb1IYJj42N5Iii05KAY9ssvv2D7th1MDs1Rr35DXDh/gXFoGxNGCoN5KhYtWoKDvxzGlm3OvUUopXsPi40zZ86gETsiITHByg+5n5CbmXkYI0eNRN26rsRp3KQh92M44SfUIpRvPT8lp0eyMhzOqDVZUZqNjY3DzwcO0mWGYD2toCOH4tvly5ctm2ZnZ1MJq829jh8/HqKth4SEpvwk0g0LKORBG6sVH4f77/9ni0FdunRhIdzSjjLUAxFWavEpMhBjAv9u1Hh06toG//XOO2jW3CGNHmnF6+qvvsbdk6dU2y+ley9zwYYNG6ADa7PExES0aJ5k2TORCqpXt54F9djYWFOSUFtWVsJ9lcwCXFvKc+ZGK3JPnzmFOXP+Bd26dzPkexvVDF9BpLngLqiKYFFRIY8fu1C3Tn2cOHECaXTTtF1pWLVyJeORc6vmzZtbOfDAAw9Y4JRS58+fb0xo7ZgxYzBt2nS7Im7bti0tWdfiZAFjYfapbFy5nM9Y2JiIqWOxTIzt3ZuOvPzTmPHQfAqfwKzJmtCveAvSLcOIESOw/ofvGWtPmWHS6Zp/ePQRvPjnFzBt+nRLGjH0hghaw3kvsyazqEqOouIrVvErvCi+qpVxTM+jiLSbWNyW0YDxSlB0TQ9AmleFMueuASFL7nj+3HksXvIhLufnMx7lIYOngLxLuZg8ebLWoX///pjODJXSMwUx0THmYrNmzcKkiZPIjLvh2Ju+lwF1Ph5//HFW2g+iYcOGVM4VnDp9ivfsW8y9Nm36BxRj1N588zUKO8Wsmk8lquZTWdCtWzcTppyFtJTpwodOIn6ioDtdvhfHmahOnRYZunkrNCLSinm4L69ksqLQYAxU6aH1Slwio4pRmVdN/ZGkUU6FypC1GIIqfSq/VFbZFJvjnqp/B6wUoG+rFfMItDNtJ5KSWuJo1hE88cQTvEfqR+EbmdvFxcXxLFabiNhr87t27WrWUpA+efIkvln3jfXLCN9++y3T+Bme9X6ikr6zfn0NGzacd+7zWE+1Y3bsYApT1jt0KBPvv/8uHnv0j0RgU0NWuIVVbNtJgDTEcwQF1KFcrVYtooOoUfUu5Egh+khNrrmSSnFbCcSKXYYizfVQV1TM41/Aq8m8dcHlNV7takj+3ZjHoNmzZ1l8OXr0KFJXrCRsR7Hc6GHuKriW8ppFddDevbuNWqNGjSwGVjJzbty4gSXITsau+7BkyWKcPn0muCPoplNwY58beWPRKRjE69EN4qATRRndThXC0azjNr9L12TWcCwV6JrKWOJN10KqtSpY4WcczGDsO4imrMmyjhyxNbGxMRReiBKChDB9PEndr1OYYmgFE1oMk8h5fLH8Cxbl0Rg0cADatWuLEoYmHdq1Vh5oNLScz86A6uNtirKijhhlpbrqBeNYPZw+JTeBVeO6xSgrLyZTIkb4cp0C5rBh/8QYEmfQz8m9wITxra2ZNnUK7p0+DTt27OT6+mjfoZ0F51geV4RAPy2thPLDD1vx2adfMOb1wh23T0BW5glb3759a8YjxqGKIs6P4sE9mh5Qgl9+3o+NDNjPPPeMzdPX9T2vt+f4+PigkFJaUFnkk6kthDr1C60BfxTOncnFe+99gFdem2frm/P78w0/oFO3rmYsIZrYpGocrQCLYhnQEMy5LhFokNqUFaQkBfIJE+5k1qnrXIHa1xQfY4r+syEj4yBdK9nqJlXNSg6pqV8zgD9gxy25m9K7WYrfZjPSKCwoQnb2SXzOs+x8FrRqixYvQi8q7tixLLRs3JkIam4H9shIIosW2rM7HatWfcX494bN//248Wh3XTueHArw5ZerrE9HHnmCPlrjtZAC2WHuTXfMybmAP/3pRXy0bAnuGP97Xp7G0zOW4Plnn8NCHvybJ7WgRzEmUl7d4Yn5cDqi7ZRGhbmEwKsdVuwK1BMmTDSl6AxHMEqn5gLKRnv27MG0qX0sGxLwTiskVrt2HZYrsaJrTQmipLgMBaR5+PAhovFHvPzyXBsbPOhmTJo0kQlmgM1J/XIlBg8dzJuUWkw0tTj/CFasWMES4EWb//CMhzF27CgrmLWHBLlr8iQWydnBE0lpEG1k9lea5JNSL+blmsIS6d7JHToQ9VfQuG597Nq0DUuXLsXQYcN4ldQWfpYjsSyVRM0uCPjrKc+ypwgquAoh8vW0tF14+unZnETLEUlmQcYtZSFZeP++g3Q9WpeM88LACsap0+7Ga6+/YTVO2zZtcJlnSGXizMwsrF33dyaGtSbKfffdb3fuPbp3IaoamwtmZR1lhj2B6JhIxrkCbNq8GW+/vYD104+47bbbMJUx8YbevQ0VKgc8BXTltVG3rp0taei22K7Xf0Vh6hKvWpvEGnHZJx9j4YK3MXf+q6HZ17VqgxfmzLPPpIkTLIyMGDkCt9wyFrE8/8rDDDhUvN2nSYNiRL9ngxkpMbEZ3wVrWShE21zNmGCnHEHE4mmRmQ/PBCsETJkyrWpy8GncuLE8X87jjUQ/tKFCY1g8g0ZQYamQEB0dhd69ruetyREicR6WfvShrXzjjfkYe8sYIqmRxRpldzElI0oC3X4o9rDT9V21c1iHhGHz0/Cjx4zCjTTCmdNnEc2D/A/fr8fjs/+IHp072a3x7u3bcYBIX/rxJ3h53hzcffddNHBTu53RvlSaI+ygV8nYctQ6oqPdrYLQ5TCqbh113GWebjiG8X9pdDWti8kWSUl4ae4cPDTjQeRevGhCxDAz1W9YD3Xq1GFGjLPrHCG6tLTIIO+usCusnpt+3zTePDxqe0+ZMhm33nYLbr55AJOCjxm9kJeGAUQozVoTT8xn5E3JUgb/7aZxp1ztL+WrTKmf3BFRPInUr9cAK1KXY/2mDWjBKqJLt85oxOPhGdauzz73ZyvuVXcqTusU4cvMPKh8ak3ZTceirVu2YeDNA3km5P/CBP93SVdBUtBRupLcUP+r8yoLWZUdinNqWh9+heMJo1+5j2jIOEKKISQoq4f0w4cPUzYfWrZKsqt13QR7HiD6zrB6qvEcpjMPBDXniA7/WRPCL7OYVjGsJhl2796Du+68F5cKLtg+nVlDCtD5+QU8h5eYwXv26okGDRrgfwBdpuvJ2Qp4qgAAAABJRU5ErkJggg==");
				break;
				
			default:
				//throw new PreconditionException("Activity not valid", Utility.ErrorType.ET_PRECONDITION_ERROR.getValue());

			}

		}
		
		// credenziali //
		Set<Credential> credentials = new HashSet<Credential>();
		credentials.add(new Credential(new CredentialType(1, "BARCODE", null), mediumIdentifier));
		credentials.add(new Credential(new CredentialType(2, "RFID", null), "987654321"));
		medium.setCredentials(credentials);
		
		//aggiunta del documento
		medium.setDocument(doc);

		// Tutor
		if(this.utilityService.getAge(birthday) < 18) {
			tutor.setDocument(doc);
			medium.setTutor(tutor);
		}

		return medium;
	}
	
	
	public Operator generateOperator(Integer profileID) {
		Operator result = new Operator();
		result.setUsername("GeneretadUsername_"+TestBaseUtil.randomAlphaString(82));
		result.setPassword(TestBaseUtil.randomAlphaString(100));
		
		if(profileID != null &&  profileID != 1 )
		result.setProfileID(TestBaseUtil.randomInteger(1)+5);
			
		return result;
	}

	public Medium generateExternalMedium(String external_mediumTypeID, 
										 String external_mediumIdentifier,
										 String external_givenName, 
										 String external_familyName, 
										 String external_birthDate,
										 String external_birthPlace, 
										 String external_gender, 
										 String external_validTo) throws ParseException {
		Medium medium = new Medium();

		medium.setMediumType(new Identity(Integer.valueOf(external_mediumTypeID), null));
		medium.setMediumIdentifier(external_mediumIdentifier);
		medium.setGivenName(external_givenName);
		medium.setFamilyName(external_familyName);
		medium.setBirthDate(convertDate(external_birthDate));
		medium.setGender(external_gender);
		
		// Place
		Place place = new Place();
		place.setFiscalCode(external_birthPlace);
		medium.setBirthPlace(place);
		
		medium.setValidTo(convertDate(external_validTo));
		
		return medium;
	}
	
	public Date addDaysToToday(Integer numberDays) {
		
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        
        today.add(Calendar.DATE, numberDays);
		
		return today.getTime();
	}
	
	/**
	 * Aggiunge giorni espressi tramite numberDays alla data date 
	 * @param date data a cui aggiungere i giorni
	 * @param numberDays è il numero di giorni da aggiungere
	 * @return null if date is null
	 */
	public Date addDaysToDate(Date date, Integer numberDays) {
		
		Date result = null;
		if(date!= null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MILLISECOND, 0);
	        
	        calendar.add(Calendar.DATE, numberDays);
		
	        result = calendar.getTime();
		}
		return result;
	}
	
	/**
	 * Sottrai giorni espressi tramite numberDays alla data date 
	 * @param date data a cui sottrarre i giorni
	 * @param numberDays è il numero di giorni da sottrarre
	 * @return null if date is null
	 */
	public Date subtractDaysToDate(Date date, Integer numberDays) {
		
		Date result = null;
		if(date!= null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MILLISECOND, 0);
	        
	        calendar.add(Calendar.DATE, -numberDays);
		
	        result = calendar.getTime();
		}
		return result;
	}

	/**
	 * restituisce uno degli stati finali validi per disabilitare un medium
	 * @return lo stato cui verterà il medium dopo la disabilitazione
	 */
	public Integer getRandomDisablingState(boolean isCenMedium) {
		int maxStateId = isCenMedium ? 5 : 4;
		
		return this.rnd.nextInt(maxStateId) + 3;
	}
	
	public Date getTodayDate() {
		return this.utilityService.getTodayDate();
	}

}
