package it.integrasistemi.maigesdi.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.junit.runner.RunWith;
import org.opensaml.ws.transport.http.HTTPTransport.HTTP_VERSION;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.integrasistemi.maigesdi.MaigesdiApplication;
import it.integrasistemi.maigesdi.bean.ErrorDetails;
import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.MediumIdentifierRange;
import it.integrasistemi.maigesdi.bean.MediumPost;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.utility.SimpleDateFormatThreadSafe;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.businesslogic.UtilityService;
import it.integrasistemi.maigesdi.controller.MediumController;
import it.integrasistemi.maigesdi.controller.MediumIdentifierRangeController;
import it.integrasistemi.maigesdi.controller.MediumTypeController;
import it.integrasistemi.maigesdi.controller.OperatorController;
import it.integrasistemi.maigesdi.dao.MediumDAO;
import it.integrasistemi.maigesdi.dao.MediumIdentifierRangeDAO;
import it.integrasistemi.maigesdi.dao.MediumTypeDAO;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes=MaigesdiApplication.class)
public class ExternalCENSystemTest  extends AbstractTestNGSpringContextTests{
	
	private static final Logger logger = LoggerFactory.getLogger(ExternalCENSystemTest.class);
	private static final boolean ESEGUI_I_TEST = true;
	
	@Autowired
	private TestBaseUtil testUtil;
	
	@Autowired
	private MediumTypeController mediumTypeController;
	
	@Autowired
	private MediumController mediumController;
	
	@Autowired
	private OperatorController operatorController;
	
	@Autowired
	private MediumDAO mediumDao;

	@Autowired
	private MediumTypeDAO mediumTypeDao;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	private MediumType mediumTypeCreated;
	private List<Medium> mediumCreated = new ArrayList<Medium>();

	private Operator operatorUpdated;
	
	
	@BeforeTest
	@Parameters({"username", 
				 "password",
				 "ExternalCEN.MediumType.OrganizationID" 
				})
	public void checkParameter(String username, 
							  String password, 
							  Integer organizationID
							  ) throws Exception {
		
		if(username == null || username.isEmpty()) {
			logger.error("Parameter username not configurated in TestNG-config.xml");
			throw new Exception("Parameter username not configurated in TestNG-config.xml");
		}
		
		if(password == null || password.isEmpty()) {
			logger.error("Parameter password not configurated in TestNG-config.xml");
			throw new Exception("Parameter password not valorized in TestNG-config.xml");
		}
		if(organizationID == null || organizationID == 0) {
			logger.error("Parameter ExternalCEN.MediumType.OrganizationID not configurated in TestNG-config.xml");
			throw new Exception("Parameter ExternalCEN.MediumType.OrganizationID not configurated");
		}		
		
	}
	
	//chiamata di test al CEN
		@Test(enabled = ESEGUI_I_TEST)
		@Parameters({"username", "password"})
		public void testMAIPOLICEIsActive(String username, String password) throws Exception {
			//chiamata preliminare al CEN per capire se è attivo, si fa una chiamata a verifica motivi ostativi

			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Chiamata prelimare al CEN per evitare che il primo test vada in errore per inattività di MAIPOLICE");
			logger.info(" ------------------------------------------------------------------------------------");
			
			RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/externalSystems/{id}/authorizations", 3)
					.param("username", username)
					.param("password", password)
					.param("givenName", "Mario")
					.param("familyName","Rossi")
					.param("fiscalCodeBirthPlace", "H501")
					.param("birthDate", "1970-01-01")
					.param("gender", "M")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON);

			ResultActions resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Risultato chiamata prelimare al CEN: " + resultActions.andReturn().getResponse());
			logger.info(" ------------------------------------------------------------------------------------");
		}
	
	//GENERO IL MEDIUM TYPE
	@Test(enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
		 		 "password",
		 		 "ExternalCEN.MediumType.OrganizationID"})
	public void addMediumTypeTest(String username, 
			  					  String password, 
			  					  Integer organizationID) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumTypeTest");
		logger.info("====================================================================================");
		//ResponseEntity<?> result;
		MediumType resultMediumType;
		
		String inputMediumTypeJSON;
		String resultMediumTypeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		
		
		MediumType inputMediumType = testUtil.generateMediumType(3, organizationID);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Generate medium type for ExternalCen system");
		logger.info(" Description: Create a new mediumType with its configuration");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MediumType created ");
		logger.info(" Http status code expected:  201");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		inputMediumTypeJSON = this.objectMapper.writeValueAsString(inputMediumType);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/mediumTypes")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(inputMediumTypeJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isCreated());
				
		resultMvc = resultActions.andReturn();
		resultMediumTypeJSON = resultMvc.getResponse().getContentAsString();

		resultMediumType = objectMapper.readValue(resultMediumTypeJSON, MediumType.class);
		
		
		assertNotNull(resultMediumType.getID());
		assertThat(resultMediumType.getID() > 0);
		assertEquals(inputMediumType.getName().toUpperCase(), resultMediumType.getName().toUpperCase());
		assertEquals(inputMediumType.getNumberDaysValidity(), resultMediumType.getNumberDaysValidity());
		assertEquals(inputMediumType.getSeriesCode(), resultMediumType.getSeriesCode());
		
		assertNotNull(inputMediumType.getActivity());
		assertEquals(inputMediumType.getActivity().size(), resultMediumType.getActivity().size());	
		
		assertNotNull(inputMediumType.getExternalSystem());
		assertEquals(inputMediumType.getExternalSystem().getID(), resultMediumType.getExternalSystem().getID());	
		
		
		this.mediumTypeCreated = resultMediumType;
		
	}
	
	//AGGIORNO L'UTENTE CONFIGURATO
	@Test(dependsOnMethods="addMediumTypeTest", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
		 		 "password"
				})
	public void updateOperator(String username, 
			  					  String password
							  ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start updateOperator");
		logger.info("====================================================================================");
		
		//ResponseEntity<?> result;
		Operator storeOperator;
		List<Operator> listOperator;
		Operator resultOperator;
		
		String storedOperatorJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultOperatorJSON;
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get all operators");
		logger.info(" Description: get alla operators from system, from this set it choose current operator");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: All Operators returned correctly");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		
		//reperisco l'operatore nel sistema 
		//result = this.operatorController.getOperators(username, password, null) ;
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/operators")
				.param("username", username)
				.param("password", password)
				.contentType(MediaType.APPLICATION_JSON);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();
		
		listOperator = Arrays.asList(objectMapper.readValue(resultOperatorJSON, Operator[].class));

//		logger.info("------------------------------------------------------------------------------------");
//		logger.info(" Response operatorController.getOperators");
//		logger.info("------------------------------------------------------------------------------------");
//		logger.info(" Result: "+result);
//		logger.info("------------------------------------------------------------------------------------");
//		
//		assertNotNull(result);
//		assertEquals(HttpStatus.OK, result.getStatusCode());
//		
//		listOperator = (List<Operator>) result.getBody();
		
		assertNotNull(listOperator);
		assertThat(listOperator.size() > 0);
		
		//ricerco l'peratore
		storeOperator = listOperator.stream().filter(x -> x.getUsername().equals(username)).findFirst().get();
		
		assertNotNull(storeOperator);
		assertEquals(username, storeOperator.getUsername());
		
		storeOperator.getMediumTypes().add(new Identity(this.mediumTypeCreated.getID(), null));
		storeOperator.setPassword(password);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update operator rights after medium type creation");
		logger.info(" Description: Add rights to current operator to handle new medium type");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected:  Operator updated");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");

		
		//result = this.operatorController.updateOperator(username, password, null, storeOperator);

		storedOperatorJSON = this.objectMapper.writeValueAsString(storeOperator);
		
		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/operators")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(storedOperatorJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();

		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);
		
		assertNotNull(resultOperator);
		assertEquals(storeOperator.getID(), resultOperator.getID());
		assertEquals(storeOperator.getUsername(), resultOperator.getUsername());
		assertEquals(storeOperator.getMediumTypes().size(), resultOperator.getMediumTypes().size());
		assertEquals(storeOperator.getProfileID(), resultOperator.getProfileID());
		
		resultOperator.setPassword(password);
		this.operatorUpdated = resultOperator;
		
	}
	
	
	//CREO I MEDIUM
	
	//Creo il medium generando l'identificativo
	@Test(dependsOnMethods="updateOperator", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password",
				 "ExternalCEN.MediumType.ID",
				 "ExternalCEN.Medium.Identifier",
				 "ExternalCEN.Medium.GivenName" ,
				 "ExternalCEN.Medium.FamilyName",
				 "ExternalCEN.Medium.Birthdate" ,
				 "ExternalCEN.Medium.BirthPlace",
				 "ExternalCEN.Medium.Gender"    ,
				 "ExternalCEN.Medium.ValidTo"
				})
	public void addMediumPhisicalPersonWithMediumIdenfier(String username, 
														  String password,
														  String external_mediumTypeID,
														  String external_mediumIdentifier, 
														  String external_givenName,
														  String external_familyName,
														  String external_birthDate,
														  String external_birthPlace,
														  String external_gender,
														  String external_validTo
														 ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumPhisicalPersonWithMediumIdenfier");
		logger.info("====================================================================================");
//		ResponseEntity<?> result;
		Medium mediumResult;
		ErrorDetails errorResult;
		List<Medium> listMediumsResult;
		

		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		MediumPost mediumPost = new MediumPost();
		mediumPost.setActivityCode("NEW_PHISICAL_PERSON");

		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Creation medium from another system (External CEN)");
		logger.info(" Description: Medium generated programatically and not valid for this activity ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected:  CEN error because used medium doesn't exist on CEN");
		logger.info(" Http status code expected:  500");
		logger.info(" MAIGeSDi error code expected: 501 ");
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Request mediumController.handleMediumActivity");
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Username: "+username);
		logger.info(" Password: "+password);
		logger.info(" MediumPost: "+mediumPost);
		logger.info("------------------------------------------------------------------------------------");
				
		//result = mediumController.handleMediumActivity(username, password ,null, mediumPost);
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isInternalServerError());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();

		errorResult = objectMapper.readValue(resultJSON, ErrorDetails.class);
		
		assertEquals(errorResult.getCode(), Utility.ErrorType.ET_CEN_EXCEPTION.getValue());
		
		//gestire la versione con supporto esistente fornito
		//il supporto esterno esistente di test è stato fornito?
		if(external_mediumIdentifier != null && !external_mediumIdentifier.isEmpty() && 
		   external_givenName != null && !external_givenName.isEmpty() &&      
		   external_familyName != null && !external_familyName.isEmpty() &&     
		   external_birthDate != null && TestBaseUtil.isValidDate(external_birthDate) &&
		   external_birthPlace != null && !external_birthPlace.isEmpty() &&      
		   external_gender != null && !external_gender.isEmpty() &&          
		   external_validTo != null && TestBaseUtil.isValidDate(external_validTo) 
		   ) 
		{
			medium = this.testUtil.generateExternalMedium(external_mediumTypeID,
														 external_mediumIdentifier, 
													     external_givenName,
													     external_familyName,
													     external_birthDate,
													     external_birthPlace,
													     external_gender,
													     external_validTo);
			
			mediumPost.setMedium(medium);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Insert an existing medium fron another system an recognized from CEN (External CEN)");
			logger.info(" Description: Creation an existing medium created outside MAIGeSDi from another competitor system ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium valid for this activity, the medium will saved on DB");
			logger.info(" Http status code expected:  200");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			
			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertThat(listMediumsResult.size()> 0);
			mediumResult = listMediumsResult.get(0);
			
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			
			//this.mediumCreated.add(mediumResult);
			
			assertEquals(external_mediumIdentifier 				, mediumResult.getMediumIdentifier() );
			assertEquals(external_givenName						, mediumResult.getGivenName() );
			assertEquals(external_familyName	 				, mediumResult.getFamilyName() );
			
			assertThat(TestBaseUtil.convertDate(external_birthDate).compareTo(medium.getBirthDate()));
			assertThat(TestBaseUtil.convertDate(external_validTo).compareTo(medium.getValidTo()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(external_birthPlace 					, mediumResult.getBirthPlace().getFiscalCode() );
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Delete medium from DB for next test;" + mediumResult);
			logger.info("------------------------------------------------------------------------------------");
			this.mediumDao.deleteMedium(mediumResult);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: check medium is correctly deleted");
			logger.info(" Description: check if medium, previously inserted and deleted, is actually delete  ");
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium not found ");
			logger.info(" Http status code expected: 404 ");
			logger.info(" MAIGeSDi error code expected: 203");
			logger.info("------------------------------------------------------------------------------------");
					
//			result = mediumController.getMedium(username, password ,null, mediumResult.getID());
//			
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/{mediumID}", mediumResult.getID() )
					.param("username", username)
					.param("password", password);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isNotFound());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();
			errorResult = objectMapper.readValue(resultJSON, ErrorDetails.class);
			
			assertEquals(errorResult.getCode(), Utility.ErrorType.ET_MEDIUM_NOT_FOUND.getValue());
			
			
			
		}else {
			logger.info("------------------------------------------------------------------------------------");
			logger.info("Insert external medium test avoided, cause medium not configurated in TestNG-config.xml");
			logger.info("------------------------------------------------------------------------------------");
			
			
		}
	
	}
	
		
		
	//CREO I MEDIUM
	
	//Creo il medium da web (con controllo richiesto e stato finale "in attesa conferma BO") generando l'identificativo
	@Test(dependsOnMethods="addMediumPhisicalPersonWithMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password",
				 "ExternalCEN.MediumType.ID",
				 "ExternalCEN.Medium.Identifier",
				 "ExternalCEN.Medium.GivenName" ,
				 "ExternalCEN.Medium.FamilyName",
				 "ExternalCEN.Medium.Birthdate" ,
				 "ExternalCEN.Medium.BirthPlace",
				 "ExternalCEN.Medium.Gender"    ,
				 "ExternalCEN.Medium.ValidTo"})
	public void addMediumFromWebPhisicalPersonWithMediumIdenfier(String username, 
																  String password,
																  String external_mediumTypeID,
																  String external_mediumIdentifier, 
																  String external_givenName,
																  String external_familyName,
																  String external_birthDate,
																  String external_birthPlace,
																  String external_gender,
																  String external_validTo
														 		) throws ParseException, Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumFromWebPhisicalPersonWithMediumIdenfier");
		logger.info("====================================================================================");
		
		Medium mediumResult;
		ErrorDetails errorResult;
		List<Medium> listMediumsResult;
		MediumPost mediumPost = new MediumPost();
		mediumPost.setActivityCode("NEW_WEB_PHISICAL_PERSON");

		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		
		Medium medium = this.testUtil.getMediumTestPhisicalPerson(this.mediumTypeCreated, true);
		logger.debug("Medium generated: "+medium);
		mediumPost.setMedium(medium);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Creation web medium, this medium was created from another system (External CEN)");
		logger.info(" Description: Medium generated programatically and not valid for this activity ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected:  CEN error because used medium doesn't exist on CEN");
		logger.info(" Http status code expected:  500");
		logger.info(" MAIGeSDi error code expected: 501 ");
		logger.info("------------------------------------------------------------------------------------");
		
		mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(mediumPostJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isInternalServerError());
				
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();

		errorResult = objectMapper.readValue(resultJSON, ErrorDetails.class);
			
		assertEquals(errorResult.getCode(), Utility.ErrorType.ET_CEN_EXCEPTION.getValue());
		
		//gestire la versione con supporto esistente fornito
		//il supporto esterno esistente di test è stato fornito?
		if(external_mediumIdentifier != null && !external_mediumIdentifier.isEmpty() && 
		   external_givenName != null && !external_givenName.isEmpty() &&      
		   external_familyName != null && !external_familyName.isEmpty() &&     
		   external_birthDate != null && TestBaseUtil.isValidDate(external_birthDate) &&
		   external_birthPlace != null && !external_birthPlace.isEmpty() &&      
		   external_gender != null && !external_gender.isEmpty() &&          
		   external_validTo != null && TestBaseUtil.isValidDate(external_validTo) 
		   ) 
		{
			medium = this.testUtil.generateExternalMedium(external_mediumTypeID,
														 external_mediumIdentifier, 
													     external_givenName,
													     external_familyName,
													     external_birthDate,
													     external_birthPlace,
													     external_gender,
													     external_validTo);
			
			mediumPost.setMedium(medium);
			
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Insert web an existing medium fron another system an recognized from CEN (External CEN)");
			logger.info(" Description: Creation an existing medium from web,this medium was created outside MAIGeSDi from another competitor system ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium valid for this activity, the medium will saved on DB");
			logger.info(" Http status code expected:  200");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
		
			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertThat(listMediumsResult.size()> 0);
			mediumResult = listMediumsResult.get(0);
//			
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			
			assertNotNull(this.mediumCreated);
			
			this.mediumCreated.add(mediumResult);
			
			assertEquals(external_mediumIdentifier 				, mediumResult.getMediumIdentifier() );
			assertEquals(external_givenName						, mediumResult.getGivenName() );
			assertEquals(external_familyName	 				, mediumResult.getFamilyName() );
			
			assertThat(TestBaseUtil.convertDate(external_birthDate).compareTo(medium.getBirthDate()));
			assertThat(TestBaseUtil.convertDate(external_validTo).compareTo(medium.getValidTo()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(external_birthPlace 					, mediumResult.getBirthPlace().getFiscalCode() );
			
			
		}else {
			logger.info("------------------------------------------------------------------------------------");
			logger.info("Insert web for external medium test avoided, cause medium not configurated in TestNG-config.xml");
			logger.info("------------------------------------------------------------------------------------");
			
			
		}
	

	}
	
	
	@Test(dependsOnMethods="addMediumFromWebPhisicalPersonWithMediumIdenfier", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void searchMedium(String username, 
		  	 							 String password
		 								) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start searchMedium");
		logger.info("====================================================================================");
//		ResponseEntity<?> result;
		Medium mediumResult;
		Medium inputMedium;
		List<Medium> listMediumsResult;
		MediumPost mediumPost = new MediumPost();
		
		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		
		if(this.mediumCreated !=null && this.mediumCreated.size() > 0) {
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Integer mediumID = this.mediumCreated.get(0).getID();
			
			mediumPost.setActivityCode("SEARCH");
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: GET medium by ID");
			logger.info(" Description: GET medium by ID, inserted on MAIGeSDi but created outside ");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected:  Medium correctly returned");
			logger.info(" Http status code expected:  200");
			logger.info(" MAIGeSDi error code expected: no error ");
		    logger.info("------------------------------------------------------------------------------------");

	
			requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/medium/{mediumID}", mediumID)
					.param("username", username)
					.param("password", password);

			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			resultActions.andExpect(status().isNotFound());
			resultMvc = resultActions.andReturn();
			resultJSON = resultMvc.getResponse().getContentAsString();
			mediumResult = objectMapper.readValue(resultJSON, Medium.class);
			
			//ricerca medium a partire dalla sestupletta
			inputMedium = new Medium();
			inputMedium.setGivenName(mediumResult.getGivenName());
			inputMedium.setFamilyName(mediumResult.getFamilyName());
			inputMedium.setBirthDate(mediumResult.getBirthDate());
			inputMedium.setBirthPlace(mediumResult.getBirthPlace());
			inputMedium.setGender(mediumResult.getGender());
			inputMedium.setMediumType(mediumResult.getMediumType());
			inputMedium.setMediumIdentifier(mediumResult.getMediumIdentifier());
			
		    mediumPost.setMedium(inputMedium);
		    
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: Search a medium previously inserted on MAIGeSDi but created outside ");
			logger.info(" Description: search medium created in previously test using custom filter (sextain)");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected:  Medium correctly found");
			logger.info(" Http status code expected:  200");
			logger.info(" MAIGeSDi error code expected: no error ");
		    logger.info("------------------------------------------------------------------------------------");
//			logger.info(" Request mediumController.handleMediumActivity");
//			logger.info("------------------------------------------------------------------------------------");
//			logger.info(" Username: "+username);
//			logger.info(" Password: "+password);
//			logger.info(" MediumPost: "+mediumPost);
//			logger.info("------------------------------------------------------------------------------------");
			
//		    result = this.mediumController.handleMediumActivity(username, password , null, mediumPost);
	
			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
		    
//			logger.info("------------------------------------------------------------------------------------");
//			logger.info(" Response mediumController.handleMediumActivity");
//			logger.info("------------------------------------------------------------------------------------");
//			logger.info(" Result: "+result);
//			logger.info("------------------------------------------------------------------------------------");
//				
//		    assertNotNull(result);
//			assertEquals(HttpStatus.OK, result.getStatusCode());
//		    listMediumsResult = (List<Medium>) result.getBody();
		    
			assertThat(listMediumsResult.size() >= 1);
		    assertThat(listMediumsResult.stream().anyMatch(t -> t.getID().equals(mediumID)));
		}else {
			logger.info("------------------------------------------------------------------------------------");
			logger.info("Serch medium for external CEN medium test avoided, cause medium not configurated in TestNG-config.xml");
			logger.info("------------------------------------------------------------------------------------");
			
		}
	} 
	

	@Test(dependsOnMethods="searchMedium", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"})
	public void verifyMedium(String username, 
		  	 							 String password
		 								) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start verifyMedium");
		logger.info("====================================================================================");
		//ResponseEntity<?> result;
		Medium mediumResult;

		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		if(this.mediumCreated !=null && this.mediumCreated.size() > 0) {
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
						
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_ATTIVO).findFirst().get();
			mediumPost.setActivityCode("VERIFY");
			mediumPost.setMedium(medium);
		    
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: verify existing medium on MAIGeSDi DB and on CEN ");
			logger.info(" Description: this test check if this medium (previously inserted) really exist on Db and on CEN System");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium correctly on db and on CEN system ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");

			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));

		    assertThat(listMediumsResult.size() >= 1);
		    assertThat(listMediumsResult.stream().anyMatch(t -> t.getID().equals(medium.getID())));
		}else {
			logger.info("------------------------------------------------------------------------------------");
			logger.info("Verify medium, for external CEN medium, test avoided, cause medium not configurated in TestNG-config.xml");
			logger.info("------------------------------------------------------------------------------------");
			
		}
	} 
	
	
	
	@Test(dependsOnMethods="verifyMedium", enabled = ESEGUI_I_TEST)
	@Parameters({"username", 
				 "password"
				})
	public void disableMedium(String username,
							  String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start disableMedium");
		logger.info("====================================================================================");
		//ResponseEntity<?> result;
		Medium mediumResult;

		String mediumPostJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		Integer disableStateID;
		
		if(this.mediumCreated !=null && this.mediumCreated.size() > 0) {
			List<Medium> listMediumsResult;
			MediumPost mediumPost = new MediumPost();
			
			assertNotNull(this.mediumCreated);
			assertNotNull(this.mediumCreated.get(0));
			assertThat(this.mediumCreated.get(0).getID() > 0);
			
			Medium medium = this.mediumCreated.stream().filter(x -> x.getState().getID() == Utility.TSSD_ATTIVO).findFirst().get();
			
			mediumPost.setActivityCode("DISABLE");
			
			logger.debug("Medium taken from list of generated: "+medium);
			
			disableStateID = testUtil.getRandomDisablingState(true);
			medium.setState(new Identity(disableStateID, null));
			mediumPost.setMedium(medium);
			
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Test: disable a medium handled outside on MAIGeSDi ");
			logger.info(" Description: disable a medium no longer valid on CEN");
			logger.info(" ------------------------------------------------------------------------------------");
			logger.info(" Result expected: Medium disabled ");
			logger.info(" Http status code expected: 200 ");
			logger.info(" MAIGeSDi error code expected: no error ");
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Request mediumController.handleMediumActivity");
			logger.info("------------------------------------------------------------------------------------");
			logger.info(" Username: "+username);
			logger.info(" Password: "+password);
			logger.info(" MediumPost: "+mediumPost);
			logger.info("------------------------------------------------------------------------------------");
			
			//result = mediumController.handleMediumActivity(username, password ,null, mediumPost);
	
			mediumPostJSON = this.objectMapper.writeValueAsString(mediumPost);
			
			requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium")
													.param("username", username)
													.param("password", password)
													.accept(MediaType.APPLICATION_JSON)
													.content(mediumPostJSON)
													.contentType(MediaType.APPLICATION_JSON);
			
			resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
			
			resultActions.andExpect(status().isOk());
					
			resultMvc = resultActions.andReturn();
			
			resultJSON = resultMvc.getResponse().getContentAsString();

			listMediumsResult = Arrays.asList(objectMapper.readValue(resultJSON, Medium[].class));
			
			assertNotNull(listMediumsResult);
			assertThat(listMediumsResult.size()>0);
			
			mediumResult = listMediumsResult.get(0);
			
			assertNotNull(mediumResult.getID());
			assertTrue(mediumResult.getID()>0);
			
			this.mediumCreated.removeIf(x -> x.getID() == medium.getID());
			this.mediumCreated.add(mediumResult);
			
			//se lo stato è rettificato cen non devo controllare che l'anagrafica sia camuffata
			if(disableStateID  == Utility.TSSD_RETTIFICATO_CEN) {
				assertEquals("*****" 				, mediumResult.getGivenName() );
				assertEquals("*****" 				, mediumResult.getFamilyName() );
				assertEquals("*****" 				, mediumResult.getFiscalCode() );
				assertThat(mediumResult.getBirthDate().compareTo(testUtil.getTodayDate()));
				assertEquals("*" 					, mediumResult.getGender() );
				assertEquals("****" , mediumResult.getBirthPlace().getFiscalCode() );
				
			}else {
				assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
				assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
				assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
				assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
				assertEquals(medium.getGender() 					, mediumResult.getGender() );
				assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
			}
			
			assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
			
			assertThat(disableStateID == mediumResult.getState().getID());
		}else {
			logger.info("------------------------------------------------------------------------------------");
			logger.info("Disable medium, for external CEN medium, test avoided, cause medium not configurated in TestNG-config.xml");
			logger.info("------------------------------------------------------------------------------------");
			
		}
	} 
	
	@AfterClass
	@Parameters({"username", "password"})
	public void restoreSystemStateAndDeleteGeneratedObject(String username, String password) {
		logger.info("====================================================================================");
		logger.info(" Start restoreSystemStateAndDeleteGeneratedObject");
		logger.info("====================================================================================");
		
		ResponseEntity<?> result ;
		if(this.mediumCreated != null && this.mediumCreated.size() > 0) {
			MediumPost mediumPost;
			
			for(Medium m : this.mediumCreated) {
				try {
						  
					  logger.debug("Deleting nedium : "+ m);
					  this.mediumDao.deleteMedium(m);
					  logger.debug("Medium deleted "+ m);
					
				} catch (MAIGeSDiException e) {
					logger.error("Deleting medium error "+ m, e);
					
					e.printStackTrace();
				}
			}
		}
		
		if(this.mediumTypeCreated != null && this.mediumTypeCreated.getID() > 0) {
			try {
				logger.debug("MediumType created: "+ this.mediumTypeCreated);
				this.mediumTypeDao.deleteMediumType(this.mediumTypeCreated);
				logger.debug("MediumType deleted");
			} catch (MAIGeSDiException e) {
				logger.error("ERROR in delete MediumType ", e);
				
				e.printStackTrace();
			}
		}
		
		//restore operator
		
		if(this.operatorUpdated != null && this.operatorUpdated.getUsername().equals(username) ) {
			result = this.operatorController.getOperatorByID(username, password, null, this.operatorUpdated.getID());
			
			Operator operatore = (Operator) result.getBody();
			
			if(operatore!= null) {
				
				if(operatore.getMediumTypes().remove(new Identity(this.mediumTypeCreated.getID(), null))) {
				
					try {
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Request operatorController.updateOperator");
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Username: "+username);
						logger.info(" Password: "+password);
						logger.info(" Operator: "+operatorUpdated);
						logger.info("------------------------------------------------------------------------------------");
						
						result = this.operatorController.updateOperator(username, password, null, this.operatorUpdated);
		
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Response operatorController.updateOperator");
						logger.info("------------------------------------------------------------------------------------");
						logger.info(" Result: "+result);
						logger.info("------------------------------------------------------------------------------------");
						
						if(result != null && result.getStatusCode().equals(HttpStatus.OK)) {
							logger.debug("Operator updated: "+ this.operatorUpdated);
						}else {
							logger.debug("Operator updateding problem : "+ this.operatorUpdated);
							}
					} catch (Exception e) {
						logger.error("Restoring operatore error ", e);
						
						e.printStackTrace();
					}
				}
			}
		}
		
		logger.info("====================================================================================");
		logger.info(" End restoreSystemStateAndDeleteGeneratedObject");
		logger.info("====================================================================================");
		
		
	}
	
	
	
	

}
