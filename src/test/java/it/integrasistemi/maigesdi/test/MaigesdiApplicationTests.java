package it.integrasistemi.maigesdi.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.integrasistemi.maigesdi.bean.Activity;
import it.integrasistemi.maigesdi.bean.ActivityType;
import it.integrasistemi.maigesdi.bean.Version;
import it.integrasistemi.maigesdi.bean.externalsystem.ExternalSystem;
import it.integrasistemi.maigesdi.bean.utility.Utility;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class MaigesdiApplicationTests extends AbstractTestNGSpringContextTests{
	private static final Logger logger = LoggerFactory.getLogger(MaigesdiApplicationTests.class);
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	
	@BeforeTest
	@Parameters({"username", 
				 "password"
				})
	public void checkParameter(String username, 
							  String password) throws Exception {
		
		if(username == null || username.isEmpty()) {
			logger.error("Parameter username not configurated in TestNG-config.xml");
			throw new Exception("Parameter username not configurated in TestNG-config.xml");
		}
		
		if(password == null || password.isEmpty()) {
			logger.error("Parameter password not configurated in TestNG-config.xml");
			throw new Exception("Parameter password not valorized in TestNG-config.xml");
		}
	}
	
	@Test
	public void testVersion() throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start testVersion");
		logger.info("====================================================================================");
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get MAIGeSDi Version");
		logger.info(" Description: Get MAIGeSDi Version ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: version retrived ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		
		ResultActions resultActions = this.mockMvc.perform(get("/version")).andDo(print()).andDo(log());
	
//		resultActions.andExpect(status().isOk());
////					 .andExpect(jsonPath("$.version").value("2.3.0"));
//		
//		MvcResult resultMvc = resultActions.andReturn();
//		
//		String contentAsString = resultMvc.getResponse().getContentAsString();
//
//		Version versionResult = objectMapper.readValue(contentAsString, Version.class);
//
//		assertEquals("3.1.0"											                       , versionResult.getVersion()		);
//		assertEquals("V2"												                       , versionResult.getVersionCode()	);
//		assertEquals("25/02/2020"										                       , versionResult.getReleaseDate()	);
//		assertEquals("MAIGESDI DEV-1622 ready" 	   	   										   , versionResult.getNote());
		
	}
	
	
	
	@Test
	public void keepAliveTest() throws Exception {

		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		
		logger.info("====================================================================================");
		logger.info(" Start keepAliveTest");
		logger.info("====================================================================================");
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Check if MAIGeSDi is alive (online)");
		logger.info(" Description: Call MAIGeSDi keepAlive ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: result retrived correctly ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		//this.mockMvc.
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/keepAlive");

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print()).andDo(log());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		assertEquals("1", resultJSON);
		
	}
		
	@Test
	@Parameters({"username", 
	 			 "password"})
	public void activityTypesTest(String username, 
								  String password) throws Exception {

		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		List<ActivityType> listaActivityType = null;
		
		logger.info("====================================================================================");
		logger.info(" Start activityTypesTest");
		logger.info("====================================================================================");
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Retrieves all ActivityType");
		logger.info(" Description: Call GET ActivityTypes ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: result retrived correctly ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		//this.mockMvc.
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/activityTypes")
												.param("username", username)
												.param("password", password);;

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print()).andDo(log());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listaActivityType = Arrays.asList(objectMapper.readValue(resultJSON, ActivityType[].class));
		
		assertNotNull(listaActivityType);
		assertThat(listaActivityType.size()>0);
		
		assertThat(listaActivityType.stream().filter(x -> x.getCode().equals(Utility.ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM)).findFirst() != null);
	}
	
	@Test
	@Parameters({"username", 
	 			 "password"})
	public void activitiesTest(String username, 
							   String password) throws Exception {

		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultJSON;
		List<Activity> listaActivity = null;
		
		logger.info("====================================================================================");
		logger.info(" Start activitiesTest");
		logger.info("====================================================================================");
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Retrieves all Activity");
		logger.info(" Description: Call GET Activities ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: result retrived correctly ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info(" ------------------------------------------------------------------------------------");
		
		//this.mockMvc.
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/activities")
												.param("username", username)
												.param("password", password);;

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print()).andDo(log());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultJSON = resultMvc.getResponse().getContentAsString();
		
		listaActivity = Arrays.asList(objectMapper.readValue(resultJSON, Activity[].class));
		
		assertNotNull(listaActivity);
		assertThat(listaActivity.size()>0);
		
		assertThat(listaActivity.stream().filter(x -> x.getCode().equals(Utility.ACTIVITY_TYPE_ACTIVATE_ON_EXTERNALSYSTEM)).findFirst() != null);
	}
	
}
