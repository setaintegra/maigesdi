package it.integrasistemi.maigesdi.test.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.integrasistemi.maigesdi.bean.Medium;
import it.integrasistemi.maigesdi.bean.PersonValidityBody;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.test.TestBaseUtil;

@Component
public class MAIGeSDiCENSystemTestUtil {
	
	@Autowired
	private static TestBaseUtil testUtil;	
	
	public static ResultActions chiamataMediumPersonsValidity(ObjectMapper objectMapper,
															  MockMvc mockMvc,
														  	  String username, 
															  String password,
															  PersonValidityBody personValidityBody) throws JsonProcessingException, Exception {
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		String personValidityBodyJSON;
		
		personValidityBodyJSON = objectMapper.writeValueAsString(personValidityBody);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/medium/persons/validity")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(personValidityBodyJSON)
												.contentType(MediaType.APPLICATION_JSON);

		resultActions = mockMvc.perform(requestBuilder).andDo(print());
		return resultActions;
	}
	
	public static void compareMediumsGeneratedWithIdentifier(Medium mediumResult, Medium medium) {
		assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
		compareMediumsDataWithoutIdentifier(mediumResult, medium);
		
	}

	public static void compareMediumsDataWithoutIdentifier(Medium mediumResult, Medium medium) {
		
		assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
		assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
		assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
		assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
		assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
		//assertEquals(medium.getBirthDate() 					, mediumResult.getBirthDate() );
		assertEquals(medium.getGender() 					, mediumResult.getGender() );
		assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
		assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
		assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
		assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
		
		assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
		assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
		assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
		
		assertEquals(medium.getPhoneNumber()				, mediumResult.getPhoneNumber());
		assertEquals(medium.getMobileNumber()				, mediumResult.getMobileNumber());
		assertEquals(medium.getDenomination() 				, mediumResult.getDenomination());
		assertEquals(medium.geteMail()		 				, mediumResult.geteMail());
		
		if(medium.getDocument() != null) {
			assertEquals(medium.getDocument().getDocumentType() , mediumResult.getDocument().getDocumentType());
			assertEquals(medium.getDocument().getAuthority() , mediumResult.getDocument().getAuthority());
			assertEquals(medium.getDocument().getCode() , mediumResult.getDocument().getCode());
			assertThat(mediumResult.getDocument().getReleaseDate().compareTo(medium.getDocument().getReleaseDate()));
			assertThat(mediumResult.getDocument().getExpirationDate().compareTo(medium.getDocument().getExpirationDate()));
		}
		
		if(medium.getTutor() != null) {
			assertEquals(medium.getTutor().getGivenName()   , mediumResult.getTutor().getGivenName() );
			assertEquals(medium.getTutor().getFamilyName()  , mediumResult.getTutor().getFamilyName() );
			assertEquals(medium.getTutor().getFiscalCode() 	, mediumResult.getTutor().getFiscalCode() );
			assertThat(mediumResult.getTutor().getBirthDate().compareTo(medium.getTutor().getBirthDate()));
			assertEquals(medium.getTutor().getBirthPlace(), mediumResult.getTutor().getBirthPlace());
			assertEquals(medium.getTutor().getAddress1()    , mediumResult.getTutor().getAddress1() );
			assertEquals(medium.getTutor().getAddress2()    , mediumResult.getTutor().getAddress2() );
			assertEquals(medium.getTutor().getAddress3()    , mediumResult.getTutor().getAddress3()  );
			
			if(medium.getTutor().getDocument() != null) {
				assertEquals(medium.getTutor().getDocument().getDocumentType() , mediumResult.getTutor().getDocument().getDocumentType());
				assertEquals(medium.getTutor().getDocument().getAuthority() , mediumResult.getTutor().getDocument().getAuthority());
				assertEquals(medium.getTutor().getDocument().getCode() , mediumResult.getTutor().getDocument().getCode());
				assertThat(mediumResult.getTutor().getDocument().getReleaseDate().compareTo(medium.getTutor().getDocument().getReleaseDate()));
				assertThat(mediumResult.getTutor().getDocument().getExpirationDate().compareTo(medium.getTutor().getDocument().getExpirationDate()));
			}
						
		}
	}
	
	public static void compareMediumsGeneratedFromRange(String prefixMediumIndetifierExpected, Medium mediumResult, Medium medium) {
		assertThat(mediumResult.getMediumIdentifier().startsWith(prefixMediumIndetifierExpected)  );
		compareMediumsDataWithoutIdentifier(mediumResult, medium);
	}
	
	public static void compareDisabledMedium(Integer disableStateID, Medium mediumResult, Medium medium) {
		//se lo stato è rettificato cen non devo controllare che l'anagrafica sia camuffata
		if(disableStateID  == Utility.TSSD_RETTIFICATO_CEN) {
			assertEquals("*****" 				, mediumResult.getGivenName() );
			assertEquals("*****" 				, mediumResult.getFamilyName() );
			assertEquals("*****" 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(testUtil.getTodayDate()));
			assertEquals("*" 					, mediumResult.getGender() );
			assertEquals("****" , mediumResult.getBirthPlace().getFiscalCode() );
			
		}else {
			assertEquals(medium.getGivenName() 					, mediumResult.getGivenName() );
			assertEquals(medium.getFamilyName() 				, mediumResult.getFamilyName() );
			assertEquals(medium.getFiscalCode() 				, mediumResult.getFiscalCode() );
			assertThat(mediumResult.getBirthDate().compareTo(medium.getBirthDate()));
			assertEquals(medium.getGender() 					, mediumResult.getGender() );
			assertEquals(medium.getBirthPlace().getFiscalCode() , mediumResult.getBirthPlace().getFiscalCode() );
		}
		
		assertEquals(medium.getMediumIdentifier() 			, mediumResult.getMediumIdentifier() );
		assertEquals(medium.getPIN() 						, mediumResult.getPIN() );
		
		assertEquals(medium.getAddress1() 					, mediumResult.getAddress1() );
		assertEquals(medium.getAddress2() 					, mediumResult.getAddress2() );
		assertEquals(medium.getAddress3() 					, mediumResult.getAddress3()  );
		assertThat(mediumResult.getReleaseDate().compareTo(medium.getReleaseDate()));
		assertThat(mediumResult.getValidFrom().compareTo(medium.getValidFrom()));
		assertThat(mediumResult.getValidTo().compareTo(medium.getValidTo() ));
		
		assertEquals(medium.getPhoneNumber()				, mediumResult.getPhoneNumber());
		assertEquals(medium.getMobileNumber()				, mediumResult.getMobileNumber());
		assertEquals(medium.getDenomination() 				, mediumResult.getDenomination());
		
		if(medium.getDocument() != null) {
			assertEquals(medium.getDocument().getDocumentType() , mediumResult.getDocument().getDocumentType());
			assertEquals(medium.getDocument().getAuthority() , mediumResult.getDocument().getAuthority());
			assertEquals(medium.getDocument().getCode() , mediumResult.getDocument().getCode());
			assertThat(mediumResult.getDocument().getReleaseDate().compareTo(medium.getDocument().getReleaseDate()));
			assertThat(mediumResult.getDocument().getExpirationDate().compareTo(medium.getDocument().getExpirationDate()));
		}
		
		if(medium.getTutor() != null) {
			assertEquals(medium.getTutor().getGivenName()   , mediumResult.getTutor().getGivenName() );
			assertEquals(medium.getTutor().getFamilyName()  , mediumResult.getTutor().getFamilyName() );
			assertEquals(medium.getTutor().getFiscalCode() 	, mediumResult.getTutor().getFiscalCode() );
			assertThat(mediumResult.getTutor().getBirthDate().compareTo(medium.getTutor().getBirthDate()));
			assertEquals(medium.getTutor().getBirthPlace(), mediumResult.getTutor().getBirthPlace());
			assertEquals(medium.getTutor().getAddress1()    , mediumResult.getTutor().getAddress1() );
			assertEquals(medium.getTutor().getAddress2()    , mediumResult.getTutor().getAddress2() );
			assertEquals(medium.getTutor().getAddress3()    , mediumResult.getTutor().getAddress3()  );
			
			if(medium.getTutor().getDocument() != null) {
				assertEquals(medium.getTutor().getDocument().getDocumentType() , mediumResult.getTutor().getDocument().getDocumentType());
				assertEquals(medium.getTutor().getDocument().getAuthority() , mediumResult.getTutor().getDocument().getAuthority());
				assertEquals(medium.getTutor().getDocument().getCode() , mediumResult.getTutor().getDocument().getCode());
				assertThat(mediumResult.getTutor().getDocument().getReleaseDate().compareTo(medium.getTutor().getDocument().getReleaseDate()));
				assertThat(mediumResult.getTutor().getDocument().getExpirationDate().compareTo(medium.getTutor().getDocument().getExpirationDate()));
			}
						
		}
		
		assertThat(disableStateID == mediumResult.getState().getID());
	} 

}
