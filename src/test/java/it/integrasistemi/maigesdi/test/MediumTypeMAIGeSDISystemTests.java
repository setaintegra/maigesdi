package it.integrasistemi.maigesdi.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.integrasistemi.maigesdi.bean.Identity;
import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.controller.MediumTypeController;
import it.integrasistemi.maigesdi.controller.OperatorController;
import it.integrasistemi.maigesdi.dao.MediumTypeDAO;
import it.integrasistemi.maigesdi.exception.MAIGeSDiException;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class MediumTypeMAIGeSDISystemTests extends AbstractTestNGSpringContextTests{
	
	private static final Logger logger = LoggerFactory.getLogger(MediumTypeMAIGeSDISystemTests.class);
	@Autowired
	MediumTypeController controller;
	
	@Autowired
	TestBaseUtil testUtil;
	
	@Autowired
	MediumTypeDAO mediumTypeDao;
	
	@Autowired
	private OperatorController operatorController;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	
	private MediumType mediumTypeCreated;
	private Operator operatorUpdated;
	
	@BeforeTest
	@Parameters({"username", 
				 "password",
				 "MAIGeSDiCEN.MediumType.OrganizationID" 
				})
	public void checkParameter(String username, 
							  String password, 
							  Integer organizationID) throws Exception {
		
		if(username == null || username.isEmpty()) {
			logger.error("Parameter username not configurated in TestNG-config.xml");
			throw new Exception("Parameter username not configurated in TestNG-config.xml");
		}
		
		if(password == null || password.isEmpty()) {
			logger.error("Parameter password not configurated in TestNG-config.xml");
			throw new Exception("Parameter password not valorized in TestNG-config.xml");
		}
		if(organizationID == null || organizationID == 0) {
			logger.error("Parameter MAIGeSDiCEN.MediumType.OrganizationID not configurated in TestNG-config.xml");
			throw new Exception("Parameter MAIGeSDiCEN.MediumType.OrganizationID not configurated");
		}		
		
	}
	
	@Test
	@Parameters({"username", 
		 		 "password",
		 		 "MAIGeSDiCEN.MediumType.OrganizationID"})
	public void addMediumTypeTest(String username, 
			  					  String password, 
			  					  Integer organizationID) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addMediumTypeTest");
		logger.info("====================================================================================");
		
		String inputMediumTypeJSON;
		String resultMediumTypeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		
		MediumType resultMediumType;
		MediumType inputMediumType = testUtil.generateMediumType(Utility.EXTERNAL_SYSTEM_MAIGESDI_CEN, organizationID);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Add a new MediumType");
		logger.info(" Description: Create a new MediumType");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MediumType created ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		inputMediumTypeJSON = this.objectMapper.writeValueAsString(inputMediumType);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/mediumTypes")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(inputMediumTypeJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isCreated());
				
		resultMvc = resultActions.andReturn();
		resultMediumTypeJSON = resultMvc.getResponse().getContentAsString();

		resultMediumType = objectMapper.readValue(resultMediumTypeJSON, MediumType.class);
		
		assertNotNull(resultMediumType.getID());
		assertThat(resultMediumType.getID() > 0);
		assertEquals(inputMediumType.getName().toUpperCase(), resultMediumType.getName().toUpperCase());
		assertEquals(inputMediumType.getNumberDaysValidity(), resultMediumType.getNumberDaysValidity());
		assertEquals(inputMediumType.getSeriesCode(), resultMediumType.getSeriesCode());
		
		assertNotNull(inputMediumType.getActivity());
		assertEquals(inputMediumType.getActivity().size(), resultMediumType.getActivity().size());	
		
		assertNotNull(inputMediumType.getExternalSystem());
		assertEquals(inputMediumType.getExternalSystem().getID(), resultMediumType.getExternalSystem().getID());	
		
		
		this.mediumTypeCreated = resultMediumType;
		
	}
	
	
	//AGGIORNO L'UTENTE CONFIGURATO
	@Test(dependsOnMethods="addMediumTypeTest")
	@Parameters({"username", 
		 		 "password"
				})
	public void updateOperator(String username, 
			  					  String password
							  ) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start updateOperator");
		logger.info("====================================================================================");
		
		//ResponseEntity<?> result;
		Operator storeOperator;
		List<Operator> listOperator;
		Operator resultOperator;
		
		String storedOperatorJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultOperatorJSON;
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get all operators");
		logger.info(" Description: get all operators from system, from this set it choose current operator");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: All Operators returned correctly");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Request operatorController.getOperators");
	
		
		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/operators")
				.param("username", username)
				.param("password", password)
				.contentType(MediaType.APPLICATION_JSON);

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();
		
		listOperator = Arrays.asList(objectMapper.readValue(resultOperatorJSON, Operator[].class));
		
		assertNotNull(listOperator);
		assertThat(listOperator.size() > 0);
		
		//ricerco l'peratore
		storeOperator = listOperator.stream().filter(x -> x.getUsername().equals(username)).findFirst().get();
		
		assertNotNull(storeOperator);
		assertEquals(username, storeOperator.getUsername());
		
		storeOperator.getMediumTypes().add(new Identity(this.mediumTypeCreated.getID(), null));
		storeOperator.setPassword(password);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update operator rights after medium type creation");
		logger.info(" Description: Add rights to current operator to handle new medium type");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected:  Operator updated");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Request operatorController.updateOperator");
		logger.info("------------------------------------------------------------------------------------");
		
		storedOperatorJSON = this.objectMapper.writeValueAsString(storeOperator);
		
		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/operators")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(storedOperatorJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();

		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);
		
		assertNotNull(resultOperator);
		assertEquals(storeOperator.getID(), resultOperator.getID());
		assertEquals(storeOperator.getUsername(), resultOperator.getUsername());
		assertEquals(storeOperator.getMediumTypes().size(), resultOperator.getMediumTypes().size());
		assertEquals(storeOperator.getProfileID(), resultOperator.getProfileID());
		
		resultOperator.setPassword(password);
		this.operatorUpdated = resultOperator;
		
	}
	
	//effettuo il get del medium type creato
	@Test(dependsOnMethods="updateOperator")
	@Parameters({"username", 
		 		"password"})
	public void getMediumTypeTest(String username, 
			  					  String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start getMediumTypeTest");
		logger.info("====================================================================================");
		
		String resultMediumTypeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		
		MediumType resultMediumType;
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get a MediumType by id");
		logger.info(" Description: get MediumType previously created by id ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MediumType returned ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/mediumTypes/{id}",this.mediumTypeCreated.getID())
				.param("username", username)
				.param("password", password);
				
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		resultMediumTypeJSON = resultMvc.getResponse().getContentAsString();
		
		resultMediumType = objectMapper.readValue(resultMediumTypeJSON, MediumType.class);
		
		assertNotNull(resultMediumType);
		assertNotNull(resultMediumType.getID());
		assertThat(resultMediumType.getID() > 0);
		assertEquals(this.mediumTypeCreated.getName().toUpperCase(), resultMediumType.getName().toUpperCase());
		assertEquals(this.mediumTypeCreated.getNumberDaysValidity(), resultMediumType.getNumberDaysValidity());
		assertEquals(this.mediumTypeCreated.getSeriesCode(), resultMediumType.getSeriesCode());
		
		assertNotNull(this.mediumTypeCreated.getActivity());
		assertEquals(this.mediumTypeCreated.getActivity().size(), resultMediumType.getActivity().size());	
		
		assertNotNull(this.mediumTypeCreated.getExternalSystem());
		assertEquals(this.mediumTypeCreated.getExternalSystem().getID(), resultMediumType.getExternalSystem().getID());	
			
		
	}
	
	//effettuo il get del medium type creato a partire dal codice serie e dal codice organizzazione
	@Test(dependsOnMethods="getMediumTypeTest")
	@Parameters({"username", 
		 		"password"})
	public void getMediumTypeByCodesTest(String username, 
			  					  String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start getMediumTypeByCodesTest");
		logger.info("====================================================================================");
		
		String resultMediumTypeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		
		MediumType resultMediumType;
		
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		assertThat(this.mediumTypeCreated.getSeriesCode() != null);
		assertThat(this.mediumTypeCreated.getOrganizationCode() != null);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: get a MediumType by codes (seriesCode and organizationCode)");
		logger.info(" Description: get MediumType previously created by seriesCode and organizationCode pair ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MediumType returned ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/mediumTypes/codes")
				.param("username", username)
				.param("password", password)
				.param("organizationCode", this.mediumTypeCreated.getOrganizationCode())
				.param("seriesCode", this.mediumTypeCreated.getSeriesCode());
				
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		resultMvc = resultActions.andReturn();
		resultMediumTypeJSON = resultMvc.getResponse().getContentAsString();
		
		resultMediumType =  objectMapper.readValue(resultMediumTypeJSON, MediumType.class);
		
		assertNotNull(resultMediumType);
		assertNotNull(resultMediumType.getID());
		assertThat(resultMediumType.getID() > 0);
		assertEquals(this.mediumTypeCreated.getName().toUpperCase(), resultMediumType.getName().toUpperCase());
		assertEquals(this.mediumTypeCreated.getNumberDaysValidity(), resultMediumType.getNumberDaysValidity());
		assertEquals(this.mediumTypeCreated.getSeriesCode(), resultMediumType.getSeriesCode());
		
		assertNotNull(this.mediumTypeCreated.getActivity());
		assertEquals(this.mediumTypeCreated.getActivity().size(), resultMediumType.getActivity().size());	
		
		assertNotNull(this.mediumTypeCreated.getExternalSystem());
		assertEquals(this.mediumTypeCreated.getExternalSystem().getID(), resultMediumType.getExternalSystem().getID());	
			
		
	}
		
	@Test(dependsOnMethods="getMediumTypeByCodesTest")
	@Parameters({"username", 
				 "password"})
	public void updateMediumTypeTest(String username, 
			  						 String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start updateMediumTypeTest");
		logger.info("====================================================================================");

		String inputMediumTypeJSON;
		String resultMediumTypeJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		
		MediumType resultMediumType;
				
		assertNotNull(this.mediumTypeCreated);
		assertNotNull(this.mediumTypeCreated.getID());
		assertThat(this.mediumTypeCreated.getID() > 0);
		
		this.mediumTypeCreated.setName("GeneratedName-"+ TestBaseUtil.randomAlphaNumericString(46));
		
		this.mediumTypeCreated.setExternalSystem(new Identity(TestBaseUtil.randomInteger(2)+1, null));
		this.mediumTypeCreated.setNumberDaysValidity(TestBaseUtil.randomInteger(1000));
		this.mediumTypeCreated.setSeriesCode(TestBaseUtil.randomNumericString(2)+1);
				
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update  a MediumType ");
		logger.info(" Description: Update  a MediumType previously created");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: MediumType updated ");
		logger.info(" Http status code expected: 200 ");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		//result = this.controller.updateMediumType(username, password , null, this.mediumTypeCreated);
		
		inputMediumTypeJSON = this.objectMapper.writeValueAsString(this.mediumTypeCreated);
		
		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/mediumTypes")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(inputMediumTypeJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		resultMediumTypeJSON = resultMvc.getResponse().getContentAsString();

		resultMediumType = objectMapper.readValue(resultMediumTypeJSON, MediumType.class);
		
		assertNotNull(resultMediumType);
		
		assertNotNull(resultMediumType.getID());
		assertThat(resultMediumType.getID() > 0);
		assertEquals(this.mediumTypeCreated.getName().toUpperCase(), resultMediumType.getName().toUpperCase());
		assertEquals(this.mediumTypeCreated.getNumberDaysValidity(), resultMediumType.getNumberDaysValidity());
		assertEquals(this.mediumTypeCreated.getSeriesCode(), resultMediumType.getSeriesCode());
		
		assertNotNull(this.mediumTypeCreated.getActivity());
		assertEquals(this.mediumTypeCreated.getActivity().size(), resultMediumType.getActivity().size());	
		
		assertNotNull(this.mediumTypeCreated.getExternalSystem());
		assertEquals(this.mediumTypeCreated.getExternalSystem().getID(), resultMediumType.getExternalSystem().getID());	
			
		
	}
	
	
	@AfterClass
	@Parameters({"username", "password"})
	public void deleteGeneratedMediumType(String username, String password) {
		logger.info("====================================================================================");
		logger.info(" Start deleteGeneratedMediumType");
		logger.info("====================================================================================");
		
		if(this.mediumTypeCreated != null && this.mediumTypeCreated.getID() > 0)
			try {
				logger.debug("MediumType created: "+ this.mediumTypeCreated);
				this.mediumTypeDao.deleteMediumType(this.mediumTypeCreated);
				logger.debug("MediumType deleted");
			} catch (MAIGeSDiException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		logger.info("====================================================================================");
		logger.info(" End deleteGeneratedMediumType");
		logger.info("====================================================================================");
		
		ResponseEntity<?> result ;
		
		if(this.operatorUpdated != null && this.operatorUpdated.getUsername().equals(username) ) {
			
			this.operatorUpdated.getMediumTypes().remove(new Identity(this.mediumTypeCreated.getID(), null));
			
			try {
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Request operatorController.updateOperator");
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Username: "+username);
				logger.info(" Password: "+password);
				logger.info(" Operator: "+this.operatorUpdated);
				logger.info("------------------------------------------------------------------------------------");
				
				result = this.operatorController.updateOperator(username, password, null, this.operatorUpdated);

				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Response operatorController.updateOperator");
				logger.info("------------------------------------------------------------------------------------");
				logger.info(" Result: "+result);
				logger.info("------------------------------------------------------------------------------------");
				
				if(result != null && result.getStatusCode().equals(HttpStatus.OK)) {
					logger.debug("Operator updated: "+ this.operatorUpdated);
				}else {
					logger.debug("Operator updateding problem : "+ this.operatorUpdated);
					}
			} catch (Exception e) {
				logger.error("Restoring operatore error ", e);
				
				e.printStackTrace();
			}
			
			
		}
		
		
	}
	
}
