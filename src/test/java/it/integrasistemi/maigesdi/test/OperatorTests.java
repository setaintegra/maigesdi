package it.integrasistemi.maigesdi.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.RequestParam;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.integrasistemi.maigesdi.bean.MediumType;
import it.integrasistemi.maigesdi.bean.Operator;
import it.integrasistemi.maigesdi.bean.utility.Utility;
import it.integrasistemi.maigesdi.controller.OperatorController;
import it.integrasistemi.maigesdi.dao.OperatorDAO;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class OperatorTests extends AbstractTestNGSpringContextTests{

	@Autowired
	private TestBaseUtil testUtil;
	
	@Autowired
	private OperatorController controller;
	
	@Autowired
	private OperatorDAO operatorDAO;

	private Operator operatorCreated;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeTest
	@Parameters({"username", 
				 "password"
				 
				})
	public void checkParameter(String username, 
							  String password) throws Exception {
		
		if(username == null || username.isEmpty()) {
			logger.error("Parameter username not configurated in TestNG-config.xml");
			throw new Exception("Parameter username not configurated in TestNG-config.xml");
		}
		
		if(password == null || password.isEmpty()) {
			logger.error("Parameter password not configurated in TestNG-config.xml");
			throw new Exception("Parameter password not valorized in TestNG-config.xml");
		}
		
	}
	
	
	@Test
	@Parameters({"username", 
		 		 "password"})
	public void addAdminOperator(String username, 
			  					 String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start addAdminOperator");
		logger.info("====================================================================================");
		
		Operator resultOperator;
		String storedOperatorJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultOperatorJSON;
		
		Operator inputOperator = testUtil.generateOperator(Utility.Profile.CONFIGURATOR.getValue());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Add a new operator");
		logger.info(" Description: Add new operator with an operator that have configurator profile ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: Operator created ");
		logger.info(" Http status code expected:  201");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
	
		storedOperatorJSON = this.objectMapper.writeValueAsString(inputOperator);
		
		requestBuilder = MockMvcRequestBuilders.post("/MAIGeSDi/V2/operators")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(storedOperatorJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isCreated());
				
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();

		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);
	
		assertNotNull(resultOperator);
		
		assertThat(resultOperator.getID()>0);
		assertEquals(inputOperator.getUsername() , resultOperator.getUsername() );
		assertNull(resultOperator.getPassword());
		assertEquals(inputOperator.getProfileID(), resultOperator.getProfileID());
	
		resultOperator.setPassword(inputOperator.getPassword());
		this.operatorCreated = resultOperator;
	}
	
	@Test(dependsOnMethods="addAdminOperator")
	@Parameters({"username", 
	 			 "password"})
	public void updateOperator(String username, 
				 				String password) throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start updateOperator");
		logger.info("====================================================================================");

		Operator resultOperator;
		String storedOperatorJSON;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultOperatorJSON;
		
		assertNotNull(this.operatorCreated);
		assertThat(this.operatorCreated.getID()>0);
		assertNotNull(this.operatorCreated.getPassword());
		
		Operator inputOperator = this.operatorCreated;
		inputOperator.setProfileID(Utility.Profile.AMMINISTRATOR.getValue());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update an operator profile");
		logger.info(" Description: Update operator profile with an operator that have configurator profile ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: Operator updated ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");

		
		//result = this.controller.updateOperator(username, password, null, inputOperator);
		
		storedOperatorJSON = this.objectMapper.writeValueAsString(inputOperator);
		
		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/operators")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(storedOperatorJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();

		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);

		assertNotNull(resultOperator);
		
		assertThat(resultOperator.getID()>0);
		assertEquals(inputOperator.getUsername() , resultOperator.getUsername() );
		assertNull(resultOperator.getPassword());
		assertEquals(inputOperator.getProfileID(), resultOperator.getProfileID());
		
		inputOperator = this.operatorCreated;
		inputOperator.setProfileID(Utility.Profile.CONFIGURATOR.getValue());
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update an operator profile");
		logger.info(" Description: Restore operator's profile ");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: Operator updated ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");
		
		storedOperatorJSON = this.objectMapper.writeValueAsString(inputOperator);
		
		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/operators")
												.param("username", username)
												.param("password", password)
												.accept(MediaType.APPLICATION_JSON)
												.content(storedOperatorJSON)
												.contentType(MediaType.APPLICATION_JSON);
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
				
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();

		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);

		assertNotNull(resultOperator);
		
		assertThat(resultOperator.getID()>0);
		assertEquals(inputOperator.getUsername() , resultOperator.getUsername() );
		assertNull(resultOperator.getPassword());
		assertEquals(inputOperator.getProfileID(), resultOperator.getProfileID());
	
		//this.operatorCreated.setProfileID(Utility.Profile.AMMINISTRATOR.getValue());
		//this.operatorCreated = resultOperator;
	}
	
	@Test(dependsOnMethods="updateOperator")
	public void changeOperatorPassword() throws Exception {
		logger.info("====================================================================================");
		logger.info(" Start changeOperatorPassword");
		logger.info("====================================================================================");
	
		Operator resultOperator;
		RequestBuilder requestBuilder;
		ResultActions resultActions;
		MvcResult resultMvc;
		String resultOperatorJSON;
		
		assertNotNull(this.operatorCreated);
		assertThat(this.operatorCreated.getID()>0);
		assertNotNull(this.operatorCreated.getPassword());
		
		Operator inputOperator = this.operatorCreated;
		String oldPassword = inputOperator.getPassword();
		String newPassword = TestBaseUtil.randomAlphaNumericString(100);
		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Update an operator password");
		logger.info(" Description: Update operator password");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: Operator updated ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");


		requestBuilder = MockMvcRequestBuilders.put("/MAIGeSDi/V2/operators/password")
												.param("username", inputOperator.getUsername())
												.param("password", oldPassword)
												.param("oldPassword", oldPassword)
												.param("newPassword", newPassword)
												;
		
		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		//resultActions.andExpect(jsonPath("$.").value("Password correctlty changed"));
		
		resultMvc = resultActions.andReturn();
		
		assertEquals("Password correctlty changed", resultMvc.getResponse().getContentAsString());

		
		logger.info("------------------------------------------------------------------------------------");
		logger.info(" Test: Get operator by ID and test password password changing");
		logger.info(" Description: get operator by ID using new operator's credentials (new password)");
		logger.info(" ------------------------------------------------------------------------------------");
		logger.info(" Result expected: Operator returned ");
		logger.info(" Http status code expected:  200");
		logger.info(" MAIGeSDi error code expected: no error ");
		logger.info("------------------------------------------------------------------------------------");


		requestBuilder = MockMvcRequestBuilders.get("/MAIGeSDi/V2/operators/{id}", inputOperator.getID())
				.param("username", inputOperator.getUsername())
				.param("password", newPassword)
				;

		resultActions = this.mockMvc.perform(requestBuilder).andDo(print());
		
		resultActions.andExpect(status().isOk());
		
		
		resultMvc = resultActions.andReturn();
		
		resultOperatorJSON = resultMvc.getResponse().getContentAsString();
		
		resultOperator = objectMapper.readValue(resultOperatorJSON, Operator.class);

		assertNotNull(resultOperator);
		
		assertThat(resultOperator.getID()>0);
		assertEquals(inputOperator.getUsername() , resultOperator.getUsername() );
		assertNull(resultOperator.getPassword());
		assertEquals(inputOperator.getProfileID(), resultOperator.getProfileID());
	
		
		
	}
	
	
	
	@AfterClass
	public void deleteCreatedOperator() {
		if(this.operatorCreated !=null && this.operatorCreated.getID()>0) {
			operatorDAO.deleteOperator(this.operatorCreated);
		}
	}
		
}
